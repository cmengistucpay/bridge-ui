import { find } from "lodash";
const _nav = (userData) => {
  return [
    {
      _tag: "CSidebarNavItem",
      name: "Sales & Purchases",
      to: "/sales-purchases",
      icon: "cil-bar-chart",
    },
    {
      _tag: "CSidebarNavItem",
      name: "Files",
      to: "/files",
      icon: "cil-file",
      className: userData?.merchantSettings?.showFiles ? "" : "d-none",
    },
    {
      _tag: "CSidebarNavItem",
      name: "Payment Link",
      to: "/payment-link",
      icon: "cil-link",
      className: userData?.merchantSettings?.showPaymentLinks ? "" : "d-none",
    },
    {
      _tag: "CSidebarNavDropdown",
      name: "Reports",
      route: "/reports",
      icon: "cil-report",
      className:
        !userData?.canAccessCashBalance && !userData?.canAccessStatements
          ? "d-none"
          : "",
      _children: [
        {
          _tag: "CSidebarNavItem",
          name: "Cash Balance",
          to: "/reports/cash-balance",
          icon: "cil-cash",
          className: userData?.canAccessCashBalance && (!userData?.merchantSettings?.isFlexMerchant ?? true) ? "" : "d-none",
        },
        {
          _tag: "CSidebarNavItem",
          name: "Available to Issue",
          to: "/reports/available-to-issue",
          icon: "cil-info-circle",
          className: userData?.merchantSettings?.isFlexMerchant ? "" : "d-none",
        },
        {
          _tag: "CSidebarNavItem",
          name: "Statements",
          to: "/reports/statements",
          icon: "cil-chart",
          className: userData?.canAccessStatements ? "" : "d-none",
        },
      ],
    },
    {
      _tag: "CSidebarNavItem",
      name: "Users",
      to: "/users",
      icon: "cil-group",
      className: userData?.canManageUsers ? "" : "d-none",
    },
    {
      _tag: "CSidebarNavItem",
      name: "Labels",
      to: "/labels",
      icon: "cil-tags",
      className:
        userData?.merchantSettings?.showLabels && userData?.canManageLabels
          ? ""
          : "d-none",
    },
    {
      _tag: "CSidebarNavItem",
      name: "Admin",
      to: "/admin",
      icon: "cil-user",
      className: find(userData?.labelHierarchy, { name: "ConnexPay Admin" })
        ? ""
        : "d-none",
    },
  ];
};

export default _nav;
