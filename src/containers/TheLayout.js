import React, { useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import { differenceInMinutes, fromUnixTime } from "date-fns";
import classNames from "classnames";
import { TheContent, TheSidebar, TheAside, TheHeader } from "./index";
import { briGetUserById, salGetSaleDesc } from "src/services";
import {
  jwtDecodeBridge,
  jwtDecodeExpiration,
  removeAuthToken,
} from "src/services/serviceAuth";
import { logoutAzure } from "src/services/serviceAzure";

const TheLayout = () => {
  const dispatch = useDispatch();
  const darkMode = useSelector((state) => state.darkMode);
  const accDevice = useSelector((state) => state.accDevices?.[0]);
  const classes = classNames(
    "c-app c-default-layout",
    darkMode && "c-dark-theme"
  );

  useEffect(() => {
    const darkModeCache = localStorage.getItem("cxp-darkMode");
    if (darkModeCache === "true") {
      dispatch({ type: "set", darkMode: true });
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (accDevice?.merchantGuid) {
      _fetchSaleDescription();
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [accDevice]);

  const fetchUserData = useCallback(async () => {
    try {
      const authBridge = jwtDecodeBridge();
      const res = await briGetUserById(authBridge?.extension_Guid);
      dispatch({
        type: "set",
        userData: {
          ...res,
        },
      });
    } catch (error) {
      // console.error(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const fetchUserExpiration = useCallback(async () => {
    try {
      const authBridge = jwtDecodeExpiration();
      const expiredDateTime = fromUnixTime(authBridge);
      const currentDateTime = new Date();
      const isExpired = differenceInMinutes(expiredDateTime, currentDateTime);
      if (isExpired < 5) {
        logoutAzure();
        removeAuthToken();
        localStorage.removeItem("labelMarchantData");
      }
      dispatch({
        type: "set",
        userExpiration: { expiresAt: authBridge },
      });
    } catch (error) {
      console.error(error);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const _fetchSaleDescription = async () => {
    try {
      const res = await salGetSaleDesc(accDevice?.merchantGuid);
      dispatch({ type: "set", saleDescriptions: res });
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchUserData();
    fetchUserExpiration();
  }, [fetchUserData, fetchUserExpiration]);

  return (
    <div className={classes}>
      <TheSidebar />
      <TheAside />
      <div className="c-wrapper">
        <TheHeader />
        <div className="c-body">
          <TheContent />
        </div>
        {/* <TheFooter/> */}
      </div>
    </div>
  );
};

export default TheLayout;
