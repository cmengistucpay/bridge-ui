import React from "react";
import { withRouter } from "react-router-dom";
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { removeAuthToken } from "../services/serviceAuth";
import { logoutAzure } from "src/services/serviceAzure";

const TheHeaderDropdown = (props) => {
  const { history, userData } = props;

  const handleLogout = () => {
    localStorage.removeItem("labelMarchantData");
    removeAuthToken();
    logoutAzure();
    // window.location.href = "/login";
  };

  return (
    <CDropdown inNav className="c-header-nav-items mx-2" direction="down">
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar">
          <CImg
            src={"/avatars/user-placeholder.png"}
            className="c-avatar-img"
            alt="admin@bootstrapmaster.com"
          />
        </div>
      </CDropdownToggle>
      <CDropdownMenu placement="bottom-end">
        <CDropdownItem disabled onClick={handleLogout}>
          <CIcon name="cil-user" className="mfe-2" />
          {userData?.userName}
        </CDropdownItem>
        <CDropdownItem header tag="div" color="light" className="text-center">
          <strong>Account</strong>
        </CDropdownItem>
        {userData?.canManageCustomFormPermissions && (
          <CDropdownItem onClick={() => history.push("/account-settings")}>
            <CIcon name="cil-settings" className="mfe-2" />
            Account Settings
          </CDropdownItem>
        )}
        <CDropdownItem onClick={handleLogout}>
          <CIcon name="cil-account-logout" className="mfe-2" />
          Sign Out
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  );
};

export default withRouter(TheHeaderDropdown);
