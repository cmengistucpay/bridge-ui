import TheAside from "./TheAside";
import TheContent from "./TheContent";
import TheFooter from "./TheFooter";
import TheHeader from "./TheHeader";
import TheHeaderDropdown from "./TheHeaderDropdown";
import TheLayout from "./TheLayout";
import TheSidebar from "./TheSidebar";

export {
  TheAside,
  TheContent,
  TheFooter,
  TheHeader,
  TheHeaderDropdown,
  TheLayout,
  TheSidebar,
};
