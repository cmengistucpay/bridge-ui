import React, { Suspense } from "react";
import { Redirect, Route, Switch } from "react-router-dom";
import { CContainer, CFade } from "@coreui/react";
import { useSelector } from "react-redux";
import { Toaster } from "src/components";

// routes config
import routes from "../routes";

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

const TheContent = () => {
  const userData = useSelector((state) => state.userData);
  return (
    <main className="c-main">
      <CContainer fluid>
        <Suspense fallback={loading}>
          <Switch>
            {routes.map((route, idx) => {
              return (
                route.component && (
                  <Route
                    key={idx}
                    path={route.path}
                    exact={route.exact}
                    name={route.name}
                    render={(props) => 
                      route.name === 'Cash Balance'? (!userData?.merchantSettings?.isFlexMerchant ?? true) ?
                      (
                      <CFade>
                        <route.component {...props} />
                      </CFade>
                    ) :  <Redirect from="/" to="/sales-purchases" /> : 
                    (
                      <CFade>
                        <route.component {...props} />
                      </CFade>
                    )}
                  />
                )
              );
            })}
            <Redirect from="/" to="/sales-purchases" />
          </Switch>
        </Suspense>
      </CContainer>
      <Toaster />
    </main>
  );
};

export default React.memo(TheContent);
