import React, { useState, useEffect, useCallback } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarNavDropdown,
  CSidebarNavItem,
  CButton,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { salGetDevices } from "src/services";

// sidebar nav config
import navigation from "./_nav";

import CashSale from "src/views/ProcessSale/CashSale/CashSale";
import CreditCardSale from "src/views/ProcessSale/CreditCardSale/CreditCardSale";
import CreatePaymentLink from "src/views/PaymentLink/CreatePaymentLink/CreatePaymentLink";
import FileUpload from "src/views/ProcessSale/FileUpload/FileUpload";

import IssuePurchaseDialog from "src/views/ProcessSale/IssuePurchaseDialog/IssuePurchaseDialog";
import VirtualCardPurchase from "src/views/IssuePurchase/VirtualCardPurchase/VirtualCardPurchase";
import ACHPurchase from "src/views/IssuePurchase/ACHPurchase/ACHPurchase";
import LodgedCard from "src/views/IssuePurchase/LodgedCard/LodgedCard";
import Receipt from "src/views/Receipt/ReceiptSale";
import { useHistory } from "react-router-dom";

const TheSidebar = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const show = useSelector((state) => state.sidebarShow);
  const userData = useSelector((state) => state.userData);
  const merchantSettings = userData?.merchantSettings;

  const [activeDropdown, setActiveDropdown] = useState("");
  const [activeModal, setActiveModal] = useState("");
  const [accDevices, setAccDevices] = useState([]);

  const [type, setType] = useState("");
  const [connexPayTransaction, setConnexPayTransaction] = useState(null);
  const [customerData, setCustomerData] = useState({});
  const [customerReceipt, setCustomerReceipt] = useState("");
  const [customerReceiptType, setCustomerReceiptType] = useState("");
  const [purchaseCount, setPurchaseCount] = useState({});
  const [currentPage, setCurrentPage] = useState(history.location.pathname);

  const fetchDevices = useCallback(async () => {
    try {
      const res = await salGetDevices();
      setAccDevices(res?.ret);
      dispatch({ type: "set", accDevices: res?.ret });
    } catch (error) {
      // console.error(error);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    fetchDevices();
  }, [fetchDevices]);

  const processSaleDropdown = [
    {
      label: "Credit Card Sale",
      action: () => handleOpenModal("creditCardSale"),
    },
    {
      label: "Cash Sale",
      action: () => handleOpenModal("cashSale"),
      className: merchantSettings?.showCashSale ? "" : "d-none",
    },
    {
      label: "Create Payment Link",
      action: () => handleOpenModal("createPaymentLink"),
      className:
        merchantSettings?.showPaymentLinks && userData?.canCreatePaymentLink
          ? ""
          : "d-none",
    },
    {
      label: "File Upload",
      action: () => handleOpenModal("fileUpload"),
      className:
        userData?.canProcessPaymentInstructionFile &&
          merchantSettings?.showFileUpload
          ? ""
          : "d-none",
    },
  ];

  const issuePurchaseDropdown = [
    {
      label: "Virtual Card Purchase",
      action: () => handleOpenModal("virtualCardPurchase"),
    },
    {
      label: "ACH Purchase",
      action: () => handleOpenModal("achPurchase"),
      className:
        merchantSettings?.showACHPurchases && userData?.canIssueACH
          ? ""
          : "d-none",
    },
    {
      label: "Lodged Card",
      action: () => handleOpenModal("lodgedCard"),
      className:
        userData?.maxLodgedCardIssueAmount > 0
          ? ""
          : "d-none",
    },
  ];

  const handleOpenDropdown = (dropdownId) => {
    if (dropdownId !== activeDropdown) {
      setActiveDropdown(dropdownId);
    } else {
      setActiveDropdown("");
    }
  };

  const handleOpenModal = (modalId) => {
    setType("");
    setActiveModal(modalId);
    setActiveDropdown("");
    setCustomerData({});
    setCustomerReceipt("");
    setPurchaseCount(null);
    setConnexPayTransaction(null);
  };

  const handlePostSubmit = (
    type,
    transaction,
    customer,
    receipt,
    receiptType,
    extra
  ) => {
    setType(type);
    setConnexPayTransaction(transaction);
    setCustomerData(customer);
    setCustomerReceipt({ customerReceipt: receipt, guid: extra?.guid, incomingTransactionCode: transaction?.incomingTransCode });
    setCustomerReceiptType(receiptType);

    // if (transaction.expectedPayments > 0) {
    //   setActiveModal("issuePurchaseDialog");
    // } else {
    //   setActiveModal("customerReceipt");
    // }
    setActiveModal("customerReceipt");
  };

  const handleHideReceiptModal = (connexPayTransaction) => {
    // if (connexPayTransaction.expectedPayments === 0) {
    //   setActiveModal("");
    // } else {
    //   setActiveModal("issuePurchaseDialog");
    // }
    setActiveModal("");
  };

  const handleConfirmIssuePurchase = (purchaseType, purchaseCount) => {
    setPurchaseCount(purchaseCount);
    if (purchaseType === "virtualCard") {
      setActiveModal("virtualCardPurchase");
    }
    if (purchaseType === "ach") {
      setActiveModal("achPurchase");
    }
  };

  const handleDispatch = () => {
    if(!history.location.pathname.match(currentPage))
    {
      setCurrentPage(history.location.pathname);
      dispatch({
        type: "set",
        initialFilterPaymentLink:{}
      });
      dispatch({
        type: "set",
        initialFilterOn:false
      });
      dispatch({
        type: "set",
        initialFilterCashBalanceReport:{}
      });
      dispatch({
        type: "set",
        initialFilterSalesPurchase:{}
      });
    }
  };

  return (
    <CSidebar
      show={show}
      unfoldable
      onShowChange={(val) => dispatch({ type: "set", sidebarShow: val })}
    >
      <CSidebarBrand className="d-md-down-none" to="/">
        <CIcon
          className="c-sidebar-brand-full"
          name="logo-negative"
          height={30}
        />
        <CIcon
          className="c-sidebar-brand-minimized"
          name="sygnet"
          height={35}
        />
      </CSidebarBrand>
      <CSidebarNav onClick={(event) => {handleDispatch()}}>
        <CCreateElement
          items={navigation(userData)}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle,
          }}
        />
        <div className="btn-group-bottom">
          {userData?.canProcessSale && (
            <CButton
              onClick={() => handleOpenDropdown("processSale")}
              color="primary"
              size="md"
              className="my-2 w-100"
            >
              Process Sale
            </CButton>
          )}
          {activeDropdown === "processSale" && (
            <div className="dropdown-sidebar">
              {processSaleDropdown?.map((val, i) => (
                <div
                  key={i}
                  onClick={val.action}
                  className={`dropdown-sidebar-item ${val.className ? val.className : ""
                    }`}
                >
                  <p>{val.label}</p>
                </div>
              ))}
            </div>
          )}
          {merchantSettings?.showIssuePurchase && (
            <>
              {userData?.canCreateVCC && userData?.canIssueACH && (
                <CButton
                  onClick={() => handleOpenDropdown("issuePurchaseDialog")}
                  color="primary"
                  size="md"
                  className="my-2 w-100"
                >
                  Issue Purchase
                </CButton>
              )}
              {userData?.canCreateVCC && !userData?.canIssueACH && (
                <CButton
                  onClick={() => handleOpenModal("virtualCardPurchase")}
                  color="primary"
                  size="md"
                  className="my-2 w-100"
                >
                  Issue Purchase
                </CButton>
              )}
            </>
          )}
          {/* {!userData?.canCreateVCC &&
            userData?.canIssueACH &&
            merchantSettings?.showIssuePurchase && (
              <CButton
                onClick={() => handleOpenModal("achPurchase")}
                color="primary"
                size="md"
                className="my-2 w-100"
              >
                Issue Purchase
              </CButton>
            )} */}
          {activeDropdown === "issuePurchaseDialog" && (
            <div className="dropdown-sidebar">
              {issuePurchaseDropdown?.map((val, i) => (
                <div
                  key={i}
                  onClick={val.action}
                  className={`dropdown-sidebar-item ${val.className ? val.className : ""
                    }`}
                >
                  <p>{val.label}</p>
                </div>
              ))}
            </div>
          )}
        </div>
      </CSidebarNav>
      {/* <CSidebarMinimizer className="c-d-md-down-none"/> */}

      <CashSale
        modalShow={activeModal === "cashSale"}
        handleHideModal={() => setActiveModal("")}
        handlePostSubmit={handlePostSubmit}
        accDevice={accDevices?.[0]}
      />
      <CreditCardSale
        modalShow={activeModal === "creditCardSale"}
        handleHideModal={() => setActiveModal("")}
        handlePostSubmit={handlePostSubmit}
        accDevice={accDevices?.[0]}
      />
      <CreatePaymentLink
        modalShow={activeModal === "createPaymentLink"}
        handleHideModal={() => setActiveModal("")}
        handlePostSubmit={handlePostSubmit}
        accDevice={accDevices?.[0]}
      />
      <FileUpload
        modalShow={activeModal === "fileUpload"}
        handleHideModal={() => setActiveModal("")}
      />
      {userData?.canCreateVCC && merchantSettings?.showIssuePurchase && (
        <IssuePurchaseDialog
          type={type}
          connexPayTransaction={connexPayTransaction}
          customerData={customerData}
          modalShow={activeModal === "issuePurchaseDialog"}
          handleHideModal={() => setActiveModal("")}
          accDevice={accDevices?.[0]}
          handleShowReceipt={() => setActiveModal("customerReceipt")}
          handleConfirmIssuePurchase={handleConfirmIssuePurchase}
        />
      )}
      <VirtualCardPurchase
        connexPayTransaction={connexPayTransaction}
        customerData={customerData}
        purchaseCount={purchaseCount}
        modalShow={activeModal === "virtualCardPurchase"}
        handleHideModal={() => setActiveModal("")}
        accDevice={accDevices?.[0]}
        handlePostSubmit={() => { }}
      />
      <ACHPurchase
        connexPayTransaction={connexPayTransaction}
        customerData={customerData}
        purchaseCount={purchaseCount}
        modalShow={activeModal === "achPurchase"}
        handleHideModal={() => setActiveModal("")}
        accDevice={accDevices?.[0]}
        handlePostSubmit={() => { }}
      />
      <LodgedCard
        connexPayTransaction={connexPayTransaction}
        customerData={customerData}
        purchaseCount={purchaseCount}
        modalShow={activeModal === "lodgedCard"}
        handleHideModal={() => setActiveModal("")}
        accDevice={accDevices?.[0]}
        handlePostSubmit={() => { }}
      />
      <Receipt
        modalShow={activeModal === "customerReceipt"}
        modalData={customerReceipt}
        handleHideModal={() => handleHideReceiptModal(connexPayTransaction)}
        type={customerReceiptType}
      />
    </CSidebar>
  );
};

export default React.memo(TheSidebar);
