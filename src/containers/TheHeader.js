import React, { useState, useEffect } from "react";
import { isEmpty } from "lodash";
import { useSelector, useDispatch } from "react-redux";
import Select from "react-select";
import {
  CHeader,
  CHeaderBrand,
  CHeaderNav,
  CHeaderNavItem,
  CHeaderNavLink,
  CSubheader,
  CToggler,
  CBreadcrumbRouter,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";

import { mapOptLabels } from "src/helper";
import { loginToAnalytics } from "src/services";
import {
  getAuthTokenBridge,
} from "src/services/serviceAuth";
import { TheHeaderDropdown } from "./index";

// routes config
import routes from "../routes";

const TheHeader = () => {
  const dispatch = useDispatch();
  const darkMode = useSelector((state) => state.darkMode);
  const sidebarShow = useSelector((state) => state.sidebarShow);
  const globalLabel = useSelector((state) => state.globalLabel);
  const userData = useSelector((state) => state.userData);
  const toaster = useSelector((state) => state.toaster);
  const merchantSettings = userData?.merchantSettings;

  const [optLabels, setOptLabels] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (!isEmpty(userData)) {
      const arrOptLabels = mapOptLabels(userData?.labelHierarchy);
      setOptLabels(arrOptLabels);

      const initialLabel = arrOptLabels?.filter((val) => val.level === 0);
      if (initialLabel.length && localStorage.getItem("labelMarchantData") === null) {
        const labelMarchantData = {
          ...initialLabel[0]
        }
        localStorage.setItem("labelMarchantData", JSON.stringify(labelMarchantData));
      }

      dispatch({
        type: "set",
        globalLabel: arrOptLabels
          ?.filter((val) => val.level === 0)
          ?.map((val) => val.value),
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  const toggleSidebar = () => {
    const val = [true, "responsive"].includes(sidebarShow)
      ? false
      : "responsive";
    dispatch({ type: "set", sidebarShow: val });
  };

  const toggleSidebarMobile = () => {
    const val = [false, "responsive"].includes(sidebarShow)
      ? true
      : "responsive";
    dispatch({ type: "set", sidebarShow: val });
  };

  const toggleDarkMode = () => {
    dispatch({ type: "set", darkMode: !darkMode });
    localStorage.setItem("cxp-darkMode", !darkMode);
  };

  const handleRedirectCMS = async () => {
    try {
      const baseCmsURL = process.env.REACT_APP_CMS_URL;
      const labelMerchantData = JSON.parse(localStorage.getItem("labelMarchantData"));
      const merchantId = `merchantId=${labelMerchantData.masterMerchantId}`
      const authToken = getAuthTokenBridge();
      const bridgeToken = `token=${authToken?.access_token}`;
      const cmsUrl = `${baseCmsURL}/Account/SsoLoginBridge?${bridgeToken}&${merchantId}`;

      window.open(cmsUrl, "_blank");
    } catch (error) {
      // console.error(error);
    }
  };

  const handleLoginToAnalytics = async () => {
    try {
      setIsLoading(true);
      const payload = {
        LabelIds: globalLabel
      };
      const res = await loginToAnalytics(payload);
      if (res?.redirectUrl) {
        window.open(res?.redirectUrl, "_blank");
      }
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
      };
      if (error?.response?.data && typeof error.response.data === "string") {
        dispatch({ type: "set", toaster: [...toaster, { ...toasterConfig, body: error?.response?.data, }] });
      } else {
        dispatch({ type: "set", toaster: [...toaster, { ...toasterConfig, body: "Network Error", }] });
      }
    } finally {
      setIsLoading(false);
    }
  };

  const handleSelectLabel = (selected) => {

    if (selected.length) {
      let labelMarchantData;
      if (selected.length > 1 && selected[0].label === "ConnexPay Admin" && selected[0].level === 0) {
        labelMarchantData = {
          ...selected[1]
        }
      } else {
        labelMarchantData = {
          ...selected[0]
        }
      }
      localStorage.setItem("labelMarchantData", JSON.stringify(labelMarchantData));
    } else {
      localStorage.removeItem("labelMarchantData");
    }

    dispatch({
      type: "set",
      globalLabel: selected?.map((val) => val.value),
    })
  };

  const renderHeaderNav = () => {
    return (
      <CHeaderNav className="mr-auto">
        {userData?.canAccessAnalytics && (
          <CHeaderNavItem className="px-md-3">
            <CHeaderNavLink
              onClick={handleLoginToAnalytics}
              disabled={isLoading}
            >
              Analytics
            </CHeaderNavLink>
          </CHeaderNavItem>
        )}
        {userData?.canAccessCMS && (
          <CHeaderNavItem className="px-md-3">
            <CHeaderNavLink
              onClick={handleRedirectCMS}
            >
              CMS
            </CHeaderNavLink>
          </CHeaderNavItem>
        )}
        <CHeaderNavItem className="px-md-3">
          <CHeaderNavLink
            to={{ pathname: "https://docs.connexpay.com/docs" }}
            target="_blank"
          >
            <CIcon
              name="cil-comment-bubble-question"
              className="mr-2"
              alt="CoreUI Icons Comment Bubble Question"
            />
            Help
          </CHeaderNavLink>
        </CHeaderNavItem>
        <CDropdown inNav className="c-header-nav-items mx-2" direction="down">
          <CDropdownToggle className="c-header-nav-link" caret={false}>
            <CIcon
              name="cil-handshake"
              className="mr-2"
              alt="CoreUI Icons Handshake"
            />
            Contact Us
          </CDropdownToggle>
          <CDropdownMenu placement="bottom-end">
            <CDropdownItem onClick={() => window.open("tel:6126010935")}>
              <CIcon name="cil-phone" className="mfe-2" />
              (612) 601-0935
            </CDropdownItem>
            <CDropdownItem
              onClick={() => window.open("mailto:support@connexpay.com")}
            >
              <CIcon name="cil-mail" className="mfe-2" />
              support@connexpay.com
            </CDropdownItem>
          </CDropdownMenu>
        </CDropdown>
      </CHeaderNav>
    );
  };

  return (
    <CHeader withSubheader>
      <CToggler
        inHeader
        className="ml-md-3 d-lg-none"
        onClick={toggleSidebarMobile}
      />
      <CToggler
        inHeader
        className="ml-3 d-md-down-none"
        onClick={toggleSidebar}
      />
      <CHeaderBrand className="mx-2 d-lg-none" to="/">
        <CIcon name="logo" className="c-d-dark-none" height="35" alt="Logo" />
        <CIcon
          name="logo-negative"
          className="c-d-default-none"
          height="35"
          alt="Logo"
        />
      </CHeaderBrand>

      <div className="d-md-down-none">{renderHeaderNav()}</div>

      <CHeaderNav className="ml-auto px-md-3">
        {merchantSettings?.showLabels && (
          <div
            className="mr-2"
            style={{
              minWidth: "280px",
            }}
          >
            <Select
              classNamePrefix="default-select"
              name="GlobalLabel"
              value={
                optLabels?.filter((val) => globalLabel?.includes(val.value)) ||
                ""
              }
              options={optLabels}
              onChange={(selected) => { handleSelectLabel(selected) }}
              placeholder="Select Global Label"
              components={{
                IndicatorSeparator: () => null,
              }}
              isSearchable={true}
              isClearable={false}
              isMulti
            />
          </div>
        )}
        <CToggler
          inHeader
          className="c-d-legacy-none toggle-light-dark-btn mb-1"
          onClick={toggleDarkMode}
          title="Toggle Light/Dark Mode"
        >
          <CIcon
            name="cil-moon"
            className="c-d-dark-none"
            alt="CoreUI Icons Moon"
          />
          <CIcon
            name="cil-sun"
            className="c-d-default-none"
            alt="CoreUI Icons Sun"
          />
        </CToggler>
        <TheHeaderDropdown userData={userData} />
      </CHeaderNav>

      <CSubheader className="px-3 d-md-none">{renderHeaderNav()}</CSubheader>

      <CSubheader className="px-3 justify-content-between">
        <CBreadcrumbRouter
          className="border-0 c-subheader-nav m-0 px-0 px-md-3"
          routes={routes}
        />
      </CSubheader>
    </CHeader>
  );
};

export default TheHeader;
