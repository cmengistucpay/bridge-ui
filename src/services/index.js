import axios from "axios";
import {
  // getAuthToken,
  // removeAuthToken,
  getAuthTokenBridge,
  renewAuthToken,
} from "./serviceAuth";

const baseBridgeURL = process.env.REACT_APP_BASE_BRIDGE_URL;
const baseSalesURL = process.env.REACT_APP_BASE_SALES_URL;
const basePurchaseURL = process.env.REACT_APP_BASE_PURCHASE_URL;

const baseCmsURL = process.env.REACT_APP_CMS_URL;
const baseCmsService = axios.create({
  baseURL: `${baseCmsURL}`,
});

const baseBridgeService = axios.create({
  baseURL: `${baseBridgeURL}/api/v1`,
  headers: {
    // Authorization: getAuthToken()
    //   ? `Bearer ${getAuthToken().access_token}`
    //   : null,
    // "Content-Type": "application/x-www-form-urlencoded",
  },
});

const baseSalesService = axios.create({
  baseURL: `${baseSalesURL}/api/v1`,
  headers: {
    // Authorization: getAuthToken()
    //   ? `Bearer ${getAuthToken().access_token}`
    //   : null,
    // "Content-Type": "application/x-www-form-urlencoded",
  },
});

const basePurchaseService = axios.create({
  baseURL: `${basePurchaseURL}/api/v1`,
  headers: {
    // Authorization: getAuthToken()
    //   ? `Bearer ${getAuthToken().access_token}`
    //   : null,
    // "Content-Type": "application/json",
    // "Content-Type": "application/x-www-form-urlencoded",
  },
});

baseBridgeService.interceptors.request.use(
  function (config) {
    const authToken = getAuthTokenBridge();
    if (authToken) {
      config.headers.Authorization = `Bearer ${authToken?.access_token}`;
    }
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

baseBridgeService.interceptors.response.use(null, function (error) {
  if (error?.response?.status === 401) {
    renewAuthToken();
    // removeAuthToken();
    // logoutAzure();
    // window.location.href = "/login";
  }
  return Promise.reject(error);
});

baseSalesService.interceptors.request.use(
  function (config) {
    const authToken = getAuthTokenBridge();
    if (authToken) {
      config.headers.Authorization = `Bearer ${authToken?.access_token}`;
    }
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

baseSalesService.interceptors.response.use(null, function (error) {
  if (error?.response?.status === 401) {
    renewAuthToken();
    // removeAuthToken();
    // logoutAzure();
    // window.location.href = "/login";
  }
  return Promise.reject(error);
});

basePurchaseService.interceptors.request.use(
  function (config) {
    const authToken = getAuthTokenBridge();
    if (authToken) {
      config.headers.Authorization = `Bearer ${authToken?.access_token}`;
    }
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

basePurchaseService.interceptors.response.use(null, function (error) {
  if (error?.response?.status === 401) {
    renewAuthToken();
    // removeAuthToken();
    // logoutAzure();
    // window.location.href = "/login";
  }
  return Promise.reject(error);
});

// List of service URL

export const redirectCMS = () => {
  const authToken = getAuthTokenBridge();
  return baseCmsService
    .post(`/Account/SsoLogin`, {
      autoCreateCMSaccount: true,
      bridgeToken: authToken?.access_token,
    })
    .then((res) => res.data);
};

export const salUserLogin = (payload) => {
  return baseSalesService.post(`/token`, payload).then((res) => res.data);
};

export const purUserLogin = (payload) => {
  return basePurchaseService.post(`/token`, payload).then((res) => res.data);
};

export const salGetDevices = () => {
  return baseSalesService.get(`/devices`).then((res) => res.data);
};

export const salPostSales = (payload) => {
  return baseSalesService.post(`/sales`, payload).then((res) => res.data);
};

export const salPostSalesBatch = (payload) => {
  return baseSalesService.post(`/sales/batch`, payload).then((res) => res.data);
};

export const purGetIssuedCards = (pageNumber, pageSize, payload) => {
  return basePurchaseService
    .post(`/Search/IssuedCards/${pageNumber}/${pageSize}`, payload)
    .then((res) => res.data);
};

export const purGetCardDetail = (cardGuid) => {
  return basePurchaseService
    .get(`/Cards/${cardGuid}/true`)
    .then((res) => res.data);
};

export const purGetCardACHDetail = (cardGuid) => {
  return basePurchaseService
    .get(`/IssueACH/Transactions/${cardGuid}`)
    .then((res) => res.data);
};

export const purGetCardTransaction = (cardGuid) => {
  return basePurchaseService
    .get(`/Cards/Transactions/${cardGuid}`)
    .then((res) => res.data);
};

export const getCardDetails = (
  cardGuid
) => {
  return baseBridgeService
    .get(
      `/Cards/${cardGuid}`,
    )
    .then((res) => res.data);
};

export const purPostIssuedCard = (payload) => {
  return basePurchaseService
    .post(`/IssueCard`, payload)
    .then((res) => res.data);
};

export const purPostLodgedCard = (payload) => {
  return basePurchaseService
    .post(`/IssueCard/LodgedCard`, payload)
    .then((res) => res.data);
};

export const purUpdateIssuedCard = (cardGuid, payload) => {
  return basePurchaseService
    .put(`/IssueCard/${cardGuid}`, payload)
    .then((res) => res.data);
};

export const purUpdateLodgedCard = (cardGuid, payload) => {
  return basePurchaseService
    .put(`/IssueCard/LodgedCard/${cardGuid}`, payload)
    .then((res) => res.data);
};

export const purPostIssuedCardLite = (payload) => {
  return basePurchaseService
    .post(`/IssueCard/IssueLite`, payload)
    .then((res) => res.data);
};

export const purGetIncomingTransactionCodes = (
  pageNumber,
  pageSize,
  payload
) => {
  return basePurchaseService
    .post(`/Search/IncomingTransactionCodes/${pageNumber}/${pageSize}`, payload)
    .then((res) => res.data);
};

export const salGetSalesDetail = (itcId) => {
  return baseSalesService
    .get(`/sales/details/${itcId}`)
    .then((res) => res.data);
};

export const salPostSalesCancel = (payload) => {
  return baseSalesService.post(`/Cancel`, payload).then((res) => res.data);
};

export const salPostSalesVoid = (payload) => {
  return baseSalesService.post(`/Void`, payload).then((res) => res.data);
};

export const salPostSalesReturn = (payload) => {
  return baseSalesService
    .post(`/Returns/ComingFromVirtualTerminal`, payload)
    .then((res) => res.data);
};

export const salResendTransactionEmail = (payload) => {
  return baseSalesService
    .post(`/help/ResendTransactionEmail`, payload)
    .then((res) => res.data);
};

export const purGetIncomingTransactionCodeDetail = (itcId) => {
  return basePurchaseService
    .get(`/IncomingTransactionCodes/${itcId}`)
    .then((res) => res.data);
};

export const purUpdateIncomingTransactionCodeDetail = (itcId, payload) => {
  return basePurchaseService
    .put(`/IncomingTransactionCodes/${itcId}`,payload)
    .then((res) => res.data);
};

export const purGetPurchases = (pageNumber, pageSize, payload) => {
  return basePurchaseService
    .post(`/Search/Purchases/${pageNumber}/${pageSize}`, payload)
    .then((res) => res.data);
};

export const purPostTransmission = (cardGuid, payload) => {
  return basePurchaseService
    .put(`/IssueCard/SendPaymentInfo/${cardGuid}`, payload)
    .then((res) => res.data);
};

export const purGetMerchants = () => {
  return basePurchaseService.get(`/Merchants`).then((res) => res.data);
};

export const purGetMerchantCashBalanceSummary = (
  merchantGuid,
  pageNumber,
  pageSize,
  payload
) => {
  return basePurchaseService
    .post(
      `/Merchants/MerchantCashBalanceSummary/${merchantGuid}/${pageNumber}/${pageSize}`,
      payload
    )
    .then((res) => res.data);
};

export const updatePaymentLinkSettings = (
  merchantGuid,
  payload
) => {
  return baseBridgeService
    .patch(
      `/Merchants/${merchantGuid}`,
      payload
    )
    .then((res) => res.data);
};

export const getPaymentLinkSettings = (
  merchantGuid
) => {
  return baseBridgeService
    .get(
      `/Merchants/${merchantGuid}`,
    )
    .then((res) => res.data);
};

export const purGetMerchantCashBalance = (pageNumber, pageSize, payload) => {
  return basePurchaseService
    .post(`/Merchants/MerchantCashBalance/${pageNumber}/${pageSize}`, payload)
    .then((res) => res.data);
};

export const purGetMerchantSettlementMonthly = (
  pageNumber,
  pageSize,
  payload
) => {
  return basePurchaseService
    .post(
      `/Search/MonthlyIssuingSettlementMerchant/${pageNumber}/${pageSize}`,
      payload
    )
    .then((res) => res.data);
};

export const salPostHppRequest = (payload) => {
  return baseSalesService
    .post(`/HostedPaymentPageRequests`, payload)
    .then((res) => res.data);
};

export const briGetHppFiles = (hppId) => {
  return baseBridgeService
    .get(`/HostedPaymentLinks/${hppId}/files`)
    .then((res) => res.data);
};

export const briDeleteHppFiles = (hppId, payload) => {
  return baseBridgeService
    .delete(`/HostedPaymentLinks/${hppId}/files`, payload)
    .then((res) => res.data);
};

export const briPostHppFiles = (hppId, payload) => {
  return baseBridgeService
    .post(`/HostedPaymentLinks/${hppId}/files/upload`, payload)
    .then((res) => res.data);
};

export const purPostIssueACH = (payload) => {
  return basePurchaseService.post(`/IssueACH`, payload).then((res) => res.data);
};

export const purPostIssueACHLite = (payload) => {
  return basePurchaseService
    .post(`/IssueACH/IssueLite`, payload)
    .then((res) => res.data);
};

export const purGetMCCGroups = (merchantGuid, payload) => {
  merchantGuid = merchantGuid !== undefined && merchantGuid !== null ? merchantGuid: '';
  return basePurchaseService.get(`/MccGroups/${merchantGuid}`, payload).then((res) => res.data);
};

export const purGetMerchantSupplier = (merchantId) => {
  return basePurchaseService
    .get(`/MerchantSupplier/Settings/${merchantId}`)
    .then((res) => res.data);
};

export const purPostMerchantSupplier = (payload) => {
  return basePurchaseService
    .post(`/MerchantSupplier/Settings`, payload)
    .then((res) => res.data);
};

export const purDeleteMerchantSupplier = (supplierId) => {
  return basePurchaseService
    .post(`/MerchantSupplier/Settings/${supplierId}`)
    .then((res) => res.data);
};

export const purTerminateCard = (cardGuid) => {
  return basePurchaseService
    .post(`/TerminateCard/${cardGuid}`)
    .then((res) => res.data);
};

export const purPostAvailableToIssue = (payload) => {
  return basePurchaseService
    .post(`/merchants/AvailableToIssue`, payload)
    .then((res) => res.data);
};

export const salGetSaleDesc = (merchantGuid) => {
  return baseSalesService
    .get(`/MerchantSaleDescriptions/${merchantGuid}`)
    .then((res) => res.data);
};

export const salPostSaleDesc = (payload) => {
  return baseSalesService
    .post(`/MerchantSaleDescriptions`, payload)
    .then((res) => res.data);
};

export const salDeleteSaleDesc = (saleDescId) => {
  return baseSalesService
    .delete(`/MerchantSaleDescriptions/${saleDescId}`)
    .then((res) => res.data);
};

export const briGetMerchants = () => {
  return baseBridgeService.get(`/merchants`).then((res) => res.data);
};

export const briUpdateMerchants = (labelIds, payload) => {
  return baseBridgeService
    .patch(`/merchants?labelIds=${labelIds}`, payload)
    .then((res) => res.data);
};

export const briGetSales = (payload) => {
  return baseBridgeService
    .post(`/search/sales`, payload)
    .then((res) => res.data);
};

export const briGetLabelById = (labelId) => {
  return baseBridgeService.get(`/labels/${labelId}`).then((res) => res.data);
};

export const briUpdateLabels = (payload) => {
  return baseBridgeService.put(`/labels`, payload).then((res) => res.data);
};

export const briDeleteLabelById = (labelId) => {
  return baseBridgeService.delete(`/labels/${labelId}`).then((res) => res.data);
};

export const briGetPaymentLinks = (payload, pageNumber, pageSize) => {
  return baseBridgeService
    .post(`/HostedPaymentLinks/${pageNumber}/${pageSize}`, payload)
    .then((res) => res.data);
};

export const briUpdatePaymentLink = (payload) => {
  return baseBridgeService
    .patch(`/HostedPaymentLinks`, payload)
    .then((res) => res.data);
};

export const briEmailPaymentLink = (guid, payload) => {
  return baseBridgeService
    .post(`/HostedPaymentLinks/${guid}/SendEmail`, payload)
    .then((res) => res.data);
};

export const briGetUsers = (payload) => {
  return baseBridgeService.post(`/users`, payload).then((res) => res.data);
};

export const briCreateUser = (payload) => {
  return baseBridgeService.put(`/users`, payload).then((res) => res.data);
};

export const briCreateUserBulk = (payload) => {
  return baseBridgeService
    .post(`/users/upload`, payload)
    .then((res) => res.data);
};

export const briGetUserById = (userId) => {
  return baseBridgeService.get(`/users/${userId}`).then((res) => res.data);
};

export const briUpdateUser = (userId, payload) => {
  return baseBridgeService
    .patch(`/users/${userId}`, payload)
    .then((res) => res.data);
};

export const briRegisterUser = (payload) => {
  return baseBridgeService
    .post(`/users/register`, payload)
    .then((res) => res.data);
};

export const briUpdateUserStatus = (userId, status) => {
  return baseBridgeService
    .put(`/users/${userId}/status?status=${status}`)
    .then((res) => res.data);
};

export const briUserForgotUsername = (email) => {
  return baseBridgeService
    .post(`/users/forgot-username?emailAddress=${email}`)
    .then((res) => res.data);
};

export const briMerchantGateway = (payload) => {
  return baseBridgeService
    .post(`/merchants/merchantGatewayGuid`, payload)
    .then((res) => res.data);
};

export const loginToAnalytics = (payload) => {
  return baseBridgeService
    .post(`/Users/analytics/authenticate`, payload)
    .then((res) => res.data);
};


