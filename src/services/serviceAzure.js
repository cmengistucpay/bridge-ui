import { UserManager } from "oidc-client";
//process.env
const settings = {
  authority: process.env.REACT_APP_AZURE_AUTHORITY,
  loadUserInfo: false,
  redirect_uri: `${process.env.REACT_APP_HOST}/signin-callback.html`,
  response_type: "code", //id_token token', //code for PKCE
  client_id: process.env.REACT_APP_AZURE_CLIENT_ID,
  scope: process.env.REACT_APP_AZURE_SCOPES,
  metadata: {
    issuer: `${process.env.REACT_APP_AZURE_ISSUER}/`,
    authorization_endpoint: `${process.env.REACT_APP_AZURE_AUTH_ENDPOINT}/oauth2/v2.0/authorize?p=b2c_1_signin`,
    token_endpoint: `${process.env.REACT_APP_AZURE_AUTH_ENDPOINT}/oauth2/v2.0/token?p=b2c_1_signin`,
    jwks_uri: `${process.env.REACT_APP_AZURE_AUTH_ENDPOINT}/discovery/v2.0/keys?p=b2c_1_signin`,
    end_session_endpoint: `${process.env.REACT_APP_AZURE_AUTH_ENDPOINT}/oauth2/v2.0/logout?p=b2c_1_signin&post_logout_redirect_uri=${process.env.REACT_APP_HOST}`,
  },
};

const userManager = new UserManager(settings);

export const loginAzure = () => {
  return userManager.signinRedirect();
};

export const getUserAzure = () => {
  return userManager.getUser();
};

export const renewTokenAzure = () => {
  return userManager.signinSilent();
};

export const logoutAzure = () => {
  return userManager.signoutRedirect();
};

// export class AuthService {
//   constructor() {
//     const settings = {
//       authority:
//         "https://connexpaynonprod.b2clogin.com/connexpaynonprod.onmicrosoft.com/v2.0/.well-known/openid-configuration?p=B2C_1_SignIn",
//       loadUserInfo: false,
//       redirect_uri: "http://localhost:3000/signin-callback.html",
//       response_type: "code", //id_token token', //code for PKCE
//       client_id: "a2d4fa06-793e-42a0-a409-d035e5305736",
//       scope:
//         "openid offline_access https://connexpaynonprod.onmicrosoft.com/bridgeapi/api.access",
//       metadata: {
//         issuer:
//           "https://connexpaynonprod.b2clogin.com/214cc109-1075-4670-bc31-5c5d8c335a95/v2.0/",
//         authorization_endpoint:
//           "https://connexpaynonprod.b2clogin.com/connexpaynonprod.onmicrosoft.com/oauth2/v2.0/authorize?p=b2c_1_signin",
//         token_endpoint:
//           "https://connexpaynonprod.b2clogin.com/connexpaynonprod.onmicrosoft.com/oauth2/v2.0/token?p=b2c_1_signin",
//         jwks_uri:
//           "https://connexpaynonprod.b2clogin.com/connexpaynonprod.onmicrosoft.com/discovery/v2.0/keys?p=b2c_1_signin",
//         end_session_endpoint:
//           "https://connexpaynonprod.b2clogin.com/connexpaynonprod.onmicrosoft.com/oauth2/v2.0/logout?p=b2c_1_signin&post_logout_redirect_uri=http%3A%2F%2Flocalhost:3000%2F",
//       },
//     };

//     this.userManager = new UserManager(settings);

//     Log.logger = console;
//     Log.level = Log.INFO;
//   }

//   getUser() {
//     return this.userManager.getUser();
//   }

//   login() {
//     return this.userManager.signinRedirect();
//   }

//   renewToken() {
//     return this.userManager.signinSilent();
//   }

//   logout() {
//     return this.userManager.signoutRedirect();
//   }
// }
