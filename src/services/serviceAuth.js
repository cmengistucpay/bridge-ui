import jwt_decode from "jwt-decode";
import { isJSON } from "src/helper";
import { renewTokenAzure, logoutAzure } from "src/services/serviceAzure";

export const setAuthToken = (resAuth) => {
  localStorage.setItem("cxp-auth", JSON.stringify(resAuth));
  localStorage.setItem("cxp-auth-bridge", "Put Bridge Token Here");
};

export const getAuthToken = () => {
  const authToken = localStorage.getItem("cxp-auth");
  return authToken ? JSON.parse(authToken) : null;
};

export const setAuthTokenBridge = (resAuth) => {
  localStorage.setItem("cxp-auth-bridge", JSON.stringify(resAuth));
};

export const getAuthTokenBridge = () => {
  const authToken = localStorage.getItem("cxp-auth-bridge");
  return authToken && isJSON(authToken) ? JSON.parse(authToken) : null;
};

export const removeAuthToken = () => {
  localStorage.removeItem("cxp-auth");
  localStorage.removeItem("cxp-auth-bridge");
  localStorage.removeItem("cxp-data-persist");
};

export const renewAuthToken = async () => {
  try {
    const res = await renewTokenAzure();
    if (res) {
      setAuthTokenBridge(res);
      window.location.reload();
    }
  } catch (error) {
    removeAuthToken();
    logoutAzure();
    localStorage.removeItem("labelMarchantData");
    console.error(error);
  }
};

export const jwtDecodeBridge = () => {
  const authToken = getAuthTokenBridge();
  const authObj = jwt_decode(authToken?.access_token);
  return authObj || {};
};

export const jwtDecodeExpiration = () => {
  const authToken = getAuthTokenBridge();
  const authExpiration = authToken?.expires_at;
  return authExpiration || {};
};
