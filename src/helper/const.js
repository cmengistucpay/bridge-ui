export const optPurchaseType = [
  { value: "01", label: "Airline" },
  { value: "02", label: "Hotel" },
  { value: "03", label: "Car Rental" },
  { value: "99", label: "Other" },
  { value: "10", label: "All" },
  { value: "98", label: "No Restriction" },
];

export const optGender = [
  { value: "M", label: "Male" },
  { value: "F", label: "Female" },
];

export const optGlobalLabel = ["MN", "AZ", "CA"].map((val) => ({
  value: val,
  label: val,
}));

export const monthList = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

export const nonStateUS = [
  "Baker Island",
  "Howland Island",
  "Jarvis Island",
  "Johnston Atoll",
  "Kingman Reef",
  "Midway Atoll",
  "Navassa Island",
  "Northern Mariana Islands",
  "Palmyra Atoll",
  "United States Minor Outlying Islands",
  "Wake Island",
];
