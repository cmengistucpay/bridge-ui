import { forEach } from "lodash";

export function isEmail(email) {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export function isRoutingNumber(number) {
  if (number?.length === 9) {
    const firstFlow = number[0] * 3 + number[3] * 3 + number[6] * 3;
    const secondFlow = number[1] * 7 + number[4] * 7 + number[7] * 7;
    const thirdFlow = number[2] * 1 + number[5] * 1 + number[8] * 1;
    const sumAll = firstFlow + secondFlow + thirdFlow;
    const isValid = sumAll % 10 === 0 ? true : false;
    return isValid;
  } else {
    return false;
  }
}

export function convertDateYYMM(date) {
  const splittedDate = date?.split("-");
  const combinedDate = `${splittedDate[1]}${splittedDate[0]}`;
  return combinedDate;
}

export function splitFullName(fullName) {
  const splittedName = fullName?.split(" ");
  const firstName =
    splittedName?.length > 1
      ? splittedName?.slice(0, splittedName?.length - 1)?.join(" ")
      : splittedName[0];
  const lastName =
    splittedName?.length > 1 ? splittedName[splittedName.length - 1] : "";

  return { firstName, lastName };
}

export function convertCashSaleReceipt(receipt) {
  const receiptHeader = receipt?.split("CASH - SALE")?.[0]?.split(/\\n|\n/g);

  const receiptSummary = receipt
    ?.split("CASH - SALE")?.[1]
    ?.split("\\n\\n\\n\\n")?.[0]
    ?.split(/\\n|\n/g)
    ?.filter((val) => val && val.charAt(0) !== "-");

  const entryMode = receiptSummary?.[0]?.split(":")?.[1]?.replace(/ /g, "");
  const transactionId = receiptSummary?.[1]?.split(":")?.[1]?.replace(/ /g, "");
  const invoiceNumber = receiptSummary?.[2]?.split(":")?.[1]?.replace(/ /g, "");
  const subTotal = receiptSummary?.[3]?.split(":")?.[1]?.replace(/ /g, "");
  const total = receiptSummary?.[4]?.split(":")?.[1]?.replace(/ /g, "");

  let receiptAck = receipt
    ?.split("\\n\\n\\n\\n")?.[1]
    ?.replace(/\\n|\n/g, " ")
    ?.toLowerCase();

    receiptAck = !receiptAck?.trim().startsWith('customer') ? receiptAck?.replace('customer',"") : receiptAck;

  const objReceipt = {
    receiptHeader,
    receiptSummary: {
      entryMode,
      transactionId,
      invoiceNumber,
      subTotal,
      total,
    },
    receiptAck,
  };

  return objReceipt;
}

export function convertCreditSaleReceipt(receipt, type) {
  const receiptHeader = receipt
    ?.split(`CREDIT - ${type}`)?.[0]
    ?.split(/\\n|\n/g);

  const receiptSummary = receipt
    ?.split(`CREDIT - ${type}`)?.[1]
    ?.split("\\n\\n\\n\\n")?.[0]
    ?.split(/\\n|\n/g)
    ?.filter((val) => val && val.charAt(0) !== "-");

  const cardNumber = receiptSummary?.[0]?.split(":")?.[1]?.replace(/ /g, "");
  const cardType = receiptSummary?.[1]?.split(":")?.[1]?.replace(/ /g, "");
  const entryMode = receiptSummary?.[2]?.split(":")?.[1]?.replace(/ /g, "");
  const transactionId = receiptSummary?.[3]?.split(":")?.[1]?.replace(/ /g, "");
  const invoiceNumber = receiptSummary?.[4]?.split(":")?.[1]?.replace(/ /g, "");
  const subTotal = receiptSummary?.[5]?.split(":")?.[1]?.replace(/ /g, "");
  const total = receiptSummary?.[6]?.split(":")?.[1]?.replace(/ /g, "");

  let receiptAck = receipt
    ?.split("\\n\\n\\n\\n")?.[1]
    ?.replace(/\\n|\n/g, " ")
    ?.toLowerCase();

    receiptAck = !receiptAck?.trim().startsWith('customer') ? receiptAck?.replace('customer',"") : receiptAck;

  const objReceipt = {
    receiptHeader,
    receiptSummary: {
      cardNumber,
      cardType,
      entryMode,
      transactionId,
      invoiceNumber,
      subTotal,
      total,
    },
    receiptAck,
  };

  return objReceipt;
}

export function insertString(string, index, value) {
  const insertedString = string.substr(0, index) + value + string.substr(index);
  return insertedString;
}

export function formatUSD(string) {
  const formattedString = string?.toLocaleString("en-US", {
    style: "currency",
    currency: "USD",
  });
  const currencyString = formattedString || "-";
  return currencyString;
}

export function generateYears(maxYear, minYear) {
  const max = maxYear || new Date().getFullYear();
  const min = minYear || 1970;
  const years = [];

  for (let i = max; i >= min; i--) {
    years.push(i);
  }
  return years;
}

export function midStringEllipsis(string) {
  let injString = string;
  if (string.length > 35) {
    injString =
      string.substr(0, 10) +
      "..." +
      string.substr(string.length - 5, string.length);
  }

  return injString;
}

export function endStringEllipsis(string, length) {
  let injString = string;
  if (string.length > length) {
    injString = string.substr(0, length) + "...";
  }

  return injString;
}

export function isJSON(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

export function mapOptLabels(labelData) {
  let arrLabelData = [];

  forEach(labelData, (data0) => {
    arrLabelData.push({
      label: `${data0?.name}`,
      value: data0?.id,
      level: data0?.level,
      merchantGuid: data0?.gatewayMerchantGuid,
      merchantNumber: data0?.merchantNumber,
      deviceGuid: data0?.deviceGuid,
      masterMerchantId: data0?.masterMerchantId,
    });
    forEach(data0?.children, (data1) => {
      arrLabelData.push({
        label: `(${data0?.name}) ${data1?.name}`,
        value: data1?.id,
        level: data1?.level,
        merchantGuid: data1?.gatewayMerchantGuid,
        merchantNumber: data1?.merchantNumber,
        deviceGuid: data1?.deviceGuid,
        masterMerchantId: data1?.masterMerchantId,
      });
      forEach(data1?.children, (data2) => {
        arrLabelData.push({
          label: `(${data0?.name}
            > ${data1?.name}) ${data2?.name}`,
          value: data2?.id,
          level: data2?.level,
          merchantGuid: data2?.gatewayMerchantGuid,
          merchantNumber: data2?.merchantNumber,
          deviceGuid: data2?.deviceGuid,
          masterMerchantId: data2?.masterMerchantId,
        });
        forEach(data2?.children, (data3) => {
          arrLabelData.push({
            label: `(${data0?.name} >
              ${data1?.name} >
              ${data2?.name}) ${data3?.name}`,
            value: data3?.id,
            level: data3?.level,
            merchantGuid: data3?.gatewayMerchantGuid,
            merchantNumber: data3?.merchantNumber,
            deviceGuid: data3?.deviceGuid,
            masterMerchantId: data3?.masterMerchantId,
          });
          forEach(data3?.children, (data4) => {
            arrLabelData.push({
              label: `(${data0?.name} >
                ${data1?.name} >
                ${data2?.name} >
                ${data3?.name}) ${data4?.name}`,
              value: data4?.id,
              level: data4?.level,
              merchantGuid: data4?.gatewayMerchantGuid,
              merchantNumber: data4?.merchantNumber,
              deviceGuid: data4?.deviceGuid,
              masterMerchantId: data4?.masterMerchantId,
            });
            forEach(data4?.children, (data5) => {
              arrLabelData.push({
                label: `(${data0?.name} >
                  ${data1?.name} >
                  ${data2?.name} >
                  ${data3?.name} >
                  ${data4?.name}) ${data5?.name}`,
                value: data5?.id,
                level: data5?.level,
                merchantGuid: data5?.gatewayMerchantGuid,
                merchantNumber: data5?.merchantNumber,
                deviceGuid: data5?.deviceGuid,
                masterMerchantId: data5?.masterMerchantId,
              });
            });
          });
        });
      });
    });
  });

  return arrLabelData || [];
}
