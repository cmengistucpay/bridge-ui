import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import { CCard, CCardBody, CCardHeader, CCollapse } from "@coreui/react";
import CIcon from "@coreui/icons-react";

import PaymentLinkSettings from "./PaymentLinkSettings";
import FormSettings from "./FormSettings";
import SupplierIdList from "./SupplierIdList";
import SaleDescriptionList from "./SaleDescriptionList";

const AccountSettings = () => {
  const userData = useSelector((state) => state.userData);
  const [accordion, setAccordion] = useState(0);

  useEffect(() => {
    if (!userData?.canManageCustomFormPermissions) {
      <Redirect push to="/" />;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  const settingList = [
    {
      title: "Payment Link Settings",
      component: <PaymentLinkSettings userData={userData} />,
      className: userData?.merchantSettings?.showPaymentLinkSettings
        ? ""
        : "d-none",
    },
    {
      title: "Form Settings",
      component: <FormSettings />,
      className: userData?.merchantSettings?.showFormSettings ? "" : "d-none",
    },
    { title: "Supplier ID List", component: <SupplierIdList /> },
    {
      title: "Sale Description List",
      component: <SaleDescriptionList />,
      className: userData?.merchantSettings?.showPaymentLinkSettings
        ? ""
        : "d-none",
    },
  ];

  return (
    <>
      {settingList.map((val, i) => (
        <CCard key={i} className={`mb-2 ${val.className ? val.className : ""}`}>
          <CCardHeader
            className={`d-flex justify-content-between align-items-center hover-pointer`}
            onClick={() => setAccordion(accordion === i ? null : i)}
          >
            <h4 className="mr-2 mb-0">{val.title}</h4>
            {accordion === i ? (
              <CIcon name="cis-chevron-top" />
            ) : (
              <CIcon name="cis-chevron-bottom" />
            )}
          </CCardHeader>
          <CCollapse show={accordion === i}>
            <CCardBody>{val.component}</CCardBody>
          </CCollapse>
        </CCard>
      ))}
    </>
  );
};

export default AccountSettings;
