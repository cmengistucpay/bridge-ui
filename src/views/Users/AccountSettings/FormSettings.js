import React, { useState } from "react";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCollapse,
  CRow,
  CCol,
  CButton,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { Switch } from "src/components";

const SettingCreditCardSale = () => {
  const fields = [
    "Order Number",
    "Cardholder Name",
    "CVV2",
    "Customer ID",
    "Email Address",
    "Merchant Assigned Account Number for Consumer",
    "Product Description",
    "Billing Phone Number",
    "First Name",
    "Last Name",
    "Date of Birth",
    "Gender",
    "Phone Number",
    "SSN4",
    "Address Line 1",
    "Address Line 2",
    "Country",
    "State",
    "City",
    "Zip Code",
  ];
  return (
    <>
      <CRow>
        {fields.map((val, i) => (
          <CCol key={i} md="6">
            <Switch label={val} />
          </CCol>
        ))}
      </CRow>
    </>
  );
};

const SettingCashSale = () => {
  const fields = [
    "Order Number",
    "Customer ID",
    "First Name",
    "Last Name",
    "Address Line 1",
    "Address Line 2",
    "Country",
    "State",
    "City",
    "Zip Code",
    "Email Address",
    "Phone Number",
  ];
  return (
    <>
      <CRow>
        {fields.map((val, i) => (
          <CCol key={i} md="6">
            <Switch label={val} />
          </CCol>
        ))}
      </CRow>
    </>
  );
};

const SettingACHSale = () => {
  const fields = null;
  return (
    <>
      <CRow>
        {fields?.map((val, i) => (
          <CCol key={i} md="6">
            <Switch label={val} />
          </CCol>
        ))}
      </CRow>
    </>
  );
};

const SettingVirtualCardPurchase = () => {
  const fields = [
    "Address Line 1",
    "Address Line 2",
    "Country",
    "State",
    "Zip Code",
    "Supplier ID",
    "Customer ID",
    "Order Number",
    "Card Brand",
    "Card Class",
    "Termination Date",
  ];
  return (
    <>
      <CRow>
        {fields.map((val, i) => (
          <CCol key={i} md="6">
            <Switch label={val} />
          </CCol>
        ))}
      </CRow>
    </>
  );
};

const settingList = [
  { title: "Credit Card Sale", component: <SettingCreditCardSale /> },
  { title: "Cash Sale", component: <SettingCashSale /> },
  { title: "ACH Sale", component: <SettingACHSale /> },
  { title: "Virtual Card Purchase", component: <SettingVirtualCardPurchase /> },
];

const FormSettings = () => {
  const [accordion, setAccordion] = useState(0);

  const handleSubmit = (values) => {
    console.log({ values });
  };

  return (
    <>
      <CRow>
        <CCol xxl="12">
          {settingList.map((val, i) => (
            <CCard key={i} className="mb-2">
              <CCardHeader
                className="d-flex justify-content-between align-items-center hover-pointer"
                onClick={() => setAccordion(accordion === i ? null : i)}
              >
                <h5 className="mr-2 mb-0">{val.title}</h5>
                {accordion === i ? (
                  <CIcon name="cis-chevron-top" />
                ) : (
                  <CIcon name="cis-chevron-bottom" />
                )}
              </CCardHeader>
              <CCollapse show={accordion === i}>
                <CCardBody>{val.component}</CCardBody>
              </CCollapse>
            </CCard>
          ))}
        </CCol>
      </CRow>
      <div className="text-right mt-3">
        <CButton
          onClick={handleSubmit}
          color="primary"
          className="px-4 mx-1 w-50 w-md-unset"
        >
          Submit
        </CButton>
      </div>
    </>
  );
};

export default FormSettings;
