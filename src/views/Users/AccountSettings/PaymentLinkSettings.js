import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CButton,
  CForm,
  CFormGroup,
  CLabel,
  CInput,
  CInvalidFeedback,
  CSpinner,
  CAlert,
} from "@coreui/react";
import { useFormik } from "formik";
import * as Yup from "yup";

import {
  updatePaymentLinkSettings,
  getPaymentLinkSettings,
} from "src/services";

const initialValues = {
  clientLogoUrl: "",
  customerServicePhoneNbr: "",
};

const validationSchema = () => {
  return Yup.object().shape({});
};

const PaymentLinkSettings = (props) => {

  const [formValues, setFormValues] = useState(initialValues);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  const dispatch = useDispatch();
  const { userData } = props;
  const globalLabel = useSelector((state) => state.globalLabel);
  const toaster = useSelector((state) => state.toaster);

  const guid = userData?.guid;


  useEffect(() => {
    const labelMerchantData = JSON.parse(localStorage.getItem("labelMarchantData"));
    const merchantGuid = labelMerchantData.merchantGuid;

    if (merchantGuid && globalLabel.length) {
      fetchPaymentLinkSettings(merchantGuid);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [globalLabel]);

  const populateFormFields = (data) => {
    setFormValues(data);
  }

  const fetchPaymentLinkSettings = async (merchantGuid) => {
    try {
      setIsLoading(true);
      const res = await getPaymentLinkSettings(merchantGuid);
      if (res) {
        const paymentLinkSettingsData = {
          clientLogoUrl: res?.merchantBridgeSettings?.clientLogoUrl,
          customerServicePhoneNbr: res?.merchantBridgeSettings?.customerServicePhoneNbr,
        }
        populateFormFields(paymentLinkSettingsData)
      } else {
        populateFormFields(initialValues);
      }
    } catch (error) {
      // console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  const handleSubmitForm = async (values, { resetForm }) => {

    try {
      setIsSubmitting(true);
      const payload = {
        guid,
        clientLogoUrl: values.clientLogoUrl,
        customerServicePhoneNbr: values.customerServicePhoneNbr
      }

      const labelMerchantData = JSON.parse(localStorage.getItem("labelMarchantData"));
      const merchantGuid = labelMerchantData.merchantGuid;

      const res = await updatePaymentLinkSettings(merchantGuid, payload);

      resetForm({
        values: {
          clientLogoUrl: res?.merchantBridgeSettings?.clientLogoUrl,
          customerServicePhoneNbr: res?.merchantBridgeSettings?.customerServicePhoneNbr,
        },
      });

      const toasterConfig = {
        show: true,
        type: "success",
        title: "Success",
        body: "Payment link settings successfully updated.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
    } catch (error) {
      setResAlert({
        color: "danger",
        message: error?.response?.data?.message || "Network error.",
      });
    } finally {
      setIsSubmitting(false);
    }
  };

  const formik = useFormik({
    initialValues: formValues,
    enableReinitialize: true,
    validationSchema,
    onSubmit: handleSubmitForm,
  });

  const { values, touched, errors, handleChange, handleBlur, handleSubmit } =
    formik;

  return (
    <CForm>

      <CFormGroup>
        <CLabel htmlFor="clientLogoUrl">Logo URL</CLabel>
        <CInput
          autoComplete="none"
          name="clientLogoUrl"
          type="text"
          value={values.clientLogoUrl}
          placeholder="Enter Logo URL"
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={touched.clientLogoUrl && !!errors.clientLogoUrl}
          disabled={isLoading}
        />
        <CInvalidFeedback>{errors.clientLogoUrl}</CInvalidFeedback>
      </CFormGroup>
      <CFormGroup>
        <CLabel htmlFor="customerServicePhoneNbr">
          Customer Service Phone Number
        </CLabel>
        <CInput
          autoComplete="none"
          name="customerServicePhoneNbr"
          type="text"
          value={values.customerServicePhoneNbr}
          placeholder="Enter Customer Service Phone Number"
          onChange={handleChange}
          onBlur={handleBlur}
          invalid={
            touched.customerServicePhoneNbr && !!errors.customerServicePhoneNbr
          }
          disabled={isLoading}
        />
        <CInvalidFeedback>{errors.customerServicePhoneNbr}</CInvalidFeedback>
      </CFormGroup>

      <div className="text-right mt-3">
        <CButton
          onClick={handleSubmit}
          color="primary"
          className="px-4 mx-1 w-50 w-md-unset"
          type="submit"
          disabled={isLoading || isSubmitting}
        >
          {!isSubmitting ? (
            "Submit"
          ) : (
            <>
              <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
              Loading...
            </>
          )}
        </CButton>
      </div>
      <div>
        {resAlert && (
          <CAlert color={resAlert?.color} className="mt-3">
            {resAlert?.message}
          </CAlert>
        )}
      </div>
    </CForm>
  );
};

export default PaymentLinkSettings;
