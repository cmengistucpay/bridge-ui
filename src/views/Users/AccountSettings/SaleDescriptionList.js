import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CButton,
  CFormGroup,
  CInput,
  CListGroup,
  CListGroupItem,
  CSpinner,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import {
  salGetSaleDesc,
  salPostSaleDesc,
  salDeleteSaleDesc,
} from "src/services";

const SupplierIdList = (props) => {
  const dispatch = useDispatch();
  const accDevice = useSelector((state) => state.accDevices?.[0]);
  const toaster = useSelector((state) => state.toaster);
  const saleDescriptions = useSelector((state) => state.saleDescriptions);

  const [isInputActive, setIsInputActive] = useState(false);
  const [saleDesc, setSaleDesc] = useState("");

  const [isLoadingGet, setIsLoadingGet] = useState(false);
  const [isLoadingAdd, setIsLoadingAdd] = useState(false);
  const [isLoadingDel, setIsLoadingDel] = useState(null);

  useEffect(() => {
    if (accDevice?.merchantGuid) {
      _fetchSaleDescription();
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [accDevice]);

  const _fetchSaleDescription = async () => {
    try {
      setIsLoadingGet(true);
      const res = await salGetSaleDesc(accDevice?.merchantGuid);
      dispatch({ type: "set", saleDescriptions: res });
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoadingGet(false);
    }
  };

  const handleAddSupplier = async () => {
    try {
      setIsLoadingAdd(true);
      const payload = {
        MerchantGuid: accDevice?.merchantGuid,
        SaleDescription: saleDesc,
      };
      await salPostSaleDesc(payload);
      const toasterConfig = {
        show: true,
        type: "success",
        title: "Success",
        body: "Sale Description successfully added.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      setIsInputActive(false);
      setSaleDesc("");
      _fetchSaleDescription();
      // const newSaleDescriptions = [
      //   ...saleDescriptions,
      //   { id: saleDescriptions?.length + 1, desc: saleDesc },
      // ];
      // dispatch({ type: "set", saleDescriptions: newSaleDescriptions });
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body:
          error?.response?.data?.message || "Unable to add sale description.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      console.error(error);
    } finally {
      setIsLoadingAdd(false);
    }
  };

  const handleDeleteSupplier = async (saleDescId) => {
    try {
      setIsLoadingDel(saleDescId);
      await salDeleteSaleDesc(saleDescId);
      const toasterConfig = {
        show: true,
        type: "success",
        title: "Success",
        body: "Sale Description successfully deleted.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      _fetchSaleDescription();
      // const newSaleDescriptions = saleDescriptions?.filter(
      //   (val) => val.id !== saleDescId
      // );
      // dispatch({ type: "set", saleDescriptions: newSaleDescriptions });
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body:
          error?.response?.data?.message ||
          "Unable to delete sale description.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      console.error(error);
    } finally {
      setIsLoadingDel(null);
    }
  };

  const handleSubmit = () => {
    if (saleDesc) {
      handleAddSupplier();
    }
  };

  return (
    <>
      <CListGroup className="mb-2">
        {saleDescriptions?.map((val, i) => (
          <CListGroupItem className="d-flex justify-content-between" key={i}>
            {val?.saleDescription}
            {isLoadingDel === val.idMerchantSaleDescriptions ? (
              <CSpinner
                color="danger"
                component="span"
                size="sm"
                aria-hidden="true"
              />
            ) : (
              <CIcon
                onClick={() =>
                  handleDeleteSupplier(val.idMerchantSaleDescriptions)
                }
                className="text-danger hover-pointer"
                name="cis-x"
              />
            )}
          </CListGroupItem>
        ))}
        {isLoadingGet && (
          <div className="text-center my-2">
            <CSpinner
              color="primary"
              component="span"
              size="sm"
              aria-hidden="true"
            />
          </div>
        )}
      </CListGroup>
      {!isInputActive && (
        <CButton
          onClick={() => setIsInputActive(true)}
          color="secondary"
          className="px-2 mx-1"
        >
          <CIcon name="cis-plus" className="mr-2" />
          Add Description
        </CButton>
      )}
      {isInputActive && (
        <CFormGroup className="d-flex align-items-center">
          <CInput
            autoComplete="none"
            className="w-md-50"
            autoFocus
            type="text"
            value={saleDesc}
            placeholder="Enter Description"
            onChange={(e) => setSaleDesc(e.target.value)}
            onKeyDown={(e) => e.key === "Enter" && handleSubmit()}
          />
          <CButton
            disabled={!saleDesc || isLoadingAdd}
            onClick={handleSubmit}
            color="secondary"
            className="px-4 mx-1"
          >
            {!isLoadingAdd ? "Add" : "Loading..."}
          </CButton>
        </CFormGroup>
      )}
    </>
  );
};

export default SupplierIdList;
