import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CButton,
  CFormGroup,
  CInput,
  CListGroup,
  CListGroupItem,
  CSpinner,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import {
  purGetMerchantSupplier,
  purPostMerchantSupplier,
  purDeleteMerchantSupplier,
} from "src/services";

const SupplierIdList = (props) => {
  const dispatch = useDispatch();
  const accDevice = useSelector((state) => state.accDevices?.[0]);
  const toaster = useSelector((state) => state.toaster);

  const [isInputActive, setIsInputActive] = useState(false);
  const [supplierId, setSupplierId] = useState("");
  const [supplierList, setSupplierList] = useState([]);

  const [isLoadingGet, setIsLoadingGet] = useState(false);
  const [isLoadingAdd, setIsLoadingAdd] = useState(false);
  const [isLoadingDel, setIsLoadingDel] = useState(null);

  useEffect(() => {
    if (accDevice?.merchantGuid) {
      _fetchMerchantSupplier();
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [accDevice]);

  const _fetchMerchantSupplier = async () => {
    try {
      setIsLoadingGet(true);
      const res = await purGetMerchantSupplier(accDevice?.merchantGuid);
      setSupplierList(res);
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoadingGet(false);
    }
  };

  const handleAddSupplier = async () => {
    try {
      setIsLoadingAdd(true);
      const payload = {
        MerchantGuid: accDevice?.merchantGuid,
        SupplierName: supplierId,
      };
      const res = await purPostMerchantSupplier(payload);
      if (res) {
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "Supplier successfully added.",
        };
        dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
        setIsInputActive(false);
        _fetchMerchantSupplier();
        setSupplierId("");
      }
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body: error?.response?.data?.message || "Unable to add supplier.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      console.error(error);
    } finally {
      setIsLoadingAdd(false);
    }
  };

  const handleDeleteSupplier = async (supplierId) => {
    try {
      setIsLoadingDel(supplierId);
      await purDeleteMerchantSupplier(supplierId);
      const toasterConfig = {
        show: true,
        type: "success",
        title: "Success",
        body: "Supplier successfully deleted.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      _fetchMerchantSupplier();
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body: error?.response?.data?.message || "Unable to delete supplier.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      console.error(error);
    } finally {
      setIsLoadingAdd(null);
    }
  };

  const handleSubmit = () => {
    if (supplierId) {
      handleAddSupplier();
    }
  };

  return (
    <>
      <CListGroup className="mb-2">
        {supplierList?.map((val, i) => (
          <CListGroupItem className="d-flex justify-content-between" key={i}>
            {val?.supplierName}
            {isLoadingDel === val.idMerchantSupplierSettings ? (
              <CSpinner
                color="danger"
                component="span"
                size="sm"
                aria-hidden="true"
              />
            ) : (
              <CIcon
                onClick={() =>
                  handleDeleteSupplier(val.idMerchantSupplierSettings)
                }
                className="text-danger hover-pointer"
                name="cis-x"
              />
            )}
          </CListGroupItem>
        ))}
        {isLoadingGet && (
          <div className="text-center my-2">
            <CSpinner
              color="primary"
              component="span"
              size="sm"
              aria-hidden="true"
            />
          </div>
        )}
      </CListGroup>
      {!isInputActive && (
        <CButton
          onClick={() => setIsInputActive(true)}
          color="secondary"
          className="px-2 mx-1"
        >
          <CIcon name="cis-plus" className="mr-2" />
          Add Supplier Id
        </CButton>
      )}
      {isInputActive && (
        <CFormGroup className="d-flex align-items-center">
          <CInput
            autoComplete="none"
            className="w-md-50"
            autoFocus
            type="text"
            value={supplierId}
            placeholder="Enter Supplier Id"
            onChange={(e) => setSupplierId(e.target.value)}
            onKeyDown={(e) => e.key === "Enter" && handleSubmit()}
          />
          <CButton
            disabled={!supplierId || isLoadingAdd}
            onClick={handleSubmit}
            color="secondary"
            className="px-4 mx-1"
          >
            {!isLoadingAdd ? "Add" : "Loading..."}
          </CButton>
        </CFormGroup>
      )}
    </>
  );
};

export default SupplierIdList;
