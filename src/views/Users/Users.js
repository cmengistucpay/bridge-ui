import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { isEmpty } from "lodash";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CInput,
  CPagination,
  CButton,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { briGetUsers } from "src/services";

import UserFormModal from "./UserFormModal/UserFormModal";
import BulkUploadModal from "./BulkUploadModal";

// const tableData = [
//   {
//     UserName: "kpatel_user",
//     Email: "nathan.roberts@example.com",
//     FirstName: "Cameron",
//     LastName: "Williamson",
//     Status: "Active",
//     LabelContext: "ATL",
//     Label: "ABC",
//     lastLogin: "5/20/2021",
//     editUser: "",
//   },
//   {
//     UserName: "kpatel_user",
//     Email: "nathan.roberts@example.com",
//     FirstName: "Cameron",
//     LastName: "Williamson",
//     Status: "Inactive",
//     LabelContext: "ATL",
//     Label: "ABC",
//     lastLogin: "5/20/2021",
//     editUser: "",
//   },
//   {
//     UserName: "kpatel_user",
//     Email: "alma.lawson@example.com",
//     FirstName: "Cameron",
//     LastName: "Williamson",
//     Status: "Invited",
//     LabelContext: "ATL",
//     Label: "ABC",
//     lastLogin: "5/20/2021",
//     editUser: "",
//   },
// ];

const getBadge = (Status) => {
  switch (Status) {
    case "Active":
      return "success";
    case "Invited":
      return "warning";
    default:
      return "light";
  }
};

const ManageUsers = (props) => {
  const { history } = props;
  const [tableData, setTableData] = useState([]);
  const [tableParams, setTableParams] = useState({
    searchTerm: "",
    pageNum: 0,
    pageSize: 10,
    pageTotal: 1,
  });
  const [inputSearch, setInputSearch] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [modalUserForm, setModalUserForm] = useState(false);
  const [userData, setUserData] = useState(null);
  const [modalBulkUpload, setModalBulkUpload] = useState(false);
  const [isBulkUser, setIsBulkUser] = useState(false);
  const [userFile, setUserFile] = useState(null);
  const [formMode, setFormMode] = useState(null);

  const dispatch = useDispatch();
  const userDataGlobal = useSelector((state) => state.userData);
  const merchantSettings = userDataGlobal?.merchantSettings;
  const globalLabel = useSelector((state) => state.globalLabel);
  const toaster = useSelector((state) => state.toaster);

  useEffect(() => {
    if (!userDataGlobal?.canManageUsers) {
      history.push("/");
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userDataGlobal]);

  useEffect(() => {
    if (!isEmpty(userDataGlobal)) {
      _fetchUserList();
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    userDataGlobal,
    globalLabel,
    tableParams?.pageNum,
    tableParams?.searchTerm,
  ]);

  const _fetchUserList = async () => {
    try {
      setIsLoading(true);
      const payload = {
        searchTerm: tableParams?.searchTerm,
        labelIds: globalLabel.map((val) => val)?.join(","),
        pageNum: tableParams?.pageNum,
        pageSize: tableParams?.pageSize,
      };
      const res = await briGetUsers(payload);
      if (res) {
        setTableData(res?.searchResults);
        if (res?.totalCount) {
          setTableParams({
            ...tableParams,
            pageNum: res?.currentPage,
            pageTotal: res?.totalPageCount,
          });
        } else {
          setTableParams({
            ...tableParams,
            pageTotal: 1,
          });
        }
      }
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body: "Unable to get user list.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      setTableData([]);
      // console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  const handleChangePage = (page) => {
    setIsLoading(true);
    setTableParams({
      ...tableParams,
      pageNum: page,
    });
    setIsLoading(false);
  };

  const handleChangeSearch = (e) => {
    const { value } = e.target;
    setInputSearch(value);
  };

  const handleSubmitSearch = () => {
    const newTableParams = {
      ...tableParams,
      searchTerm: inputSearch,
    };
    setTableParams(newTableParams);
  };

  const handleShowModalUserForm = (formMode, bulkUpload, rowData) => {
    setFormMode(formMode);
    setModalUserForm(true);
    setUserData(rowData);
    setIsBulkUser(bulkUpload);
  };

  const handleHideModelUserForm = () => {
    setModalUserForm(false);
    setUserData(null);
    setIsBulkUser(false);
  };

  const handleShowModalBulkUpload = () => {
    setModalBulkUpload(true);
  };

  const handleHideModelBulkUpload = () => {
    setModalBulkUpload(false);
    setUserData(null);
  };

  const handleUploadFile = (file) => {
    handleShowModalUserForm("add", true);
    setUserFile(file);
  };

  return (
    <>
      <CCard>
        <CCardHeader className="d-md-flex flex-md-row align-items-center">
          <h4 className="mr-2 mb-md-0">Manage Users</h4>
          <div className="d-md-flex flex-auto justify-content-between align-items-center">
            <CInputGroup className="input-prepend mx-md-2 w-100 w-md-50">
              <CInputGroupPrepend>
                <CInputGroupText className="bg-primary border-primary">
                  <CIcon name="cil-search" className="text-white" />
                </CInputGroupText>
              </CInputGroupPrepend>
              <CInput
                autoComplete="none"
                name="search"
                type="text"
                placeholder="Search by Email Address, Name, Username, etc"
                value={inputSearch}
                onChange={handleChangeSearch}
                onBlur={() => handleSubmitSearch()}
                onKeyDown={(e) => {
                  if (e.key === "Enter") {
                    handleSubmitSearch();
                  }
                }}
              />
            </CInputGroup>
            <div className="d-flex mt-2 mt-md-0">
              <CButton
                color="light"
                className="px-3 py-1 mr-2"
                onClick={_fetchUserList}
              >
                <CIcon name="cil-reload" height={12} />
              </CButton>
              <CButton
                onClick={() => handleShowModalUserForm("add", false)}
                color="primary"
                className="mx-1 mx-md-2 w-50 w-md-unset"
              >
                Create User
              </CButton>
              <CButton
                onClick={() => handleShowModalBulkUpload()}
                color="primary"
                className="mx-1 mx-md-2 w-50 w-md-unset"
              >
                Bulk Upload
              </CButton>
            </div>
          </div>
        </CCardHeader>
        <CCardBody>
          <div className="table-not-relative">
            <CDataTable
              loading={isLoading}
              items={tableData}
              fields={[
                "userName",
                "emailAddress",
                "name",
                "status",
                ...(merchantSettings?.showLabels ? ["label"] : []),
                "editUser",
              ]}
              itemsPerPage={10}
              noItemsViewSlot={<div className="my-5" />}
              columnHeaderSlot={{
                lastLoginDateTime: "Last Login",
                editUser: <div className="text-center">Edit User</div>,
              }}
              scopedSlots={{
                userName: (item) => (
                  <td>
                    {item?.status === "Active" ? item?.userName : ""}
                    {/* {item?.userName || ""} */}
                  </td>
                ),
                name: (item) => (
                  <td>
                    {item?.status !== "Inactive" ? `${item?.name || ""}` : ""}
                  </td>
                ),
                status: (item) => (
                  <td>
                    <CBadge color={getBadge(item.status)}>{item.status}</CBadge>
                  </td>
                ),
                editUser: (item) => (
                  <td className="text-center">
                    <CButton
                      color="primary"
                      onClick={() => { handleShowModalUserForm("edit", false, item); }}
                    >
                      <CIcon name="cil-pencil" />
                    </CButton>
                  </td>
                ),
              }}
            />
          </div>
          <CPagination
            pages={tableParams?.pageTotal}
            activePage={tableParams?.pageNum}
            onActivePageChange={(page) =>
              !isLoading ? handleChangePage(page) : null
            }
            className={tableParams?.pageTotal < 2 ? "d-none" : ""}
          />
        </CCardBody>
      </CCard>
      <UserFormModal
        modalShow={modalUserForm}
        handleHideModal={handleHideModelUserForm}
        userData={userData}
        isBulkUser={isBulkUser}
        userFile={userFile}
        handlePostSubmit={() => _fetchUserList()}
        formMode={formMode}
      />
      <BulkUploadModal
        modalShow={modalBulkUpload}
        handleHideModal={handleHideModelBulkUpload}
        userData={userData}
        handlePostUpload={handleUploadFile}
      />
    </>
  );
};

export default ManageUsers;
