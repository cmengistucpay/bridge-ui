import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { isEmpty } from "lodash";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CRow,
  CCol,
  CForm,
  CFormGroup,
  CLabel,
  CInput,
  CInvalidFeedback,
  CButton,
  CAlert,
  CSpinner,
} from "@coreui/react";
import Select from "react-select";
import { useFormik } from "formik";
import * as Yup from "yup";
import NumberFormat from "react-number-format";
import { Switch } from "src/components";
import { mapOptLabels } from "src/helper";
import {
  briCreateUser,
  briUpdateUser,
  briCreateUserBulk,
  briGetUserById,
  briUpdateUserStatus,
} from "src/services";

const tooltipInfoHelper = {
  canProcessPaymentInstructionFile:
    "Turn this option on if you would like this user to be able to use our Process Sale > File Upload functionality.",
  canProcessSale:
    "Turn this option on if you would like this user to be able to Process Sales.",
  canVoid:
    "Turn this option on if you would like this user to be able to void Sales.",
  canReturn:
    "Turn this option on if you would like this user to be able to return Sales.",
  canCreateAPIKey: "Turn this on to allow user to create API credentials",
  canCreateVCC:
    "Turn this option on if you would like this user user to be able to create VCCs",
  canIssueACH:
    "Turn this option on if you would like this user to be able to issue ACH payments.",
  canManageUsers:
    "Turn this option on if you would like this users to be able to access the User page and modify user permissions (including their own)",
  canAccessCMS:
    "Turn this option on if you would like this users to be able to access CMS (chargeback management system)",
  canAccessAnalytics:
    "Turn this option on if you would like this users to be able to access Analytics",
  canAccessCashBalance:
    "Turn this option on if you would like this user to be able to see the cash balance report.",
  canAccessStatements:
    "Turn this option on if you would like this user to be able to see statements.",
  canManageCustomFormPermissions:
    "Turn this option on if you would like this user to be able to access the custom form permissions page.",
  canCreatePaymentLink:
    "Turn this option on if you would like this user to be able to create payment links via Issue Purchase > Create Payment Link option.",
  canManageLabels:
    "Turn this option on if you would like this user to be able to manage labels",
  canUnlockVcc:
    "Turn this option on if you would like this user to be able to unlock VCCs",
  canReloadVcc:
    "Turn this option on if you would like this user to be able to reload VCCs",
};

const objInitialValues = {
  emailAddress: "",
  userName: "",
  firstName: "",
  lastName: "",
  phoneNumber: "",
  Merchant: "",
  labelIds: undefined,
  canProcessPaymentInstructionFile: false,
  canProcessSale: false,
  canVoid: false,
  canReturn: false,
  canCreateVCC: false,
  canIssueACH: false,
  canCreateAPIKey: false,
  canManageUsers: false,
  canAccessCMS: false,
  canAccessAnalytics: false,
  canAccessCashBalance: false,
  canAccessStatements: false,
  canManageCustomFormPermissions: false,
  canCreatePaymentLink: false,
  canManageLabels: false,
  canUnlockVcc: false,
  canReloadVcc: false,
  maxDailyACHAmount: "",
  maxACHTransactionAmount: "",
  loadAmountLimitPerDay: "",
  loadAmountLimitPerVCC: "",
  IsBulkUser: false,
  maxLodgedCardIssueAmount: ""
};

const validationSchema = () => {
  const AmountLimit = 999999;
  return Yup.object().shape({
    IsBulkUser: Yup.boolean(),
    emailAddress: Yup.string().when("IsBulkUser", {
      is: false,
      then: Yup.string().required("Email Address is required"),
    }),
    maxLodgedCardIssueAmount: Yup.number()
    .max(AmountLimit, "Must be less than or equal $" + AmountLimit)
  });
};

const UserFormModal = (props) => {
  const {
    modalShow,
    handleHideModal,
    userData,
    userFile,
    isBulkUser,
    handlePostSubmit,
    formMode,
  } = props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);
  const userDataGlobal = useSelector((state) => state.userData);

  const { canCreateVCC, canIssueACH, canManageUsers, canAccessAnalytics, canAccessStatements,
    canCreatePaymentLink, canUnlockVcc, canAccessCMS, canAccessCashBalance, canManageCustomFormPermissions,
    canManageLabels, canReloadVcc, canProcessPaymentInstructionFile, canVoid, canProcessSale, canReturn
  } = userDataGlobal;

  const [initialValues, setInitialValues] = useState(objInitialValues);
  const [optLabels, setOptLabels] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  const [allSettingsOn, setAllSettingsOn] = useState(false);

  useEffect(() => {
    setResAlert(null);
    if (!modalShow) {
      setInitialValues(objInitialValues);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  useEffect(() => {
    if (!isEmpty(userDataGlobal)) {
      const arrOptLabels = mapOptLabels(userDataGlobal?.labelHierarchy);
      setOptLabels(arrOptLabels);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userDataGlobal]);

  useEffect(() => {
    if (userData?.guid) {
      fetchUserDetail();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  const fetchUserDetail = async () => {
    try {
      setIsLoading(true);
      let res = await briGetUserById(userData?.guid);
      setInitialValues({
        ...initialValues,
        ...res,
        emailAddress: res?.email,
        labelIds: res?.labelHierarchy?.map((val) => val.id),
        phoneNumber: res?.phone,
        merchantSettings: {
          ...res?.merchantSettings,
          showLabels: false,
          showPaymentLinks: false,
        },
      });
    } catch (error) {
      setResAlert({
        color: "danger",
        message:
          error?.response?.data?.message ||
          error?.response?.data ||
          "Request failed. Unable to get user detail.",
      });
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    setFieldValue("IsBulkUser", isBulkUser);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isBulkUser]);

  const handleUpdateStatus = async (status) => {
    try {
      setIsSubmitting(true);
      await briUpdateUserStatus(userData?.guid, status);

      const toasterConfig = {
        show: true,
        type: "success",
        title: "Success",
        body: "User status successfully updated",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      handleHideModal();
      resetForm();
      handlePostSubmit();
    } catch (error) {
      setResAlert({
        color: "danger",
        message:
          error?.response?.data?.message ||
          error?.response?.data ||
          "Request failed. Unable to update user status.",
      });
    } finally {
      setIsSubmitting(false);
    }
  };

  const handleSubmitForm = async (values) => {
    try {
      setIsSubmitting(true);
      setResAlert(null);
      const payload = {
        ...values,
        maxLodgedCardIssueAmount: values?.maxLodgedCardIssueAmount || undefined,
        maxDailyACHAmount: values?.maxDailyACHAmount || undefined,
        maxACHTransactionAmount: values?.maxACHTransactionAmount || undefined,
        loadAmountLimitPerDay: values?.loadAmountLimitPerDay || undefined,
        loadAmountLimitPerVCC: values?.loadAmountLimitPerVCC || undefined,
        phoneNumber: values?.phoneNumber || undefined,
        userId: undefined,
        labelIds: values?.labelIds,
        guid: userData?.guid,
        labelHierarchy: undefined,
        merchantSettings: undefined,
      };

      if (formMode === "add" && !isBulkUser) {
        const res = await briCreateUser(payload);
        if (res) {
          const toasterConfig = {
            show: true,
            type: "success",
            title: "Success",
            body: "User has been created and has sent an email to complete the signup process",
          };
          dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
          handleHideModal();
        }
      } else if (formMode === "edit" && !isBulkUser) {
        const res = await briUpdateUser(userData?.guid, payload);
        if (res) {
          const toasterConfig = {
            show: true,
            type: "success",
            title: "Success",
            body: "User successfully edited",
          };
          dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
          handleHideModal();
        }
      } else {
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "Bulk Users successfully created",
        };
        dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
        handleHideModal();
      }
      resetForm();
      handlePostSubmit();
    } catch (error) {
      setResAlert({
        color: "danger",
        message: error || "Network error.",
      });
    } finally {
      setIsSubmitting(false);
    }
  };

  const handleSubmitBulk = async (values) => {
    try {
      setIsSubmitting(true);
      setResAlert(null);
      const formData = new FormData();
      formData.append("file", userFile?.file);
      if (!userFile?.isLabelIncluded) {
        formData.append("labelIds", values?.labelIds);
      }
      formData.append(
        "canProcessPaymentInstructionFile",
        values?.canProcessPaymentInstructionFile
      );
      formData.append("canProcessSale", values?.canProcessSale);
      formData.append("canVoid", values?.canVoid);
      formData.append("canReturn", values?.canReturn);
      formData.append("canCreateVCC", values?.canCreateVCC);
      formData.append("canIssueACH", values?.canIssueACH);
      formData.append(
        "loadAmountLimitPerVCC",
        values?.loadAmountLimitPerVCC || 0
      );
      formData.append(
        "maxACHTransactionAmount",
        values?.maxACHTransactionAmount || 0
      );
      formData.append(
        "loadAmountLimitPerDay",
        values?.loadAmountLimitPerDay || 0
      );
      formData.append("maxDailyACHAmount", values?.maxDailyACHAmount || 0);
      formData.append("canCreateAPIKey", values?.canCreateAPIKey);
      formData.append("canAccessCMS", values?.canAccessCMS);
      formData.append("canAccessCashBalance", values?.canAccessCashBalance);
      formData.append(
        "canManageCustomFormPermissions",
        values?.canManageCustomFormPermissions
      );
      formData.append("canManageUsers", values?.canManageUsers);
      formData.append("canAccessAnalytics", values?.canAccessAnalytics);
      formData.append("canAccessStatements", values?.canAccessStatements);
      formData.append("canCreatePaymentLink", values?.canCreatePaymentLink);

      const res = await briCreateUserBulk(formData);
      if (res) {
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "Bulk Users successfully created",
        };
        dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
        handleHideModal();
      }
      resetForm();
      handlePostSubmit();
    } catch (error) {
      setResAlert({
        color: "danger",
        message:
          error?.response?.data?.message ||
          error?.response?.data?.title ||
          error?.response?.data ||
          "Network error.",
      });
    } finally {
      setIsSubmitting(false);
    }
  };

  const validateForm = (values) => {
    let errors = {};
    if (!values?.labelIds && !userFile?.isLabelIncluded) {
      errors = {
        labelIds: "Label is required",
      };
    }
    return errors;
  };

  const toggleAbleItems = [
    "canProcessPaymentInstructionFile",
    "canProcessSale",
    "canVoid",
    "canReturn",
    "canCreateVCC",
    "canIssueACH",
    "canCreateAPIKey",
    "canManageUsers",
    "canAccessCMS",
    "canAccessAnalytics",
    "canAccessCashBalance",
    "canAccessStatements",
    "canManageCustomFormPermissions",
    "canCreatePaymentLink",
    "canManageLabels",
    "canUnlockVcc",
    "canReloadVcc",
  ];

  const toggleAllSettings = () => {
    const value = !allSettingsOn;
    setAllSettingsOn(value);
    toggleAbleItems.forEach((name) => {
      setFieldValue(name, value);
    });
  };

  const handleOnChangedForm = (name, value) => {
    if (name && (value === true || value === false)) {
      values[name] = value;
    }

    let isAllToggledOn = true;
    let isAllToggledOff = true;
    toggleAbleItems.forEach((name) => {
      if (values[name] === false && isAllToggledOn === true) {
        isAllToggledOn = false;
      }
      if (values[name] === true && isAllToggledOff === true) {
        isAllToggledOff = false;
      }
    });

    if (isAllToggledOn === true) {
      setAllSettingsOn(true);
    } else {
      setAllSettingsOn(false);
    }
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: !isBulkUser ? handleSubmitForm : handleSubmitBulk,
    enableReinitialize: true,
    validate: validateForm,
  });
  const {
    values,
    touched,
    errors,
    setFieldValue,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
  } = formik;

  const handleSwitchValueChanged = (name, value) => {
    setFieldValue(name, value);
    handleOnChangedForm(name, value);
  };

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
      size="lg"
    >
      <CModalHeader closeButton>
        <CModalTitle>
          {formMode === "edit" ? "Update" : "Create"}{" "}
          {!isBulkUser ? "User" : "Users"}
        </CModalTitle>
      </CModalHeader>
      <CModalBody>
        {isLoading && (
          <div className="text-center my-2">
            <CSpinner color="primary" component="span" aria-hidden="true" />
          </div>
        )}
        <CForm>
          <CRow>
            {!isBulkUser && (
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="emailAddress">
                    Email<span className="label-required">*</span>
                  </CLabel>
                  <CInput
                    autoComplete="none"
                    name="emailAddress"
                    type="text"
                    value={values.emailAddress}
                    placeholder="Enter Email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={touched.emailAddress && !!errors.emailAddress}
                  />
                  <CInvalidFeedback>{errors.emailAddress}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
            )}
            {formMode === "edit" && (
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="userName">User Name</CLabel>
                  <CInput
                    autoComplete="none"
                    name="userName"
                    type="text"
                    value={values.userName}
                    placeholder="Enter User Name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={touched.userName && !!errors.userName}
                  />
                  <CInvalidFeedback>{errors.userName}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
            )}

            {formMode === "edit" && (
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="firstName">First Name</CLabel>
                  <CInput
                    autoComplete="none"
                    name="firstName"
                    type="text"
                    value={values.firstName}
                    placeholder="Enter First Name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={touched.firstName && !!errors.firstName}
                  />
                  <CInvalidFeedback>{errors.firstName}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
            )}
            {formMode === "edit" && (
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="lastName">Last Name</CLabel>
                  <CInput
                    autoComplete="none"
                    name="lastName"
                    type="text"
                    value={values.lastName}
                    placeholder="Enter Last Name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={touched.lastName && !!errors.lastName}
                  />
                  <CInvalidFeedback>{errors.lastName}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
            )}

            {formMode === "edit" && (
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="phoneNumber">Phone Number</CLabel>
                  <NumberFormat
                    name="phoneNumber"
                    customInput={CInput}
                    value={values.phoneNumber}
                    format="(###) ###-####"
                    placeholder="Enter Phone Number"
                    onValueChange={(val) =>
                      setFieldValue("phoneNumber", val.value)
                    }
                    mask={"_"}
                    onBlur={handleBlur}
                    invalid={touched.phoneNumber && !!errors.phoneNumber}
                  />
                  <CInvalidFeedback>{errors.phoneNumber}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
            )}
            {!userDataGlobal?.merchantSettings?.showLabels &&
              !userFile?.isLabelIncluded && (
                <CCol md="6">
                  <CFormGroup>
                    <CLabel htmlFor="Merchant">
                      Merchant<span className="label-required">*</span>
                    </CLabel>
                    <Select
                      className={
                        touched.labelIds && !!errors.labelIds
                          ? "is-invalid"
                          : ""
                      }
                      classNamePrefix="default-select"
                      name="labelIds"
                      value={
                        optLabels?.filter((val) =>
                          values?.labelIds?.includes(val.value)
                        ) || ""
                      }
                      onChange={(selected) =>
                        setFieldValue(
                          "labelIds",
                          selected?.map((val) => val.value)
                        )
                      }
                      options={optLabels?.filter((val) => val?.level === 0)}
                      placeholder="Select Merchant"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                      isSearchable={true}
                      isClearable={false}
                      isMulti
                    />
                    <CInvalidFeedback>{errors.labelIds}</CInvalidFeedback>
                  </CFormGroup>
                </CCol>
              )}

            {userDataGlobal?.merchantSettings?.showLabels &&
              !userFile?.isLabelIncluded && (
                <CCol md="6">
                  <CFormGroup>
                    <CLabel htmlFor="labelIds">
                      Label<span className="label-required">*</span>
                    </CLabel>
                    <Select
                      className={
                        touched.labelIds && !!errors.labelIds
                          ? "is-invalid"
                          : ""
                      }
                      classNamePrefix="default-select"
                      name="labelIds"
                      value={
                        optLabels?.filter((val) =>
                          values?.labelIds?.includes(val.value)
                        ) || ""
                      }
                      onChange={(selected) =>
                        setFieldValue(
                          "labelIds",
                          selected?.map((val) => val.value)
                        )
                      }
                      options={optLabels}
                      placeholder="Select Label"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                      isSearchable={true}
                      isClearable={false}
                      isMulti
                    />
                    <CInvalidFeedback>{errors.labelIds}</CInvalidFeedback>
                  </CFormGroup>
                </CCol>
              )}
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="maxLodgedCardIssueAmount">
                    Max Lodged Card Issue Amount
                  </CLabel>
                  <CInput
                    autoComplete="none"
                    name="maxLodgedCardIssueAmount"
                    type="text"
                    value={values.maxLodgedCardIssueAmount === 0 ? "" : values.maxLodgedCardIssueAmount}
                    placeholder="Enter Amount"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={
                      touched.maxLodgedCardIssueAmount &&
                      !!errors.maxLodgedCardIssueAmount
                    }
                  />
                  <CInvalidFeedback>
                    {errors.maxLodgedCardIssueAmount}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>          
          </CRow>

          <CRow>
            <CCol md="6">
              <h5 className="mt-2">Custom Settings</h5>
            </CCol>
            <CCol md="6" className="mt-2 toggle-all-setting">
              <Switch
                label="Turn on all settings"
                checked={allSettingsOn}
                onChange={() => toggleAllSettings()}
                tooltipInfo={"Turn on all settings"}
              />
            </CCol>
          </CRow>

          <CRow>

            {
              !!canProcessPaymentInstructionFile &&
              <CCol md="6">
                <Switch
                  label="Can process payment instruction file"
                  checked={values.canProcessPaymentInstructionFile}
                  onChange={() =>
                    handleSwitchValueChanged(
                      "canProcessPaymentInstructionFile",
                      !values.canProcessPaymentInstructionFile
                    )
                  }
                  tooltipInfo={
                    tooltipInfoHelper["canProcessPaymentInstructionFile"]
                  }
                />
              </CCol>
            }

            {
              !!canProcessSale &&
              <CCol md="6">
                <Switch
                  label="Can process Sale"
                  checked={values.canProcessSale}
                  onChange={() =>
                    handleSwitchValueChanged(
                      "canProcessSale",
                      !values.canProcessSale
                    )
                  }
                  tooltipInfo={tooltipInfoHelper["canProcessSale"]}
                />
              </CCol>
            }

            {
              !!canVoid &&
              <CCol md="6">
                <Switch
                  label="Can void"
                  checked={values.canVoid}
                  onChange={() =>
                    handleSwitchValueChanged("canVoid", !values.canVoid)
                  }
                  tooltipInfo={tooltipInfoHelper["canVoid"]}
                />
              </CCol>
            }

            {
              !!canReturn &&
              <CCol md="6">
                <Switch
                  label="Can return"
                  checked={values.canReturn}
                  onChange={() =>
                    handleSwitchValueChanged("canReturn", !values.canReturn)
                  }
                  tooltipInfo={tooltipInfoHelper["canReturn"]}
                />
              </CCol>
            }
          </CRow>

          <CRow>
            {
              (!!canCreateVCC || !!canIssueACH) &&
              <CCol md={12}>
                <h5 className="mt-2">VCC Settings</h5>
              </CCol>
            }

            {
              !!canCreateVCC &&
              <CCol md="6">
                <Switch
                  label="Can create VCC"
                  checked={values.canCreateVCC}
                  onChange={() => {
                    handleSwitchValueChanged(
                      "canCreateVCC",
                      !values.canCreateVCC
                    );
                    if (!values.canCreateVCC === false) {
                      handleSwitchValueChanged("loadAmountLimitPerVCC", "");
                      handleSwitchValueChanged("loadAmountLimitPerDay", "");
                    }
                  }}
                  tooltipInfo={tooltipInfoHelper["canCreateVCC"]}
                />
                <CFormGroup>
                  <CLabel htmlFor="loadAmountLimitPerVCC">
                    Load amount limit per VCC
                  </CLabel>
                  <CInput
                    autoComplete="none"
                    disabled={!values.canCreateVCC}
                    name="loadAmountLimitPerVCC"
                    type="text"
                    value={values.canCreateVCC ? values.loadAmountLimitPerVCC : ""}
                    placeholder="Enter Amount"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={
                      touched.loadAmountLimitPerVCC &&
                      !!errors.loadAmountLimitPerVCC
                    }
                  />
                  <CInvalidFeedback>
                    {errors.loadAmountLimitPerVCC}
                  </CInvalidFeedback>
                </CFormGroup>
                <CFormGroup>
                  <CLabel htmlFor="loadAmountLimitPerDay">
                    Load amount limit per day
                  </CLabel>
                  <CInput
                    autoComplete="none"
                    disabled={!values.canCreateVCC}
                    name="loadAmountLimitPerDay"
                    type="text"
                    value={values.canCreateVCC ? values.loadAmountLimitPerDay : ""}
                    placeholder="Enter Amount"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={
                      touched.loadAmountLimitPerDay &&
                      !!errors.loadAmountLimitPerDay
                    }
                  />
                  <CInvalidFeedback>
                    {errors.loadAmountLimitPerDay}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>
            }

            {
              !!canIssueACH &&
              <CCol md="6">
                <Switch
                  label="Can issue ACH"
                  checked={values.canIssueACH}
                  onChange={() => {
                    handleSwitchValueChanged("canIssueACH", !values.canIssueACH);
                    if (!values.canIssueACH === false) {
                      handleSwitchValueChanged("maxACHTransactionAmount", "");
                      handleSwitchValueChanged("maxDailyACHAmount", "");
                    }
                  }}
                  tooltipInfo={tooltipInfoHelper["canIssueACH"]}
                />
                <CFormGroup>
                  <CLabel htmlFor="maxACHTransactionAmount">
                    Max ACH transaction amount
                  </CLabel>
                  <CInput
                    autoComplete="none"
                    disabled={!values.canIssueACH}
                    name="maxACHTransactionAmount"
                    type="text"
                    value={values.canIssueACH ? values.maxACHTransactionAmount : ""}
                    placeholder="Enter Amount"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={
                      touched.maxACHTransactionAmount &&
                      !!errors.maxACHTransactionAmount
                    }
                  />
                  <CInvalidFeedback>
                    {errors.maxACHTransactionAmount}
                  </CInvalidFeedback>
                </CFormGroup>
                <CFormGroup>
                  <CLabel htmlFor="maxDailyACHAmount">
                    Max daily ACH amount
                  </CLabel>
                  <CInput
                    autoComplete="none"
                    disabled={!values.canIssueACH}
                    name="maxDailyACHAmount"
                    type="text"
                    value={values.canIssueACH ? values.maxDailyACHAmount : ""}
                    placeholder="Enter Amount"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={
                      touched.maxDailyACHAmount && !!errors.maxDailyACHAmount
                    }
                  />
                  <CInvalidFeedback>
                    {errors.maxDailyACHAmount}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>
            }
          </CRow>

          <h5 className="mt-2">Access Settings</h5>
          <CRow>
            {
              !!canManageUsers &&
              <CCol md="6">
                <Switch
                  label="Can manage users"
                  checked={values.canManageUsers}
                  onChange={() =>
                    handleSwitchValueChanged(
                      "canManageUsers",
                      !values.canManageUsers
                    )
                  }
                  tooltipInfo={tooltipInfoHelper["canManageUsers"]}
                />
              </CCol>
            }

            {
              !!canAccessCMS &&
              <CCol md="6">
                <Switch
                  label="Can access CMS"
                  checked={values.canAccessCMS}
                  onChange={() =>
                    handleSwitchValueChanged("canAccessCMS", !values.canAccessCMS)
                  }
                  tooltipInfo={tooltipInfoHelper["canAccessCMS"]}
                />
              </CCol>
            }

            {
              !!canAccessAnalytics &&
              <CCol md="6">
                <Switch
                  label="Can access analytics"
                  checked={values.canAccessAnalytics}
                  onChange={() =>
                    handleSwitchValueChanged(
                      "canAccessAnalytics",
                      !values.canAccessAnalytics
                    )
                  }
                  tooltipInfo={tooltipInfoHelper["canAccessAnalytics"]}
                />
              </CCol>
            }

            {
              !!canAccessCashBalance &&
              <CCol md="6">
                <Switch
                  label="Can access Reports > Cash Balance"
                  checked={values.canAccessCashBalance}
                  onChange={() =>
                    handleSwitchValueChanged(
                      "canAccessCashBalance",
                      !values.canAccessCashBalance
                    )
                  }
                  tooltipInfo={tooltipInfoHelper["canAccessCashBalance"]}
                />
              </CCol>
            }

            {
              !!canAccessStatements &&
              <CCol md="6">
                <Switch
                  label="Can access Reports > Statements"
                  checked={values.canAccessStatements}
                  onChange={() =>
                    handleSwitchValueChanged(
                      "canAccessStatements",
                      !values.canAccessStatements
                    )
                  }
                  tooltipInfo={tooltipInfoHelper["canAccessStatements"]}
                />
              </CCol>
            }

            {
              !!canManageCustomFormPermissions &&
              <CCol md="6">
                <Switch
                  label="Can manage account settings"
                  checked={values.canManageCustomFormPermissions}
                  onChange={() =>
                    handleSwitchValueChanged(
                      "canManageCustomFormPermissions",
                      !values.canManageCustomFormPermissions
                    )
                  }
                  tooltipInfo={
                    tooltipInfoHelper["canManageCustomFormPermissions"]
                  }
                />
              </CCol>
            }

            {
              !!canCreatePaymentLink &&
              <CCol md="6">
                <Switch
                  label="Can create payment link"
                  checked={values.canCreatePaymentLink}
                  onChange={() =>
                    handleSwitchValueChanged(
                      "canCreatePaymentLink",
                      !values.canCreatePaymentLink
                    )
                  }
                  tooltipInfo={tooltipInfoHelper["canCreatePaymentLink"]}
                />
              </CCol>
            }

            {
              !!canManageLabels &&
              <CCol md="6">
                <Switch
                  label="Can manage labels"
                  checked={values.canManageLabels}
                  onChange={() =>
                    handleSwitchValueChanged(
                      "canManageLabels",
                      !values.canManageLabels
                    )
                  }
                  tooltipInfo={tooltipInfoHelper["canManageLabels"]}
                />
              </CCol>
            }

            {
              !!canUnlockVcc &&
              <CCol md="6">
                <Switch
                  label="Can unlock VCC"
                  checked={values.canUnlockVcc}
                  onChange={() =>
                    handleSwitchValueChanged("canUnlockVcc", !values.canUnlockVcc)
                  }
                  tooltipInfo={tooltipInfoHelper["canUnlockVcc"]}
                />
              </CCol>
            }

            {
              !!canReloadVcc &&
              <CCol md="6">
                <Switch
                  label="Can reload VCC"
                  checked={values.canReloadVcc}
                  onChange={() =>
                    handleSwitchValueChanged("canReloadVcc", !values.canReloadVcc)
                  }
                  tooltipInfo={tooltipInfoHelper["canReloadVcc"]}
                />
              </CCol>
            }
          </CRow>

          <div>
            {resAlert && (
              <CAlert color={resAlert?.color} className="mt-2 mb-0">
                {resAlert?.message}
              </CAlert>
            )}
          </div>
        </CForm>
      </CModalBody>
      <CModalFooter>
        {formMode === "edit" && userData?.status === "Active" && (
          <CButton
            disabled={isSubmitting}
            color="danger"
            className="px-4"
            onClick={() => handleUpdateStatus(false)}
          >
            {!isSubmitting ? (
              "Disable User"
            ) : (
              <>
                <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
                Loading...
              </>
            )}
          </CButton>
        )}
        {formMode === "edit" && userData?.status === "Deactivated" && (
          <CButton
            disabled={isSubmitting}
            color="success"
            className="px-4"
            onClick={() => handleUpdateStatus(true)}
          >
            {!isSubmitting ? (
              "Enable User"
            ) : (
              <>
                <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
                Loading...
              </>
            )}
          </CButton>
        )}
        <CButton
          disabled={isSubmitting}
          color="primary"
          className="px-4"
          onClick={handleSubmit}
          type="submit"
        >
          {!isSubmitting ? (
            `${formMode === "edit" ? "Update" : "Create"}
            ${!isBulkUser ? "User" : "Users"}`
          ) : (
            <>
              <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
              Loading...
            </>
          )}
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default UserFormModal;
