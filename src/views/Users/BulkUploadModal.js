import { useRef } from "react";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CTooltip,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { CSVLink } from "react-csv";
import { CSVReader } from "react-papaparse";

const csvData = [
  ["FirstName", "LastName", "EmailAddress"],
  ["John", "Doe", "john.doe@test.com"],
  ["Jane", "Doe", "jane.doe@test.com"],
];

const tooltipInfoHelper = {
  bulkUpload:
    "Use the bulk uploader to upload groups of users who will all have the same permissions. Click 'Download Sample File' and fill in columns of the excel sheet with the list of username/email IDs. Once done, save the file and use the upload file button to upload and go to the next screen.",
};

const BulkUploadModal = (props) => {
  const { modalShow, handleHideModal, handlePostUpload } = props;

  const buttonUpload = useRef(null);

  // const [resAlert, setResAlert] = useState(null);

  // const inputFile = useRef(null);

  const handleOpenDialog = (e) => {
    // Note that the ref is set async, so it might be null at some point
    if (buttonUpload.current) {
      buttonUpload.current.open(e);
    }
  };

  const handleOnFileLoad = (data, file) => {
    console.log("---------------------------");
    console.log({ data });
    console.log("---------------------------");

    const isLabelIncluded = data?.[0]?.data?.length > 3 ? true : false;
    console.log({ isLabelIncluded });

    handleHideModal();
    handlePostUpload({ file, isLabelIncluded });
  };

  // const handleUploadCsv = () => {
  //   inputFile.current.click();
  // };

  // const handleFileSelected = (e) => {
  //   try {
  //     const file = e.target.files?.[0];
  //     if (file) {
  //       handleHideModal();
  //       handlePostUpload(file);
  //     }
  //   } catch (error) {
  //     setResAlert({
  //       color: "danger",
  //       message: error?.response?.data?.message || "Network error.",
  //     });
  //     console.error(error);
  //   }
  // };

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Bulk Upload</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <div className="d-flex align-items-center">
          <p className="mb-0">
            Upload a list of names and email address for users who have same
            permissions.{" "}
          </p>
          <CTooltip
            placement="bottom-end"
            content={tooltipInfoHelper["bulkUpload"]}
          >
            <CIcon className="ml-3" name="cil-info-circle" height={18} />
          </CTooltip>
        </div>
        {/* <div>
          {resAlert && (
            <CAlert color={resAlert?.color} className="mt-2 mb-0">
              {resAlert?.message}
            </CAlert>
          )}
        </div> */}
      </CModalBody>
      <CModalFooter>
        <CSVLink filename="sample-file.csv" data={csvData}>
          <CButton color="light" className="mx-1 w-50 w-md-unset">
            Download Sample File
          </CButton>
        </CSVLink>
        <CSVReader
          ref={buttonUpload}
          onFileLoad={handleOnFileLoad}
          noClick
          noDrag
          noProgressBar
          style={null}
        >
          {() => (
            <CButton
              onClick={handleOpenDialog}
              color="primary"
              className="mx-1 w-50 w-md-unset"
            >
              Upload File
            </CButton>
          )}
        </CSVReader>
        {/* <div>
          <CButton
            onClick={handleUploadCsv}
            color="primary"
            className="mx-1 w-50 w-md-unset"
          >
            Upload File
          </CButton>
          <input
            accept=".csv"
            ref={inputFile}
            className="d-none"
            type="file"
            onChange={handleFileSelected}
          />
        </div> */}
      </CModalFooter>
    </CModal>
  );
};

export default BulkUploadModal;
