import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CRow,
  CCol,
  CForm,
  CFormGroup,
  CLabel,
  CInput,
  CSwitch,
  CInvalidFeedback,
  CButton,
  CAlert,
  CSpinner,
  CTooltip,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useFormik } from "formik";
import * as Yup from "yup";
import Select from "react-select";
import NumberFormat from "react-number-format";
import csc from "country-state-city";
import { isEmail, mapOptLabels, nonStateUS } from "src/helper";
import { salPostSales } from "src/services";
import { isEmpty } from "lodash";

const tooltipInfoHelper = {
  orderNumber:
    "Client provided transaction ID associated with the order. Unique to you.",
  customerId:
    "This value acts as a secondary identifier in conjunction with OrderNumber. The value is searchable and reportable in the ConnexPay portal.",
  expectedPayments:
    "The number of outbound payments that will be made to suppliers. If paying a single supplier the value is 1, if paying two suppliers the value is 2, etc.",
};

const initialValues = {
  Amount: "",
  OrderNumber: "",
  CustomerId: "",
  SendReceipt: false,
  LabelIds: "",
  ConnexPayTransaction: {
    ExpectedPayments: "",
  },
  Customer: {
    FirstName: "",
    LastName: "",
    Address1: "",
    Address2: "",
    State: "",
    City: "",
    Country: "",
    Zip: "",
    Email: "",
    Phone: "",
  },
};

const validationSchema = (values) => {
  return Yup.object().shape({
    Amount: Yup.number()
      .min(0.01, "Must be more than $ 0.01")
      .max(999999.99, "Must be less than $ 999,999.99")
      .required("Amount is required"),
    OrderNumber: Yup.string()
      .max(50, "Maximum 50 characters")
      .matches(
        /^[ A-Za-z0-9'|.-]*$/,
        "Only letters & numbers, -, ., | and ' are allowed"
      ),
    CustomerId: Yup.string()
      .max(50, "Maximum 50 characters")
      .matches(
        /^[ A-Za-z0-9'|.-]*$/,
        "Only letters & numbers, -, ., | and ' are allowed"
      ),
    ConnexPayTransaction: Yup.object().shape({
      ExpectedPayments: Yup.number()
        .min(1, "Must be more than 0")
        .max(99, "Must be less than 100")
        .required("Expected Payments is required"),
    }),
    Customer: Yup.object().shape({
      FirstName: Yup.string()
        .max(50, "Maximum 50 characters")
        .matches(/^[ A-Za-z]*$/, "Only letters & whitespace are allowed"),
      LastName: Yup.string()
        .max(50, "Maximum 50 characters")
        .matches(/^[ A-Za-z]*$/, "Only letters & whitespace are allowed"),
      Address1: Yup.string()
        .max(50, "Maximum 50 characters")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
      Address2: Yup.string()
        .max(50, "Maximum 50 characters")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
      Email: Yup.string().email("Email format is invalid"),
      Zip: Yup.string()
        .min(2, "Minimum 2 characters")
        .max(15, "Maximum 15 characters")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
      Phone: Yup.string().when("Country", {
        is: "" || "US",
        then: Yup.string()
          .max(10, "Invalid phone number")
          .matches(/^[2-9][0-9]*$/, "Number format is invalid"),
        otherwise: Yup.string()
          .max(15, "Invalid phone number")
          .matches(/^\+?[0-9]*$/, "Number format is invalid"),
      }),
    }),
  });
};

const CashSale = (props) => {
  const { modalShow, handleHideModal, handlePostSubmit } = props;
  const labelMerchantData = JSON.parse(localStorage.getItem("labelMarchantData"));
  const dispatch = useDispatch();
  // const globalLabel = useSelector((state) => state.globalLabel);
  const toaster = useSelector((state) => state.toaster);
  const userData = useSelector((state) => state.userData);
  const merchantSettings = userData?.merchantSettings;

  const [optLabels, setOptLabels] = useState([]);
  const [optCountries, setOptCountries] = useState([]);
  const [optStates, setOptStates] = useState([]);
  const [optCities, setOptCities] = useState([]);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  useEffect(() => {
    if (!isEmpty(userData)) {
      const arrOptLabels = mapOptLabels(userData?.labelHierarchy);
      setOptLabels(arrOptLabels);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  useEffect(() => {
    const getOptCountries = () => {
      const getData = csc.getAllCountries();
      const populateData = getData.map((val) => ({
        value: val.isoCode,
        label: `${val.name}`,
      }));
      setOptCountries(populateData);
    };
    getOptCountries();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const getOptStates = (countryCode) => {
    const getData = csc
      .getStatesOfCountry(countryCode)
      ?.filter((val) => !nonStateUS?.includes(val?.name));
    const populateData = getData.map((val) => ({
      value: val.isoCode,
      label: `${val.name}`,
    }));
    setOptStates(populateData);
  };

  const getOptCities = (countryCode, stateCode) => {
    const getData = csc.getCitiesOfState(countryCode, stateCode);
    const populateData = getData.map((val) => ({
      value: val.name,
      label: `${val.name}`,
    }));
    setOptCities(populateData);
  };

  const handleSelectCountry = (selected) => {
    setFieldValue("Customer.Country", selected.value);
    setFieldValue("Customer.State", "");
    setFieldValue("Customer.City", "");
    setFieldValue("Customer.Phone", "");
    getOptStates(selected.value);
  };

  const handleSelectState = (selected) => {
    setFieldValue("Customer.State", selected.value);
    setFieldValue("Customer.City", "");

    getOptCities(values.Customer?.Country, selected.value);
  };

  const handleSelectCity = (selected) => {
    setFieldValue("Customer.City", selected.value);
  };

  const handleSubmitForm = async (values) => {
    try {
      setIsSubmitting(true);
      setResAlert(null);
      const payload = {
        ...values,
        Customer: {
          ...values?.Customer,
          FirstName: values?.Customer?.FirstName || null,
          LastName: values?.Customer?.LastName || null,
        },
        DeviceGuid: labelMerchantData?.deviceGuid,
        TenderType: "Cash",
        LabelIds: merchantSettings?.showLabels ? [values?.LabelIds] : undefined,
      };
      const res = await salPostSales(payload);
      if (res) {
        resetForm();
        setOptStates([]);
        setOptCities([]);
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "Request has been submitted.",
        };
        dispatch({ type: "set", toaster: [...toaster, toasterConfig], shouldReloadSalesAndPurchasesTable: true, });
        handlePostSubmit(
          "cashSale",
          res?.connexPayTransaction,
          values.Customer,
          res?.customerReceipt,
          "cashSale",
          { guid: res?.guid }
        );
      }
    } catch (error) {
      setResAlert({
        color: "danger",
        message: error?.response?.data?.message || "Network error.",
      });
    } finally {
      setIsSubmitting(false);
    }
  };

  const validateForm = (values) => {
    let errors = {};
    if (merchantSettings?.showLabels && !values?.LabelIds) {
      errors = {
        LabelIds: "Label is required",
      };
    }
    return errors;
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitForm,
    validate: validateForm,
  });
  const {
    values,
    touched,
    errors,
    setFieldValue,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
  } = formik;

  useEffect(() => {
    setResAlert(null);
    // setFieldValue("LabelIds", globalLabel?.[0] || "");

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  const renderEmailReceipt = () => {
    return (
      <div className="email-receipt">
        <p>Send Email Receipt?</p>
        <CSwitch
          className="ml-2"
          color="secondary"
          checked={values.SendReceipt}
          onChange={() => setFieldValue("SendReceipt", !values.SendReceipt)}
          // disabled={!isEmail(values.Customer?.Email)}
          shape="pill"
        />
      </div>
    );
  };

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
      size="lg"
    >
      <CModalHeader closeButton>
        <CModalTitle>Enter Sales Information</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CForm>
          <CRow>
            <CCol md="6">
              <CFormGroup>
                <CLabel htmlFor="Amount">
                  Amount<span className="label-required">*</span>
                </CLabel>
                <NumberFormat
                  name="Amount"
                  value={values.Amount}
                  onValueChange={(val) =>
                    setFieldValue("Amount", val.floatValue)
                  }
                  prefix="$"
                  thousandSeparator
                  customInput={CInput}
                  placeholder="Enter Amount"
                  onBlur={handleBlur}
                  invalid={touched.Amount && !!errors.Amount}
                />
                <CInvalidFeedback>{errors.Amount}</CInvalidFeedback>
              </CFormGroup>
            </CCol>
            <CCol md="6">
              <CFormGroup>
                <div className="d-flex justify-content-between align-items-center">
                  <CLabel htmlFor="ExpectedPayments">
                    Expected Payments<span className="label-required">*</span>
                  </CLabel>
                  <CTooltip
                    placement="bottom-end"
                    content={tooltipInfoHelper["expectedPayments"]}
                  >
                    <CIcon
                      tabIndex="-1"
                      className="mr-3"
                      name="cil-info-circle"
                      height={14}
                    />
                  </CTooltip>
                </div>
                <NumberFormat
                  name="ConnexPayTransaction.ExpectedPayments"
                  customInput={CInput}
                  value={values.ConnexPayTransaction?.ExpectedPayments}
                  placeholder="Enter Expected Payments"
                  onValueChange={(val) =>
                    setFieldValue(
                      "ConnexPayTransaction.ExpectedPayments",
                      val.floatValue
                    )
                  }
                  onBlur={handleBlur}
                  invalid={
                    touched.ConnexPayTransaction?.ExpectedPayments &&
                    !!errors.ConnexPayTransaction?.ExpectedPayments
                  }
                />
                <CInvalidFeedback>
                  {errors.ConnexPayTransaction?.ExpectedPayments}
                </CInvalidFeedback>
              </CFormGroup>
            </CCol>

            <CCol md="6">
              <CFormGroup>
                <div className="d-flex justify-content-between align-items-center">
                  <CLabel htmlFor="OrderNumber">Order Number</CLabel>
                  <CTooltip
                    placement="bottom-end"
                    content={tooltipInfoHelper["orderNumber"]}
                  >
                    <CIcon
                      tabIndex="-1"
                      className="mr-3"
                      name="cil-info-circle"
                      height={14}
                    />
                  </CTooltip>
                </div>
                <CInput
                  autoComplete="none"
                  name="OrderNumber"
                  type="text"
                  value={values.OrderNumber}
                  placeholder="Enter Order Number"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={touched.OrderNumber && !!errors.OrderNumber}
                />
                <CInvalidFeedback>{errors.OrderNumber}</CInvalidFeedback>
              </CFormGroup>
            </CCol>
            <CCol md="6">
              <CFormGroup>
                <div className="d-flex justify-content-between align-items-center">
                  <CLabel htmlFor="CustomerId">Customer ID</CLabel>
                  <CTooltip
                    placement="bottom-end"
                    content={tooltipInfoHelper["customerId"]}
                  >
                    <CIcon
                      tabIndex="-1"
                      className="mr-3"
                      name="cil-info-circle"
                      height={14}
                    />
                  </CTooltip>
                </div>
                <CInput
                  autoComplete="none"
                  name="CustomerId"
                  type="text"
                  value={values.CustomerId}
                  placeholder="Enter Customer ID"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={touched.CustomerId && !!errors.CustomerId}
                />
                <CInvalidFeedback>{errors.CustomerId}</CInvalidFeedback>
              </CFormGroup>
            </CCol>

            <CCol md="6">
              <CFormGroup>
                <CLabel htmlFor="FirstName">First Name</CLabel>
                <CInput
                  autoComplete="none"
                  name="Customer.FirstName"
                  type="text"
                  value={values.Customer?.FirstName}
                  placeholder="Enter First Name"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={
                    touched.Customer?.FirstName && !!errors.Customer?.FirstName
                  }
                />
                <CInvalidFeedback>
                  {errors.Customer?.FirstName}
                </CInvalidFeedback>
              </CFormGroup>
            </CCol>
            <CCol md="6">
              <CFormGroup>
                <CLabel htmlFor="LastName">Last Name</CLabel>
                <CInput
                  autoComplete="none"
                  name="Customer.LastName"
                  type="text"
                  value={values.Customer?.LastName}
                  placeholder="Enter Last Name"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={
                    touched.Customer?.LastName && !!errors.Customer?.LastName
                  }
                />
                <CInvalidFeedback>{errors.Customer?.LastName}</CInvalidFeedback>
              </CFormGroup>
            </CCol>

            <CCol md="6">
              <CFormGroup>
                <CLabel htmlFor="Address1">Address Line 1</CLabel>
                <CInput
                  autoComplete="none"
                  name="Customer.Address1"
                  type="text"
                  value={values.Customer?.Address1}
                  placeholder="Enter Address Line 1"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={
                    touched.Customer?.Address1 && !!errors.Customer?.Address1
                  }
                />
                <CInvalidFeedback>{errors.Customer?.Address1}</CInvalidFeedback>
              </CFormGroup>
            </CCol>
            <CCol md="6">
              <CFormGroup>
                <CLabel htmlFor="Address2">Address Line 2</CLabel>
                <CInput
                  autoComplete="none"
                  name="Customer.Address2"
                  type="text"
                  value={values.Customer?.Address2}
                  placeholder="Enter Address Line 2"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={
                    touched.Customer?.Address2 && !!errors.Customer?.Address2
                  }
                />
                <CInvalidFeedback>{errors.Customer?.Address2}</CInvalidFeedback>
              </CFormGroup>
            </CCol>

            <CCol md="6">
              <CFormGroup>
                <CLabel htmlFor="Country">Country</CLabel>
                <Select
                  classNamePrefix="default-select"
                  name="Customer.Country"
                  value={
                    optCountries.find(
                      (val) => val.value === values.Customer?.Country
                    ) || ""
                  }
                  options={optCountries}
                  onChange={handleSelectCountry}
                  placeholder="Select Country"
                  components={{
                    IndicatorSeparator: () => null,
                  }}
                />
                <CInvalidFeedback>{errors.Customer?.Country}</CInvalidFeedback>
              </CFormGroup>
            </CCol>
            <CCol md="6">
              <CFormGroup>
                <CLabel htmlFor="State">State</CLabel>
                <Select
                  classNamePrefix="default-select"
                  name="country"
                  value={
                    optStates.find(
                      (val) => val.value === values.Customer?.State
                    ) || ""
                  }
                  options={optStates}
                  onChange={handleSelectState}
                  placeholder="Select State"
                  components={{
                    IndicatorSeparator: () => null,
                  }}
                />
                <CInvalidFeedback>{errors.Customer?.State}</CInvalidFeedback>
              </CFormGroup>
            </CCol>

            <CCol md="6">
              <CFormGroup>
                <CLabel htmlFor="City">City</CLabel>
                <Select
                  classNamePrefix="default-select"
                  name="city"
                  value={
                    optCities.find(
                      (val) => val.value === values.Customer?.City
                    ) || ""
                  }
                  options={optCities}
                  onChange={handleSelectCity}
                  placeholder="Select City"
                  components={{
                    IndicatorSeparator: () => null,
                  }}
                />
                <CInvalidFeedback>{errors.Customer?.City}</CInvalidFeedback>
              </CFormGroup>
            </CCol>
            <CCol md="6">
              <CFormGroup>
                <CLabel htmlFor="Zip">Zip Code</CLabel>
                <CInput
                  autoComplete="none"
                  name="Customer.Zip"
                  type="text"
                  value={values.Customer?.Zip}
                  placeholder="Enter Zip Code"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={touched.Customer?.Zip && !!errors.Customer?.Zip}
                />
                <CInvalidFeedback>{errors.Customer?.Zip}</CInvalidFeedback>
              </CFormGroup>
            </CCol>

            <CCol md="6">
              <CFormGroup>
                <CLabel htmlFor="Email">Email Address</CLabel>
                <CInput
                  autoComplete="none"
                  name="Customer.Email"
                  type="text"
                  value={values.Customer?.Email}
                  placeholder="Enter Email Address"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  valid={
                    touched.Customer?.Email && isEmail(values.Customer?.Email)
                  }
                  invalid={touched.Customer?.Email && !!errors.Customer?.Email}
                />
                <CInvalidFeedback>{errors.Customer?.Email}</CInvalidFeedback>
                <div className="d-block d-md-none mt-2">
                  {renderEmailReceipt()}
                </div>
              </CFormGroup>
            </CCol>
            <CCol md="6">
              <CFormGroup>
                <CLabel htmlFor="Phone">Phone Number</CLabel>
                {!values.Customer?.Country ||
                (values.Customer?.Country &&
                  values.Customer?.Country === "US") ? (
                  <NumberFormat
                    id="Phone"
                    name="Customer.Phone"
                    customInput={CInput}
                    value={values.Customer?.Phone}
                    format="(###) ###-####"
                    placeholder="Enter Phone Number"
                    onValueChange={(val) =>
                      setFieldValue("Customer.Phone", val.value)
                    }
                    mask={"_"}
                    onBlur={handleBlur}
                    invalid={
                      touched.Customer?.Phone && !!errors.Customer?.Phone
                    }
                  />
                ) : (
                  <CInput
                    id="Phone"
                    name="Customer.Phone"
                    type="text"
                    value={values.Customer?.Phone}
                    placeholder="Enter Phone Number"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={
                      touched.Customer?.Phone && !!errors.Customer?.Phone
                    }
                  />
                )}
                <CInvalidFeedback>{errors.Customer?.Phone}</CInvalidFeedback>
              </CFormGroup>
            </CCol>
            <CCol md="6">
              {merchantSettings?.showLabels && (
                <CFormGroup>
                  <CLabel htmlFor="Label">
                    Label<span className="label-required">*</span>
                  </CLabel>
                  <Select
                    className={
                      touched.LabelIds && !!errors.LabelIds ? "is-invalid" : ""
                    }
                    classNamePrefix="default-select"
                    name="Label"
                    value={
                      optLabels.find((val) => val.value === values.LabelIds) ||
                      ""
                    }
                    options={optLabels}
                    placeholder="Select Label"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    onChange={(selected) => {
                      setFieldValue("LabelIds", selected.value);
                    }}
                    isSearchable={true}
                  />
                  <CInvalidFeedback>{errors.LabelIds}</CInvalidFeedback>
                </CFormGroup>
              )}
            </CCol>
          </CRow>

          <div className="mt-2 d-flex justify-content-between">
            <div className="d-none d-md-flex">{renderEmailReceipt()}</div>
            <CButton
              disabled={isSubmitting}
              color="primary"
              className="px-4 py-2"
              onClick={handleSubmit}
              type="submit"
            >
              {!isSubmitting ? (
                "Process Cash Sale"
              ) : (
                <>
                  <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
                  Loading...
                </>
              )}
            </CButton>
          </div>
          <div>
            {resAlert && (
              <CAlert color={resAlert?.color} className="mt-3">
                {resAlert?.message}
              </CAlert>
            )}
          </div>
        </CForm>
      </CModalBody>
    </CModal>
  );
};

export default CashSale;
