import { useState, useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CTooltip,
  CAlert,
  CSpinner,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { CSVLink } from "react-csv";
import { CSVReader } from "react-papaparse";
import { salPostSalesBatch } from "src/services";

const csvData = [
  [
    "715d0320-836f-49b8-b820-40ddc82868e3",
    "3.03",
    "USD",
    "Cash",
    "00011",
    "UPLOAD-TEST-003",
    "CustomerId01",
    "1",
    "Stmt Descrip",
    "",
    "Joe Cardholder",
    "999",
    "2101",
    "Joe",
    "Cardholder",
    "",
    "Alpharetta",
    "GA",
    "US",
    "joecard@holder.com",
    "123 Street",
    "Suite 123",
    "30092",
    "66FC90B0-E7AC-48C9-834B-030D5D358B6B",
    "Joe",
    "Cardholder",
    "4",
    "3.03",
    "2021-10-15",
    "2020-12-15",
    "01",
    "DL",
  ],
  [
    "715d0320-836f-49b8-b820-40ddc82868e3",
    "2.87",
    "USD",
    "Cash",
    "00021",
    "UPLOAD-TEST-004",
    "CustomerId01",
    "1",
    "Stmt Descrip",
    "",
    "Joe Cardholder",
    "999",
    "2101",
    "Joe",
    "Cardholder",
    "",
    "Alpharetta",
    "GA",
    "US",
    "joecard@holder.com",
    "123 Street",
    "Suite 123",
    "30092",
    "66FC90B0-E7AC-48C9-834B-030D5D358B6B",
    "Joe",
    "Cardholder",
    "4",
    "2.87",
    "2021-10-15",
    "2020-12-15",
    "01",
    "DL",
  ],
];

const tooltipInfoHelper = {
  bulkUpload: "Sales File Upload documentations.",
};

const FileUpload = (props) => {
  const { modalShow, handleHideModal } = props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  const buttonUpload = useRef(null);

  const handleOpenDialog = (e) => {
    // Note that the ref is set async, so it might be null at some point
    if (buttonUpload.current) {
      buttonUpload.current.open(e);
    }
  };

  const handleOnFileLoad = (data, file) => {
    console.log("---------------------------");
    console.log({ data });
    console.log("---------------------------");

    handleSubmit(data, file?.name);
  };

  const handleSubmit = async (fileData, fileName) => {
    try {
      setIsSubmitting(true);
      setResAlert(null);
      const arrFileData = fileData?.map((val) => {
        const data = val?.data;
        return {
          Sale: {
            DeviceGuid: data?.[0],
            Amount: data?.[1],
            CurrencyCode: data?.[2],
            TenderType: data?.[3],
            SequenceNumber: data?.[4],
            OrderNumber: data?.[5],
            CustomerID: data?.[6],
            StatementDescription: data?.[8],
            ConnexPayTransaction: {
              ExpectedPayments: data?.[7],
            },
            Customer:
              data?.[3] === "Cash"
                ? {
                    FirstName: data?.[13],
                    LastName: data?.[14],
                    Phone: data?.[15],
                    City: data?.[16],
                    State: data?.[17],
                    Country: data?.[18],
                    Email: data?.[19],
                    Address1: data?.[20],
                    Address2: data?.[21],
                    Zip: data?.[22],
                  }
                : null,
          },
          IssueCard: {
            MerchantGuid: data?.[23],
            FirstName: data?.[24],
            LastName: data?.[25],
            UsageLimit: data?.[26],
            AmountLimit: data?.[27],
            ExpirationDate: data?.[28],
            TerminateDate: data?.[29],
            PurchaseType: data?.[30],
            SupplierId: data?.[31],
          },
          Card:
            data?.[3] === "Credit"
              ? {
                  CardNumber: data?.[9],
                  CardHolderName: data?.[10],
                  Cvv2: data?.[11],
                  ExpirationDate: data?.[12],
                  Customer: {
                    FirstName: data?.[13],
                    LastName: data?.[14],
                    Phone: data?.[15],
                    City: data?.[16],
                    State: data?.[17],
                    Country: data?.[18],
                    Email: data?.[19],
                    Address1: data?.[20],
                    Address2: data?.[21],
                    Zip: data?.[22],
                  },
                }
              : null,
        };
      });
      const payload = {
        FileName: fileName,
        Sales: arrFileData,
      };
      console.log({ payload });
      const res = await salPostSalesBatch(payload);
      if (res) {
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "File uploaded successfully.",
        };
        dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
        handleHideModal();
      }
    } catch (error) {
      console.error(error);
      setResAlert({
        color: "danger",
        message:
          error?.response?.data?.message ||
          error?.response?.data ||
          "Request failed. Please check the file and upload again.",
      });
    } finally {
      setIsSubmitting(false);
    }
  };

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>File Upload</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <div className="d-flex align-items-center">
          <p className="mb-0">
            Upload comma-delimited sales files to create sales transactions.
          </p>
          <a
            href="https://docs.connexpay.com/docs/sales-file-upload"
            target="_blank"
            rel="noreferrer"
          >
            <CTooltip
              placement="bottom-end"
              content={tooltipInfoHelper["bulkUpload"]}
            >
              <CIcon className="ml-3" name="cil-info-circle" height={18} />
            </CTooltip>
          </a>
        </div>
        <div>
          {resAlert && (
            <CAlert color={resAlert?.color} className="mt-2 mb-0">
              {resAlert?.message}
            </CAlert>
          )}
        </div>
      </CModalBody>
      <CModalFooter>
        <CSVLink filename="Sample_CedarLake.csv" data={csvData}>
          <CButton color="light" className="mx-1 w-50 w-md-unset">
            Download Sample File
          </CButton>
        </CSVLink>
        <CSVReader
          ref={buttonUpload}
          onFileLoad={handleOnFileLoad}
          noClick
          noDrag
          noProgressBar
          style={null}
        >
          {() => (
            <CButton
              disabled={isSubmitting}
              onClick={handleOpenDialog}
              color="primary"
              className="mx-1 w-50 w-md-unset"
            >
              {!isSubmitting ? (
                "Upload File"
              ) : (
                <>
                  <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
                  Loading...
                </>
              )}
            </CButton>
          )}
        </CSVReader>
      </CModalFooter>
    </CModal>
  );
};

export default FileUpload;
