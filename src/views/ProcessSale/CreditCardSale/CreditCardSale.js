import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CForm,
  CButton,
  CAlert,
  CSpinner,
} from "@coreui/react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { convertDateYYMM } from "src/helper";
import { salPostSales } from "src/services";

import SalesForm from "./SalesForm";
import CustomerForm from "./CustomerForm";
import RiskForm from "./RiskForm";
import FlightForm from "./FlightForm";

const initialValues = {
  Amount: "",
  OrderNumber: "",
  CustomerId: "",
  SendReceipt: false,
  isDelaySaleActivation: false,
  LabelIds: "",
  StatementDescription: "",
  ConnexPayTransaction: {
    ExpectedPayments: "",
  },
  RiskData: {
    SellerId: "",
    Email: "",
    Gender: "",
    DateOfBirth: "",
    ProductType: "",
    ProductItem: "",
    ProductDesc: "",
    BillingPhoneNumber: "",
    BillingAddress1: "",
    BillingAddress2: "",
    BillingCountryCode: "",
    BillingState: "",
    BillingCity: "",
    BillingPostalCode: "",
    FlightData: {
      Airline: "",
      DepartureAirport: "",
      DepartureDate: "",
      DestinationAirport: "",
      HoursToDeparture: "",
      JourneyType: "",
      Route: "",
      RouteByCountry: "",
    },
    FlightPassenger: [],
  },
  Card: {
    CardNumber: "",
    CardHolderName: "",
    Cvv2: "",
    ExpirationDate: "",
    Customer: {
      FirstName: "",
      LastName: "",
      Email: "",
      Address1: "",
      Address2: "",
      State: "",
      City: "",
      Country: "",
      Zip: "",
      Phone: "",
      SSN4: "",
    },
  },
};

const validationSchema = () => {
  return Yup.object().shape({
    Amount: Yup.number()
      .min(0.5, "Must be at least $0.50")
      .max(999999.99, "Must be less than $999,999.99")
      .required("Amount is required"),
    OrderNumber: Yup.string()
      .max(50, "Maximum 50 characters")
      .matches(
        /^[ A-Za-z0-9'|.-]*$/,
        "Only letters & numbers, -, ., | and ' are allowed"
      ),
    CustomerId: Yup.string()
      .max(50, "Maximum 50 characters")
      .matches(
        /^[ A-Za-z0-9'|.-]*$/,
        "Only letters & numbers, -, ., | and ' are allowed"
      ),
    StatementDescription: Yup.string()
      .max(25, "Maximum 25 characters")
      .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
    ConnexPayTransaction: Yup.object().shape({
      ExpectedPayments: Yup.number()
        .min(0, "Must be at least 0")
        .max(99, "Must be less than 100")
        .required("Expected Payments is required"),
    }),
    RiskData: Yup.object().shape({
      SellerId: Yup.string()
        .max(32, "Maximum 32 characters")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
      Email: Yup.string().email("Email format is invalid"),
      ProductType: Yup.string()
        .max(50, "Maximum 50 characters")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed")
        .required("Product Type is required"),
      ProductItem: Yup.string()
        .max(50, "Maximum 50 characters")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed")
        .required("Product Item is required"),
      ProductDesc: Yup.string()
        .max(50, "Maximum 50 characters")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
      BillingPhoneNumber: Yup.string().when("BillingCountryCode", {
        is: "" || "US",
        then: Yup.string()
          .max(10, "Invalid phone number")
          .matches(/^[2-9][0-9]*$/, "Number format is invalid"),
        otherwise: Yup.string()
          .max(15, "Invalid phone number")
          .matches(/^\+?[0-9]*$/, "Number format is invalid"),
      }),
      BillingAddress1: Yup.string()
        .max(256, "Maximum 256 characters")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
      BillingAddress2: Yup.string()
        .max(256, "Maximum 256 characters")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
      BillingPostalCode: Yup.string().matches(
        /^[ A-Za-z0-9]*$/,
        "Only letters & numbers are allowed"
      ),
      DateOfBirth: Yup.date().max(
        new Date(),
        "Date of Birth must be less than today"
      ),
      FlightData: Yup.object().shape({
        Airline: Yup.string()
          .max(50, "Maximum 50 characters")
          .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
        DepartureAirport: Yup.string()
          .max(50, "Maximum 50 characters")
          .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
        DestinationAirport: Yup.string()
          .max(50, "Maximum 50 characters")
          .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
        HoursToDeparture: Yup.string()
          .max(2, "Maximum 2 digits")
          .matches(/^[ 0-9]*$/, "Only numbers are allowed"),
        Route: Yup.string()
          .max(50, "Maximum 50 characters")
          .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
        RouteByCountry: Yup.string()
          .max(50, "Maximum 50 characters")
          .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
        DepartureDate: Yup.date().min(
          new Date(),
          "Departure Date must be equal or greater than today"
        ),
      }),
    }),
    Card: Yup.object().shape({
      CardNumber: Yup.string()
        .max(16, "Minimum 16 characters")
        .max(16, "Maximum 16 characters")
        .required("Card Number is required"),
      ExpirationDate: Yup.string()
        .min(5, "Expiration Date format is invalid")
        .max(5, "Expiration Date format is invalid")
        .required("Expiration Date is required"),
      Cvv2: Yup.string()
        .min(3, "Minimum 3 digits")
        .max(4, "Maximum 4 digits")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
      CardHolderName: Yup.string()
        .max(64, "Maximum 64 characters")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
      Customer: Yup.object().shape({
        FirstName: Yup.string()
          .min(2, "Minimum 2 characters")
          .max(30, "Maximum 30 characters")
          .matches(/^[ A-Za-z]*$/, "Only letters & whitespace are allowed"),
        LastName: Yup.string()
          .min(2, "Minimum 2 characters")
          .max(30, "Maximum 30 characters")
          .matches(/^[ A-Za-z]*$/, "Only letters & whitespace are allowed"),
        Address1: Yup.string()
          .max(50, "Maximum 50 characters")
          .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
        Address2: Yup.string()
          .max(50, "Maximum 50 characters")
          .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
        Zip: Yup.string()
          .min(2, "Minimum 2 characters")
          .max(15, "Maximum 15 characters")
          .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
        Phone: Yup.string().when("Country", {
          is: "" || "US",
          then: Yup.string()
            .max(10, "Invalid phone number")
            .matches(/^[2-9][0-9]*$/, "Number format is invalid"),
          otherwise: Yup.string()
            .max(15, "Invalid phone number")
            .matches(/^\+?[0-9]*$/, "Number format is invalid"),
        }),
        SSN4: Yup.string()
          .min(4, "Must be 4 digits")
          .max(4, "Must be  4 digits")
          .matches(/^[0-9]*$/, "Only numbers are allowed"),
      }),
    }),
  });
};

const CreditCardSale = (props) => {
  const { modalShow, handleHideModal, handlePostSubmit } = props;
  const labelMerchantData = JSON.parse(localStorage.getItem("labelMarchantData"));
  const dispatch = useDispatch();
  // const globalLabel = useSelector((state) => state.globalLabel);
  const toaster = useSelector((state) => state.toaster);
  const userData = useSelector((state) => state.userData);
  const merchantSettings = userData?.merchantSettings;

  const [activeForm, setActiveForm] = useState("sales");
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  const handleSubmitForm = async (values) => {
    try {
      setIsSubmitting(true);
      setResAlert(null);
      const payload = {
        ...values,
        LabelIds: merchantSettings?.showLabels ? [values?.LabelIds] : undefined,
        RiskData: {
          ...values?.RiskData,
          BillingCountryCode: values?.RiskData?.BillingCountryCode || undefined,
        },
        Card: {
          ...values?.Card,
          ExpirationDate: convertDateYYMM(values?.Card?.ExpirationDate),
          Customer: {
            ...values?.Card?.Customer,
            FirstName: values?.Card?.Customer?.FirstName || null,
            LastName: values?.Card?.Customer?.LastName || null,
          },
        },
        DeviceGuid: labelMerchantData?.deviceGuid,
        TenderType: "Credit",
      };

      const res = await salPostSales(payload);
      if (res) {
        if (res?.status?.includes("Declined")) {
          const toasterConfig = {
            show: true,
            type: "danger",
            title: "Failed",
            body: "Transaction Declined",
          };
          dispatch({ type: "set", toaster: [...toaster, toasterConfig], shouldReloadSalesAndPurchasesTable: true, });
          return;
        }
        resetForm();
        setActiveForm("sales");
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "Request has been submitted.",
        };
        dispatch({ type: "set", toaster: [...toaster, toasterConfig], shouldReloadSalesAndPurchasesTable: true, });
        handlePostSubmit(
          "creditCardSale",
          res?.connexPayTransaction,
          values.Card?.Customer,
          res?.customerReceipt,
          "creditSale",
          { guid: res?.guid }
        );
      }
    } catch (error) {
      setResAlert({
        color: "danger",
        message: error?.response?.data?.message || "Network error.",
      });
    } finally {
      setIsSubmitting(false);
    }
  };

  const validateForm = (values) => {
    let errors = {};
    if (merchantSettings?.showLabels && !values?.LabelIds) {
      errors = {
        LabelIds: "Label is required",
      };
    }

    return errors;
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitForm,
    validate: validateForm,
  });
  const { handleSubmit, resetForm } = formik;

  useEffect(() => {
    setResAlert(null);
    // setFieldValue("LabelIds", globalLabel?.[0] || "");

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  const handleBackForm = () => {
    if (activeForm === "flight") {
      setActiveForm("customer");
    }
    if (activeForm === "customer") {
      setActiveForm("sales");
    }
    if (activeForm === "risk") {
      setActiveForm("customer");
    }
  };

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
      size="lg"
    >
      <CModalHeader closeButton>
        <CModalTitle>{`Enter ${activeForm} Information`}</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CForm>
          <div>
            {activeForm === "sales" && <SalesForm formik={formik} />}
            {activeForm === "customer" && <CustomerForm formik={formik} />}
            {activeForm === "risk" && <RiskForm formik={formik} />}
            {activeForm === "flight" && <FlightForm formik={formik} />}
          </div>

          {activeForm === "customer" && (
            <CButton
              color="primary"
              className="d-sm-none px-4 py-2 w-100"
              onClick={() => setActiveForm("flight")}
              variant="outline"
            >
              Add Flight Data
            </CButton>
          )}

          <div className="mt-2 d-flex justify-content-between">
            <div>
              {activeForm === "sales" && (
                <CButton
                  color="primary"
                  className="px-4 py-2"
                  onClick={() => setActiveForm("customer")}
                  variant="outline"
                >
                  Add Customer Detail
                </CButton>
              )}
              <div className="d-flex">
                {(activeForm === "customer" || activeForm === "flight") && (
                  <CButton
                    color="primary"
                    className="d-none d-sm-block px-4 py-2 mx-1"
                    onClick={() => setActiveForm("risk")}
                    variant="outline"
                  >
                    Add Risk Data
                  </CButton>
                )}
                {(activeForm === "customer" || activeForm === "risk") &&
                  merchantSettings?.showFlightFormElements && (
                    <CButton
                      color="primary"
                      className="d-none d-sm-block px-4 py-2 mx-1"
                      onClick={() => setActiveForm("flight")}
                      variant="outline"
                    >
                      Add Flight Data
                    </CButton>
                  )}
              </div>
            </div>
            <div className="d-flex">
              {activeForm !== "sales" && (
                <CButton
                  disabled={isSubmitting}
                  color="light"
                  className="px-4 py-2 mx-2"
                  onClick={handleBackForm}
                >
                  Back
                </CButton>
              )}
              <CButton
                disabled={isSubmitting}
                color="primary"
                className="px-4 py-2"
                onClick={handleSubmit}
                type="submit"
              >
                {!isSubmitting ? (
                  "Process Credit Sale"
                ) : (
                  <>
                    <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
                    Loading...
                  </>
                )}
              </CButton>
            </div>
          </div>
          <div>
            {resAlert && (
              <CAlert color={resAlert?.color} className="mt-3">
                {resAlert?.message}
              </CAlert>
            )}
          </div>
        </CForm>
      </CModalBody>
    </CModal>
  );
};

export default CreditCardSale;
