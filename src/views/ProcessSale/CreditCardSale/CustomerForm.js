import { useState, useEffect } from "react";
import {
  CRow,
  CCol,
  CFormGroup,
  CLabel,
  CInput,
  CInvalidFeedback,
  CTooltip,
} from "@coreui/react";
import Select from "react-select";
import CIcon from "@coreui/icons-react";
import NumberFormat from "react-number-format";
import csc from "country-state-city";
import { nonStateUS } from "src/helper";

const tooltipInfoHelper = {
  riskInfo:
    "Provide this information to get a more accurate risk analysis score",
  productDesc:
    "ConnexPay suggests clients submitted a high level description such as Flight, Hotel, Car Rental, etc.",
  statementDescription:
    "The statement description allows a client to customize the Merchant name that appears on the cardholder statement such that the cardholder recognizes the transaction on their statement.",
};

const CustomerForm = (props) => {
  const { formik } = props;

  const { values, touched, errors, setFieldValue, handleChange, handleBlur } =
    formik;

  const [optCountries, setOptCountries] = useState([]);
  const [optStates, setOptStates] = useState([]);
  const [optCities, setOptCities] = useState([]);

  useEffect(() => {
    const getOptCountries = () => {
      const getData = csc.getAllCountries();
      const populateData = getData.map((val) => ({
        value: val.isoCode,
        label: `${val.name}`,
      }));
      setOptCountries(populateData);
    };
    const countyCode = values.Card.Customer.Country;
    const state = values.Card.Customer.State;

    getOptCountries();
    getOptStates(countyCode);
    getOptCities(countyCode, state);
  }, [values.Card.Customer.Country, values.Card.Customer.State]);

  const getOptStates = (countryCode) => {
    const getData = csc
      .getStatesOfCountry(countryCode)
      ?.filter((val) => !nonStateUS?.includes(val?.name));
    const populateData = getData.map((val) => ({
      value: val.isoCode,
      label: `${val.name}`,
    }));
    setOptStates(populateData);
  };

  const getOptCities = (countryCode, stateCode) => {
    const getData = csc.getCitiesOfState(countryCode, stateCode);
    const populateData = getData.map((val) => ({
      value: val.name,
      label: `${val.name}`,
    }));
    setOptCities(populateData);
  };

  const handleSelectCountry = (selected) => {
    setFieldValue("Card.Customer.Country", selected.value);
    setFieldValue("Card.Customer.State", "");
    setFieldValue("Card.Customer.City", "");
    setFieldValue("Card.Customer.Phone", "");
    getOptStates(selected.value);
  };

  const handleSelectState = (selected) => {
    setFieldValue("Card.Customer.State", selected.value);
    setFieldValue("Card.Customer.City", "");

    getOptCities(values.Card?.Customer?.Country, selected.value);
  };

  const handleSelectCity = (selected) => {
    setFieldValue("Card.Customer.City", selected.value);
  };

  return (
    <>
      <CRow>
        {/* <CCol md="6">
          <CFormGroup>
            <CLabel htmlFor="SelectCustomer">Select Customer</CLabel>
            <Select
              classNamePrefix="default-select"
              name="SelectCustomer"
              value={
                [].find((val) => val.value === values.SelectCustomer) || ""
              }
              options={[]}
              placeholder="Select Customer"
              components={{
                IndicatorSeparator: () => null,
              }}
              isSearchable={false}
            />
            <CInvalidFeedback>{errors.SelectCustomer}</CInvalidFeedback>
          </CFormGroup>
        </CCol> */}
        {/* <CCol md="6">
          <CFormGroup>
            <CLabel htmlFor="SellerId">
              Merchant Assigned Account Number for Consumer
            </CLabel>
            <CInput
              autoComplete="none"
              name="RiskData.SellerId"
              type="text"
              value={values.RiskData?.SellerId}
              placeholder="Enter Account for Consumer"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.SellerId && !!errors.RiskData?.SellerId
              }
            />
            <CInvalidFeedback>{errors.RiskData?.SellerId}</CInvalidFeedback>
          </CFormGroup>
        </CCol> */}

        {/* <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="ProductDesc">Product Description</CLabel>
              <div className="d-flex flex-auto justify-content-between align-items-center">
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["riskInfo"]}
                >
                  <CIcon
                    tabIndex="-1"
                    name="cil-shield-check"
                    height={14}
                    className="mx-2"
                  />
                </CTooltip>
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["productDesc"]}
                >
                  <CIcon
                    tabIndex="-1"
                    className="mr-3"
                    name="cil-info-circle"
                    height={14}
                  />
                </CTooltip>
              </div>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.ProductDesc"
              type="text"
              value={values.RiskData?.ProductDesc}
              placeholder="Product Description"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.ProductDesc && !!errors.RiskData?.ProductDesc
              }
            />
            <CInvalidFeedback>{errors.RiskData?.ProductDesc}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="BillingPhoneNumber">Billing Phone Number</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip>
            </div>
            <NumberFormat
              name="RiskData.BillingPhoneNumber"
              customInput={CInput}
              value={values.RiskData?.BillingPhoneNumber}
              format="(###) ###-####"
              placeholder="Enter Billing Phone Number"
              onValueChange={(val) =>
                setFieldValue("RiskData.BillingPhoneNumber", val.value)
              }
              mask={"_"}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.BillingPhoneNumber &&
                !!errors.RiskData?.BillingPhoneNumber
              }
            />
            <CInvalidFeedback>
              {errors.RiskData?.BillingPhoneNumber}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol> */}

        <CCol md="6">
          <CFormGroup>
            <CLabel htmlFor="FirstName">First Name</CLabel>
            <CInput
              autoComplete="none"
              name="Card.Customer.FirstName"
              type="text"
              value={values.Card?.Customer?.FirstName}
              placeholder="Enter First Name"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.Card?.Customer?.FirstName &&
                !!errors.Card?.Customer?.FirstName
              }
            />
            <CInvalidFeedback>
              {errors.Card?.Customer?.FirstName}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <CLabel htmlFor="LastName">Last Name</CLabel>
            <CInput
              autoComplete="none"
              name="Card.Customer.LastName"
              type="text"
              value={values.Card?.Customer?.LastName}
              placeholder="Enter Last Name"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.Card?.Customer?.LastName &&
                !!errors.Card?.Customer?.LastName
              }
            />
            <CInvalidFeedback>
              {errors.Card?.Customer?.LastName}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="DateOfBirth">Date of Birth</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.DateOfBirth"
              type="date"
              value={values.RiskData?.DateOfBirth}
              placeholder="dd/mm/yyyy"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.DateOfBirth && !!errors.RiskData?.DateOfBirth
              }
            />
            <CInvalidFeedback>{errors.RiskData?.DateOfBirth}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="CustomerId">Statement Description</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["statementDescription"]}
              >
                <CIcon
                  tabIndex="-1"
                  className="mr-3"
                  name="cil-info-circle"
                  height={14}
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="StatementDescription"
              type="text"
              value={values.StatementDescription}
              placeholder="Enter Statement Description"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.StatementDescription && !!errors.StatementDescription
              }
            />
            <CInvalidFeedback>{errors.StatementDescription}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <CLabel htmlFor="Phone">Phone Number</CLabel>
            {
              (!values.Card?.Customer?.Country || (values.Card?.Customer?.Country && values.Card?.Customer?.Country === "US")) ?
                <NumberFormat
                  name="Card.Customer.Phone"
                  customInput={CInput}
                  value={values.Card?.Customer?.Phone}
                  format="(###) ###-####"
                  placeholder="Enter Phone Number"
                  onValueChange={(val) => {
                    setFieldValue("Card.Customer.Phone", val.value)
                  }}
                  mask={"_"}
                  onBlur={handleBlur}
                  invalid={
                    touched.Card?.Customer?.Phone && !!errors.Card?.Customer?.Phone
                  }
                /> :
                <CInput
                  name="Card.Customer.Phone"
                  type="text"
                  value={values.Card?.Customer?.Phone}
                  placeholder="Enter Phone Number"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={
                    touched.Card?.Customer?.Phone && !!errors.Card?.Customer?.Phone
                  }
                />
            }
            <CInvalidFeedback>{errors.Card?.Customer?.Phone}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <CLabel htmlFor="SSN4">SSN4</CLabel>
            <CInput
              autoComplete="none"
              name="Card.Customer.SSN4"
              type="text"
              value={values.Card?.Customer?.SSN4}
              placeholder="Enter SSN4"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.Card?.Customer?.SSN4 && !!errors.Card?.Customer?.SSN4
              }
            />
            <CInvalidFeedback>{errors.Card?.Customer?.SSN4}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="Address1">Address Line 1</CLabel>
              {/* <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip> */}
            </div>
            <CInput
              autoComplete="none"
              name="Card.Customer.Address1"
              type="text"
              value={values.Card?.Customer?.Address1}
              placeholder="Enter Address Line 1"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.Card?.Customer?.Address1 &&
                !!errors.Card?.Customer?.Address1
              }
            />
            <CInvalidFeedback>
              {errors.Card?.Customer?.Address1}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="Address2">Address Line 2</CLabel>
              {/* <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip> */}
            </div>
            <CInput
              autoComplete="none"
              name="Card.Customer.Address2"
              type="text"
              value={values.Card?.Customer?.Address2}
              placeholder="Enter Address Line 2"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.Card?.Customer?.Address2 &&
                !!errors.Card?.Customer?.Address2
              }
            />
            <CInvalidFeedback>
              {errors.Card?.Customer?.Address2}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="Country">Country</CLabel>
              {/* <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip> */}
            </div>
            <Select
              classNamePrefix="default-select"
              name="country"
              value={
                optCountries.find(
                  (val) => val.value === values.Card?.Customer?.Country
                ) || ""
              }
              options={optCountries}
              onChange={handleSelectCountry}
              placeholder="Select Country"
              components={{
                IndicatorSeparator: () => null,
              }}
            />
            <CInvalidFeedback>
              {errors.Card?.Customer?.Country}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="State">State</CLabel>
              {/* <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip> */}
            </div>
            <Select
              classNamePrefix="default-select"
              name="country"
              value={
                optStates.find(
                  (val) => val.value === values.Card?.Customer?.State
                ) || ""
              }
              options={optStates}
              onChange={handleSelectState}
              placeholder="Select State"
              components={{
                IndicatorSeparator: () => null,
              }}
            />
            <CInvalidFeedback>{errors.Card?.Customer?.State}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="City">City</CLabel>
              {/* <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip> */}
            </div>
            <Select
              classNamePrefix="default-select"
              name="city"
              value={
                optCities.find(
                  (val) => val.value === values.Card?.Customer?.City
                ) || ""
              }
              options={optCities}
              onChange={handleSelectCity}
              placeholder="Select City"
              components={{
                IndicatorSeparator: () => null,
              }}
            />
            <CInvalidFeedback>{errors.Card?.Customer?.City}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex">
              <CLabel htmlFor="Zip align-items-center">Zip Code</CLabel>
              {/* <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip> */}
            </div>
            <CInput
              autoComplete="none"
              name="Card.Customer.Zip"
              type="text"
              value={values.Card?.Customer?.Zip}
              placeholder="Enter Zip Code"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.Card?.Customer?.Zip && !!errors.Card?.Customer?.Zip
              }
            />
            <CInvalidFeedback>{errors.Card?.Customer?.Zip}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
      </CRow>
    </>
  );
};

export default CustomerForm;
