import { useState, useEffect } from "react";
import {
  CRow,
  CCol,
  CFormGroup,
  CLabel,
  CInput,
  CInputCheckbox,
  CInvalidFeedback,
  CTooltip,
} from "@coreui/react";
import Select from "react-select";
import CIcon from "@coreui/icons-react";
import NumberFormat from "react-number-format";
import csc from "country-state-city";
import { isEmail, nonStateUS } from "src/helper";

const optGender = [
  { value: "M", label: "Male" },
  { value: "F", label: "Female" },
];

const tooltipInfoHelper = {
  riskInfo:
    "Provide this information to get a more accurate risk analysis score",
  productDesc:
    "General shopping cart information that describes the type of item being purchased",
  sellerId:
    "This value acts as a secondary identifier in conjunction with Order Number. The value is searchable in the Kount portal.",
};

const RiskForm = (props) => {
  const { formik } = props;

  const { values, touched, errors, setFieldValue, handleChange, handleBlur } =
    formik;

  const [optCountries, setOptCountries] = useState([]);
  const [optStates, setOptStates] = useState([]);
  const [optCities, setOptCities] = useState([]);

  useEffect(() => {
    const getOptCountries = () => {
      const getData = csc.getAllCountries();
      const populateData = getData.map((val) => ({
        value: val.isoCode,
        label: `${val.name}`,
      }));
      setOptCountries(populateData);
    };

    getOptCountries();
    const billingCountryCode = values.RiskData?.BillingCountryCode;
    const state = values.RiskData.BillingState;
    getOptStates(billingCountryCode);
    getOptCities(billingCountryCode, state);
  }, [values.RiskData.BillingCountryCode, values.RiskData.BillingState]);

  const getOptStates = (countryCode) => {
    const getData = csc
      .getStatesOfCountry(countryCode)
      ?.filter((val) => !nonStateUS?.includes(val?.name));
    const populateData = getData.map((val) => ({
      value: val.isoCode,
      label: `${val.name}`,
    }));
    setOptStates(populateData);
  };

  const getOptCities = (countryCode, stateCode) => {
    const getData = csc.getCitiesOfState(countryCode, stateCode);
    const populateData = getData.map((val) => ({
      value: val.name,
      label: `${val.name}`,
    }));
    setOptCities(populateData);
  };

  const handleSelectCountry = (selected) => {
    setFieldValue("RiskData.BillingCountryCode", selected.value);
    setFieldValue("RiskData.BillingState", "");
    setFieldValue("RiskData.BillingCity", "");
    setFieldValue("RiskData.BillingPhoneNumber", "");
    getOptStates(selected.value);
  };

  const handleSelectState = (selected) => {
    setFieldValue("RiskData.BillingState", selected.value);
    setFieldValue("RiskData.BillingCity", "");

    getOptCities(values.RiskData?.BillingCountryCode, selected.value);
  };

  const handleSelectCity = (selected) => {
    setFieldValue("RiskData.BillingCity", selected.value);
  };

  const handleCheckboxCustomer = (e) => {
    const { checked } = e.target;
    if (checked) {
      getOptStates(values.Card?.Customer?.Country);
      getOptCities(
        values.Card?.Customer?.Country,
        values.Card?.Customer?.State
      );

      setFieldValue(
        "RiskData.BillingPhoneNumber",
        values.Card?.Customer?.Phone
      );
      setFieldValue(
        "RiskData.BillingAddress1",
        values.Card?.Customer?.Address1
      );
      setFieldValue(
        "RiskData.BillingAddress2",
        values.Card?.Customer?.Address2
      );
      setFieldValue(
        "RiskData.BillingCountryCode",
        values.Card?.Customer?.Country
      );
      setFieldValue("RiskData.BillingState", values.Card?.Customer?.State);
      setFieldValue("RiskData.BillingCity", values.Card?.Customer?.City);
      setFieldValue("RiskData.BillingPostalCode", values.Card?.Customer?.Zip);
    } else {
      setFieldValue("RiskData.BillingPhoneNumber", "");
      setFieldValue("RiskData.BillingAddress1", "");
      setFieldValue("RiskData.BillingAddress2", "");
      setFieldValue("RiskData.BillingCountryCode", "");
      setFieldValue("RiskData.BillingState", "");
      setFieldValue("RiskData.BillingCity", "");
      setFieldValue("RiskData.BillingPostalCode", "");
    }
  };

  return (
    <>
      <CFormGroup className="mb-3" variant="custom-checkbox" inline>
        <CInputCheckbox
          custom
          id="inline-checkbox1"
          name="inline-checkbox1"
          value="option1"
          onChange={handleCheckboxCustomer}
        />
        <CLabel variant="custom-checkbox" htmlFor="inline-checkbox1">
          Same as customer address
        </CLabel>
      </CFormGroup>
      <CRow>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="BillingPhoneNumber">Billing Phone Number</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip>
            </div>
            {
              (!values.RiskData?.BillingCountryCode || (values.RiskData?.BillingCountryCode && values.RiskData?.BillingCountryCode === "US")) ?
                <NumberFormat
                  name="RiskData.BillingPhoneNumber"
                  customInput={CInput}
                  value={values.RiskData?.BillingPhoneNumber}
                  format="(###) ###-####"
                  placeholder="Enter Billing Phone Number"
                  onValueChange={(val) =>
                    setFieldValue("RiskData.BillingPhoneNumber", val.value)
                  }
                  mask={"_"}
                  onBlur={handleBlur}
                  invalid={
                    touched.RiskData?.BillingPhoneNumber &&
                    !!errors.RiskData?.BillingPhoneNumber
                  }
                /> :
                <CInput
                  name="RiskData.BillingPhoneNumber"
                  type="text"
                  value={values.RiskData?.BillingPhoneNumber}
                  placeholder="Enter Billing Phone Number"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={
                    touched.RiskData?.BillingPhoneNumber &&
                    !!errors.RiskData?.BillingPhoneNumber
                  }
                />
            }

            <CInvalidFeedback>
              {errors.RiskData?.BillingPhoneNumber}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6" />

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="Address1">Billing Address Line 1</CLabel>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.BillingAddress1"
              type="text"
              value={values.RiskData?.BillingAddress1}
              placeholder="Enter Address Line 1"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.BillingAddress1 &&
                !!errors.RiskData?.BillingAddress1
              }
            />
            <CInvalidFeedback>
              {errors.RiskData?.BillingAddress1}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="Address2">Billing Address Line 2</CLabel>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.BillingAddress2"
              type="text"
              value={values.RiskData?.BillingAddress2}
              placeholder="Enter Address Line 2"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.BillingAddress2 &&
                !!errors.RiskData?.BillingAddress2
              }
            />
            <CInvalidFeedback>
              {errors.RiskData?.BillingAddress2}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="Country">Country</CLabel>
            </div>
            <Select
              classNamePrefix="default-select"
              name="country"
              value={
                optCountries.find(
                  (val) => val.value === values.RiskData?.BillingCountryCode
                ) || ""
              }
              options={optCountries}
              onChange={handleSelectCountry}
              placeholder="Select Country"
              components={{
                IndicatorSeparator: () => null,
              }}
            />
            <CInvalidFeedback>
              {errors.RiskData?.BillingCountryCode}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="State">State</CLabel>
            </div>
            <Select
              classNamePrefix="default-select"
              name="country"
              value={
                optStates.find(
                  (val) => val.value === values.RiskData?.BillingState
                ) || ""
              }
              options={optStates}
              onChange={handleSelectState}
              placeholder="Select State"
              components={{
                IndicatorSeparator: () => null,
              }}
            />
            <CInvalidFeedback>{errors.RiskData?.BillingState}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="City">City</CLabel>
            </div>
            <Select
              classNamePrefix="default-select"
              name="city"
              value={
                optCities.find(
                  (val) => val.value === values.RiskData?.BillingCity
                ) || ""
              }
              options={optCities}
              onChange={handleSelectCity}
              placeholder="Select City"
              components={{
                IndicatorSeparator: () => null,
              }}
            />
            <CInvalidFeedback>{errors.RiskData?.BillingCity}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex">
              <CLabel htmlFor="Zip align-items-center">Zip Code</CLabel>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.BillingPostalCode"
              type="text"
              value={values.RiskData?.BillingPostalCode}
              placeholder="Enter Zip Code"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.BillingPostalCode &&
                !!errors.RiskData?.BillingPostalCode
              }
            />
            <CInvalidFeedback>
              {errors.RiskData?.BillingPostalCode}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="Email">Email Address</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["emailAddress"]}
              >
                <CIcon
                  tabIndex="-1"
                  className="mr-3"
                  name="cil-info-circle"
                  height={14}
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.Email"
              type="text"
              value={values.RiskData?.Email}
              placeholder="Enter Email Address"
              onChange={handleChange}
              onBlur={handleBlur}
              valid={touched.RiskData?.Email && isEmail(values.RiskData?.Email)}
              invalid={touched.RiskData?.Email && !!errors.RiskData?.Email}
            />
            <CInvalidFeedback>{errors.RiskData?.Email}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="ProductDesc">Product Description</CLabel>
              <div className="d-flex flex-auto justify-content-between align-items-center">
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["riskInfo"]}
                >
                  <CIcon
                    tabIndex="-1"
                    name="cil-shield-check"
                    height={14}
                    className="mx-2"
                  />
                </CTooltip>
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["productDesc"]}
                >
                  <CIcon
                    tabIndex="-1"
                    className="mr-3"
                    name="cil-info-circle"
                    height={14}
                  />
                </CTooltip>
              </div>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.ProductDesc"
              type="text"
              value={values.RiskData?.ProductDesc}
              placeholder="Product Description"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.ProductDesc && !!errors.RiskData?.ProductDesc
              }
            />
            <CInvalidFeedback>{errors.RiskData?.ProductDesc}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="Gender">Gender</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip>
            </div>
            <Select
              classNamePrefix="default-select"
              name="SelectGender"
              value={
                optGender.find(
                  (val) => val.value === values.RiskData?.Gender
                ) || ""
              }
              options={optGender}
              placeholder="Select Customer"
              components={{
                IndicatorSeparator: () => null,
              }}
              onChange={(selected) =>
                setFieldValue("RiskData.Gender", selected.value)
              }
              isSearchable={false}
            />
            <CInvalidFeedback>{errors.Card?.Customer?.Gender}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <CLabel htmlFor="SellerId">
              Merchant Assigned Account Number for Consumer
            </CLabel>
            <CInput
              autoComplete="none"
              name="RiskData.SellerId"
              type="text"
              value={values.RiskData?.SellerId}
              placeholder="Enter Account for Consumer"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.SellerId && !!errors.RiskData?.SellerId
              }
            />
            <CInvalidFeedback>{errors.RiskData?.SellerId}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
      </CRow>
    </>
  );
};

export default RiskForm;
