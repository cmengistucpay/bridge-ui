import { useState, useEffect } from "react";
import {
  CRow,
  CCol,
  CFormGroup,
  CLabel,
  CInput,
  CInvalidFeedback,
  CTooltip,
  CButton,
  CDataTable,
} from "@coreui/react";
import Select from "react-select";
import CIcon from "@coreui/icons-react";
import csc from "country-state-city";

const tooltipInfoHelper = {
  riskInfo:
    "Provide this information to get a more accurate risk analysis score",
  journeyType: "The journey type. Eg: 'Day', 'Night'",
  route: "The route type. Eg: 'Direct'",
  routeByCountry: "Provide the route by country. Eg: 'UK, USA'",
};

const fields = ["Name", "Id", "DateOfBirth", "Country"];
const initialPassengerForm = {
  Country: "",
  DateOfBirth: "",
  Id: "",
  Name: "",
};

const FlightForm = (props) => {
  const { formik } = props;

  const { values, touched, errors, setFieldValue, handleChange, handleBlur } =
    formik;

  const [optCountries, setOptCountries] = useState([]);
  const [passengerForm, setPassengerForm] = useState(initialPassengerForm);
  const [passengerList, setPassengerList] = useState(
    values.RiskData?.FlightPassenger
  );

  useEffect(() => {
    const getOptCountries = () => {
      const getData = csc.getAllCountries();
      const populateData = getData.map((val) => ({
        value: val.isoCode,
        label: `${val.name}`,
      }));
      setOptCountries(populateData);
    };

    getOptCountries();
  }, []);

  const handleChangePassenger = (key, value) => {
    setPassengerForm({
      ...passengerForm,
      [key]: value,
    });
  };

  const handleAddPassenger = () => {
    const arrPassengerList = [...passengerList, passengerForm];
    const arrPassengerListFormik = [
      ...values.RiskData?.FlightPassenger,
      passengerForm,
    ];

    setPassengerList(arrPassengerList);
    setPassengerForm(initialPassengerForm);
    setFieldValue("RiskData.FlightPassenger", arrPassengerListFormik);
  };

  return (
    <>
      <CRow>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="Airline">Airline</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.FlightData.Airline"
              type="text"
              value={values.RiskData?.FlightData?.Airline}
              placeholder="Enter Airline Details"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.FlightData?.Airline &&
                !!errors.RiskData?.FlightData?.Airline
              }
            />
            <CInvalidFeedback>
              {errors.RiskData?.FlightData?.Airline}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="DepartureAirport">Departure Airport</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.FlightData.DepartureAirport"
              type="text"
              value={values.RiskData?.FlightData?.DepartureAirport}
              placeholder="Enter Departure Airport"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.FlightData?.DepartureAirport &&
                !!errors.RiskData?.FlightData?.DepartureAirport
              }
            />
            <CInvalidFeedback>
              {errors.RiskData?.FlightData?.MerchantAssigned}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="DepartureDate">Departure Date</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.FlightData.DepartureDate"
              type="date"
              value={values.RiskData?.FlightData?.DepartureDate}
              placeholder="dd/mm/yyyy"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.FlightData?.DepartureDate &&
                !!errors.RiskData?.FlightData?.DepartureDate
              }
            />
            <CInvalidFeedback>
              {errors.RiskData?.FlightData?.DepartureDate}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="DestinationAirport">Destination Airport</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.FlightData.DestinationAirport"
              type="text"
              value={values.RiskData?.FlightData?.DestinationAirport}
              placeholder="Enter Destination Airport"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.FlightData?.DestinationAirport &&
                !!errors.RiskData?.FlightData?.DestinationAirport
              }
            />
            <CInvalidFeedback>
              {errors.RiskData?.FlightData?.DestinationAirport}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="HoursToDeparture">Hours to Departure</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.FlightData.HoursToDeparture"
              type="text"
              value={values.RiskData?.FlightData?.HoursToDeparture}
              placeholder="Enter Hours to Departure"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.FlightData?.HoursToDeparture &&
                !!errors.RiskData?.FlightData?.HoursToDeparture
              }
            />
            <CInvalidFeedback>
              {errors.RiskData?.FlightData?.HoursToDeparture}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="JourneyType">Journey Type</CLabel>
              <div className="d-flex flex-auto justify-content-between align-items-center">
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["riskInfo"]}
                >
                  <CIcon
                    tabIndex="-1"
                    name="cil-shield-check"
                    height={14}
                    className="mx-2"
                  />
                </CTooltip>
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["journeyType"]}
                >
                  <CIcon
                    tabIndex="-1"
                    className="mr-3"
                    name="cil-info-circle"
                    height={14}
                  />
                </CTooltip>
              </div>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.FlightData.JourneyType"
              type="text"
              value={values.RiskData?.FlightData?.JourneyType}
              placeholder="Enter Journey Type"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.FlightData?.JourneyType &&
                !!errors.RiskData?.FlightData?.JourneyType
              }
            />
            <CInvalidFeedback>
              {errors.RiskData?.FlightData?.JourneyType}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="Route">Route</CLabel>
              <div className="d-flex flex-auto justify-content-between align-items-center">
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["riskInfo"]}
                >
                  <CIcon
                    tabIndex="-1"
                    name="cil-shield-check"
                    height={14}
                    className="mx-2"
                  />
                </CTooltip>
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["route"]}
                >
                  <CIcon
                    tabIndex="-1"
                    className="mr-3"
                    name="cil-info-circle"
                    height={14}
                  />
                </CTooltip>
              </div>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.FlightData.Route"
              type="text"
              value={values.RiskData?.FlightData?.Route}
              placeholder="Enter Route"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.FlightData?.Route &&
                !!errors.RiskData?.FlightData?.Route
              }
            />
            <CInvalidFeedback>
              {errors.RiskData?.FlightData?.Route}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="RouteByCountry">Route by Country</CLabel>
              <div className="d-flex flex-auto justify-content-between align-items-center">
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["riskInfo"]}
                >
                  <CIcon
                    tabIndex="-1"
                    name="cil-shield-check"
                    height={14}
                    className="mx-2"
                  />
                </CTooltip>
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["routeByCountry"]}
                >
                  <CIcon
                    tabIndex="-1"
                    className="mr-3"
                    name="cil-info-circle"
                    height={14}
                  />
                </CTooltip>
              </div>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.FlightData.RouteByCountry"
              type="text"
              value={values.RiskData?.FlightData?.RouteByCountry}
              placeholder="Enter Route Country"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.FlightData?.RouteByCountry &&
                !!errors.RiskData?.FlightData?.RouteByCountry
              }
            />
            <CInvalidFeedback>
              {errors.RiskData?.FlightData?.RouteByCountry}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>
      </CRow>

      <CLabel>Flight Passengers</CLabel>
      <div className="px-4 py-3 flight-passenger-form">
        <CRow>
          <CCol md="6">
            <CFormGroup>
              <div className="d-flex align-items-center">
                <CLabel htmlFor="Name">Name</CLabel>
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["riskInfo"]}
                >
                  <CIcon
                    tabIndex="-1"
                    name="cil-shield-check"
                    height={14}
                    className="mx-2"
                  />
                </CTooltip>
              </div>
              <CInput
                autoComplete="none"
                name="RiskData.FlightData.Name"
                type="text"
                value={passengerForm?.Name}
                placeholder="Enter Name"
                onChange={(e) => handleChangePassenger("Name", e.target.value)}
              />
            </CFormGroup>
          </CCol>
          <CCol md="6">
            <CFormGroup>
              <div className="d-flex align-items-center">
                <CLabel htmlFor="Id">ID</CLabel>
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["riskInfo"]}
                >
                  <CIcon
                    tabIndex="-1"
                    name="cil-shield-check"
                    height={14}
                    className="mx-2"
                  />
                </CTooltip>
              </div>
              <CInput
                autoComplete="none"
                name="Card.Customer.Id"
                type="text"
                value={passengerForm?.Id}
                placeholder="Enter ID"
                onChange={(e) => handleChangePassenger("Id", e.target.value)}
              />
            </CFormGroup>
          </CCol>

          <CCol md="6">
            <CFormGroup>
              <div className="d-flex align-items-center">
                <CLabel htmlFor="DateOfBirth">Date Of Birth</CLabel>
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["riskInfo"]}
                >
                  <CIcon
                    tabIndex="-1"
                    name="cil-shield-check"
                    height={14}
                    className="mx-2"
                  />
                </CTooltip>
              </div>
              <CInput
                autoComplete="none"
                name="RiskData.FlightData.DateOfBirth"
                type="date"
                value={passengerForm?.DateOfBirth}
                placeholder="Enter DateOfBirth"
                onChange={(e) =>
                  handleChangePassenger("DateOfBirth", e.target.value)
                }
              />
            </CFormGroup>
          </CCol>
          <CCol md="6">
            <CFormGroup>
              <div className="d-flex align-items-center">
                <CLabel htmlFor="Country">Country</CLabel>
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["riskInfo"]}
                >
                  <CIcon
                    tabIndex="-1"
                    name="cil-shield-check"
                    height={14}
                    className="mx-2"
                  />
                </CTooltip>
              </div>

              <Select
                classNamePrefix="default-select"
                name="country"
                value={
                  optCountries.find(
                    (val) => val.value === passengerForm?.Country
                  ) || ""
                }
                options={optCountries}
                onChange={(selected) =>
                  handleChangePassenger("Country", selected.value)
                }
                placeholder="Select Country"
                components={{
                  IndicatorSeparator: () => null,
                }}
              />
            </CFormGroup>
          </CCol>
        </CRow>

        <div className="mt-2 d-flex justify-content-between">
          <div></div>
          <div className="d-flex">
            <CButton
              disabled={
                !passengerForm?.Country ||
                !passengerForm?.DateOfBirth ||
                !passengerForm?.Id ||
                !passengerForm?.Name
              }
              color="secondary"
              className="px-4 py-2"
              onClick={handleAddPassenger}
            >
              Add
            </CButton>
          </div>
        </div>
      </div>

      {!!passengerList?.length && (
        <div className="mt-4">
          <CDataTable
            addTableClasses="table-flight-passenger"
            items={passengerList}
            fields={fields}
            itemsPerPage={5}
            noItemsViewSlot={<div className="my-5" />}
            columnHeaderSlot={{
              Id: "ID",
            }}
          />
        </div>
      )}
    </>
  );
};

export default FlightForm;
