import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { isEmpty } from "lodash";
import {
  CRow,
  CCol,
  CFormGroup,
  CLabel,
  CInput,
  CSwitch,
  CInvalidFeedback,
  CTooltip,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import NumberFormat from "react-number-format";
import Select from "react-select";
import { isEmail, splitFullName, mapOptLabels } from "src/helper";

const tooltipInfoHelper = {
  riskInfo:
    "Provide this information to get a more accurate risk analysis score.",
  orderNumber:
    "Client provided transaction ID associated with the order. Unique to you. Can be any combination of alpha/numeric characters. Searchable in both Bridge and Kount.",
  customerId:
    "This value acts as a secondary identifier in conjunction with OrderNumber. The value is searchable and reportable in the ConnexPay portal.",
  productType:
    "Generalized description of the item added passed as plain text. This could be flight, tour, hotel, etc.",
  productItem:
    "ConnexPay suggests clients submit a high level description such as One Way, Round Trip, Seven Nights, etc",
  expectedPayments:
    "The number of outbound payments that will be made to suppliers. If paying a single supplier the value is 1, if paying two suppliers the value is 2, etc.",
  emailAddress:
    "Provide this information to allow for easy searching by email ID and for better analytics based on individual customers. If you turn on email address to cardholder, then they will receive a receipt upon the processing of the sale.",
  selectGroup: "",
  delayedSale:
    "A sale is created but the consumer card is not charged until a future date (activation date).",
};

const SalesForm = (props) => {
  const { formik } = props;

  const userData = useSelector((state) => state.userData);
  const merchantSettings = userData?.merchantSettings;

  const [optLabels, setOptLabels] = useState([]);

  useEffect(() => {
    if (!isEmpty(userData)) {
      const arrOptLabels = mapOptLabels(userData?.labelHierarchy);
      setOptLabels(arrOptLabels);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  const { values, touched, errors, setFieldValue, handleChange, handleBlur } =
    formik;

  return (
    <>
      <CRow>
        <CCol md="6">
          <CFormGroup>
            <CLabel htmlFor="Amount">
              Amount<span className="label-required">*</span>
            </CLabel>
            <NumberFormat
              name="Amount"
              value={values.Amount}
              onValueChange={(val) => setFieldValue("Amount", val.floatValue)}
              prefix="$"
              thousandSeparator
              customInput={CInput}
              placeholder="Enter Amount"
              onBlur={handleBlur}
              invalid={touched.Amount && !!errors.Amount}
            />
            <CInvalidFeedback>{errors.Amount}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="ExpectedPayments">
                Expected Payments<span className="label-required">*</span>
              </CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["expectedPayments"]}
              >
                <CIcon
                  tabIndex="-1"
                  className="mr-3"
                  name="cil-info-circle"
                  height={14}
                />
              </CTooltip>
            </div>
            <NumberFormat
              name="ConnexPayTransaction.ExpectedPayments"
              customInput={CInput}
              value={values.ConnexPayTransaction?.ExpectedPayments}
              placeholder="Enter Expected Payments"
              onValueChange={(val) =>
                setFieldValue(
                  "ConnexPayTransaction.ExpectedPayments",
                  val.floatValue
                )
              }
              onBlur={handleBlur}
              invalid={
                touched.ConnexPayTransaction?.ExpectedPayments &&
                !!errors.ConnexPayTransaction?.ExpectedPayments
              }
            />
            <CInvalidFeedback>
              {errors.ConnexPayTransaction?.ExpectedPayments}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="ProductType">
                Product Type<span className="label-required">*</span>
              </CLabel>
              <div className="d-flex flex-auto justify-content-between align-items-center">
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["riskInfo"]}
                >
                  <CIcon
                    tabIndex="-1"
                    name="cil-shield-check"
                    height={14}
                    className="mx-2"
                  />
                </CTooltip>
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["productType"]}
                >
                  <CIcon
                    tabIndex="-1"
                    className="mr-3"
                    name="cil-info-circle"
                    height={14}
                  />
                </CTooltip>
              </div>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.ProductType"
              type="text"
              value={values.RiskData?.ProductType}
              placeholder="Select Product Type"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.ProductType && !!errors.RiskData?.ProductType
              }
            />
            <CInvalidFeedback>{errors.RiskData?.ProductType}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="ProductItem">
                Product Item<span className="label-required">*</span>
              </CLabel>
              <div className="d-flex flex-auto justify-content-between align-items-center">
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["riskInfo"]}
                >
                  <CIcon
                    tabIndex="-1"
                    name="cil-shield-check"
                    height={14}
                    className="mx-2"
                  />
                </CTooltip>
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["productItem"]}
                >
                  <CIcon
                    tabIndex="-1"
                    className="mr-3"
                    name="cil-info-circle"
                    height={14}
                  />
                </CTooltip>
              </div>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.ProductItem"
              type="text"
              value={values.RiskData?.ProductItem}
              placeholder="Select Product Item"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.ProductItem && !!errors.RiskData?.ProductItem
              }
            />
            <CInvalidFeedback>{errors.RiskData?.ProductItem}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <CLabel htmlFor="CardNumber">
              Card Number<span className="label-required">*</span>
            </CLabel>
            <NumberFormat
              onValueChange={(val) => {
                setFieldValue("Card.CardNumber", val.formattedValue);
              }}
              isAllowed={(val) => {
                return val.formattedValue?.length <= 16;
              }}
              customInput={CInput}
              onBlur={handleBlur}
              name="Card.CardNumber"
              type="text"
              value={values.Card?.CardNumber}
              placeholder="Enter Card Number"
              invalid={touched.Card?.CardNumber && !!errors.Card?.CardNumber}
              allowLeadingZeros
            />
            <CInvalidFeedback>{errors.Card?.CardNumber}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <CLabel htmlFor="ExpirationDate">
              Expiration Date<span className="label-required">*</span>
            </CLabel>
            <NumberFormat
              name="Card.ExpirationDate"
              value={values.Card?.ExpirationDate}
              onValueChange={(val) =>
                setFieldValue("Card.ExpirationDate", val.formattedValue)
              }
              format="##-##"
              customInput={CInput}
              placeholder="MM-YY"
              onBlur={handleBlur}
              invalid={
                touched.Card?.ExpirationDate && !!errors.Card?.ExpirationDate
              }
              allowLeadingZeros
            />
            <CInvalidFeedback>{errors.Card?.ExpirationDate}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="CardHolderName">Cardholder Name</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="Card.CardHolderName"
              type="text"
              value={values.Card?.CardHolderName}
              placeholder="Enter Cardholder Name"
              onChange={(e) => {
                setFieldValue("Card.CardHolderName", e.target.value);
                setFieldValue("RiskData.Name", e.target.value);
              }}
              onBlur={(e) => {
                handleBlur(e);
                setFieldValue(
                  "Card.Customer.FirstName",
                  splitFullName(e.target.value).firstName
                );
                setFieldValue(
                  "Card.Customer.LastName",
                  splitFullName(e.target.value).lastName
                );
              }}
              invalid={
                touched.Card?.CardHolderName && !!errors.Card?.CardHolderName
              }
            />
            <CInvalidFeedback>{errors.Card?.CardHolderName}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <CLabel htmlFor="Cvv2">CVV2</CLabel>
            <CInput
              autoComplete="none"
              name="Card.Cvv2"
              type="text"
              value={values.Card?.Cvv2}
              placeholder="Enter CVV2"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.Card?.Cvv2 && !!errors.Card?.Cvv2}
            />
            <CInvalidFeedback>{errors.Card?.Cvv2}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="CustomerId">Customer ID</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["customerId"]}
              >
                <CIcon
                  tabIndex="-1"
                  className="mr-3"
                  name="cil-info-circle"
                  height={14}
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="CustomerId"
              type="text"
              value={values.CustomerId}
              placeholder="Enter Customer ID"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.CustomerId && !!errors.CustomerId}
            />
            <CInvalidFeedback>{errors.CustomerId}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="OrderNumber">Order Number</CLabel>
              <div className="d-flex flex-auto justify-content-between align-items-center">
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["riskInfo"]}
                >
                  <CIcon
                    tabIndex="-1"
                    name="cil-shield-check"
                    height={14}
                    className="mx-2"
                  />
                </CTooltip>
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["orderNumber"]}
                >
                  <CIcon
                    tabIndex="-1"
                    className="mr-3"
                    name="cil-info-circle"
                    height={14}
                  />
                </CTooltip>
              </div>
            </div>
            <CInput
              autoComplete="none"
              name="OrderNumber"
              type="text"
              value={values.OrderNumber}
              placeholder="Enter Order Number"
              onChange={(e) => {
                setFieldValue("OrderNumber", e.target.value);
                setFieldValue("RiskData.OrderNumber", e.target.value);
              }}
              onBlur={handleBlur}
              invalid={touched.OrderNumber && !!errors.OrderNumber}
            />
            <CInvalidFeedback>{errors.OrderNumber}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="Email">Email Address</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["emailAddress"]}
              >
                <CIcon
                  tabIndex="-1"
                  className="mr-3"
                  name="cil-info-circle"
                  height={14}
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="Card.Customer.Email"
              type="text"
              value={values.Card?.Customer?.Email}
              placeholder="Enter Email Address"
              onChange={handleChange}
              onBlur={handleBlur}
              valid={touched.Card?.Customer?.Email && isEmail(values.Card?.Customer?.Email)}
              invalid={touched.Card?.Customer?.Email && !!errors.Card?.Customer?.Email}
            />
            <CInvalidFeedback>{errors.Card?.Customer?.Email}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          {merchantSettings?.showLabels && (
            <CFormGroup>
              <CLabel htmlFor="Label">
                Label<span className="label-required">*</span>
              </CLabel>
              <Select
                className={
                  touched.LabelIds && !!errors.LabelIds ? "is-invalid" : ""
                }
                classNamePrefix="default-select"
                name="Label"
                value={
                  optLabels.find((val) => val.value === values.LabelIds) || ""
                }
                options={optLabels}
                placeholder="Select Label"
                components={{
                  IndicatorSeparator: () => null,
                }}
                onChange={(selected) => {
                  setFieldValue("LabelIds", selected.value);
                }}
                isSearchable={true}
              />
              <CInvalidFeedback>{errors.LabelIds}</CInvalidFeedback>
            </CFormGroup>
          )}
        </CCol>
        <CCol md="6">
          <div className="d-flex justify-content-between align-items-center mb-2">
            <p className="mb-0">Email Receipt to Cardholder</p>
            <CSwitch
              className="ml-2"
              color="secondary"
              checked={values.SendReceipt}
              onChange={() => setFieldValue("SendReceipt", !values.SendReceipt)}
              // disabled={!isEmail(values.RiskData?.Email)}
              shape="pill"
            />
          </div>
        </CCol>
        <CCol md="6">
          {merchantSettings?.allowDelayedActivation && (
            <div className="d-flex justify-content-between align-items-center mb-2">
              <p className="mb-0">Delayed Sale</p>
              <div className="d-flex align-items-center">
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["delayedSale"]}
                >
                  <CIcon
                    tabIndex="-1"
                    className="mx-3"
                    name="cil-info-circle"
                    height={14}
                  />
                </CTooltip>
                <CSwitch
                  className="ml-2"
                  color="secondary"
                  checked={values.isDelaySaleActivation}
                  onChange={() =>
                    setFieldValue(
                      "isDelaySaleActivation",
                      !values.isDelaySaleActivation
                    )
                  }
                  shape="pill"
                />
              </div>
            </div>
          )}
        </CCol>
      </CRow>
    </>
  );
};

export default SalesForm;
