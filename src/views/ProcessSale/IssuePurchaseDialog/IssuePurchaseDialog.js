import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
} from "@coreui/react";
import NumberFormat from "react-number-format";

const IssuePurchaseDialog = (props) => {
  const {
    modalShow,
    handleHideModal,
    connexPayTransaction,
    handleShowReceipt,
    handleConfirmIssuePurchase,
  } = props;

  const userData = useSelector((state) => state.userData);

  const [purchaseType, setPurchaseType] = useState("");
  const [purchaseCount, setPurchaseCount] = useState(null);
  const [activeBody, setActiveBody] = useState("purchaseDialog");

  useEffect(() => {
    setPurchaseType("");
    setPurchaseCount(null);
    setActiveBody("purchaseDialog");

    setPurchaseCount(connexPayTransaction?.expectedPayments);
  }, [connexPayTransaction?.expectedPayments]);

  const handleSubmitConfirm = () => {
    handleConfirmIssuePurchase(purchaseType, purchaseCount);
  };

  const renderPurchaseDialog = () => {
    return (
      <>
        <CModalBody>
          <div>
            <p className="mb-0">Do you want to issue a Purchase?</p>
          </div>
        </CModalBody>
        <CModalFooter>
          <CButton
            color="light"
            className="px-4 py-2 mx-2"
            onClick={handleHideModal}
          >
            No
          </CButton>
          <CButton
            color="primary"
            className="px-4 py-2 mx-2"
            onClick={() => setActiveBody("purchaseDetail")}
          >
            Yes
          </CButton>
        </CModalFooter>
      </>
    );
  };

  const renderPurchaseDetail = () => {
    return (
      <>
        <CModalBody>
          <div className="">
            <p className="">What type of Purchase?</p>
            <div className="d-flex justify-content-around">
              <CButton
                color="secondary"
                className="px-4 py-2 w-48"
                onClick={() => setPurchaseType("virtualCard")}
                variant={"outline"}
                active={purchaseType === "virtualCard"}
              >
                Virtual Credit Card
              </CButton>

              {userData?.merchantSettings?.showACHPurchases && (
                <CButton
                  color="secondary"
                  className="px-4 py-2 w-48"
                  onClick={() => setPurchaseType("ach")}
                  variant={"outline"}
                  active={purchaseType === "ach"}
                >
                  ACH
                </CButton>
              )}
            </div>
          </div>
          <div className="mt-4">
            <p className="">How Many?</p>
            <div className="d-flex justify-content-around">
              <div className="w-30">
                <CButton
                  color="secondary"
                  className="px-4 py-2 w-100"
                  onClick={() => {
                    setPurchaseCount(1);
                  }}
                  variant="outline"
                  active={purchaseCount === 1}
                >
                  1
                </CButton>
              </div>
              <div className="w-30">
                <CButton
                  disabled={connexPayTransaction?.expectedPayments === 1}
                  color="secondary"
                  className={`px-4 py-2 w-100 ${
                    connexPayTransaction?.expectedPayments === 1
                      ? "btn-disabled-grey"
                      : ""
                  }`}
                  onClick={() => {
                    setPurchaseCount(2);
                  }}
                  variant="outline"
                  active={purchaseCount === 2}
                >
                  2
                </CButton>
              </div>
              <div className="w-30">
                {connexPayTransaction?.expectedPayments === 1 ||
                connexPayTransaction?.expectedPayments === 2 ? (
                  <CButton
                    disabled
                    color="secondary"
                    className={`px-4 py-2 w-100 btn-disabled-grey`}
                    variant="outline"
                  >
                    More
                  </CButton>
                ) : (
                  <NumberFormat
                    className={`purchase-count-more ${
                      purchaseCount > 2 ? "active" : ""
                    }`}
                    value={purchaseCount > 2 ? purchaseCount : null}
                    onValueChange={(val) => {
                      setPurchaseCount(val.floatValue);
                    }}
                    decimalScale={0}
                    fixedDecimalScale
                    placeholder="More"
                    allowEmptyFormatting
                    allowNegative={false}
                  />
                )}
              </div>
            </div>
          </div>
        </CModalBody>
        <CModalFooter>
          <CButton
            disabled={!purchaseType || !purchaseCount}
            color="primary"
            className="px-4 py-2"
            onClick={handleSubmitConfirm}
          >
            Submit
          </CButton>
        </CModalFooter>
      </>
    );
  };

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
      size={activeBody === "virtualCardForm" ? "lg" : ""}
    >
      <CModalHeader>
        <CModalTitle>
          {activeBody !== "purchaseDialog"
            ? "Enter Purchase Information"
            : "Transaction Approved"}
        </CModalTitle>
        <CButton color="success" onClick={handleShowReceipt} size="sm">
          Show Receipt
        </CButton>
      </CModalHeader>

      {activeBody === "purchaseDialog" && renderPurchaseDialog()}
      {activeBody === "purchaseDetail" && renderPurchaseDetail()}
    </CModal>
  );
};

export default IssuePurchaseDialog;
