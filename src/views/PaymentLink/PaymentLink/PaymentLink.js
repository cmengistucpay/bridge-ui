import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { isEmpty } from "lodash";
import { format, parseISO } from "date-fns";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CPagination,
  CTooltip,
  CButton,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
// import DatePicker from "react-datepicker";
import copy from "copy-to-clipboard";
import {
  formatUSD,
  midStringEllipsis,
  endStringEllipsis,
  mapOptLabels,
} from "src/helper";
import { briGetPaymentLinks } from "src/services";

import FilterTableModal from "src/views/PaymentLink/PaymentLink/FilterTableModal";
import ModalFiles from "src/views/PaymentLink/PaymentLink/ModalFiles";
import EditEmailModal from "src/views/PaymentLink/PaymentLink/EditEmailModal";
import EditDetailModal from "src/views/PaymentLink/PaymentLink/EditDetailModal";

// const dummyTableData = [
//   {
//     guid: "c634c47c-478f-4709-ad9d-6fd1146d7675",
//     amount: 755,
//     url: "https://sandboxsales.connexpay.com/app/v1/HostedPaymentPage/c634c47c-478f-4709-ad9d-6fd1146d7675",
//     expiration: "2021-06-12T13:44:29.03",
//     used: false,
//     orderNumber: "06072021-55420",
//     name: "",
//     email: null,
//     emailDate: null,
//   },
//   {
//     amount: 100,
//     url: "https://sandboxsales.connexpay.com/#!/HostedPaymentPage/5362627b-bf21-4033-bd79-1fc6703f2a4e",
//     status: "Unpaid",
//     orderNumber: "tes order",
//     email: "nathan.roberts@example.com",
//     name: "Cameron Williamson",
//     label: "FL",
//     emailDate: "2021-07-30T23:59:59",
//     expiration: "2021-07-30T23:59:59",
//   },
// ];

const PaymentLink = (props) => {
  const { history } = props;

  const [tableData, setTableData] = useState([]);
  const [tableParams, setTableParams] = useState({
    pageCurrent: 1,
    pageSize: 10,
    pageTotal: 1,
  });
  const [tableFilter, setTableFilter] = useState(useSelector((state) => state.initialFilterPaymentLink) ?? {
    name: "",
    customerID: undefined,
    orderNumber: "",
    used: undefined,
    amountFrom: "",
    amountTo: "",
    expirationFrom: "",
    expirationTo: "",
  });
  const [isLoading, setIsLoading] = useState(false);
  const [modalEditShow, setModalEditShow] = useState(false);
  const [optLabels, setOptLabels] = useState([]);
  // const [startDate, setStartDate] = useState(new Date());
  const [modalEmailShow, setModalEmailShow] = useState(false);
  const [modalFiles, setModalFiles] = useState(false);
  const [modalRowData, setModalRowData] = useState();
  // const [modalEmailData, setModalEmailData] = useState(false);
  const [modalFilter, setModalFilter] = useState(false);

  // const ref = React.createRef();
  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);
  const userData = useSelector((state) => state.userData);
  const globalLabel = useSelector((state) => state.globalLabel);
  const merchantSettings = userData?.merchantSettings;

  useEffect(() => {
    if (!isEmpty(userData)) {
      const arrOptLabels = mapOptLabels(userData?.labelHierarchy);
      setOptLabels(arrOptLabels);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  const fetchPaymentLinks = async () => {
    try {
      setIsLoading(true);
      const payload = {
        labelIds: globalLabel?.map((val) => val),
        ...tableFilter,
      };
      const res = await briGetPaymentLinks(
        payload,
        tableParams?.pageCurrent,
        tableParams?.pageSize
      );
      if (res) {
        setTableData(res?.searchResults);
        if (res?.totalCount) {
          setTableParams({
            ...tableParams,
            pageCurrent: res?.currentPage,
            pageTotal: res?.totalPageCount,
          });
        } else {
          setTableParams({
            ...tableParams,
            pageTotal: 1,
          });
        }
      }
    } catch (error) {
      // console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (!userData?.merchantSettings?.showPaymentLinks) {
      history.push("/");
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  useEffect(() => {
    if (!isEmpty(globalLabel)) {
      fetchPaymentLinks();
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData, globalLabel, tableFilter, tableParams?.pageCurrent]);

  const handleChangePage = (page) => {
    setIsLoading(true);
    setTableParams({
      ...tableParams,
      pageCurrent: page,
    });
    setIsLoading(false);
  };

  const handleClickCopy = (paymentLink) => {
    const toasterConfig = {
      show: true,
      type: "success",
      title: "Success",
      body: " Copied link to clipboard.",
    };
    copy(paymentLink, {
      format: "text/plain",
      onCopy: () =>
        dispatch({ type: "set", toaster: [...toaster, toasterConfig] }),
    });
  };

  const handleShowModalEdit = (rowData) => {
    setModalRowData(rowData);
    setModalEditShow(true);
  };

  const handleShowModalEmail = (rowData) => {
    setModalRowData(rowData);
    setModalEmailShow(true);
  };

  const handleShowModalFiles = (rowData) => {
    setModalRowData(rowData);
    setModalFiles(true);
  };

  const handleShowModalFilter = () => {
    setModalFilter(true);
  };

  const handleHideModelFilter = () => {
    setModalFilter(false);
  };

  const handleSubmitFilter = (values) => {
    setTableFilter(values);
  };

  const reloadTableData = () => {
    setTableFilter({});
    setTableParams({ ...tableParams, pageCurrent: 1 });
    dispatch({
      type: "set",
      initialFilterPaymentLink: {}
    });
  };
  return (
    <>
      <CCard>
        <CCardHeader className="d-md-flex flex-md-row justify-content-between align-items-center">
          <h4 className="mr-2 mb-md-0">Payment Link</h4>
          <div>
            <CTooltip
              placement="bottom-end"
              content="Payment links can take upto 10s to appear in the grid. Click here to refresh if you do not see your payment link"
            >
              <CButton
                color="light"
                className="px-3 py-2 mx-1"
                onClick={reloadTableData}
              >
                <CIcon name="cil-reload" height={12} />
              </CButton>
            </CTooltip>
            <CButton
              color="primary"
              className="px-4 py-2 mx-md-1 w-48 w-md-unset"
              onClick={handleShowModalFilter}
            >
              <CIcon className="mr-2" name="cil-filter" height={12} />
              Filter
            </CButton>
          </div>
        </CCardHeader>
        <CCardBody>
          <div className="table-not-relative">
            <CDataTable
              loading={isLoading}
              items={tableData}
              fields={[
                "orderNumber",
                "amount",
                "status",
                "name",
                "customerID",
                "email",
                "emailDate",
                "expiration",
                ...(merchantSettings?.showLabels ? ["labelIds"] : []),
                "action",
              ]}
              itemsPerPage={10}
              noItemsViewSlot={<div className="my-5" />}
              columnHeaderSlot={{
                Email: "Email Address",
                editUser: <div className="text-center">Edit User</div>,
                orderNumber: <div className="text-center">Order Number</div>,
                labelIds: <div>Label</div>,
                url: <div style={{ width: "200px" }}>URL</div>,
                action: <div />,
              }}
              scopedSlots={{
                amount: (item) => (
                  <td className="text-center">{formatUSD(item?.amount)}</td>
                ),
                url: (item) => (
                  <td>
                    <div className="d-flex justify-content-between">
                      <a href={item.url} target="_blank" rel="noreferrer">
                        {midStringEllipsis(item?.url)}
                      </a>
                      <div className="d-flex"></div>
                    </div>
                  </td>
                ),
                labelIds: (item) => {
                  const currLabel =
                    optLabels?.find((val) => item?.labelIds?.[0] === val.value)
                      ?.label ?? "-";
                  return (
                    <td>
                      <CTooltip
                        placement="bottom-end"
                        content={currLabel}
                        className="d-none"
                      >
                        <span>{endStringEllipsis(currLabel, 25)}</span>
                      </CTooltip>
                    </td>
                  );
                },
                email: (item) => {
                  const currText = item?.email || "";
                  return <td>{endStringEllipsis(currText, 35)}</td>;
                },
                orderNumber: (item) => {
                  const currText = item?.orderNumber || "";
                  return (
                    <td className="text-center">
                      <CTooltip
                        placement="bottom-end"
                        content={currText}
                        className="d-none"
                      >
                        <span>{endStringEllipsis(currText, 20)}</span>
                      </CTooltip>
                    </td>
                  );
                },
                customerID: (item) => {
                  const currText = item?.customerID || "";
                  return (
                    <td>
                      <CTooltip
                        placement="bottom-end"
                        content={currText}
                        className="d-none"
                      >
                        <span>{endStringEllipsis(currText, 20)}</span>
                      </CTooltip>
                    </td>
                  );
                },
                name: (item) => {
                  const currText = item?.name || "";
                  return (
                    <td>
                      <CTooltip
                        placement="bottom-end"
                        content={currText}
                        className="d-none"
                      >
                        <span>{endStringEllipsis(currText, 25)}</span>
                      </CTooltip>
                    </td>
                  );
                },
                status: (item) => <td>{item?.used ? "Paid" : "Unpaid"}</td>,
                emailDate: (item) => (
                  <td>
                    {item.emailDate && format(parseISO(item.emailDate), "P")}
                  </td>
                ),
                expiration: (item) => (
                  <td>
                    {item.expiration && format(parseISO(item.expiration), "P")}
                  </td>
                ),
                action: (item) => (
                  <td>
                    {!item?.used && (
                      <CTooltip placement="bottom-end" content="Email URL">
                        <CIcon
                          onClick={() => handleShowModalEmail(item)}
                          className="ml-3 text-primary hover-pointer"
                          name="cil-mail"
                        />
                      </CTooltip>
                    )}
                    <CTooltip placement="bottom-end" content="Copy URL">
                      <CIcon
                        onClick={() => handleClickCopy(item?.url)}
                        className="ml-3 text-primary hover-pointer"
                        name="cil-copy"
                      />
                    </CTooltip>
                    <CTooltip placement="bottom-end" content="Files">
                      <CIcon
                        onClick={() => handleShowModalFiles(item)}
                        className="ml-3 text-primary hover-pointer"
                        name="cil-file"
                      />
                    </CTooltip>
                    {!item?.used && (
                      <CTooltip placement="bottom-end" content="Edit Details">
                        <CIcon
                          onClick={() => handleShowModalEdit(item)}
                          className="ml-3 text-primary hover-pointer"
                          name="cil-pencil"
                        />
                      </CTooltip>
                    )}
                  </td>
                ),
              }}
            />
          </div>
          <CPagination
            pages={tableParams?.pageTotal}
            activePage={tableParams?.pageCurrent}
            onActivePageChange={(page) =>
              !isLoading ? handleChangePage(page) : null
            }
            className={tableParams?.pageTotal < 2 ? "d-none" : ""}
          />
        </CCardBody>
      </CCard>
      <FilterTableModal
        modalShow={modalFilter}
        handleHideModal={handleHideModelFilter}
        handleSubmitFilter={handleSubmitFilter}
      />
      <EditEmailModal
        modalShow={modalEmailShow}
        modalData={modalRowData}
        handleHideModal={() => setModalEmailShow(false)}
        handlePostSubmit={() => { }}
      />
      <EditDetailModal
        modalShow={modalEditShow}
        modalData={modalRowData}
        // activeCardGuid={activeCardGuid}
        handleHideModal={() => setModalEditShow(false)}
        handlePostSubmit={() => { fetchPaymentLinks(); }}
      />
      <ModalFiles
        modalShow={modalFiles}
        modalData={modalRowData}
        handleHideModal={() => setModalFiles(false)}
      />
    </>
  );
};

export default PaymentLink;
