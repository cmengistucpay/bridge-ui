import { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { isEmpty } from "lodash";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CAlert,
  CSpinner,
  CListGroup,
  CListGroupItem,
  CFormGroup,
  CInputCheckbox,
  CLabel,
} from "@coreui/react";
import { briGetHppFiles, briDeleteHppFiles } from "src/services";

const ModalFiles = (props) => {
  const { modalShow, modalData, handleHideModal } = props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);

  const [isLoadingGet, setIsLoadingGet] = useState(false);
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [isLoaded, setIsLoaded] = useState(false);
  const [files, setFiles] = useState([]);
  const [checkedFiles, setCheckedFiles] = useState([]);
  const [resAlert, setResAlert] = useState(null);

  useEffect(() => {
    if (modalShow) {
      _fetchHppFiles();
      setIsLoaded(false);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  const _fetchHppFiles = async () => {
    try {
      setIsLoadingGet(true);
      setResAlert(null);
      // const res = await briGetHppFiles(616);
      const res = await briGetHppFiles(modalData?.idHostedPaymentPageRequest);
      // const res = [
      //   {
      //     idHostedPaymentPageRequestFile: 6,
      //     idHostedPaymentPageRequest: 324,
      //     fileName: "TestFileUpload.pdf",
      //     storageUrl:
      //       "https://cpaystagebridge.blob.core.windows.net/hostedpaymentpagefiles/324/TestFileUpload_2021-09-21-T20:03:10.055.pdf",
      //     approvedBy: null,
      //     approvalDate: null,
      //     isDeleted: false,
      //     deletedBy: null,
      //     deletionDate: null,
      //   },
      //   {
      //     idHostedPaymentPageRequestFile: 6,
      //     idHostedPaymentPageRequest: 324,
      //     fileName: "TestFileUpload.pdf",
      //     storageUrl:
      //       "https://cpaystagebridge.blob.core.windows.net/hostedpaymentpagefiles/324/TestFileUpload_2021-09-21-T20:03:10.055.pdf",
      //     approvedBy: null,
      //     approvalDate: null,
      //     isDeleted: false,
      //     deletedBy: null,
      //     deletionDate: null,
      //   },
      // ];
      setFiles(res);
      setIsLoaded(true);
    } catch (error) {
      setResAlert({
        color: "danger",
        message: error?.response?.data?.message || "Unable to fetch HPP Files.",
      });
      console.error(error);
    } finally {
      setIsLoadingGet(false);
    }
  };

  const handleDeleteFiles = async () => {
    try {
      setIsSubmitting(true);
      const payload = checkedFiles;
      await briDeleteHppFiles(modalData?.idHostedPaymentPageRequest, payload);
      const toasterConfig = {
        show: true,
        type: "success",
        title: "Success",
        body: "Files successfully deleted.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      _fetchHppFiles();
      // const newSaleDescriptions = saleDescriptions?.filter(
      //   (val) => val.id !== saleDescId
      // );
      // dispatch({ type: "set", saleDescriptions: newSaleDescriptions });
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body: error?.response?.data?.message || "Unable to delete files.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      console.error(error);
    } finally {
      setIsSubmitting(false);
    }
  };

  const handleCheckFile = (e, item) => {
    const { checked } = e.target;

    console.log({ checked, item });
    let arrChecked = [];
    if (checked) {
      arrChecked = [...checkedFiles, item?.idHostedPaymentPageRequestFile];
    } else {
      arrChecked = checkedFiles?.filter(
        (val) => val !== item?.idHostedPaymentPageRequestFile
      );
    }

    setCheckedFiles(arrChecked);
  };

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>HPP Files</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <div>
          {isLoadingGet && (
            <div className="text-center my-2">
              <CSpinner color="primary" component="span" aria-hidden="true" />
            </div>
          )}
          {isLoaded && !isEmpty(files) && (
            <CListGroup className="mb-2">
              {files?.map((val, i) => (
                <CListGroupItem
                  className="d-flex justify-content-between"
                  key={i}
                >
                  <CFormGroup variant="custom-checkbox" inline>
                    <CInputCheckbox
                      custom
                      id={val?.idHostedPaymentPageRequestFile}
                      name={val?.idHostedPaymentPageRequestFile}
                      value={val?.idHostedPaymentPageRequestFile}
                      checked={checkedFiles?.includes(
                        val?.idHostedPaymentPageRequestFile
                      )}
                      onChange={(e) => handleCheckFile(e, val)}
                    />
                    <CLabel
                      variant="custom-checkbox"
                      htmlFor={val?.idHostedPaymentPageRequestFile}
                    >
                      <a
                        href={val?.storageUrl}
                        alt={val?.fileName}
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        {val?.fileName}
                      </a>
                    </CLabel>
                  </CFormGroup>
                </CListGroupItem>
              ))}
            </CListGroup>
          )}
          {isLoaded && isEmpty(files) && (
            <p className="mb-0">
              There are no files uploaded on this Payment Link
            </p>
          )}
          {!isEmpty(checkedFiles) && (
            <CButton
              disabled={isSubmitting}
              onClick={handleDeleteFiles}
              color="danger"
              className="mx-1 px-2"
              size="sm"
            >
              Delete
            </CButton>
          )}
          {resAlert && (
            <CAlert color={resAlert?.color} className="mt-2 mb-0">
              {resAlert?.message}
            </CAlert>
          )}
        </div>
      </CModalBody>
      <CModalFooter>
        <CButton
          onClick={handleHideModal}
          color="light"
          className="mx-1 w-50 w-md-unset"
        >
          Close
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default ModalFiles;
