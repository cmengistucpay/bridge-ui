import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CRow,
  CCol,
  CButton,
  CFormGroup,
  CLabel,
  CInput,
  CInvalidFeedback,
} from "@coreui/react";
import { useFormik } from "formik";
import * as Yup from "yup";
import Select from "react-select";
import NumberFormat from "react-number-format";
import { useSelector, useDispatch } from "react-redux";

const optStatus = [
  { value: true, label: "Paid" },
  { value: false, label: "Unpaid" },
];

const validationSchema = () => {
  return Yup.object().shape({});
};

const FilterTableModal = (props) => {
  const { modalShow, handleHideModal, handleSubmitFilter } = props;

  const dispatch = useDispatch();

  const initialValues = useSelector((state) => state.initialFilterPaymentLink) ?? {
    name: "",
    customerID: undefined,
    orderNumber: "",
    used: undefined,
    amountFrom: "",
    amountTo: "",
    expirationFrom: "",
    expirationTo: "",
  };

  const handleSubmitForm = (values) => {
    dispatch({
      type: "set",
      initialFilterPaymentLink:values
    });
    handleSubmitFilter(values);
    handleHideModal();
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitForm,
  });

  const resetSearchValue = () => {
    formik.setFieldValue("name", "");
    formik.setFieldValue("customerID", undefined);
    formik.setFieldValue("orderNumber", "");
    formik.setFieldValue("used", undefined);
    formik.setFieldValue("amountFrom", "");
    formik.setFieldValue("amountTo", "");
    formik.setFieldValue("expirationFrom", "");
    formik.setFieldValue("expirationTo", "");
    dispatch({
      type: "set",
      initialFilterPaymentLink:{}
    });
  };
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    setFieldValue,
    handleSubmit
  } = formik;

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Filter Table</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <div>
          <CFormGroup>
            <CLabel htmlFor="name">Name</CLabel>
            <CInput
              autoComplete="none"
              name="name"
              type="text"
              value={values.name}
              placeholder="Enter Name"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.name && !!errors.name}
            />
            <CInvalidFeedback>{errors.name}</CInvalidFeedback>
          </CFormGroup>
          <CFormGroup>
            <CLabel htmlFor="customerID">Customer ID</CLabel>
            <CInput
              autoComplete="none"
              name="customerID"
              type="text"
              value={values.customerID}
              placeholder="Enter Customer ID"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.customerID && !!errors.customerID}
            />
            <CInvalidFeedback>{errors.customerID}</CInvalidFeedback>
          </CFormGroup>
          <CFormGroup>
            <CLabel htmlFor="orderNumber">Order Number</CLabel>
            <CInput
              autoComplete="none"
              name="orderNumber"
              type="text"
              value={values.orderNumber}
              placeholder="Enter Order Number"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.orderNumber && !!errors.orderNumber}
            />
            <CInvalidFeedback>{errors.orderNumber}</CInvalidFeedback>
          </CFormGroup>
          <CFormGroup>
            <CLabel htmlFor="Status">Status</CLabel>
            <Select
              classNamePrefix="default-select"
              value={optStatus.find((val) => val.value === values.used) || ""}
              options={optStatus}
              onChange={(selected) => {
                setFieldValue("used", selected.value);
              }}
              placeholder="Select Status"
              components={{
                IndicatorSeparator: () => null,
              }}
            />
            <CInvalidFeedback>{errors.used}</CInvalidFeedback>
          </CFormGroup>
          <CFormGroup>
            <CLabel htmlFor="Amount">Amount</CLabel>
            <CRow>
              <CCol xs="6">
                <NumberFormat
                  name="amountFrom"
                  value={values.amountFrom}
                  onValueChange={(val) =>
                    setFieldValue("amountFrom", val.floatValue)
                  }
                  prefix="$"
                  thousandSeparator
                  customInput={CInput}
                  placeholder="Amount From"
                  onBlur={handleBlur}
                  invalid={touched.amountFrom && !!errors.amountFrom}
                />
              </CCol>
              <CCol xs="6">
                <NumberFormat
                  name="amountTo"
                  value={values.amountTo}
                  onValueChange={(val) =>
                    setFieldValue("amountTo", val.floatValue)
                  }
                  prefix="$"
                  thousandSeparator
                  customInput={CInput}
                  placeholder="Amount To"
                  onBlur={handleBlur}
                  invalid={touched.amountTo && !!errors.amountTo}
                />
              </CCol>
            </CRow>
            <CInvalidFeedback>{errors.Amount}</CInvalidFeedback>
          </CFormGroup>
          <CFormGroup>
            <CLabel htmlFor="TimeStamp">Expiration Date</CLabel>
            <CRow>
              <CCol xs="6">
                <CInput
                  autoComplete="none"
                  name="expirationFrom"
                  type="date"
                  value={values.expirationFrom}
                  placeholder="Date From"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={touched.expirationFrom && !!errors.expirationFrom}
                />
              </CCol>
              <CCol xs="6">
                <CInput
                  autoComplete="none"
                  name="expirationTo"
                  type="date"
                  value={values.expirationTo}
                  placeholder="Date To"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={touched.expirationTo && !!errors.expirationTo}
                />
              </CCol>
            </CRow>
            <CInvalidFeedback>{errors.TimeStamp}</CInvalidFeedback>
          </CFormGroup>
        </div>
        <div></div>
      </CModalBody>
      <CModalFooter>
        <CButton
          onClick={resetSearchValue}
          color="light"
          className="px-4 mx-1 w-50 w-md-unset"
        >
          Clear
        </CButton>
        <CButton
          onClick={handleSubmit}
          color="primary"
          className="px-4 mx-1 w-50 w-md-unset"
        >
          Submit
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default FilterTableModal;
