import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { format, parseISO } from "date-fns";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CForm,
  CFormGroup,
  CLabel,
  CInput,
  CInvalidFeedback,
  CButton,
  CAlert,
  CSpinner,
  CTooltip,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useFormik } from "formik";
import * as Yup from "yup";
import NumberFormat from "react-number-format";
import { briUpdatePaymentLink } from "src/services";

const tooltipInfoHelper = {
  expiration:
    "This is the date that the link will expire and will no longer work for accepting payment. Ensure that this value is greater than the Email Date",
};

const initialValues = {
  expiration: "",
  amount: "",
};

const validationSchema = (values) => {
  return Yup.object().shape({
    amount: Yup.number()
      .min(1, "Must be more than $ 1")
      .max(999999, "Must be less than $ 999,999")
      .required("amount is required"),
  });
};

const TransmissionModal = (props) => {
  const { modalShow, modalData, handleHideModal, handlePostSubmit } = props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  const handleSubmitForm = async (values) => {
    try {
      setIsSubmitting(true);
      setResAlert(null);
      const payload = {
        ...values,
        guid: modalData?.guid,
      };

      const res = await briUpdatePaymentLink(payload);
      if (res) {
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "Data Successfully Edited",
        };

        dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
        resetForm();
        handleHideModal();
        handlePostSubmit();
      }
    } catch (error) {
      setResAlert({
        color: "danger",
        message:
          error?.response?.data?.message ||
          error?.response?.data ||
          "Network error.",
      });
    } finally {
      setIsSubmitting(false);
    }
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitForm,
  });
  const {
    values,
    touched,
    errors,
    setFieldValue,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
  } = formik;

  useEffect(() => {
    setResAlert(null);

    if (modalData) {
      setFieldValue("amount", modalData?.amount);
      setFieldValue(
        "expiration",
        format(parseISO(modalData?.expiration), "yyyy-MM-dd")
      );
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Details</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CForm>
          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="expiration">Link Expiration Date</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["expiration"]}
              >
                <CIcon
                  tabIndex="-1"
                  className="mr-3"
                  name="cil-info-circle"
                  height={14}
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="expiration"
              type="date"
              value={values.expiration}
              placeholder="Enter Link Expiration Date"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.expiration && !!errors.expiration}
            />
            <CInvalidFeedback>{errors.expiration}</CInvalidFeedback>
          </CFormGroup>

          <CFormGroup>
            <CLabel htmlFor="amount">
              Amount<span className="label-required">*</span>
            </CLabel>
            <NumberFormat
              name="amount"
              value={values.amount}
              onValueChange={(val) => setFieldValue("amount", val.floatValue)}
              prefix="$"
              thousandSeparator
              customInput={CInput}
              placeholder="Enter Amount"
              onBlur={handleBlur}
              invalid={touched.amount && !!errors.amount}
            />
            <CInvalidFeedback>{errors.amount}</CInvalidFeedback>
          </CFormGroup>

          <div>
            {resAlert && (
              <CAlert color={resAlert?.color} className="mt-2 mb-0">
                {resAlert?.message}
              </CAlert>
            )}
          </div>
        </CForm>
      </CModalBody>
      <CModalFooter>
        <CButton color="light" className="px-4" onClick={handleHideModal}>
          Cancel
        </CButton>
        <CButton
          disabled={isSubmitting}
          color="primary"
          className="px-4"
          onClick={handleSubmit}
          type="submit"
        >
          {!isSubmitting ? (
            "Submit"
          ) : (
            <>
              <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
              Loading...
            </>
          )}
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default TransmissionModal;
