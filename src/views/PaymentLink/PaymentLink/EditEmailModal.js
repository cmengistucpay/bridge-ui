import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { format, parseISO } from "date-fns";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CForm,
  CFormGroup,
  CLabel,
  CInput,
  CTextarea,
  CInvalidFeedback,
  CButton,
  CAlert,
  CSpinner,
  CTooltip,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { isEmail } from "src/helper";
import { briEmailPaymentLink } from "src/services";

const tooltipInfoHelper = {
  scheduledDate:
    "This is the date that the link will be emailed automatically to the recipients address",
};

const initialValues = {
  recipient: "",
  sender: "",
  scheduledDate: "",
  subject: "",
  message: "",
};

const validationSchema = (values) => {
  return Yup.object().shape({
    recipient: Yup.string().required("Email Recipient is required"),
    subject: Yup.string().required("Subject is required"),
    message: Yup.string().required("Message is required"),
  });
};

const EditEmailModal = (props) => {
  const { modalShow, modalData, handleHideModal, handlePostSubmit } = props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  const handleSubmitForm = async (values) => {
    try {
      setIsSubmitting(true);
      setResAlert(null);
      const payload = {
        ...values,
      };
      console.log({ payload });
      await briEmailPaymentLink(modalData?.guid, payload);
      const toasterConfig = {
        show: true,
        type: "success",
        title: "Success",
        body: "Email Successfully Scheduled",
      };

      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      resetForm();
      handleHideModal();
      handlePostSubmit();
    } catch (error) {
      setResAlert({
        color: "danger",
        message:
          error?.response?.data?.message ||
          error?.response?.data ||
          "Network error.",
      });
      console.error(error);
    } finally {
      setIsSubmitting(false);
    }
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitForm,
  });
  const {
    values,
    touched,
    errors,
    setFieldValue,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
  } = formik;

  useEffect(() => {
    setResAlert(null);

    if (modalData) {
      setFieldValue("recipient", modalData?.email);
      setFieldValue(
        "scheduledDate",
        modalData?.emailDate
          ? format(parseISO(modalData?.emailDate), "yyyy-MM-dd")
          : ""
      );
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Email Details</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CForm>
          <CFormGroup>
            <CLabel htmlFor="recipient">
              Recipient’s Email Address<span className="label-required">*</span>
            </CLabel>
            <CInput
              autoComplete="none"
              name="recipient"
              type="text"
              value={values.recipient}
              placeholder="Enter Recipient’s Email Address"
              onChange={handleChange}
              onBlur={handleBlur}
              valid={touched.recipient && isEmail(values.recipient)}
              invalid={touched.recipient && !!errors.recipient}
            />
            <CInvalidFeedback>{errors.recipient}</CInvalidFeedback>
          </CFormGroup>

          <CFormGroup>
            <CLabel htmlFor="sender">Sender’s Email Address</CLabel>
            <CInput
              autoComplete="none"
              name="sender"
              type="text"
              value={values.sender}
              placeholder="Enter Sender’s Email Address"
              onChange={handleChange}
              onBlur={handleBlur}
              valid={touched.sender && isEmail(values.sender)}
              invalid={touched.sender && !!errors.sender}
            />
            <CInvalidFeedback>{errors.sender}</CInvalidFeedback>
          </CFormGroup>

          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="scheduledDate">Email Date</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["scheduledDate"]}
              >
                <CIcon
                  tabIndex="-1"
                  className="mr-3"
                  name="cil-info-circle"
                  height={14}
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="scheduledDate"
              type="date"
              value={values.scheduledDate}
              placeholder="Enter Email Date"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.scheduledDate && !!errors.scheduledDate}
            />
            <CInvalidFeedback>{errors.scheduledDate}</CInvalidFeedback>
          </CFormGroup>

          <CFormGroup>
            <CLabel htmlFor="subject">
              Email Subject<span className="label-required">*</span>
            </CLabel>
            <CInput
              autoComplete="none"
              name="subject"
              type="text"
              value={values.subject}
              placeholder="Enter Email Subject"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.subject && !!errors.subject}
            />
            <CInvalidFeedback>{errors.subject}</CInvalidFeedback>
          </CFormGroup>

          <CFormGroup>
            <CLabel htmlFor="message">
              Email Message<span className="label-required">*</span>
            </CLabel>
            <CTextarea
              name="message"
              rows="4"
              value={values.message}
              placeholder="Enter Email Message"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.message && !!errors.message}
            />
            <CInvalidFeedback>{errors.message}</CInvalidFeedback>
          </CFormGroup>

          <div>
            {resAlert && (
              <CAlert color={resAlert?.color} className="mt-2 mb-0">
                {resAlert?.message}
              </CAlert>
            )}
          </div>
        </CForm>
      </CModalBody>
      <CModalFooter>
        <CButton color="light" className="px-4" onClick={handleHideModal}>
          Cancel
        </CButton>
        <CButton
          disabled={isSubmitting}
          color="primary"
          className="px-4"
          onClick={handleSubmit}
          type="submit"
        >
          {!isSubmitting ? (
            "Submit"
          ) : (
            <>
              <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
              Loading...
            </>
          )}
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default EditEmailModal;
