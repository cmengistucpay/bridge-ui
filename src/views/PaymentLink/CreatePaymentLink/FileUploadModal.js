import { useState, useRef } from "react";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CAlert,
  CListGroup,
  CListGroupItem,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";

const FileUploadModal = (props) => {
  const {
    modalShow,
    modalData,
    handleDeleteFile,
    handleHideModal,
    handlePostUpload,
  } = props;

  const [resAlert, setResAlert] = useState(null);

  const inputFile = useRef(null);

  const handleUploadCsv = () => {
    inputFile.current.click();
  };

  const handleFileSelected = (e) => {
    try {
      const files = e.target.files?.[0];
      if (files) {
        handlePostUpload(files);
      }
    } catch (error) {
      setResAlert({
        color: "danger",
        message: error?.response?.data?.message || "Network error.",
      });
      console.error(error);
    }
  };

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Payment Link Files</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <div className="d-flex align-items-center">
          <p className="mb-0">
            Upload up to ten PDFs with a maximum size of 10MB each. These files
            will be displayed to the customer in their hosted payment page for
            their approval.
          </p>
        </div>
        <div className="border-1">
          <CButton
            disabled={modalData?.length >= 10}
            onClick={handleUploadCsv}
            color="secondary"
            size="sm"
            className="mt-2 w-50 w-md-unset"
          >
            Upload File
          </CButton>
          <input
            accept=".pdf"
            ref={inputFile}
            className="d-none"
            type="file"
            onChange={handleFileSelected}
          />
        </div>
        <CListGroup className="mt-2">
          {modalData?.map((val, i) => (
            <CListGroupItem className="d-flex justify-content-between" key={i}>
              {val?.name}
              <CIcon
                onClick={() => handleDeleteFile(val)}
                className="text-danger hover-pointer"
                name="cis-x"
              />
            </CListGroupItem>
          ))}
        </CListGroup>

        <div>
          {resAlert && (
            <CAlert color={resAlert?.color} className="mt-2 mb-0">
              {resAlert?.message}
            </CAlert>
          )}
        </div>
      </CModalBody>
      <CModalFooter>
        {/* <CSVLink filename="sample-file.csv" data={csvData}>
          <CButton color="light" className="mx-1 w-50 w-md-unset">
            Download Sample File
          </CButton>
        </CSVLink> */}

        <CButton
          onClick={handleHideModal}
          color="primary"
          className="mx-1 px-3 w-50 w-md-unset"
        >
          Done
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default FileUploadModal;
