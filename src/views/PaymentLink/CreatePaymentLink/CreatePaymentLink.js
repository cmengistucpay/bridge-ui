import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { isEmpty } from "lodash";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CForm,
  CButton,
  CAlert,
  CSpinner,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import copy from "copy-to-clipboard";
import { useFormik } from "formik";
import * as Yup from "yup";

import { salPostHppRequest, briPostHppFiles } from "src/services";
import SalesForm from "./SalesForm";
import MailModal from "src/views/PaymentLink/CreatePaymentLink/MailModal";
import FileUploadModal from "src/views/PaymentLink/CreatePaymentLink/FileUploadModal";

const appHost = process.env.REACT_APP_HOST;

const rootHPP = (host) => {
  switch (true) {
    case host?.includes("sandbox"):
      return "https://sandboxsales.connexpay.com";
    case host?.includes("stage"):
    case host?.includes("localhost"):
      return "https://stagesales.connexpay.com";
    default:
      return "https://sales.connexpay.com";
  }
};

const initialValues = {
  Amount: "",
  OrderNumber: "",
  CustomerId: "",
  ExpirationDate: "",
  Description: "",
  AllowPaymentACH: false,
  // AllowPaymentCreditCard: true,
  RiskAnalysis: false,
  // AllowGooglePlay: true,
  LabelIds: "",
  FirstName: "",
  LastName: "",
  StatementDescription: "",
  ConnexPayTransaction: {
    ExpectedPayments: "",
  },
  RiskData: {
    Email: "",
    Gender: "",
    DateOfBirth: "",
    ProductType: "",
    ProductItem: "",
    ProductDesc: "",
  },
};

const validationSchema = (values) => {
  return Yup.object().shape({
    Amount: Yup.number()
      .min(0.5, "Must be at least $0.50")
      .max(999999.99, "Must be less than $ 999,999.99")
      .required("Amount is required"),
    ExpirationDate: Yup.date().min(
      new Date(),
      "Expiration Date must be equal or greater than today"
    ),
    OrderNumber: Yup.string()
      .max(32, "Maximum 32 characters")
      .matches(
        /^[ A-Za-z0-9'|.-]*$/,
        "Only letters & numbers, -, ., | and ' are allowed"
      ),
    CustomerId: Yup.string()
      .max(32, "Maximum 32 characters")
      .matches(
        /^[ A-Za-z0-9'|.-]*$/,
        "Only letters & numbers, -, ., | and ' are allowed"
      ),
    Description: Yup.string()
      .max(2048, "Maximum 2048 characters")
      .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
    StatementDescription: Yup.string()
      .max(25, "Maximum 25 characters")
      .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
    FirstName: Yup.string()
      .min(2, "Minimum 2 characters")
      .max(30, "Maximum 30 characters")
      .required("First Name is required")
      .matches(/^[ A-Za-z]*$/, "Only letters & whitespace are allowed"),
    LastName: Yup.string()
      .min(2, "Minimum 2 characters")
      .max(30, "Maximum 30 characters")
      .required("Last Name is required")
      .matches(/^[ A-Za-z]*$/, "Only letters & whitespace are allowed"),
    ConnexPayTransaction: Yup.object().shape({
      ExpectedPayments: Yup.number()
        .min(0, "Must be greater than or equals to 0")
        .max(99, "Must be less than 100")
        .when('RiskAnalysis', {
          is: false,
          then: Yup.number().required("Expected Payments is required"),
          otherwise: Yup.number()
        })
    }),
    RiskData: Yup.object().shape({
      Email: Yup.string().email("Email format is invalid"),
      ProductType: Yup.string()
        .max(50, "Maximum 50 characters")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed")
        .required("Product Type is required"),
      ProductItem: Yup.string()
        .max(50, "Maximum 50 characters")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed")
        .required("Product Item is required"),
      ProductDesc: Yup.string()
        .max(50, "Maximum 50 characters")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
      DateOfBirth: Yup.date().max(
        new Date(),
        "Date of Birth must be less than today"
      ),
    }),
  });
};

const CreditCardSale = (props) => {
  const { modalShow, handleHideModal, accDevice } = props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);
  const userData = useSelector((state) => state.userData);
  const merchantSettings = userData?.merchantSettings;

  const [activeForm, setActiveForm] = useState("sales");
  const [modalFilesShow, setModalFilesShow] = useState(false);
  const [modalFilesData, setModalFilesData] = useState([]);
  const [modalMailShow, setModalMailShow] = useState(false);
  const [modalMailData, setModalMailData] = useState(null);
  const [modalLinkShow, setModalLinkShow] = useState(false);
  const [modalLinkData, setModalLinkData] = useState("");
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  const handleSubmitForm = async (values) => {
    if (
      !values?.RiskAnalysis &&
      typeof values?.ConnexPayTransaction?.ExpectedPayments !== 'number'
    ) {
      setFieldError(
        "ConnexPayTransaction.ExpectedPayments",
        "Expected Payments is required"
      );
      return;
    }
    try {
      setIsSubmitting(true);
      setResAlert(null);

      const { allowCreditCardPayment, allowGooglePay } = merchantSettings;
      let tenderType = [];
      const logoUrl = "https://connexpay.com/wp-content/uploads/2018/10/newlogo.png";

      if (allowCreditCardPayment) {
        tenderType = [...tenderType, "Credit"];
      }

      if (allowGooglePay) {
        tenderType = [...tenderType, "GooglePay"];
      }

      if (values?.AllowPaymentACH) {
        tenderType = [...tenderType, "ACH"];
      }

      const payload = {
        MerchantName: "ACME Screw",
        Description: values?.RiskData?.ProductDesc,
        LogoUrl: merchantSettings.clientLogoUrl ? merchantSettings.clientLogoUrl : logoUrl,
        TenderTypeOptions: tenderType,
        StatementDescription: values?.StatementDescription,
        sale: {
          CustomerId: values?.CustomerId,
          LabelIds: merchantSettings?.showLabels
            ? [values?.LabelIds]
            : undefined,
          DeviceGuid: accDevice?.guid,
          Amount: values?.Amount,
          IncludeRiskAnalysis: values?.RiskAnalysis,
          OrderNumber: values?.OrderNumber,
          ConnexPayTransaction: !values?.RiskAnalysis
            ? {
                ExpectedPayments:
                  values?.ConnexPayTransaction?.ExpectedPayments,
              }
            : undefined,
          Customer: {
            FirstName: values?.FirstName,
            LastName: values?.LastName,
            Email: values?.RiskData?.Email || undefined,
            DateOfBirth: values?.RiskData?.DateOfBirth || undefined,
          },
          RiskData: {
            ...values?.RiskData,
            Name: `${values?.FirstName || ""} ${values?.LastName || ""}`,
            OrderNumber: values?.OrderNumber,
            // ProductPrice: 100,
          },
        },
      };

      const hppTokenRequestPayload = values.ExpirationDate
        ? { ...payload, expiration: values.ExpirationDate }
        : { ...payload };

      const res = await salPostHppRequest(hppTokenRequestPayload);
      if (res) {
        if (!isEmpty(modalFilesData) && res?.idHostedPaymentPageRequest) {
          const formData = new FormData();
          modalFilesData.forEach((val, i) =>
            formData.append("files", modalFilesData[i])
          );
          await briPostHppFiles(res?.idHostedPaymentPageRequest, formData);
        }
        resetForm();
        setActiveForm("sales");
        handleHideModal();
        handleShowModalLink(
          `${rootHPP(appHost)}/#!/HostedPaymentPage/${res?.tempToken}`
        );
        setModalMailData(res);
      }
    } catch (error) {
      setResAlert({
        color: "danger",
        message: error?.response?.data?.message || "File upload failed. Make sure it's a PDF file less than 10 MB in size.",
      });
      // console.error(error);
    } finally {
      setIsSubmitting(false);
    }
  };

  const handleShowModalLink = (paymentLink) => {
    setModalLinkShow(true);
    setModalLinkData(paymentLink);
  };

  const handleHideModalLink = () => {
    setModalLinkShow(false);
    setModalLinkData("");
  };

  const handleShowModalFiles = () => {
    setModalFilesShow(true);
  };

  const handleHideModalFiles = () => {
    setModalFilesShow(false);
  };

  const handleUploadFiles = (files) => {
    setModalFilesData([...modalFilesData, files]);
  };

  const handleDeleteFile = (file) => {
    const arrFileData = modalFilesData.filter((val) => val.name !== file.name);
    setModalFilesData(arrFileData);
  };

  const validateForm = (values) => {
    let errors = {};
    if (merchantSettings?.showLabels && !values?.LabelIds) {
      errors = {
        LabelIds: "Label is required",
      };
    }

    return errors;
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitForm,
    validate: validateForm,
  });
  const { setFieldError, resetForm, handleSubmit } = formik;

  useEffect(() => {
    setResAlert(null);
    setModalFilesData([]);
    // setFieldValue("LabelIds", globalLabel?.[0] || "");

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  const handleBackForm = () => {
    if (activeForm === "flight") {
      setActiveForm("customer");
    }
    if (activeForm === "customer") {
      setActiveForm("sales");
    }
  };

  const handleClickCopy = (paymentLink) => {
    const toasterConfig = {
      show: true,
      type: "success",
      title: "Success",
      body: " Copied link to clipboard.",
    };
    copy(paymentLink, {
      format: "text/plain",
      onCopy: () =>
        dispatch({ type: "set", toaster: [...toaster, toasterConfig] }),
    });
  };

  const renderModalLink = () => {
    return (
      <CModal
        className="modal-sidebar"
        show={modalLinkShow}
        onClose={handleHideModalLink}
      >
        <CModalHeader closeButton>
          <CModalTitle>Payment Link</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <div className="d-flex justify-content-between align-items-center">
            <a
              className="text-secondary wrap-anywhere"
              href={modalLinkData}
              target="_blank"
              rel="noreferrer"
            >
              {modalLinkData}
            </a>
            <div className="d-flex ml-2">
              <div
                className="copy-button hover-pointer mr-1"
                onClick={() => handleClickCopy(modalLinkData)}
              >
                <CIcon name="cil-copy" className="text-secondary" />
              </div>
              {/* <div
                className="copy-button hover-pointer ml-1"
                onClick={() => setModalMailShow(true)}
              >
                <CIcon name="cil-mail" className="text-secondary" />
              </div> */}
            </div>
          </div>
        </CModalBody>
      </CModal>
    );
  };

  return (
    <>
      <CModal
        className="modal-sidebar"
        show={modalShow}
        onClose={handleHideModal}
        size="lg"
      >
        <CModalHeader closeButton>
          <CModalTitle>{`Enter Payment Link Information`}</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CForm>
            <div>
              {activeForm === "sales" && (
                <SalesForm
                  formik={formik}
                  showModalFiles={handleShowModalFiles}
                  filesData={modalFilesData}
                  userData={userData}
                />
              )}
              {/* {activeForm === "customer" && <CustomerForm formik={formik} />}
              {activeForm === "flight" && <FlightForm formik={formik} />} */}
            </div>

            {activeForm === "customer" && (
              <CButton
                color="primary"
                className="d-sm-none px-4 py-2 w-100"
                onClick={() => setActiveForm("flight")}
                variant="outline"
              >
                Add Flight Data
              </CButton>
            )}

            <div className="mt-2 d-flex justify-content-between">
              <div>
                {/* {activeForm === "sales" && (
                  <CButton
                    color="primary"
                    className="px-4 py-2"
                    onClick={() => setActiveForm("customer")}
                    variant="outline"
                  >
                    Add More Detail
                  </CButton>
                )} */}
                {/* {activeForm === "customer" && (
                  <CButton
                    color="primary"
                    className="d-none d-sm-block px-4 py-2"
                    onClick={() => setActiveForm("flight")}
                    variant="outline"
                  >
                    Add Flight Data
                  </CButton>
                )} */}
              </div>
              <div className="d-flex">
                {activeForm !== "sales" && (
                  <CButton
                    disabled={isSubmitting}
                    color="light"
                    className="px-4 py-2 mx-2"
                    onClick={handleBackForm}
                  >
                    Back
                  </CButton>
                )}
                <CButton
                  disabled={isSubmitting}
                  color="primary"
                  className="px-4 py-2"
                  onClick={handleSubmit}
                  type="submit"
                >
                  {!isSubmitting ? (
                    "Generate Link"
                  ) : (
                    <>
                      <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
                      Loading...
                    </>
                  )}
                </CButton>
              </div>
            </div>
            <div>
              {resAlert && (
                <CAlert color={resAlert?.color} className="mt-3">
                  {resAlert?.message}
                </CAlert>
              )}
            </div>
          </CForm>
        </CModalBody>
      </CModal>
      {renderModalLink()}
      <MailModal
        modalShow={modalMailShow}
        modalData={modalMailData}
        // activeCardGuid={activeCardGuid}
        handleHideModal={() => setModalMailShow(false)}
        handlePostSubmit={handleHideModalLink}
      />
      <FileUploadModal
        modalShow={modalFilesShow}
        modalData={modalFilesData}
        handleDeleteFile={handleDeleteFile}
        handleHideModal={handleHideModalFiles}
        handlePostUpload={handleUploadFiles}
      />
    </>
  );
};

export default CreditCardSale;
