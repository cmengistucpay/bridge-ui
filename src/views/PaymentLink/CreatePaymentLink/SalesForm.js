import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { isEmpty } from "lodash";
import {
  CRow,
  CCol,
  CFormGroup,
  CLabel,
  CInput,
  CInvalidFeedback,
  CTooltip,
  CSwitch,
  CBadge,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import NumberFormat from "react-number-format";
import Select from "react-select";
import { isEmail, mapOptLabels, optGender } from "src/helper";

const tooltipInfoHelper = {
  riskInfo:
    "Provide this information to get a more accurate risk analysis score",
  orderNumber:
    "Client provided transaction ID associated with the order. Unique to you. Can be any combination of alpha/numeric characters",
  customerId:
    "This value acts as a secondary identifier in conjunction with OrderNumber. The value is searchable and reportable in the ConnexPay portal.",
  productType:
    "Generalized description of the item added passed as plain text. This could be flight, tour, hotel, etc.",
  productItem:
    "ConnexPay suggests clients submit a high level description such as One Way, Round Trip, Seven Nights, etc",
  expectedPayments:
    "The number of outbound payments that will be made to suppliers. If paying a single supplier the value is 1, if paying two suppliers the value is 2, etc.",
  productDesc:
    "ConnexPay suggests clients submitted a high level description such as Flight, Hotel, Car Rental, etc. This text will appear on the Payment Page.",
  emailAddress:
    "Provide this information to allow for easy searching by email ID and for better analytics based on individual customers. If you turn on email address to cardholder, then they will receive a receipt upon the processing of the sale.",
  riskAnalysis:
    "There is a cost associated with running risk analysis on ACH transactions. If you don’t run risk analysis the sale will process but the purchases will be delayed.",
  linkExpirationDate:
    "This is the date that the link will expire and will no longer work for accepting payment. Ensure that this value is greater than the Email Date. If left blank, this link will default to expire in 24 hours from creation.",
  statementDescription:
    "The statement description allows a client to customize the Merchant name that appears on the cardholder statement such that the cardholder recognizes the transaction on their statement.",
};

const SalesForm = (props) => {
  const { formik, showModalFiles, filesData, userData } = props;
  const merchantSettings = userData?.merchantSettings;

  const [optLabels, setOptLabels] = useState([]);

  const saleDescriptions = useSelector((state) => state.saleDescriptions);
  const optSaleDesc = saleDescriptions?.map((val) => ({
    label: val?.saleDescription,
    value: val?.saleDescription,
  }));

  useEffect(() => {
    if (!isEmpty(userData)) {
      const arrOptLabels = mapOptLabels(userData?.labelHierarchy);
      setOptLabels(arrOptLabels);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  const { values, touched, errors, setFieldValue, handleChange, handleBlur } =
    formik;

  return (
    <>
      <CRow>
        <CCol md="6">
          <CFormGroup>
            <CLabel htmlFor="Amount">
              Amount<span className="label-required">*</span>
            </CLabel>
            <NumberFormat
              name="Amount"
              value={values.Amount}
              onValueChange={(val) => setFieldValue("Amount", val.floatValue)}
              prefix="$"
              thousandSeparator
              customInput={CInput}
              placeholder="Enter Amount"
              onBlur={handleBlur}
              invalid={touched.Amount && !!errors.Amount}
            />
            <CInvalidFeedback>{errors.Amount}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="ExpectedPayments">
                Expected Payments
                {!values?.RiskAnalysis && (
                  <span className="label-required">*</span>
                )}
              </CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["expectedPayments"]}
              >
                <CIcon
                  tabIndex="-1"
                  className="mr-3"
                  name="cil-info-circle"
                  height={14}
                />
              </CTooltip>
            </div>
            <NumberFormat
              disabled={values?.RiskAnalysis}
              name="ConnexPayTransaction.ExpectedPayments"
              customInput={CInput}
              value={values.ConnexPayTransaction?.ExpectedPayments}
              placeholder="Enter Expected Payments"
              onValueChange={(val) =>
                setFieldValue(
                  "ConnexPayTransaction.ExpectedPayments",
                  val.floatValue
                )
              }
              onBlur={handleBlur}
              invalid={
                touched.ConnexPayTransaction?.ExpectedPayments &&
                !!errors.ConnexPayTransaction?.ExpectedPayments
              }
            />
            <CInvalidFeedback>
              {errors.ConnexPayTransaction?.ExpectedPayments}
            </CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="FirstName">
                Customer First Name<span className="label-required">*</span>
              </CLabel>
            </div>
            <CInput
              autoComplete="none"
              name="FirstName"
              type="text"
              value={values.FirstName}
              placeholder="Enter First Name"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.FirstName && !!errors.FirstName}
            />
            <CInvalidFeedback>{errors.FirstName}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="LastName">
                Customer Last Name<span className="label-required">*</span>
              </CLabel>
            </div>
            <CInput
              autoComplete="none"
              name="LastName"
              type="text"
              value={values.LastName}
              placeholder="Enter Last Name"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.LastName && !!errors.LastName}
            />
            <CInvalidFeedback>{errors.LastName}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="ProductType">
                Product Type<span className="label-required">*</span>
              </CLabel>
              <div className="d-flex flex-auto justify-content-between align-items-center">
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["riskInfo"]}
                >
                  <CIcon
                    tabIndex="-1"
                    name="cil-shield-check"
                    height={14}
                    className="mx-2"
                  />
                </CTooltip>
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["productType"]}
                >
                  <CIcon
                    tabIndex="-1"
                    className="mr-3"
                    name="cil-info-circle"
                    height={14}
                  />
                </CTooltip>
              </div>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.ProductType"
              type="text"
              value={values.RiskData?.ProductType}
              placeholder="Select Product Type"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.ProductType && !!errors.RiskData?.ProductType
              }
            />
            <CInvalidFeedback>{errors.RiskData?.ProductType}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="ProductItem">
                Product Item<span className="label-required">*</span>
              </CLabel>
              <div className="d-flex flex-auto justify-content-between align-items-center">
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["riskInfo"]}
                >
                  <CIcon
                    tabIndex="-1"
                    name="cil-shield-check"
                    height={14}
                    className="mx-2"
                  />
                </CTooltip>
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["productItem"]}
                >
                  <CIcon
                    tabIndex="-1"
                    className="mr-3"
                    name="cil-info-circle"
                    height={14}
                  />
                </CTooltip>
              </div>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.ProductItem"
              type="text"
              value={values.RiskData?.ProductItem}
              placeholder="Select Product Item"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.ProductItem && !!errors.RiskData?.ProductItem
              }
            />
            <CInvalidFeedback>{errors.RiskData?.ProductItem}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="OrderNumber">Order Number</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["orderNumber"]}
              >
                <CIcon
                  tabIndex="-1"
                  className="mr-3"
                  name="cil-info-circle"
                  height={14}
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="OrderNumber"
              type="text"
              value={values.OrderNumber}
              placeholder="Enter Order Number"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.OrderNumber && !!errors.OrderNumber}
            />
            <CInvalidFeedback>{errors.OrderNumber}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="CustomerId">Customer ID</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["customerId"]}
              >
                <CIcon
                  tabIndex="-1"
                  className="mr-3"
                  name="cil-info-circle"
                  height={14}
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="CustomerId"
              type="text"
              value={values.CustomerId}
              placeholder="Enter Customer ID"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.CustomerId && !!errors.CustomerId}
            />
            <CInvalidFeedback>{errors.CustomerId}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="Email">Email Address</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["emailAddress"]}
              >
                <CIcon
                  tabIndex="-1"
                  className="mr-3"
                  name="cil-info-circle"
                  height={14}
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.Email"
              type="text"
              value={values.RiskData?.Email}
              placeholder="Enter Email Address"
              onChange={handleChange}
              onBlur={handleBlur}
              valid={touched.RiskData?.Email && isEmail(values.RiskData?.Email)}
              invalid={touched.RiskData?.Email && !!errors.RiskData?.Email}
            />
            <CInvalidFeedback>{errors.RiskData?.Email}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="ExpirationDate">Link Expiration Date</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["linkExpirationDate"]}
              >
                <CIcon
                  tabIndex="-1"
                  className="mr-3"
                  name="cil-info-circle"
                  height={14}
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="ExpirationDate"
              type="date"
              value={values.ExpirationDate}
              placeholder="Enter Expiration Date"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.ExpirationDate && !!errors.ExpirationDate}
            />
            <CInvalidFeedback>{errors.ExpirationDate}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="DateOfBirth">Date of Birth</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="RiskData.DateOfBirth"
              type="date"
              value={values.RiskData?.DateOfBirth}
              placeholder="dd/mm/yyyy"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.RiskData?.DateOfBirth && !!errors.RiskData?.DateOfBirth
              }
            />
            <CInvalidFeedback>{errors.RiskData?.DateOfBirth}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="Gender">Gender</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["riskInfo"]}
              >
                <CIcon
                  tabIndex="-1"
                  name="cil-shield-check"
                  height={14}
                  className="mx-2"
                />
              </CTooltip>
            </div>
            <Select
              classNamePrefix="default-select"
              name="SelectGender"
              value={
                optGender.find(
                  (val) => val.value === values.RiskData?.Gender
                ) || ""
              }
              options={optGender}
              placeholder="Select Customer"
              components={{
                IndicatorSeparator: () => null,
              }}
              onChange={(selected) =>
                setFieldValue("RiskData.Gender", selected.value)
              }
              isSearchable={false}
            />
            <CInvalidFeedback>{errors.Card?.Customer?.Gender}</CInvalidFeedback>
          </CFormGroup>
        </CCol>

        <CCol md="6">
          <CFormGroup>
            <div className="d-flex align-items-center">
              <CLabel htmlFor="ProductDesc">Description</CLabel>
              <div className="d-flex flex-auto justify-content-between align-items-center">
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["riskInfo"]}
                >
                  <CIcon
                    tabIndex="-1"
                    name="cil-shield-check"
                    height={14}
                    className="mx-2"
                  />
                </CTooltip>
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["productDesc"]}
                >
                  <CIcon
                    tabIndex="-1"
                    className="mr-3"
                    name="cil-info-circle"
                    height={14}
                  />
                </CTooltip>
              </div>
            </div>

            <>
              {optSaleDesc?.length ? (
                <Select
                  classNamePrefix="default-select"
                  name="SelectProductDesc"
                  value={
                    optSaleDesc.find(
                      (val) => val.value === values.RiskData?.ProductDesc
                    ) || ""
                  }
                  options={optSaleDesc}
                  placeholder="Enter Description"
                  components={{
                    IndicatorSeparator: () => null,
                  }}
                  onChange={(selected) =>
                    setFieldValue("RiskData.ProductDesc", selected.value)
                  }
                />
              ) : (
                <CInput
                  autoComplete="none"
                  name="RiskData.ProductDesc"
                  type="text"
                  value={values.RiskData?.ProductDesc}
                  placeholder="Payment Link Description"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={
                    touched.RiskData?.ProductDesc &&
                    !!errors.RiskData?.ProductDesc
                  }
                />
              )}
              <CInvalidFeedback>
                {errors.RiskData?.ProductDesc}
              </CInvalidFeedback>
            </>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="CustomerId">Statement Description</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["statementDescription"]}
              >
                <CIcon
                  tabIndex="-1"
                  className="mr-3"
                  name="cil-info-circle"
                  height={14}
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="StatementDescription"
              type="text"
              value={values.StatementDescription}
              placeholder="Enter Statement Description"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.StatementDescription && !!errors.StatementDescription
              }
            />
            <CInvalidFeedback>{errors.StatementDescription}</CInvalidFeedback>
          </CFormGroup>
        </CCol>
        <CCol md="6">
          {merchantSettings?.showLabels && (
            <CFormGroup>
              <CLabel htmlFor="Label">
                Label<span className="label-required">*</span>
              </CLabel>
              <Select
                className={
                  touched.LabelIds && !!errors.LabelIds ? "is-invalid" : ""
                }
                classNamePrefix="default-select"
                name="Label"
                value={
                  optLabels.find((val) => val.value === values.LabelIds) || ""
                }
                options={optLabels}
                placeholder="Select Label"
                components={{
                  IndicatorSeparator: () => null,
                }}
                onChange={(selected) => {
                  setFieldValue("LabelIds", selected.value);
                }}
                isSearchable={false}
              />
              <CInvalidFeedback>{errors.LabelIds}</CInvalidFeedback>
            </CFormGroup>
          )}
        </CCol>

        <CCol md="6" />
      </CRow>
      <CRow>
        {merchantSettings?.showACHSales && (
          <CCol md="6">
            <div className="d-flex justify-content-between align-items-center mb-2">
              <p className="mb-0">Allow ACH Payment</p>
              <CSwitch
                className="ml-2"
                color="secondary"
                checked={values.AllowPaymentACH}
                onChange={() => {
                  setFieldValue("AllowPaymentACH", !values.AllowPaymentACH);
                  setFieldValue("RiskAnalysis", false);
                }}
                shape="pill"
              />
            </div>
            <div className="d-flex justify-content-between align-items-center mb-2">
              <p className="mb-0">Run Risk Analysis</p>
              <div className="d-flex align-items-center">
                <CTooltip
                  placement="bottom-end"
                  content={tooltipInfoHelper["riskAnalysis"]}
                >
                  <CIcon
                    tabIndex="-1"
                    className="mx-3"
                    name="cil-info-circle"
                    height={14}
                  />
                </CTooltip>
                <CSwitch
                  disabled={!values.AllowPaymentACH}
                  className="ml-2"
                  color="secondary"
                  checked={values.RiskAnalysis}
                  onChange={() => {
                    setFieldValue("RiskAnalysis", !values.RiskAnalysis);
                    setFieldValue("ConnexPayTransaction.ExpectedPayments", "");
                  }}
                  shape="pill"
                />
              </div>
            </div>
          </CCol>
        )}
        <CCol md="6">
          <div className="d-flex justify-content-between align-items-center mb-2">
            <p className="mb-0">Approval Documents</p>
            <div className="d-flex align-items-center">
              <CBadge color="success" className="mx-3">
                {filesData?.length}
              </CBadge>
              <div
                className="bg-secondary px-2 mr-1 pb-1 rounded hover-pointer ml-1"
                onClick={() => showModalFiles()}
              >
                <CIcon size="sm" name="cil-cloud-upload" />
              </div>
            </div>
          </div>
        </CCol>
      </CRow>
    </>
  );
};

export default SalesForm;
