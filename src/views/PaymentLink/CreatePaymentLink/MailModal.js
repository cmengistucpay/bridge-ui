import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { format, addDays } from "date-fns";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CForm,
  CFormGroup,
  CLabel,
  CInput,
  CTextarea,
  CInvalidFeedback,
  CButton,
  CAlert,
  CSpinner,
  CTooltip,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useFormik } from "formik";
import * as Yup from "yup";
import NumberFormat from "react-number-format";
// import { purPostTransmission } from "src/services";

const tooltipInfoHelper = {
  emailDate:
    "This is the date that the link will be emailed automatically to the recipients address",
  linkExpirationDate:
    "This is the date that the link will expire and will no longer work for accepting payment. Ensure that this value is greater than the Email Date",
};

const initialValues = {
  EmailRecipient: "",
  EmailFrom: "",
  EmailDate: "",
  LinkExpirationDate: "",
  Amount: "",
  Subject: "",
  Message: "",
};

const validationSchema = (values) => {
  return Yup.object().shape({
    EmailRecipient: Yup.string()
      .email("Email format is invalid")
      .required("Email Recipient is required"),
    EmailFrom: Yup.string().email("Email format is invalid"),
    Amount: Yup.number()
      .min(1, "Must be more than $ 1")
      .max(999999, "Must be less than $ 999,999")
      .required("Amount is required"),
    Subject: Yup.string().required("Subject is required"),
    Message: Yup.string().required("Message is required"),
    EmailDate: Yup.date().max(
      Yup.ref("LinkExpirationDate"),
      "Email Date must be earlier than Link Expiration Date"
    ),
    LinkExpirationDate: Yup.date().min(
      Yup.ref("EmailDate"),
      "Link Expiration Date must be greater than Email Date"
    ),
  });
};

const MailModal = (props) => {
  const { modalShow, modalData, handleHideModal, handlePostSubmit } = props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  const handleSubmitForm = async (values) => {
    try {
      setIsSubmitting(true);
      setResAlert(null);
      const payload = {
        ...values,
      };
      console.log({ payload });
      // const res = await purPostTransmission(activeCardGuid, payload);
      // if (res) {
      const toasterConfig = {
        show: true,
        type: "success",
        title: "Success",
        body: "Email Successfully Scheduled",
      };

      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      resetForm();
      handleHideModal();
      handlePostSubmit();
      // }
    } catch (error) {
      setResAlert({
        color: "danger",
        message:
          error?.response?.data?.message ||
          error?.response?.data ||
          "Network error.",
      });
      console.error(error);
    } finally {
      setIsSubmitting(false);
    }
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitForm,
  });
  const {
    values,
    touched,
    errors,
    setFieldValue,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
  } = formik;

  useEffect(() => {
    setResAlert(null);
    setFieldValue("Amount", modalData?.amount);
    setFieldValue("EmailDate", format(new Date(), "yyyy-MM-dd"));
    setFieldValue(
      "LinkExpirationDate",
      format(addDays(new Date(), 3), "yyyy-MM-dd")
    );

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Details</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CForm>
          <CFormGroup>
            <CLabel htmlFor="EmailRecipient">
              Recipient’s Email Address<span className="label-required">*</span>
            </CLabel>
            <CInput
              autoComplete="none"
              name="EmailRecipient"
              type="text"
              value={values.EmailRecipient}
              placeholder="Enter Recipient’s Email Address"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.EmailRecipient && !!errors.EmailRecipient}
            />
            <CInvalidFeedback>{errors.EmailRecipient}</CInvalidFeedback>
          </CFormGroup>

          <CFormGroup>
            <CLabel htmlFor="EmailFrom">Sender’s Email Address</CLabel>
            <CInput
              autoComplete="none"
              name="EmailFrom"
              type="text"
              value={values.EmailFrom}
              placeholder="Enter Sender’s Email Address"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.EmailFrom && !!errors.EmailFrom}
            />
            <CInvalidFeedback>{errors.EmailFrom}</CInvalidFeedback>
          </CFormGroup>

          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="EmailDate">Email Date</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["emailDate"]}
              >
                <CIcon
                  tabIndex="-1"
                  className="mr-3"
                  name="cil-info-circle"
                  height={14}
                />
              </CTooltip>
            </div>

            <CInput
              autoComplete="none"
              name="EmailDate"
              type="date"
              value={values.EmailDate}
              placeholder="Enter Email Date"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.EmailDate && !!errors.EmailDate}
            />
            <CInvalidFeedback>{errors.EmailDate}</CInvalidFeedback>
          </CFormGroup>

          <CFormGroup>
            <div className="d-flex justify-content-between align-items-center">
              <CLabel htmlFor="LinkExpirationDate">Link Expiration Date</CLabel>
              <CTooltip
                placement="bottom-end"
                content={tooltipInfoHelper["linkExpirationDate"]}
              >
                <CIcon
                  tabIndex="-1"
                  className="mr-3"
                  name="cil-info-circle"
                  height={14}
                />
              </CTooltip>
            </div>
            <CInput
              autoComplete="none"
              name="LinkExpirationDate"
              type="date"
              value={values.LinkExpirationDate}
              placeholder="Enter Link Expiration Date"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={
                touched.LinkExpirationDate && !!errors.LinkExpirationDate
              }
            />
            <CInvalidFeedback>{errors.LinkExpirationDate}</CInvalidFeedback>
          </CFormGroup>

          <CFormGroup>
            <CLabel htmlFor="Amount">
              Amount<span className="label-required">*</span>
            </CLabel>
            <NumberFormat
              name="Amount"
              value={values.Amount}
              onValueChange={(val) => setFieldValue("Amount", val.floatValue)}
              prefix="$"
              thousandSeparator
              customInput={CInput}
              placeholder="Enter Amount"
              onBlur={handleBlur}
              invalid={touched.Amount && !!errors.Amount}
            />
            <CInvalidFeedback>{errors.Amount}</CInvalidFeedback>
          </CFormGroup>

          <CFormGroup>
            <CLabel htmlFor="Subject">
              Email Subject<span className="label-required">*</span>
            </CLabel>
            <CInput
              autoComplete="none"
              name="Subject"
              type="text"
              value={values.Subject}
              placeholder="Enter Email Subject"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.Subject && !!errors.Subject}
            />
            <CInvalidFeedback>{errors.Subject}</CInvalidFeedback>
          </CFormGroup>

          <CFormGroup>
            <CLabel htmlFor="Message">
              Email Message<span className="label-required">*</span>
            </CLabel>
            <CTextarea
              name="Message"
              rows="4"
              value={values.Message}
              placeholder="Enter Email Message"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.Message && !!errors.Message}
            />
            <CInvalidFeedback>{errors.Message}</CInvalidFeedback>
          </CFormGroup>

          <div>
            {resAlert && (
              <CAlert color={resAlert?.color} className="mt-2 mb-0">
                {resAlert?.message}
              </CAlert>
            )}
          </div>
        </CForm>
      </CModalBody>
      <CModalFooter>
        <CButton color="light" className="px-4" onClick={handleHideModal}>
          Cancel
        </CButton>
        <CButton
          disabled={isSubmitting}
          color="primary"
          className="px-4"
          onClick={handleSubmit}
          type="submit"
        >
          {!isSubmitting ? (
            "Submit"
          ) : (
            <>
              <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
              Loading...
            </>
          )}
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default MailModal;
