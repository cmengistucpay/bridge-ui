import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { isEmpty, find } from "lodash";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCollapse,
  CRow,
  CCol,
  CButton,
  CFormGroup,
  CLabel,
  CInvalidFeedback,
} from "@coreui/react";
import Select from "react-select";
import CIcon from "@coreui/icons-react";
import { Switch } from "src/components";
import { useFormik } from "formik";
import * as Yup from "yup";
import { briUpdateMerchants } from "src/services";
import { mapOptLabels } from "src/helper";

const initialValues = {
  labelIds: "",
  showFiles: false,
  showPaymentLinks: false,
  showFlightFormElements: false,
  showAchsales: false,
  showAchpurchases: false,
  showLabels: false,
  showSupplierIdDropdown: false,
  allowGooglePay: false,
  allowACHPayment: false,
  allowCreditCardPayment: false,
};

const validationSchema = () => {
  return Yup.object().shape({});
};

const Admin = (props) => {
  const { history } = props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);
  const userData = useSelector((state) => state.userData);

  const [accordion, setAccordion] = useState(0);
  const [optLabels, setOptLabels] = useState([]);
  const [isSubmitting, setIsSubmitting] = useState(false);

  useEffect(() => {
    if (!userData?.merchantSettings?.showFiles) {
      history.push("/");
    }
    if (!isEmpty(userData)) {
      const arrOptLabels = mapOptLabels(userData?.labelHierarchy)?.filter(
        (val) => val?.level <= 1
      );
      setOptLabels(arrOptLabels);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  useEffect(() => {
    if (
      !userData?.merchantSettings?.showFiles ||
      !find(userData?.labelHierarchy, { name: "ConnexPay Admin" })
    ) {
      history.push("/");
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  const handleSubmitForm = async (values) => {
    try {
      setIsSubmitting(true);
      const payload = {
        ...values,
        guid: userData?.guid,
        labelIds: undefined,
      };
      await briUpdateMerchants(values?.labelIds, payload);
      const toasterConfig = {
        show: true,
        type: "success",
        title: "Success",
        body: "Merchants successfully updated",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body: "User update failed",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
    } finally {
      setIsSubmitting(false);
    }
  };

  const MerchantGateway = () => {
    const fields = [
      { label: "Show Files", value: "showFiles", info:"Hides the Files option in the sidebar. Always turn this off until we implement this section."},
      { label: "Show Payment Links", value: "showPaymentLinks", info:"Turn this off for clients who will not use Payment Links. Make sure you turn off the Payment Link Settings option too. This hides Process Sale > Payment Link" },
      { label: "Show Flight Form Elements", value: "showFlightFormElements", info:"Turn off for non-travel clients. This hides the Add Flight Data form in Credit Sale" },
      { label: "Show ACH Sales", value: "showAchsales", info:"Turn off for clients who will not process ACH Sales. This hides the Allow ACH Payment and Run Risk Analysis options in the Create Payment Link form" },
      { label: "Show ACH Purchases", value: "showAchpurchases", info:"Turn off for clients who will not process ACH Purchases. This hides Issue Purchase > ACH Purchase option." },
      { label: "Show Labels", value: "showLabels", info:"Turn off for clients who will not be using Labels. This hides all mentions of Labels" },
      { label: "Show Supplier ID Dropdown", value: "showSupplierIdDropdown", info:" " },
      { label: "Show Cash Sale", value: "showCashSale", info:"Turn off for non-cash clients. Hides Process Sale > Cash Sale" },
      { label: "Show Payment Link Settings", value: "showPaymentLinkSettings", info:"Turn off for clients who are not using Payment Links. This hides settings in Account Settings related to Payment Links. Account settings can be found by clicking the profile on the top right." },
      { label: "Show Form Settings", value: "showFormSettings", info:"Hides the Form Settings in Account Settings. Always turn this off until we implement this section." },
      { label: "Show Issue Purchase", value: "showIssuePurchase", info:"Turn off for clients who do not use issue lite or ACH Purchase options. Hides the Issue Purchase button." },
      { label: "Show File Upload", value: "showFileUpload", info:"Turn off for clients who will not be processing sales using File Upload. Hides the Process Sale > File Upload option" },
    ];
    return (
      <>
        <CFormGroup>
          <div className="callout">
            <p>This is an admin panel used to set Merchant-level settings. All users created under that merchant will have these settings applied. In order to test the settings, if needed, create a test user for the merchant and login as that merchant in a separate browser or in incognito mode. Then set the settings below, hit submit and you can refresh the separate browser and the changes should be reflected instantly.</p>
          </div>
          <CLabel htmlFor="Label">
            Company<span className="label-required">*</span>
          </CLabel>
          <Select
            className={
              touched.labelIds && !!errors.labelIds ? "is-invalid" : ""
            }
            classNamePrefix="default-select"
            name="Label"
            value={optLabels.find((val) => val.value === values.labelIds) || ""}
            options={optLabels}
            placeholder="Select Company"
            components={{
              IndicatorSeparator: () => null,
            }}
            onChange={(selected) => {
              resetForm();
              setFieldValue("labelIds", selected.value);
            }}
          />
          <CInvalidFeedback>{errors.labelIds}</CInvalidFeedback>
        </CFormGroup>
        <CRow>
          {fields.map((val, i) => (
            <CCol key={i} md="6">
              <Switch
                label={val?.label}
                checked={values?.[val?.value]}
                onChange={() => handleSwitch(val?.value, !values?.[val?.value])}
                tooltipInfo={val?.info}
              />
            </CCol>
          ))}
        </CRow>
        <div className="text-right mt-3">
          <CButton
            disabled={isSubmitting}
            onClick={handleSubmit}
            color="primary"
            className="px-4 mx-1 w-50 w-md-unset"
          >
            Submit
          </CButton>
        </div>
      </>
    );
  };

  const settingList = [
    { title: "Merchant-level Settings", component: <MerchantGateway /> },
  ];

  const validateForm = (values) => {
    let errors = {};
    if (!values?.labelIds) {
      errors = {
        labelIds: "Label is required",
      };
    }

    return errors;
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitForm,
    validate: validateForm,
  });
  const { values, touched, errors, setFieldValue, handleSubmit, resetForm } =
    formik;

  const handleSwitch = (name, value) => {
    setFieldValue(name, value);
  };

  return (
    <>
      <>
        <CRow>
          <CCol xxl="12">
            {settingList.map((val, i) => (
              <CCard key={i} className="mb-2">
                <CCardHeader
                  className="d-flex justify-content-between align-items-center hover-pointer"
                  onClick={() => setAccordion(accordion === i ? null : i)}
                >
                  <h5 className="mr-2 mb-0">{val.title}</h5>
                  {/* {accordion === i ? (
                    <CIcon name="cis-chevron-top" />
                  ) : (
                    <CIcon name="cis-chevron-bottom" />
                  )} */}
                </CCardHeader>
                {/* <CCollapse show={accordion === i}> */}
                <CCardBody>{val.component}</CCardBody>
                {/* </CCollapse> */}
              </CCard>
            ))}
          </CCol>
        </CRow>
      </>
    </>
  );
};

export default Admin;
