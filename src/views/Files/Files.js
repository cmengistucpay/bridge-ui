import React, { useEffect } from "react";
import { useSelector } from "react-redux";

const Dashboard = (props) => {
  const { history } = props;
  const userData = useSelector((state) => state.userData);

  useEffect(() => {
    if (!userData?.merchantSettings?.showFiles) {
      history.push("/");
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  return <>Files Page</>;
};

export default Dashboard;
