import React, { useState, useEffect } from "react";
import { useSelector, } from "react-redux";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
} from "@coreui/react";
import {
  purPostAvailableToIssue,
} from "src/services";
import { formatUSD } from "src/helper";

const fields = [
  "clientName",
  "ledgerBalance",
  "outstandingLiabilities",
  "availableToIssuePlus",
  "availableToIssue",
];

const ReportAvailableToIssue = (props) => {

  const { history } = props;
  const userData = useSelector((state) => state.userData);
  const globalLabel = useSelector((state) => state.globalLabel);

  const [availableToIssue, setAvailableToIssue] = useState(null);
  const [availableToIssueReportData, setAvailableToIssueReportData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {

    fetchAvailableToIssueData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [globalLabel]);

  const fetchAvailableToIssueData = async () => {
    try {
      setIsLoading(true);
      const labelMerchantData = JSON.parse(localStorage.getItem("labelMarchantData"));
      const payload = {
        GatewayMerchantGuid: labelMerchantData?.merchantGuid,
      };

      const res = await purPostAvailableToIssue(payload);

      if (res) {
        setAvailableToIssueReportData([res]);
        setAvailableToIssue(res?.availableToIssue);
      } else {
        setAvailableToIssueReportData([]);
        setAvailableToIssue(null);
      }

    } catch (error) {

    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {

    if (!userData?.merchantSettings?.isFlexMerchant) {
      history.push("/");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  return (
    <>
      <CCard>
        <CCardHeader className="d-md-flex flex-md-row justify-content-between align-items-center">
          <h4 className="mr-2 mb-md-0">Available to Issue</h4>
        </CCardHeader>
        <CCardBody>
          <div className="p-4 bg-secondary text-center text-white rounded">
            <h5>Available to Issue</h5>
            <h1 className="mb-0">
              {!isLoading ? formatUSD(availableToIssue) : "..."}
            </h1>
          </div>

          <CDataTable
            loading={isLoading}
            items={availableToIssueReportData}
            fields={fields}
            noItemsViewSlot={<div className="my-5" />}
            columnHeaderSlot={{
              availableToIssuePlus: <div>Overage Allowed</div>,
              outstandingLiabilities: <div>Outstanding Liability</div>,
            }}
          />
        </CCardBody>
      </CCard>
    </>
  );
};

export default ReportAvailableToIssue;
