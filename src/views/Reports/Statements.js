import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { format, parseISO, getMonth, getYear } from "date-fns";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CPagination,
  CButton,
  CSpinner,
} from "@coreui/react";
import Select from "react-select";

import { purGetMerchantSettlementMonthly, } from "src/services";
import { formatUSD, generateYears, monthList } from "src/helper";
import { jsonToCSV } from "react-papaparse";
import FileSaver from "file-saver";

const optYears = generateYears().map((val) => ({ value: val, label: val }));
const optMonths = monthList.map((val, i) => ({ value: i + 1, label: val }));

const fields = [
  "dba",
  "postDate",
  "transactionStatus",
  "merchant",
  "merchantCategoryCode",
  "network",
  "purchases",
  "returns",
];

const date = new Date();

const ReportStatements = (props) => {
  const { history } = props;
  const userData = useSelector((state) => state.userData);
  const accDevice = useSelector((state) => state.accDevices?.[0]);

  const [settleMonth, setSettleMonth] = useState(getMonth(date) + 1);
  const [settleYear, setSettleYear] = useState(getYear(date));
  const [tableData, setTableData] = useState([]);
  const [tableParams, setTableParams] = useState({
    pageCurrent: 1,
    pageSize: 10,
    pageTotal: 1,
    totalResults: 0,
  });
  const [isLoadingTable, setIsLoadingTable] = useState(false);
  const [isLoadingDownload, setIsLoadingDownload] = useState(false);

  useEffect(() => {
    if (!userData?.canAccessStatements) {
      history.push("/");
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  const fetchMerchantSettlement = async (merchantGuid) => {
    try {
      setIsLoadingTable(true);
      const payload = {
        MerchantGuid: merchantGuid || accDevice?.merchantGuid,
        SettleMonth: settleMonth,
        SettleYear: settleYear,
      };
      let res = await purGetMerchantSettlementMonthly(
        tableParams?.pageCurrent,
        tableParams?.pageSize,
        payload
      );
      if (res) {
        setTableData(res?.searchResultDTO);
        if (res?.totalResults) {
          setTableParams({
            pageCurrent: res?.pageCurrent,
            pageSize: res?.pageSize,
            pageTotal: res?.pageTotal,
            totalResults: res?.totalResults,
          });
        }
      }
    } catch (error) {
      // console.error(error);
    } finally {
      setIsLoadingTable(false);
    }
  };

  useEffect(() => {
    if (accDevice?.merchantGuid) {
      fetchMerchantSettlement(accDevice?.merchantGuid);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [accDevice, tableParams.pageCurrent, tableParams.pageSize]);

  const handleChangePage = (page) => {
    setTableParams({
      ...tableParams,
      pageCurrent: page,
    });
  };

  const handleLoad = () => {
    fetchMerchantSettlement(accDevice?.merchantGuid);
  };

  const handleDownload = async () => {
    try {
      setIsLoadingDownload(true);
      const payload = {
        MerchantGuid: accDevice?.merchantGuid,
        SettleMonth: settleMonth,
        SettleYear: settleYear,
      };
      let res = await purGetMerchantSettlementMonthly(
        tableParams?.pageCurrent,
        tableParams?.totalResults,
        payload
      );
      if (res) {
        const csv = jsonToCSV(tableData, { quotes: true });
        const csvData = new Blob([csv], { type: "text/csv;charset=utf-8;" });

        const fileName = `StatementReport.${settleMonth}.${settleYear}.csv`;

        FileSaver.saveAs(csvData, fileName);
      }
    } catch (error) {
      // console.error(error);
    } finally {
      setIsLoadingDownload(false);
    }
  };

  return (
    <>
      <CCard>
        <CCardHeader className="d-md-flex flex-md-row justify-content-between align-items-center">
          <h4 className="mr-2 mb-md-0">Monthly Statement Report</h4>

          <div className="d-md-flex w-100 w-md-75 justify-content-end">
            <div className="d-flex mb-2 mb-md-0 w-100 w-md-50">
              <div className="mx-1 w-50 w-md-50">
                <Select
                  classNamePrefix="default-select"
                  name="month"
                  value={
                    optMonths.find((val) => val.value === settleMonth) || ""
                  }
                  options={optMonths}
                  onChange={(selected) => setSettleMonth(selected.value)}
                  placeholder="Select Month"
                  components={{
                    IndicatorSeparator: () => null,
                  }}
                />
              </div>
              <div className="mx-1 w-50 w-md-50">
                <Select
                  classNamePrefix="default-select"
                  name="year"
                  value={optYears.find((val) => val.value === settleYear) || ""}
                  options={optYears}
                  onChange={(selected) => setSettleYear(selected.value)}
                  placeholder="Select Year"
                  components={{
                    IndicatorSeparator: () => null,
                  }}
                />
              </div>
            </div>
            <div className="d-flex d-md-block justify-content-around">
              <CButton
                disabled={isLoadingTable}
                color="primary"
                className="px-4 py-2 mx-md-1 w-48 w-md-unset"
                onClick={handleLoad}
              >
                {!isLoadingTable ? (
                  "Submit"
                ) : (
                  <>
                    <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
                    Loading...
                  </>
                )}
              </CButton>
              <CButton
                disabled={isLoadingDownload}
                color="primary"
                className="px-4 py-2 mx-md-1 w-48 w-md-unset"
                onClick={handleDownload}
              >
                {!isLoadingDownload ? (
                  "Download"
                ) : (
                  <>
                    <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
                    Loading...
                  </>
                )}
              </CButton>
            </div>
          </div>
        </CCardHeader>
        <CCardBody>
          <CDataTable
            loading={isLoadingTable}
            items={tableData}
            fields={fields}
            itemsPerPage={10}
            noItemsViewSlot={<div className="my-5" />}
            columnHeaderSlot={{
              dba: "Customer Name",
              merchantCategoryCode: "Merchant Category",
              purchases: <div className="text-center">Purchases</div>,
              returns: <div className="text-center">Returns</div>,
            }}
            scopedSlots={{
              postDate: (item) => (
                <td>
                  {item.postDate && format(parseISO(item.postDate), "Pp")}
                </td>
              ),
              merchantCategoryCode: (item) => (
                <td className="text-center">
                  {item?.itemmerchantCategoryCode || ""}
                </td>
              ),
              purchases: (item) => (
                <td className="text-center">{formatUSD(item?.purchases)}</td>
              ),
              returns: (item) => (
                <td className="text-center">{formatUSD(item?.returns)}</td>
              ),
            }}
          />
          <CPagination
            pages={tableParams?.pageTotal}
            activePage={tableParams?.pageCurrent}
            onActivePageChange={(page) => handleChangePage(page)}
            className={tableParams?.pageTotal < 2 ? "d-none" : ""}
          />
        </CCardBody>
      </CCard>
    </>
  );
};

export default ReportStatements;
