import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CRow,
  CCol,
  CButton,
  CFormGroup,
  CLabel,
  CInput,
  CInvalidFeedback,
} from "@coreui/react";
import { useFormik } from "formik";
import * as Yup from "yup";
import Select from "react-select";
import NumberFormat from "react-number-format";
import { useSelector, useDispatch } from "react-redux";

const optType = [
  { value: 1, label: "Bank A Deposit" },
  { value: 2, label: "Bank A Withdrawal" },
  { value: 3, label: "Sale" },
  { value: 4, label: "Return" },
  { value: 6, label: "Bank A Daily Funding" },
  { value: 21, label: "Bank B Deposit" },
  { value: 22, label: "Bank B Withdrawal" },
  { value: 23, label: "Bank B Daily Funding" },
];

const validationSchema = () => {
  return Yup.object().shape({
    IsBulkUser: Yup.boolean(),
    Email: Yup.string().when("IsBulkUser", {
      is: false,
      then: Yup.string().required("Email Address is required"),
    }),
  });
};

const FilterTableModal = (props) => {
  const { modalShow, handleHideModal, handleSubmitFilter } = props;

  const dispatch = useDispatch();

  const initialValues = useSelector((state) => state.initialFilterCashBalanceReport) ?? {
    OrderNumber: "",
    Type: "",
    CustomerId: "",
    CashBalanceFrom: "",
    CashBalanceTo: "",
    TimeStampFrom: "",
    TimeStampTo: "",
    AmountFrom: "",
    AmountTo: ""
  };

  const handleSubmitForm = (values) => {
    dispatch({
      type: "set",
      initialFilterCashBalanceReport:values
    });
    handleSubmitFilter(values);
    handleHideModal();
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitForm,
  });

  const resetSearchValue = () => {
    formik.setFieldValue("OrderNumber", "");
    formik.setFieldValue("Type", "");
    formik.setFieldValue("CustomerId", "");
    formik.setFieldValue("CashBalanceFrom", "");
    formik.setFieldValue("CashBalanceTo", "");
    formik.setFieldValue("TimeStampFrom", "");
    formik.setFieldValue("TimeStampTo", "");
    formik.setFieldValue("AmountFrom", "");
    formik.setFieldValue("AmountTo", "");
    dispatch({
      type: "set",
      initialFilterCashBalanceReport:{}
    });
  };
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    setFieldValue,
    handleSubmit
  } = formik;

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Filter Table</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <div>
          <CFormGroup>
            <CLabel htmlFor="TimeStamp">Date</CLabel>
            <CRow>
              <CCol xs="6">
                <CInput
                  autoComplete="none"
                  name="TimeStampFrom"
                  type="date"
                  value={values.TimeStampFrom}
                  placeholder="Date From"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={touched.TimeStampFrom && !!errors.TimeStampFrom}
                />
              </CCol>
              <CCol xs="6">
                <CInput
                  autoComplete="none"
                  name="TimeStampTo"
                  type="date"
                  value={values.TimeStampTo}
                  placeholder="Date To"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={touched.TimeStampTo && !!errors.TimeStampTo}
                />
              </CCol>
            </CRow>
            <CInvalidFeedback>{errors.TimeStamp}</CInvalidFeedback>
          </CFormGroup>
          <CFormGroup>
            <CLabel htmlFor="Type">Type</CLabel>
            <Select
              classNamePrefix="default-select"
              name="city"
              value={optType.find((val) => val.value === values.Type) || ""}
              options={optType}
              onChange={(selected) => {
                setFieldValue("Type", selected.value);
              }}
              placeholder="Select Type"
              components={{
                IndicatorSeparator: () => null,
              }}
            />
            <CInvalidFeedback>{errors.Type}</CInvalidFeedback>
          </CFormGroup>
          <CFormGroup>
            <CLabel htmlFor="CustomerId">Customer ID</CLabel>
            <CInput
              autoComplete="none"
              name="CustomerId"
              type="text"
              value={values.CustomerId}
              placeholder="Enter Customer ID"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.CustomerId && !!errors.CustomerId}
            />
            <CInvalidFeedback>{errors.CustomerId}</CInvalidFeedback>
          </CFormGroup>
          <CFormGroup>
            <CLabel htmlFor="OrderNumber">Order Number</CLabel>
            <CInput
              autoComplete="none"
              name="OrderNumber"
              type="text"
              value={values.OrderNumber}
              placeholder="Enter Order Number"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.OrderNumber && !!errors.OrderNumber}
            />
            <CInvalidFeedback>{errors.OrderNumber}</CInvalidFeedback>
          </CFormGroup>
          <CFormGroup>
            <CLabel htmlFor="Amount">Amount</CLabel>
            <CRow>
              <CCol xs="6">
                <NumberFormat
                  name="AmountFrom"
                  value={values.AmountFrom}
                  onValueChange={(val) =>
                    setFieldValue("AmountFrom", val.floatValue)
                  }
                  prefix="$"
                  thousandSeparator
                  customInput={CInput}
                  placeholder="Amount From"
                  onBlur={handleBlur}
                  invalid={touched.AmountFrom && !!errors.AmountFrom}
                />
              </CCol>
              <CCol xs="6">
                <NumberFormat
                  name="AmountTo"
                  value={values.AmountTo}
                  onValueChange={(val) =>
                    setFieldValue("AmountTo", val.floatValue)
                  }
                  prefix="$"
                  thousandSeparator
                  customInput={CInput}
                  placeholder="Amount To"
                  onBlur={handleBlur}
                  invalid={touched.AmountTo && !!errors.AmountTo}
                />
              </CCol>
            </CRow>
            <CInvalidFeedback>{errors.Amount}</CInvalidFeedback>
          </CFormGroup>
          <CFormGroup>
            <CLabel htmlFor="Balance">Available Balance</CLabel>
            <CRow>
              <CCol xs="6">
                <NumberFormat
                  name="CashBalanceFrom"
                  value={values.CashBalanceFrom}
                  onValueChange={(val) =>
                    setFieldValue("CashBalanceFrom", val.floatValue)
                  }
                  prefix="$"
                  thousandSeparator
                  customInput={CInput}
                  placeholder="Balance From"
                  onBlur={handleBlur}
                  invalid={touched.CashBalanceFrom && !!errors.CashBalanceFrom}
                />
              </CCol>
              <CCol xs="6">
                <NumberFormat
                  name="CashBalanceTo"
                  value={values.CashBalanceTo}
                  onValueChange={(val) =>
                    setFieldValue("CashBalanceTo", val.floatValue)
                  }
                  prefix="$"
                  thousandSeparator
                  customInput={CInput}
                  placeholder="Balance To"
                  onBlur={handleBlur}
                  invalid={touched.CashBalanceTo && !!errors.CashBalanceTo}
                />
              </CCol>
            </CRow>
            <CInvalidFeedback>{errors.Balance}</CInvalidFeedback>
          </CFormGroup>
        </div>
        <div></div>
      </CModalBody>
      <CModalFooter>
        <CButton
          onClick={resetSearchValue}
          color="light"
          className="px-4 mx-1 w-50 w-md-unset"
        >
          Clear
        </CButton>
        <CButton
          onClick={handleSubmit}
          color="primary"
          className="px-4 mx-1 w-50 w-md-unset"
        >
          Submit
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default FilterTableModal;
