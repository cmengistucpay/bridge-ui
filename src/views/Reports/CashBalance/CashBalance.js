import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { format, parseISO } from "date-fns";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CPagination,
  CButton,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import {
  purGetMerchantCashBalanceSummary,
  purGetMerchantCashBalance,
} from "src/services";
import { formatUSD } from "src/helper";

import FilterTableModal from "./FilterTableModal";

const fields = [
  "client",
  "mid",
  "date",
  "type",
  "customerId",
  "orderNumber",
  "amount",
  "availableCashBankCashBalanceAsOf",
];

const ReportCashBalance = (props) => {
  const { history } = props;
  const accDevice = useSelector((state) => state.accDevices?.[0]);

  const dispatch = useDispatch();

  const toaster = useSelector((state) => state.toaster);
  const userData = useSelector((state) => state.userData);

  const [balanceData, setbalanceData] = useState("");
  const [modalFilter, setModalFilter] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [tableFilter, setTableFilter] = useState(useSelector((state) => state.initialFilterCashBalanceReport) ?? {
    OrderNumber: "",
    Type: [1, 2],
    CustomerId: "",
    CashBalanceFrom: "",
    CashBalanceTo: "",
    TimeStampFrom: "",
    TimeStampTo: "",
    AmountFrom: "",
    AmountTo: "",
  });
  const [tableParams, setTableParams] = useState({
    pageCurrent: 1,
    pageSize: 10,
    pageTotal: 10,
  });
  const [isLoadingBalance, setIsLoadingBalance] = useState(false);
  const [isLoadingTable, setIsLoadingTable] = useState(false);

  useEffect(() => {
    if (!userData?.canAccessCashBalance) {
      history.push("/");
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  const fetchCashBalanceSummary = async (merchantGuid) => {
    try {
      setIsLoadingBalance(true);
      const payload = {};
      const res = await purGetMerchantCashBalanceSummary(
        merchantGuid || accDevice?.merchantGuid,
        tableParams?.pageCurrent,
        tableParams?.pageSize,
        payload
      );
      if (res) {
        setbalanceData(res?.searchResultDTO?.[0]?.availableCashBalanceInTotal);
      }
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoadingBalance(false);
    }
  };

  const fetchCashBalanceTable = async (merchantGuid) => {
    try {
      setIsLoadingTable(true);
      const payload = {
        MerchantGuid: merchantGuid || accDevice?.merchantGuid,
        ...tableFilter,
      };
      const res = await purGetMerchantCashBalance(
        tableParams?.pageCurrent,
        tableParams?.pageSize,
        payload
      );
      if (res) {
        setTableData(res?.searchResultDTO);
        if (res?.pageTotal) {
          setTableParams({
            pageCurrent: res?.pageCurrent,
            pageSize: res?.pageSize,
            pageTotal: res?.pageTotal,
          });
        }
      }
    } catch (error) {
      console.error(error);
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body:
          error?.response?.data?.message || "The request cannot be completed.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
    } finally {
      setIsLoadingTable(false);
    }
  };

  useEffect(() => {
    if (accDevice?.merchantGuid) {
      fetchCashBalanceSummary(accDevice?.merchantGuid);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [accDevice]);

  useEffect(() => {
    if (accDevice?.merchantGuid) {
      fetchCashBalanceTable(accDevice?.merchantGuid);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [accDevice, tableParams.pageCurrent, tableParams.pageSize, tableFilter]);

  const handleChangePage = (page) => {
    setTableParams({
      ...tableParams,
      pageCurrent: page,
    });
  };

  const handleShowModalFilter = () => {
    setModalFilter(true);
  };

  const handleHideModelFilter = () => {
    setModalFilter(false);
  };

  const handleSubmitFilter = (values) => {
    setTableFilter(values);
  };

  return (
    <>
      <CCard>
        <CCardHeader className="d-md-flex flex-md-row justify-content-between align-items-center">
          <h4 className="mr-2 mb-md-0">Cash Balance</h4>
          <CButton
            color="primary"
            className="px-4 py-2 mx-md-1 w-48 w-md-unset"
            onClick={handleShowModalFilter}
          >
            <CIcon className="mr-2" name="cil-filter" height={12} />
            Filter
          </CButton>
        </CCardHeader>
        <CCardBody>
          <div className="p-4 bg-secondary text-center text-white rounded">
            <h5>Available Balance</h5>
            <h1 className="mb-0">
              {!isLoadingBalance ? formatUSD(balanceData) : "..."}
            </h1>
          </div>
          <CDataTable
            loading={isLoadingTable}
            items={tableData}
            fields={fields}
            itemsPerPage={10}
            noItemsViewSlot={<div className="my-5" />}
            columnHeaderSlot={{
              mid: "MID",
              customerId: "Customer ID",
              amount: <div className="text-center">Amount</div>,
              availableCashBankCashBalanceAsOf: (
                <div className="text-center">Available Balance</div>
              ),
            }}
            scopedSlots={{
              date: (item) => (
                <td>{item.date && format(parseISO(item.date), "Pp")}</td>
              ),
              amount: (item) => (
                <td className="text-center">{formatUSD(item?.amount)}</td>
              ),
              availableCashBankCashBalanceAsOf: (item) => (
                <td className="text-center">
                  {formatUSD(
                    item?.availableCashBankCashBalanceAsOf +
                      item?.availableCashBalanceAsOf
                  )}
                </td>
              ),
            }}
          />
          <CPagination
            pages={tableParams?.pageTotal}
            activePage={tableParams?.pageCurrent}
            onActivePageChange={(page) =>
              !isLoadingTable ? handleChangePage(page) : null
            }
            className={tableParams?.pageTotal < 2 ? "d-none" : ""}
          />
        </CCardBody>
      </CCard>
      <FilterTableModal
        modalShow={modalFilter}
        handleHideModal={handleHideModelFilter}
        handleSubmitFilter={handleSubmitFilter}
      />
    </>
  );
};

export default ReportCashBalance;
