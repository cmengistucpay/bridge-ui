import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { isEmpty, sortBy, find, split, trim } from "lodash";
import {
  CRow,
  CCol,
  CForm,
  CFormGroup,
  CLabel,
  CInput,
  CSwitch,
  CInvalidFeedback,
  CButton,
  CAlert,
  CSpinner,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter
} from "@coreui/react";
import { useFormik } from "formik";
import * as Yup from "yup";
import Select from "react-select";
import NumberFormat from "react-number-format";

import {
  purUpdateLodgedCard,
  purGetMCCGroups,
  purGetCardDetail
} from "src/services";

let initialValues = {
    purchaseType: '',
    midWhitelist:'',
    midBlacklist: '',
    limitWindow: '',
    amountLimit: '',
    usageLimit: '',
    activated: false
};
const UpdateLodgedCard = (props) => {
  const {
    modalShow,
    activeCardGuid,
    handleHideModal,
    handlePostSubmit,
  } = props;
  const dispatch = useDispatch();
  // const globalLabel = useSelector((state) => state.globalLabel);
  const toaster = useSelector((state) => state.toaster);
  const userData = useSelector((state) => state.userData);
  // const merchantSettings = userData?.merchantSettings;
  const [card, setCard] = useState({});
  const [optPurchaseType, setOptPurchaseType] = useState([]);
  const [optLimitWindow, setOptLimitWindow] = useState([
    { label: 'Day', value: 'Day'},
    { label: 'Week', value: 'Week'},
    { label: 'Month', value: 'Month'},
    { label: 'Lifetime', value: 'Lifetime'}
  ]);
  const [modalConfirmation, setModalConfirmation] = useState(false);
  const [formValues, setFormValues] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  const labelMerchantData = JSON.parse(localStorage.getItem("labelMarchantData"));
  useEffect(() => {
    if(activeCardGuid){
      fetchCardDetails(activeCardGuid);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [activeCardGuid]);

  useEffect(() => {
    if (modalShow) {
      setResAlert(null);
    }
  }, [modalShow]);

  const fetchCardDetails = async (cardGuid) => {
    try {
      const res = await purGetCardDetail(cardGuid);
      if (res) {
        initialValues.purchaseType = res?.purchaseType;
        initialValues.midWhitelist = res?.midWhitelist;
        initialValues.midBlacklist = res?.midBlacklist;
        initialValues.limitWindow = res?.limitWindow;
        initialValues.amountLimit = res?.amountLimit;
        initialValues.usageLimit = res?.usageLimit? res.usageLimit === -1? undefined : res?.usageLimit : undefined;
        initialValues.activated = res?.status.includes('Card - Active') ? true : false;
        setCard(res);
      }
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body:
          error?.response?.data?.message || "Unable to complete the request.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
    } finally {
    //  setIsLoadingPurchase(false);
    }
  };
  useEffect(() => {
    if (!isEmpty(userData)) {
      if (labelMerchantData?.label == "ConnexPay Admin") {
        fetchMccGroups();
      } else if (labelMerchantData?.merchantGuid) {
        fetchMccGroups(labelMerchantData?.merchantGuid);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData, modalShow]);

  const fetchMccGroups = async (merchantGuid) => {
    try {
      // setIsLoadingSales(true);
      const res = await purGetMCCGroups(merchantGuid);
      if (res) {
        const resOpt = res?.ret?.map((val) => ({
          label: val.name,
          value: val.purchaseType,
        }));
        setOptPurchaseType(sortBy(resOpt, ["label"], ["asc"]));
      }
    } catch (error) {
      // console.error(error);
    } finally {
      // setIsLoadingSales(false);
    }
  };

  const handleCancelConfirmation = () => {
    resetForm();
    handleHideModal();
  };

  const handleSubmitConfirmation = async (values) => {
    try {
      setModalConfirmation(true);
      setFormValues(values);
      setIsSubmitting(true);
      setResAlert(null);
      const payload = {
        ...values,
        amountLimit: Number.parseFloat(values.amountLimit).toFixed(2)
      };
      payload['midWhitelist'] = split(payload['midWhitelist'], ',').filter(item=> trim(item) !=='');
      payload['midBlacklist'] = split(payload['midBlacklist'], ',').filter(item => trim(item) !== '');

      const res = await purUpdateLodgedCard(activeCardGuid,payload);
      if (res) {
            const toasterConfig = {
                show: true,
                type: "success",
                title: "Success",
                body: "LCC successfully updated.",
            };
            fetchCardDetails(activeCardGuid);
            dispatch({ type: "set", toaster: [...toaster, toasterConfig], shouldReloadSalesAndPurchasesTable: true, });
            resetForm();
            handleHideModal();
            handlePostSubmit();
        }
    } catch (error) {
      setResAlert({
        color: "danger",
        message:
          error?.response?.data?.message ||
          "Unable to submit transaction. Please verify account information.",
      });
    } finally {
      setModalConfirmation(false);
      setIsSubmitting(false);
    }
  };

  const validationSchema = () => {
    const AmountLimit = Math.min((+userData?.maxLodgedCardIssueAmount), 999999);
    return Yup.object().shape({
        limitWindow: Yup.string().required("This is required."),
      amountLimit: Yup.number()
        .min(1, "Must be at least $ 1")
        .max(AmountLimit, "Must be less than or equal $" + AmountLimit)
        .required("Max Spend is required"),
      usageLimit: Yup.number()
        .min(1, "Must be more than 1")
        .max(999999, "Max allowed value is 999999")
        .integer("Decimal numbers not allowed")
    });
  };

  const validateForm = (values) => {
    let errors = {};
    // if (merchantSettings?.showLabels && !values?.LabelIds) {
    //   errors = {
    //     LabelIds: "Label is required",
    //   };
    // }

    return errors;
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitConfirmation,
    validate: validateForm,
    enableReinitialize: true,
  });
  const {
    values,
    touched,
    errors,
    setFieldValue,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm
  } = formik;

  useEffect(() => {
    setResAlert(null);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  return (
    <>
      <CModal
        className="modal-sidebar"
        show={modalShow}
        onClose={handleHideModal}
      >
        <CModalHeader closeButton>
          <CModalTitle>Edit Spend Settings</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CForm>
            <CRow>
              <CCol md="12">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="limitWindow">
                      Limit Window<span className="label-required">*</span>
                    </CLabel>
                  </div>

                  <div
                    className={
                      touched.limitWindow && !!errors.limitWindow
                        ? "is-invalid"
                        : ""
                    }
                  >
                    <Select
                      classNamePrefix="default-select"
                      name="limitWindow"
                      value={
                        optLimitWindow.find(
                          (val) => val?.value?.toLowerCase() === values?.limitWindow?.toLowerCase()
                        ) || ""
                      }
                      options={optLimitWindow}
                      onChange={(selected) =>
                        setFieldValue("limitWindow", selected.value)
                      }
                      placeholder="Select Limit Window"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                      invalid={touched.limitWindow && !!errors.limitWindow}
                      isSearchable={false}
                    />
                  </div>
                  <CInvalidFeedback>{errors.limitWindow}</CInvalidFeedback>
                </CFormGroup>
              </CCol>

              <CCol md="12">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="MaxSpend">
                      Max Spend<span className="label-required">*</span>
                    </CLabel>
                  </div>
                  <NumberFormat
                    name="amountLimit"
                    value={values.amountLimit || ''}
                    onValueChange={(val) =>
                      setFieldValue("amountLimit", val.floatValue)
                    }
                    prefix="$"
                    thousandSeparator
                    customInput={CInput}
                    placeholder="Enter Max Spend"
                    onBlur={handleBlur}
                    invalid={touched.amountLimit && !!errors.amountLimit}
                  />
                  <CInvalidFeedback>{errors.amountLimit}</CInvalidFeedback>
                </CFormGroup>
              </CCol>

              <CCol md="12">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="usageLimit">Max Authorizations</CLabel>
                  </div>
                  <NumberFormat
                    name="usageLimit"
                    value={values.usageLimit || ''}
                    onValueChange={(val) =>
                      setFieldValue("usageLimit", val.floatValue)
                    }
                    customInput={CInput}
                    placeholder="Enter Max Authorizations"
                    onBlur={handleBlur}
                    invalid={touched.usageLimit && !!errors.usageLimit}
                  />
                  <CInvalidFeedback>{errors.usageLimit}</CInvalidFeedback>
                </CFormGroup>
              </CCol>

              <CCol md="12">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="purchaseType">
                      Purchase Type
                    </CLabel>
                  </div>

                  <div
                    className={
                      touched.purchaseType && !!errors.purchaseType
                        ? "is-invalid"
                        : ""
                    }
                  >
                    <Select
                      classNamePrefix="default-select"
                      name="purchaseType"
                      value={
                        optPurchaseType.find(
                          (val) => val.value === values.purchaseType
                        ) || ""
                      }
                      options={optPurchaseType}
                      onChange={(selected) =>
                        setFieldValue("purchaseType", selected.value)
                      }
                      placeholder="Select Purchase Type"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                      invalid={touched.purchaseType && !!errors.purchaseType}
                      isSearchable={false}
                    />
                  </div>
                  <CInvalidFeedback>{errors.purchaseType}</CInvalidFeedback>
                </CFormGroup>
              </CCol>

              <CCol md="12">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="midWhitelist">MID Whitelist</CLabel>
                  </div>
                  <CInput
                    autoComplete="none"
                    name="midWhitelist"
                    type="text"
                    value={values.midWhitelist || ''}
                    placeholder="Enter Comma Separated List of MID's"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={touched.midWhitelist && !!errors.midWhitelist}
                  />
                  <CInvalidFeedback>{errors.midWhitelist}</CInvalidFeedback>
                </CFormGroup>
              </CCol>

              <CCol md="12">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="midBlacklist">MID Blacklist</CLabel>
                  </div>
                  <CInput
                    autoComplete="none"
                    name="midBlacklist"
                    type="text"
                    value={values.midBlacklist || ''}
                    placeholder="Enter Comma Separated List of MID's"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={touched.midBlacklist && !!errors.midBlacklist}
                  />
                  <CInvalidFeedback>{errors.midBlacklist}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="12">
                <div className="d-flex align-items-center">
                  <p className="mb-0">Activate</p>
                  <div className="d-flex align-items-center">
                    <CSwitch
                      className="ml-2"
                      color="secondary"
                      checked={values.activated}
                      onChange={() =>
                        setFieldValue(
                          "activated",
                          !values.activated
                        )
                      }
                      shape="pill"
                    />
                  </div>
                </div>
              </CCol>
            </CRow>
          <div>
            {resAlert && (
              <CAlert color={resAlert?.color} className="mt-3">
                {resAlert?.message}
              </CAlert>
            )}
          </div>
          </CForm>
        </CModalBody>
        <CModalFooter>
          <CButton
            disabled={isSubmitting}
            className="px-4"
            onClick={handleSubmit}
            type="submit"
            color="primary"
          >
            {!isSubmitting ? (
              "Save"
            ) : (
              <>
                <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
                Loading...
              </>
            )}
          </CButton>
          <CButton
            className="px-4"
            color="light"
            onClick={handleCancelConfirmation}
          >
            Cancel
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  );
};

export default UpdateLodgedCard;
