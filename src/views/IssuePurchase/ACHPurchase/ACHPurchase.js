import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { isEmpty } from "lodash";
import {
  CRow,
  CCol,
  CForm,
  CFormGroup,
  CLabel,
  CInput,
  CInvalidFeedback,
  CButton,
  CAlert,
  CSpinner,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CTooltip,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useFormik } from "formik";
import * as Yup from "yup";
import Select from "react-select";
import NumberFormat from "react-number-format";
import csc from "country-state-city";
import {
  purPostIssueACH,
  purPostIssueACHLite,
  purGetIncomingTransactionCodeDetail,
} from "src/services";
import { mapOptLabels, isEmail, isRoutingNumber, nonStateUS } from "src/helper";

const tooltipInfoHelper = {
  firstName:
    "Account Holders First Name. For Business Accounts, this is the signer first name on the business account.",
  lastName:
    "Account Holders Last Name. For Business Accounts, this is the signer last name on the business account.",
  businessName: "Required if payment is to a business.",
  orderNumber:
    "Client provided transaction ID associated with the order. Unique to you. Can be any combination of alpha/numeric characters",
};

const optAccountType = ["Checking", "Saving"].map((val) => ({
  value: val,
  label: val,
}));

let initialValues = {
  PayeeName: "",
  Amount: "",
  Description: "",
  OrderNumber: "",
  LabelIds: "",
  AccountHolder: {
    FirstName: "",
    LastName: "",
    BusinessName: "",
    Email: "",
    Phone: "",
    Address: {
      Address1: "",
      Address2: "",
      Country: "",
      State: "",
      City: "",
      ZipCode: "",
    },
    BankAccount: {
      RoutingNumber: "",
      AccountNumber: "",
      AccountType: "",
    },
  },
};

const validationSchema = () => {
  return Yup.object().shape({
    PayeeName: Yup.string()
      .max(100, "Maximum 100 characters")
      .required("Payee Name is required")
      .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
    Amount: Yup.number()
      .min(0.5, "Must be at least $ 0.5")
      .max(999999, "Must be less than $ 999,999")
      .required("Amount is required"),
    AccountHolder: Yup.object().shape({
      BusinessName: Yup.string()
        .min(2, "Minimum 2 characters")
        .max(50, "Maximum 50 characters"),
      FirstName: Yup.string()
        .min(2, "Minimum 2 characters")
        .max(40, "Maximum 40 characters")
        .required("First Name is required")
        .matches(/^[ A-Za-z]*$/, "Only letters & whitespace are allowed"),
      LastName: Yup.string()
        .min(2, "Minimum 2 characters")
        .max(40, "Maximum 40 characters")
        .required("Last Name is required")
        .matches(/^[ A-Za-z]*$/, "Only letters & whitespace are allowed"),
      Email: Yup.string().email("Email format is invalid"),
      Address: Yup.object().shape({
        Address1: Yup.string()
          .max(50, "Maximum 50 characters")
          .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
        Address2: Yup.string()
          .max(50, "Maximum 50 characters")
          .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
        ZipCode: Yup.string()
          .max(10, "Maximum 10 characters")
          .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
      }),
      BankAccount: Yup.object().shape({
        RoutingNumber: Yup.string()
          .test(
            "validRoutingNumber",
            "Routing Number format is not valid",
            (value) => isRoutingNumber(value)
          )
          .required("Routing Number is required"),
        AccountNumber: Yup.string()
          .max(17, "Maximum 17 characters")
          .required("Account Number is required"),
        AccountType: Yup.string().required("Account Type is required"),
      }),
    }),
    OrderNumber: Yup.string()
      .max(50, "Maximum 50 characters")
      .matches(/^[a-zA-Z0-9 ]+$/, "Only accept alpha/numeric characters"),
    ConfirmAccountNumber: Yup.string()
      .oneOf(
        [Yup.ref("AccountHolder.BankAccount.AccountNumber"), null],
        "Account Number must match"
      )
      .required("Confirm Account Number is required"),
  });
};

const VirtualCardForm = (props) => {
  const {
    modalShow,
    handleHideModal,
    purchaseCount,
    connexPayTransaction,
    customerData,
    accDevice,
    handlePostSubmit,
  } = props;
  const labelMerchantData = JSON.parse(localStorage.getItem("labelMarchantData"));
  const dispatch = useDispatch();
  // const globalLabel = useSelector((state) => state.globalLabel);
  const toaster = useSelector((state) => state.toaster);
  const userData = useSelector((state) => state.userData);
  const merchantSettings = userData?.merchantSettings;

  const [optLabels, setOptLabels] = useState([]);
  const [itcData, setItcData] = useState({});
  const [issuedCount, setIssuedCount] = useState(0);
  // const [optCountries, setOptCountries] = useState([]);
  const [optStates, setOptStates] = useState([]);
  const [optCities, setOptCities] = useState([]);

  const [modalConfirmation, setModalConfirmation] = useState(false);
  const [formValues, setFormValues] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  useEffect(() => {
    if (modalShow) {
      setResAlert(null);
      setIssuedCount(0);
    }

    if (connexPayTransaction?.incomingTransCode) {
      fetchDataItcDetails();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  useEffect(() => {
    if (!isEmpty(userData)) {
      const arrOptLabels = mapOptLabels(userData?.labelHierarchy);
      setOptLabels(arrOptLabels);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  useEffect(() => {
    getOptStates("US");
    getOptCities("US", customerData?.State || customerData?.state || values.AccountHolder.Address.State);

    setFieldValue(
      "AccountHolder.FirstName",
      customerData?.FirstName || customerData?.firstName || values.AccountHolder.FirstName
    );
    setFieldValue(
      "AccountHolder.LastName",
      customerData?.LastName || customerData?.lastName || values.AccountHolder.LastName
    );
    setFieldValue(
      "AccountHolder.Address.Address1",
      customerData?.Address1 || customerData?.address1 || values.AccountHolder.Address.Address1
    );
    setFieldValue(
      "AccountHolder.Address.Address2",
      customerData?.Address2 || customerData?.address2 || values.AccountHolder.Address.Address2
    );
    setFieldValue(
      "AccountHolder.Address.State",
      customerData?.State || customerData?.state || values.AccountHolder.Address.State
    );
    setFieldValue(
      "AccountHolder.Address.City",
      customerData?.City || customerData?.city || values.AccountHolder.Address.City
    );
    setFieldValue(
      "AccountHolder.Address.ZipCode",
      customerData?.Zip || customerData?.zip || values.AccountHolder.Address.ZipCode
    );

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  const fetchDataItcDetails = async () => {
    try {
      // setIsLoadingSales(true);
      const res = await purGetIncomingTransactionCodeDetail(
        connexPayTransaction?.incomingTransCode
      );

      setItcData(res);
    } catch (error) {
      // console.error(error);
    } finally {
      // setIsLoadingSales(false);
    }
  };

  const getOptStates = (countryCode) => {
    const getData = csc
      .getStatesOfCountry(countryCode)
      ?.filter((val) => !nonStateUS?.includes(val?.name));
    const populateData = getData.map((val) => ({
      value: val.isoCode,
      label: `${val.name}`,
    }));
    setOptStates(populateData);
  };

  const getOptCities = (countryCode, stateCode) => {
    const getData = csc.getCitiesOfState(countryCode, stateCode);
    const populateData = getData.map((val) => ({
      value: val.name,
      label: `${val.name}`,
    }));
    setOptCities(populateData);
  };

  // const handleSelectCountry = (selected) => {
  //   setFieldValue("AccountHolder.Address.Country", selected.value);
  //   setFieldValue("AccountHolder.Address.State", "");
  //   setFieldValue("AccountHolder.Address.City", "");

  //   getOptStates(selected.value);
  // };

  const handleSelectState = (selected) => {
    setFieldValue("AccountHolder.Address.State", selected.value);
    setFieldValue("AccountHolder.Address.City", "");

    getOptCities("US", selected.value);
  };

  const handleSelectCity = (selected) => {
    setFieldValue("AccountHolder.Address.City", selected.value);
  };

  const handleSubmitForm = (values) => {
    setModalConfirmation(true);
    setFormValues(values);
  };

  const handleCancelConfirmation = () => {
    setModalConfirmation(false);
  };

  const handleSubmitConfirmation = async () => {
    try {
      setIsSubmitting(true);
      setResAlert(null);
      const payload = {
        ...formValues,
        LabelIds: [values?.LabelIds],
        MerchantGuid: labelMerchantData.merchantGuid,
        IncomingTransactionCode: connexPayTransaction?.incomingTransCode,
        AccountHolder: {
          ...formValues?.AccountHolder,
          BusinessName: formValues?.AccountHolder?.BusinessName || null,
          Address: {
            ...formValues?.AccountHolder?.Address,
            Country: formValues?.AccountHolder?.Address?.Country || null,
          },
          BankAccount: {
            ...formValues?.AccountHolder?.BankAccount,
            AccountHolderName: formValues?.PayeeName,
          },
        },
        ConfirmAccountNumber: undefined,
      };

      if (connexPayTransaction) {
        await purPostIssueACH(payload);
        await fetchDataItcDetails();
      } else {
        await purPostIssueACHLite(payload);
      }
      await setIssuedCount(issuedCount + 1);

      if (purchaseCount === issuedCount + 1 || !purchaseCount) {
        resetForm();
        handleHideModal();
      }
      const toasterConfig = {
        show: true,
        type: "success",
        title: "Success",
        body: "ACH Payment successfully created.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig], shouldReloadSalesAndPurchasesTable: true, });
      handlePostSubmit();
    } catch (error) {
      setResAlert({
        color: "danger",
        message:
          error?.response?.data?.message ||
          "Unable to submit payment. Please verify account information.",
      });
    } finally {
      setModalConfirmation(false);
      setIsSubmitting(false);
    }
  };

  const validateForm = (values) => {
    let errors = {};
    if (merchantSettings?.showLabels && !values?.LabelIds) {
      errors = {
        LabelIds: "Label is required",
      };
    }

    return errors;
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitForm,
    validate: validateForm,
    enableReinitialize: true,
  });
  const {
    values,
    touched,
    errors,
    setFieldValue,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
  } = formik;

  useEffect(() => {
    setResAlert(null);
    // setFieldValue("LabelIds", globalLabel?.[0] || "");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  return (
    <>
      <CModal
        className="modal-sidebar"
        show={modalShow}
        onClose={handleHideModal}
        size="lg"
      >
        <CModalHeader closeButton>
          <CModalTitle>Enter ACH Information</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CForm>
            <CRow>
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="PayeeName">
                    Payee Name<span className="label-required">*</span>
                  </CLabel>
                  <CInput
                    autoComplete="none"
                    name="PayeeName"
                    type="text"
                    value={values.PayeeName}
                    placeholder="Enter Payee Name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={touched.PayeeName && !!errors.PayeeName}
                  />
                  <CInvalidFeedback>{errors.PayeeName}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="Amount">
                      Amount<span className="label-required">*</span>
                    </CLabel>
                  </div>
                  <NumberFormat
                    name="Amount"
                    value={values.Amount}
                    onValueChange={(val) =>
                      setFieldValue("Amount", val.floatValue)
                    }
                    prefix="$"
                    thousandSeparator
                    customInput={CInput}
                    placeholder="Enter Amount"
                    onBlur={handleBlur}
                    invalid={touched.Amount && !!errors.Amount}
                  />
                  <CInvalidFeedback>{errors.Amount}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
            </CRow>

            <h5>Account Holder</h5>
            <CRow>
              <CCol md="6">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="FirstName">
                      First Name<span className="label-required">*</span>
                    </CLabel>
                    <CTooltip
                      placement="bottom-end"
                      content={tooltipInfoHelper["firstName"]}
                    >
                      <CIcon
                        tabIndex="-1"
                        className="mr-3"
                        name="cil-info-circle"
                        height={14}
                      />
                    </CTooltip>
                  </div>
                  <CInput
                    autoComplete="none"
                    name="AccountHolder.FirstName"
                    type="text"
                    value={values.AccountHolder?.FirstName}
                    placeholder="Enter First Name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={
                      touched.AccountHolder?.FirstName &&
                      !!errors.AccountHolder?.FirstName
                    }
                  />
                  <CInvalidFeedback>
                    {errors.AccountHolder?.FirstName}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="LastName">
                      Last Name<span className="label-required">*</span>
                    </CLabel>
                    <CTooltip
                      placement="bottom-end"
                      content={tooltipInfoHelper["lastName"]}
                    >
                      <CIcon
                        tabIndex="-1"
                        className="mr-3"
                        name="cil-info-circle"
                        height={14}
                      />
                    </CTooltip>
                  </div>
                  <CInput
                    autoComplete="none"
                    name="AccountHolder.LastName"
                    type="text"
                    value={values.AccountHolder?.LastName}
                    placeholder="Enter Last Name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={
                      touched.AccountHolder?.LastName &&
                      !!errors.AccountHolder?.LastName
                    }
                  />
                  <CInvalidFeedback>
                    {errors.AccountHolder?.LastName}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>

              <CCol md="6">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="BusinessName">Business Name</CLabel>
                    <CTooltip
                      placement="bottom-end"
                      content={tooltipInfoHelper["businessName"]}
                    >
                      <CIcon
                        tabIndex="-1"
                        className="mr-3"
                        name="cil-info-circle"
                        height={14}
                      />
                    </CTooltip>
                  </div>
                  <CInput
                    autoComplete="none"
                    name="AccountHolder.BusinessName"
                    type="text"
                    value={values.AccountHolder?.BusinessName}
                    placeholder="Enter Business Name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={
                      touched.AccountHolder?.BusinessName &&
                      !!errors.AccountHolder?.BusinessName
                    }
                  />
                  <CInvalidFeedback>
                    {errors.AccountHolder?.BusinessName}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>

              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="Email">Email</CLabel>
                  <CInput
                    autoComplete="none"
                    name="AccountHolder.Email"
                    type="text"
                    value={values.AccountHolder?.Email}
                    placeholder="Enter Email Address"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    valid={
                      touched.AccountHolder?.Email &&
                      isEmail(values.AccountHolder?.Email)
                    }
                    invalid={
                      touched.AccountHolder?.Email &&
                      !!errors.AccountHolder?.Email
                    }
                  />
                  <CInvalidFeedback>
                    {errors.AccountHolder?.Email}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="Phone">Phone</CLabel>
                  <NumberFormat
                    name="AccountHolder.Phone"
                    customInput={CInput}
                    value={values.AccountHolder?.Phone}
                    format="(###) ###-####"
                    placeholder="Enter Phone Number"
                    onValueChange={(val) =>
                      setFieldValue("AccountHolder.Phone", val.value)
                    }
                    mask={"_"}
                    onBlur={handleBlur}
                    invalid={
                      touched.AccountHolder?.Phone &&
                      !!errors.AccountHolder?.Phone
                    }
                  />
                  <CInvalidFeedback>
                    {errors.AccountHolder?.Phone}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6"></CCol>

              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="Address1">Address Line 1</CLabel>
                  <CInput
                    autoComplete="none"
                    name="AccountHolder.Address.Address1"
                    type="text"
                    value={values.AccountHolder?.Address?.Address1}
                    placeholder="Enter Address Line 1"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={
                      touched.AccountHolder?.Address?.Address1 &&
                      !!errors.AccountHolder?.Address?.Address1
                    }
                  />
                  <CInvalidFeedback>
                    {errors.AccountHolder?.Address?.Address1}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="Address2">Address Line 2</CLabel>
                  <CInput
                    autoComplete="none"
                    name="AccountHolder.Address.Address2"
                    type="text"
                    value={values.AccountHolder?.Address?.Address2}
                    placeholder="Enter Address Line 2"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={
                      touched.AccountHolder?.Address?.Address2 &&
                      !!errors.AccountHolder?.Address?.Address2
                    }
                  />
                  <CInvalidFeedback>
                    {errors.AccountHolder?.Address?.Address2}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>

              {/* <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="Country">Country</CLabel>
                  <Select
                    classNamePrefix="default-select"
                    name="country"
                    value={
                      optCountries.find(
                        (val) =>
                          val.value === values.AccountHolder?.Address?.Country
                      ) || ""
                    }
                    options={optCountries}
                    onChange={handleSelectCountry}
                    placeholder="Select Country"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                  />
                  <CInvalidFeedback>
                    {errors.AccountHolder?.Address?.Country}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol> */}
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="State">State</CLabel>
                  <Select
                    classNamePrefix="default-select"
                    name="AccountHolder.Address.State"
                    value={
                      optStates.find(
                        (val) =>
                          val.value === values.AccountHolder?.Address?.State
                      ) || ""
                    }
                    options={optStates}
                    onChange={handleSelectState}
                    placeholder="Select State"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                  />
                  <CInvalidFeedback>
                    {errors.AccountHolder?.Address?.State}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>

              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="City">City</CLabel>
                  <Select
                    classNamePrefix="default-select"
                    name="AccountHolder.Address.City"
                    value={
                      optCities.find(
                        (val) =>
                          val.value === values.AccountHolder?.Address?.City
                      ) || ""
                    }
                    options={optCities}
                    onChange={handleSelectCity}
                    placeholder="Select City"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                  />
                  <CInvalidFeedback>
                    {errors.AccountHolder?.Address?.City}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="Zip">Zip Code</CLabel>
                  <CInput
                    autoComplete="none"
                    name="AccountHolder.Address.ZipCode"
                    type="text"
                    value={values.AccountHolder?.Address?.ZipCode}
                    placeholder="Enter Zip Code"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={
                      touched.AccountHolder?.Address?.ZipCode &&
                      !!errors.AccountHolder?.Address?.ZipCode
                    }
                  />
                  <CInvalidFeedback>
                    {errors.AccountHolder?.Address?.ZipCode}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                {merchantSettings?.showLabels && (
                  <CFormGroup>
                    <CLabel htmlFor="Label">
                      Label<span className="label-required">*</span>
                    </CLabel>
                    <Select
                      className={
                        touched.LabelIds && !!errors.LabelIds
                          ? "is-invalid"
                          : ""
                      }
                      classNamePrefix="default-select"
                      name="Label"
                      value={
                        optLabels.find(
                          (val) => val.value === values.LabelIds
                        ) || ""
                      }
                      options={optLabels}
                      placeholder="Select Label"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                      onChange={(selected) => {
                        setFieldValue("LabelIds", selected.value);
                      }}
                      isSearchable={true}
                    />
                    <CInvalidFeedback>{errors.LabelIds}</CInvalidFeedback>
                  </CFormGroup>
                )}
              </CCol>

              {/* <CCol md="6">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="OrderNumber">Order Number</CLabel>
                    <CTooltip
                      placement="bottom-end"
                      content={tooltipInfoHelper["orderNumber"]}
                    >
                      <CIcon
                        tabIndex="-1"
                        className="mr-3"
                        name="cil-info-circle"
                        height={14}
                      />
                    </CTooltip>
                  </div>
                  <CInput
                  autoComplete="none"
                    name="OrderNumber"
                    type="text"
                    value={values.OrderNumber}
                    placeholder="Enter Order Number"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={touched.OrderNumber && !!errors.OrderNumber}
                  />
                  <CInvalidFeedback>{errors.OrderNumber}</CInvalidFeedback>
                </CFormGroup>
              </CCol> */}
            </CRow>

            <h5>Account Details</h5>
            <CRow>
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="RoutingNumber">
                    Routing Number<span className="label-required">*</span>
                  </CLabel>
                  <NumberFormat
                    name="AccountHolder.BankAccount.RoutingNumber"
                    value={values.AccountHolder?.BankAccount?.RoutingNumber}
                    onValueChange={(val) =>
                      setFieldValue(
                        "AccountHolder.BankAccount.RoutingNumber",
                        val.value
                      )
                    }
                    allowLeadingZeros
                    customInput={CInput}
                    placeholder="Enter Routing Number"
                    onBlur={handleBlur}
                    valid={
                      touched.AccountHolder?.BankAccount?.RoutingNumber &&
                      isRoutingNumber(
                        values.AccountHolder?.BankAccount?.RoutingNumber
                      )
                    }
                    invalid={
                      touched.AccountHolder?.BankAccount?.RoutingNumber &&
                      !!errors.AccountHolder?.BankAccount?.RoutingNumber
                    }
                  />
                  <CInvalidFeedback>
                    {errors.AccountHolder?.BankAccount?.RoutingNumber}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="AccountNumber">
                    Account Number<span className="label-required">*</span>
                  </CLabel>
                  <NumberFormat
                    name="AccountHolder.BankAccount.AccountNumber"
                    value={values.AccountHolder?.BankAccount?.AccountNumber}
                    onValueChange={(val) =>
                      setFieldValue(
                        "AccountHolder.BankAccount.AccountNumber",
                        val.value
                      )
                    }
                    customInput={CInput}
                    placeholder="Enter Account Number"
                    onBlur={handleBlur}
                    invalid={
                      touched.AccountHolder?.BankAccount?.AccountNumber &&
                      !!errors.AccountHolder?.BankAccount?.AccountNumber
                    }
                  />
                  <CInvalidFeedback>
                    {errors.AccountHolder?.BankAccount?.AccountNumber}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="AccountNumber">
                    Confirm Account Number
                    <span className="label-required">*</span>
                  </CLabel>
                  <NumberFormat
                    name="ConfirmAccountNumber"
                    value={values.ConfirmAccountNumber}
                    onValueChange={(val) =>
                      setFieldValue("ConfirmAccountNumber", val.value)
                    }
                    customInput={CInput}
                    placeholder="Enter Account Number"
                    onBlur={handleBlur}
                    invalid={
                      touched.ConfirmAccountNumber &&
                      !!errors.ConfirmAccountNumber
                    }
                  />
                  <CInvalidFeedback>
                    {errors.ConfirmAccountNumber}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="AccountType">
                    Account Type<span className="label-required">*</span>
                  </CLabel>
                  <Select
                    className={
                      touched.AccountHolder?.BankAccount?.AccountType &&
                        !!errors.AccountHolder?.BankAccount?.AccountType
                        ? "is-invalid"
                        : ""
                    }
                    classNamePrefix="default-select"
                    name="city"
                    value={
                      optAccountType.find(
                        (val) =>
                          val.value ===
                          values?.AccountHolder?.BankAccount?.AccountType
                      ) || ""
                    }
                    options={optAccountType}
                    onChange={(selected) =>
                      setFieldValue(
                        "AccountHolder.BankAccount.AccountType",
                        selected.value
                      )
                    }
                    placeholder="Select Account Type"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                  />
                  <CInvalidFeedback>
                    {errors.AccountHolder?.BankAccount?.AccountType}
                  </CInvalidFeedback>
                </CFormGroup>
              </CCol>
            </CRow>
            <div className="mt-2 d-md-flex justify-content-between">
              <div className="non-domestic-supplier justify-content-between"></div>
              {(purchaseCount > 1 ||
                itcData?.expectedOutcomingPayments > 1) && (
                  <div className="mt-3 mt-md-0 d-flex justify-content-between align-items-center">
                    <p className="mb-0 mx-2 text-secondary">
                      Issued:{" "}
                      {itcData?.expectedOutcomingPayments -
                        itcData?.openTransactions}
                    </p>
                    <p className="mb-0 mx-2 text-secondary">
                      Available to Issue: {itcData?.openTransactions}
                    </p>
                  </div>
                )}
              <CButton
                disabled={isSubmitting}
                color="primary"
                className="mt-3 mt-md-0 px-4 py-2 w-100 w-md-unset"
                onClick={handleSubmit}
                type="submit"
              >
                {!isSubmitting ? (
                  "Create ACH Payment"
                ) : (
                  <>
                    <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
                    Loading...
                  </>
                )}
              </CButton>
            </div>
            <div>
              {resAlert && (
                <CAlert color={resAlert?.color} className="mt-3">
                  {resAlert?.message}
                </CAlert>
              )}
            </div>
          </CForm>
        </CModalBody>
      </CModal>
      <CModal
        className="modal-sidebar"
        show={modalConfirmation}
        onClose={handleCancelConfirmation}
      >
        <CModalHeader closeButton>
          <CModalTitle>Purchase Confirmation</CModalTitle>
        </CModalHeader>
        <CModalBody>
          Are you sure you want to create this ACH purchase?
        </CModalBody>
        <CModalFooter>
          <CButton
            className="px-4"
            color="light"
            onClick={handleCancelConfirmation}
          >
            Cancel
          </CButton>
          <CButton
            disabled={isSubmitting}
            className="px-4"
            onClick={handleSubmitConfirmation}
            color="primary"
          >
            {!isSubmitting ? (
              "Submit"
            ) : (
              <>
                <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
                Loading...
              </>
            )}
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  );
};

export default VirtualCardForm;
