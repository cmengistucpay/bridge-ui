import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { isEmpty, sortBy, find } from "lodash";
import {
  CRow,
  CCol,
  CForm,
  CFormGroup,
  CLabel,
  CInput,
  CSwitch,
  CInvalidFeedback,
  CButton,
  CAlert,
  CSpinner,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CTooltip,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useFormik } from "formik";
import * as Yup from "yup";
import Select from "react-select";
import NumberFormat from "react-number-format";
import csc from "country-state-city";
import { isValid, addYears, isBefore } from 'date-fns';

import {
  purGetIncomingTransactionCodeDetail,
  purPostIssuedCard,
  purPostIssuedCardLite,
  purGetMCCGroups,
  purGetMerchantSupplier,
} from "src/services";
import { mapOptLabels, nonStateUS } from "src/helper";

const tooltipInfoHelper = {
  firstName:
    "Cardholder's first name. This is the first name placed on the virtual card provided to the travel supplier. The value is also searchable in the ConnexPay portal.",
  lastName:
    "Cardholder's last name. This is the last name placed on the virtual card provided to the travel supplier. The value is also searchable in the ConnexPay portal.",
  purchaseType:
    "Security Control: The industry where the virtual card will be utilized. For example, if value set to airline and the card is used at hotel, it will be declined.",
  amountLimit:
    "Security Control: Maximum dollar amount the card can be authorized and settled; value must incorporate any overage i.e. currency conversion, taxes that travel supplier may associate with the transaction. If a travel supplier authorizes card for more than the amount limit the transaction will be decline. Any balance remaining on the card is returned to the ConnexPay client 8-10 days through Residual Margin after expiration and all pending transactions is cleared.",
  usageLimit:
    "Security Control: Maximum number of times the card may be used - for both pre-auths and standard auths. All attempts greater than the max authorizations should be declined. Even though many times the VCC is considered a “one-time-use” card, we recommend you default this value to 10 to avoid unwanted declines in the event the supplier needs to authorize the card multiple times. For example, an airline may run the same card for each individual ticket on a PNR that is connected to the original authorization.",
  supplierId:
    "The SupplierId is used to assist with Intelligent Routing functionality. Providing this information allows ConnexPay to issue a card with optimal interchange.",
  terminateDate:
    "The TerminateDate (YYYY-MM-DD format) is the date the Virtual Credit Card will be terminated by ConnexPay's system. TerminateDate is different than the ExpirationDate in that TerminateDate just triggers Residual Margin and ExpirationDate is the month and year that will be applied to the actual VCC. The recommendation is to set the ExpirationDate one or two years in the future and setting the TerminateDate just a day or two after the VCC is expected to be processed. Note, Returns can still be processed on terminated VCCs.",
  orderNumber:
    "Client provided transaction ID associated with the order. Unique to you. Can be any combination of alpha/numeric characters",
  toleranceAllowed:
    "Tolerance allows you to increase the value of a virtual card above the original sale amount. Once applied, you will only see the original issued amount on the card but the supplier will be able to authorize the card for the full issued plus tolerance amount.",
  nonDomesticSupplier:
    "We can issue the “Global VCC” if the Supplier accepting that VCC has an overseas merchant account. This is an optional field. Indicating true will result in issuing this Global VCC. Indicating false (or not including this property in your request) will result in receiving a VCC created for domestic use.",
};

const optCardBrand = ["Visa", "MasterCard"].map((val) => ({
  value: val,
  label: val,
}));

const optCardClass = ["Commercial", "Consumer"].map((val) => ({
  value: val,
  label: val,
}));

let initialValues = {
  FirstName: "",
  LastName: "",
  Address1: "",
  Address2: "",
  Country: "",
  State: "",
  City: "",
  Zip: "",
  PurchaseType: "",
  SupplierId: "",
  CardBrand: "",
  CardClass: "",
  AmountLimit: "",
  UsageLimit: "",
  ExpirationDate: "",
  TerminateDate: "",
  LabelIds: "",
  OrderNumber: "",
  Tolerance: "",
  AllowTolerance: false,
  NonDomesticSupplier: false,
};

const VirtualCardForm = (props) => {
  const {
    modalShow,
    handleHideModal,
    purchaseCount,
    connexPayTransaction,
    customerData,
    handlePostSubmit,
  } = props;
  const labelMerchantData = JSON.parse(localStorage.getItem("labelMarchantData"));
  const dispatch = useDispatch();

  const toaster = useSelector((state) => state.toaster);
  const accMerchant = useSelector((state) => state.accMerchants?.[0]);
  const userData = useSelector((state) => state.userData);
  const merchantSettings = userData?.merchantSettings;

  const [optLabels, setOptLabels] = useState([]);
  const [itcData, setItcData] = useState({});
  const [optPurchaseType, setOptPurchaseType] = useState([]);
  const [issuedCount, setIssuedCount] = useState(0);
  const [optCountries, setOptCountries] = useState([]);
  const [optStates, setOptStates] = useState([]);
  const [optCities, setOptCities] = useState([]);
  const [optSupplier, setOptSupplier] = useState([]);

  const [modalConfirmation, setModalConfirmation] = useState(false);
  const [formValues, setFormValues] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  useEffect(() => {
    if (modalShow) {
      setResAlert(null);
      setIssuedCount(0);
    }

    if (connexPayTransaction?.incomingTransCode) {
      fetchDataItcDetails();
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  useEffect(() => {
    if (!isEmpty(userData)) {
      const arrOptLabels = mapOptLabels(userData?.labelHierarchy);
      setOptLabels(arrOptLabels);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  useEffect(() => {
    if (labelMerchantData?.merchantGuid) {
      _fetchMerchantSupplier();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  useEffect(() => {
    if (!isEmpty(userData)) {
      // eslint-disable-next-line react-hooks/exhaustive-deps
      if (labelMerchantData?.label == "ConnexPay Admin") {
        fetchMccGroups();
      } else if (labelMerchantData?.merchantGuid) {
        fetchMccGroups(labelMerchantData?.merchantGuid);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData, modalShow]);

  const _fetchMerchantSupplier = async () => {
    try {
      const res = await purGetMerchantSupplier(labelMerchantData?.merchantGuid);
      if (!isEmpty(res)) {
        setOptSupplier(
          res?.map((val) => ({
            label: val?.supplierName,
            value: val?.idMerchantSupplierSettings,
          }))
        );
      }
    } catch (error) {
      // console.error(error);
    }
  };

  useEffect(() => {
    const getOptCountries = () => {
      const getData = csc.getAllCountries();
      const populateData = getData.map((val) => ({
        value: val.isoCode,
        label: `${val.name}`,
      }));
      setOptCountries(populateData);
    };

    getOptCountries();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    getOptStates(customerData?.Country || customerData?.country || values.Country);
    getOptCities(
      customerData?.Country || customerData?.country || values.Country,
      customerData?.State || customerData?.state || values.State
    );

    setFieldValue(
      "FirstName",
      customerData?.FirstName || customerData?.firstName || values.FirstName
    );
    setFieldValue(
      "LastName",
      customerData?.LastName || customerData?.lastName || values.LastName
    );
    setFieldValue(
      "Address1",
      customerData?.Address1 || customerData?.address1 || values.Address1
    );
    setFieldValue(
      "Address2",
      customerData?.Address2 || customerData?.address2 || values.Address2
    );
    setFieldValue(
      "Country",
      customerData?.Country || customerData?.country || values.Country
    );
    setFieldValue("State", customerData?.State || customerData?.state || values.State);
    setFieldValue("City", customerData?.City || customerData?.city || values.City);
    setFieldValue("Zip", customerData?.Zip || customerData?.zip || values.Zip);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  const fetchDataItcDetails = async () => {
    try {
      // setIsLoadingSales(true);
      const res = await purGetIncomingTransactionCodeDetail(
        connexPayTransaction?.incomingTransCode
      );
      if (res?.openTransactions === 0) {
        handleHideModal();
      }
      setItcData(res);
    } catch (error) {
      // console.error(error);
    } finally {
      // setIsLoadingSales(false);
    }
  };

  const fetchMccGroups = async (merchantGuid) => {
    try {
      // setIsLoadingSales(true);
      const res = await purGetMCCGroups(merchantGuid);
      if (res) {
        const resOpt = res?.ret?.map((val) => ({
          label: val.name,
          value: val.purchaseType,
        }));
        setOptPurchaseType(sortBy(resOpt, ["label"], ["asc"]));
      }
    } catch (error) {
      // console.error(error);
    } finally {
      // setIsLoadingSales(false);
    }
  };

  const getOptStates = (countryCode) => {
    const getData = csc
      .getStatesOfCountry(countryCode)
      ?.filter((val) => !nonStateUS?.includes(val?.name));
    const populateData = getData.map((val) => ({
      value: val.isoCode,
      label: `${val.name}`,
    }));
    setOptStates(populateData);
  };

  const getOptCities = (countryCode, stateCode) => {
    const getData = csc.getCitiesOfState(countryCode, stateCode);
    const populateData = getData.map((val) => ({
      value: val.name,
      label: `${val.name}`,
    }));
    setOptCities(populateData);
  };

  const handleSelectCountry = (selected) => {
    setFieldValue("Country", selected.value);
    setFieldValue("State", "");
    setFieldValue("City", "");

    getOptStates(selected.value);
  };

  const handleSelectState = (selected) => {
    setFieldValue("State", selected.value);
    setFieldValue("City", "");

    getOptCities(values.Country, selected.value);
  };

  const handleSelectCity = (selected) => {
    setFieldValue("City", selected.value);
  };

  const handleSubmitForm = (values) => {
    setModalConfirmation(true);
    setFormValues(values);
  };

  const handleCancelConfirmation = () => {
    setModalConfirmation(false);
  };

  const handleSubmitConfirmation = async () => {
    try {
      setIsSubmitting(true);
      setResAlert(null);
      const payload = {
        ...formValues,
        AmountLimit: Number.parseFloat(formValues.AmountLimit).toFixed(2),
        LabelIds: merchantSettings?.showLabels ? [values?.LabelIds] : undefined,
        MerchantGuid: labelMerchantData?.merchantGuid,
        IncomingTransactionCode: connexPayTransaction?.incomingTransCode,
      };

      if (connexPayTransaction) {
        await purPostIssuedCard(payload);
        await fetchDataItcDetails();
      } else {
        await purPostIssuedCardLite(payload);
      }
      await setIssuedCount(issuedCount + 1);

      if (purchaseCount === issuedCount + 1 || !purchaseCount) {
        resetForm();
        handleHideModal();
      }
      const toasterConfig = {
        show: true,
        type: "success",
        title: "Success",
        body: "VCC successfully created.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig], shouldReloadSalesAndPurchasesTable: true, });
      handlePostSubmit();
    } catch (error) {
      setResAlert({
        color: "danger",
        message:
          error?.response?.data?.message ||
          "Unable to submit transaction. Please verify account information.",
      });
    } finally {
      setModalConfirmation(false);
      setIsSubmitting(false);
    }
  };

  const validationSchema = () => {
    const toleranceLimit =
      values?.AmountLimit * (accMerchant?.toleranceAllowed * 0.01);
    return Yup.object().shape({
      FirstName: Yup.string()
        .max(50, "Maximum 50 characters")
        .required("First Name is required")
        .matches(/^[ A-Za-z]*$/, "Only letters & whitespace are allowed"),
      LastName: Yup.string()
        .max(50, "Maximum 50 characters")
        .required("Last Name is required")
        .matches(/^[ A-Za-z]*$/, "Only letters & whitespace are allowed"),
      Address1: Yup.string()
        .max(50, "Maximum 50 characters")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
      Address2: Yup.string()
        .max(50, "Maximum 50 characters")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
      Zip: Yup.string()
        .max(15, "Maximum 15 characters")
        .matches(/^[ A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
      PurchaseType: Yup.string().required("Purchase Type is required"),
      SupplierId: Yup.string().max(100, "Maximum 100 characters"),
      AmountLimit: Yup.number()
        .min(1, "Must be at least $ 1")
        .max(999999, "Must be less than $ 999,999")
        .required("Max Spend is required")
        .test(
          'AmountLimit',
          'Only numbers up to two decimal places are allowed',
          value => (value + "").match(/^(?:\d*\.\d{1,2}|\d+)$/),
        ),
      UsageLimit: Yup.number()
        .min(1, "Must be more than 1")
        .max(999999, "Max allowed value is 999999")
        .integer("Decimal numbers not allowed"),
      ExpirationDate: Yup.date()
        .min(new Date(), "Expiration Date must be greater than today")
        .required("Expiration Date is required")
        .test(
          "ExpirationDate",
          "Expiration date must be less than 3 years",
          (value) => isValidExpirationDate(value)
        ),
      OrderNumber: Yup.string()
        .max(50, "Maximum 50 characters")
        .matches(
          /^[ A-Za-z0-9'|.-]*$/,
          "Only letters, numbers, -, ., | and ' are allowed"
        ),
      Tolerance: Yup.number().max(
        toleranceLimit,
        `Must be less than $ ${toleranceLimit || "-"}`
      ),
      TerminateDate: Yup.date()
        .min(new Date(), "Termination Date must be greater than today").when("ExpirationDate", {
          is: (value) => isValid(value),
          then: Yup.date().max(Yup.ref("ExpirationDate"), "Termination Date must be equal or less than expiration date"),
        })
    });
  };

  const isValidExpirationDate = (date) => {
    const thresholdDate = addYears(new Date(), 3);
    return isBefore(date, thresholdDate);
  };

  const validateForm = (values) => {
    let errors = {};
    if (merchantSettings?.showLabels && !values?.LabelIds) {
      errors = {
        LabelIds: "Label is required",
      };
    }

    return errors;
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitForm,
    validate: validateForm,
    enableReinitialize: true,
  });
  const {
    values,
    touched,
    errors,
    setFieldValue,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
  } = formik;

  useEffect(() => {
    setResAlert(null);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  return (
    <>
      <CModal
        className="modal-sidebar"
        show={modalShow}
        onClose={handleHideModal}
        size="lg"
      >
        <CModalHeader closeButton>
          <CModalTitle>Enter Virtual Card Information</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <CForm>
            <CRow>
              <CCol md="6">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="FirstName">
                      First Name<span className="label-required">*</span>
                    </CLabel>
                    <CTooltip
                      placement="bottom-end"
                      content={tooltipInfoHelper["firstName"]}
                    >
                      <CIcon
                        tabIndex="-1"
                        className="mr-3"
                        name="cil-info-circle"
                        height={14}
                      />
                    </CTooltip>
                  </div>
                  <CInput
                    autoComplete="none"
                    name="FirstName"
                    type="text"
                    value={values.FirstName}
                    placeholder="Enter First Name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={touched.FirstName && !!errors.FirstName}
                  />
                  <CInvalidFeedback>{errors.FirstName}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="LastName">
                      Last Name<span className="label-required">*</span>
                    </CLabel>
                    <CTooltip
                      placement="bottom-end"
                      content={tooltipInfoHelper["lastName"]}
                    >
                      <CIcon
                        tabIndex="-1"
                        className="mr-3"
                        name="cil-info-circle"
                        height={14}
                      />
                    </CTooltip>
                  </div>
                  <CInput
                    autoComplete="none"
                    name="LastName"
                    type="text"
                    value={values.LastName}
                    placeholder="Enter Last Name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={touched.LastName && !!errors.LastName}
                  />
                  <CInvalidFeedback>{errors.LastName}</CInvalidFeedback>
                </CFormGroup>
              </CCol>

              <CCol md="6">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="PurchaseType">
                      Purchase Type<span className="label-required">*</span>
                    </CLabel>
                    <CTooltip
                      placement="bottom-end"
                      content={tooltipInfoHelper["purchaseType"]}
                    >
                      <CIcon
                        tabIndex="-1"
                        className="mr-3"
                        name="cil-info-circle"
                        height={14}
                      />
                    </CTooltip>
                  </div>

                  <div
                    className={
                      touched.PurchaseType && !!errors.PurchaseType
                        ? "is-invalid"
                        : ""
                    }
                  >
                    <Select
                      classNamePrefix="default-select"
                      name="PurchaseType"
                      value={
                        optPurchaseType.find(
                          (val) => val.value === values.PurchaseType
                        ) || ""
                      }
                      options={optPurchaseType}
                      onChange={(selected) =>
                        setFieldValue("PurchaseType", selected.value)
                      }
                      placeholder="Select Purchase Type"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                      invalid={touched.PurchaseType && !!errors.PurchaseType}
                      isSearchable={false}
                    />
                  </div>
                  <CInvalidFeedback>{errors.PurchaseType}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="ExpirationDate">
                    Expiration Date<span className="label-required">*</span>
                  </CLabel>
                  <CInput
                    name="ExpirationDate"
                    type="date"
                    value={values.ExpirationDate}
                    placeholder="Enter Expiration Date"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={touched.ExpirationDate && !!errors.ExpirationDate}
                  />
                  <CInvalidFeedback>{errors.ExpirationDate}</CInvalidFeedback>
                </CFormGroup>
              </CCol>

              <CCol md="6">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="MaxSpend">
                      Max Spend<span className="label-required">*</span>
                    </CLabel>
                    <CTooltip
                      placement="bottom-end"
                      content={tooltipInfoHelper["amountLimit"]}
                    >
                      <CIcon
                        tabIndex="-1"
                        className="mr-3"
                        name="cil-info-circle"
                        height={14}
                      />
                    </CTooltip>
                  </div>
                  <NumberFormat
                    name="AmountLimit"
                    value={values.AmountLimit}
                    onValueChange={(val) =>
                      setFieldValue("AmountLimit", val.floatValue)
                    }
                    prefix="$"
                    thousandSeparator
                    customInput={CInput}
                    placeholder="Enter Max Spend"
                    onBlur={handleBlur}
                    invalid={touched.AmountLimit && !!errors.AmountLimit}
                  />
                  <CInvalidFeedback>{errors.AmountLimit}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="UsageLimit">Max Authorizations</CLabel>
                    <CTooltip
                      placement="bottom-end"
                      content={tooltipInfoHelper["usageLimit"]}
                    >
                      <CIcon
                        tabIndex="-1"
                        className="mr-3"
                        name="cil-info-circle"
                        height={14}
                      />
                    </CTooltip>
                  </div>
                  <NumberFormat
                    name="UsageLimit"
                    value={values.UsageLimit}
                    onValueChange={(val) =>
                      setFieldValue("UsageLimit", val.floatValue)
                    }
                    customInput={CInput}
                    placeholder="Enter Max Authorizations"
                    onBlur={handleBlur}
                    invalid={touched.UsageLimit && !!errors.UsageLimit}
                  />
                  <CInvalidFeedback>{errors.UsageLimit}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
              {(merchantSettings?.allowSetCardBrandAndClass === true) ? (
                <CCol md="6">
                  <CFormGroup>
                    <CLabel htmlFor="CardBrand">Card Brand</CLabel>
                    <div
                      className={
                        touched.CardBrand && !!errors.CardBrand
                          ? "is-invalid"
                          : ""
                      }
                    >
                      <Select
                        classNamePrefix="default-select"
                        name="CardBrand"
                        value={
                          optCardBrand.find(
                            (val) => val.value === values.CardBrand
                          ) || ""
                        }
                        options={optCardBrand}
                        onChange={(selected) =>
                          setFieldValue("CardBrand", selected.value)
                        }
                        placeholder="Select Card Brand"
                        components={{
                          IndicatorSeparator: () => null,
                        }}
                        invalid={touched.CardBrand && !!errors.CardBrand}
                        isSearchable={false}
                      />
                    </div>
                    <CInvalidFeedback>{errors.CardBrand}</CInvalidFeedback>
                  </CFormGroup>
                </CCol>
              ) : ('')}

              {(merchantSettings?.allowSetVCbrandClassBySupplierAndClient === true) ? (
                <CCol md="6">
                  <CFormGroup>
                    <CLabel htmlFor="CardClass">Card Class</CLabel>
                    <div
                      className={
                        touched.CardClass && !!errors.CardClass
                          ? "is-invalid"
                          : ""
                      }
                    >
                      <Select
                        classNamePrefix="default-select"
                        name="CardClass"
                        value={
                          optCardClass.find(
                            (val) => val.value === values.CardClass
                          ) || ""
                        }
                        options={optCardClass}
                        onChange={(selected) =>
                          setFieldValue("CardClass", selected.value)
                        }
                        placeholder="Select Card Class"
                        components={{
                          IndicatorSeparator: () => null,
                        }}
                        invalid={touched.CardClass && !!errors.CardClass}
                        isSearchable={false}
                      />
                    </div>
                    <CInvalidFeedback>{errors.CardClass}</CInvalidFeedback>
                  </CFormGroup>
                </CCol>
              ) : ('')}
              <CCol md="6">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="SupplierId">Supplier ID</CLabel>
                    <CTooltip
                      placement="bottom-end"
                      content={tooltipInfoHelper["supplierId"]}
                    >
                      <CIcon
                        tabIndex="-1"
                        className="mr-3"
                        name="cil-info-circle"
                        height={14}
                      />
                    </CTooltip>
                  </div>
                  {!isEmpty(optSupplier) ? (
                    <Select
                      classNamePrefix="default-select"
                      name="SupplierId"
                      value={
                        optSupplier.find(
                          (val) => val.value === values.SupplierId
                        ) || ""
                      }
                      options={optSupplier}
                      onChange={(selected) =>
                        setFieldValue("SupplierId", selected.value)
                      }
                      placeholder="Select Supplier ID"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                      invalid={touched.SupplierId && !!errors.SupplierId}
                      isSearchable={false}
                    />
                  ) : (
                    <CInput
                      autoComplete="none"
                      name="SupplierId"
                      type="text"
                      value={values.SupplierId}
                      placeholder="Enter Supplier ID"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      invalid={touched.SupplierId && !!errors.SupplierId}
                    />
                  )}

                  <CInvalidFeedback>{errors.SupplierId}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="TerminateDate">Termination Date</CLabel>
                    <CTooltip
                      placement="bottom-end"
                      content={tooltipInfoHelper["terminateDate"]}
                    >
                      <CIcon
                        tabIndex="-1"
                        className="mr-3"
                        name="cil-info-circle"
                        height={14}
                      />
                    </CTooltip>
                  </div>
                  <CInput
                    autoComplete="none"
                    name="TerminateDate"
                    type="date"
                    value={values.TerminateDate}
                    placeholder="Enter Termination Date"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={touched.TerminateDate && !!errors.TerminateDate}
                  />
                  <CInvalidFeedback>{errors.TerminateDate}</CInvalidFeedback>
                </CFormGroup>
              </CCol>

              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="Address1">Address Line 1</CLabel>
                  <CInput
                    autoComplete="none"
                    name="Address1"
                    type="text"
                    value={values.Address1}
                    placeholder="Enter Address Line 1"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={touched.Address1 && !!errors.Address1}
                  />
                  <CInvalidFeedback>{errors.Address1}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="Address2">Address Line 2</CLabel>
                  <CInput
                    autoComplete="none"
                    name="Address2"
                    type="text"
                    value={values.Address2}
                    placeholder="Enter Address Line 2"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={touched.Address2 && !!errors.Address2}
                  />
                  <CInvalidFeedback>{errors.Address2}</CInvalidFeedback>
                </CFormGroup>
              </CCol>

              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="Country">Country</CLabel>
                  <Select
                    classNamePrefix="default-select"
                    name="Country"
                    value={
                      optCountries.find(
                        (val) => val.value === values.Country
                      ) || ""
                    }
                    options={optCountries}
                    onChange={handleSelectCountry}
                    placeholder="Select Country"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                  />
                  <CInvalidFeedback>{errors.Country}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="State">State</CLabel>
                  <Select
                    classNamePrefix="default-select"
                    name="State"
                    value={
                      optStates.find((val) => val.value === values.State) || ""
                    }
                    options={optStates}
                    onChange={handleSelectState}
                    placeholder="Select State"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                  />
                  <CInvalidFeedback>{errors.State}</CInvalidFeedback>
                </CFormGroup>
              </CCol>

              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="City">City</CLabel>
                  <Select
                    classNamePrefix="default-select"
                    name="City"
                    value={
                      optCities.find((val) => val.value === values.City) || ""
                    }
                    options={optCities}
                    onChange={handleSelectCity}
                    placeholder="Select City"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                  />
                  <CInvalidFeedback>{errors.City}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                <CFormGroup>
                  <CLabel htmlFor="Zip">Zip Code</CLabel>
                  <CInput
                    autoComplete="none"
                    name="Zip"
                    type="text"
                    value={values.Zip}
                    placeholder="Enter Zip Code"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={touched.Zip && !!errors.Zip}
                  />
                  <CInvalidFeedback>{errors.Zip}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                <CFormGroup>
                  <div className="d-flex justify-content-between align-items-center">
                    <CLabel htmlFor="OrderNumber">Order Number</CLabel>
                    <CTooltip
                      placement="bottom-end"
                      content={tooltipInfoHelper["orderNumber"]}
                    >
                      <CIcon
                        tabIndex="-1"
                        className="mr-3"
                        name="cil-info-circle"
                        height={14}
                      />
                    </CTooltip>
                  </div>
                  <CInput
                    autoComplete="none"
                    name="OrderNumber"
                    type="text"
                    value={values.OrderNumber}
                    placeholder="Enter Order Number"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    invalid={touched.OrderNumber && !!errors.OrderNumber}
                  />
                  <CInvalidFeedback>{errors.OrderNumber}</CInvalidFeedback>
                </CFormGroup>
              </CCol>
              <CCol md="6">
                {merchantSettings?.showLabels && (
                  <CFormGroup>
                    <CLabel htmlFor="Label">
                      Label<span className="label-required">*</span>
                    </CLabel>
                    <Select
                      className={
                        touched.LabelIds && !!errors.LabelIds
                          ? "is-invalid"
                          : ""
                      }
                      classNamePrefix="default-select"
                      name="Label"
                      value={
                        optLabels.find(
                          (val) => val.value === values.LabelIds
                        ) || ""
                      }
                      options={optLabels}
                      placeholder="Select Label"
                      components={{
                        IndicatorSeparator: () => null,
                      }}
                      onChange={(selected) => {
                        setFieldValue("LabelIds", selected.value);
                      }}
                      isSearchable={true}
                    />
                    <CInvalidFeedback>{errors.LabelIds}</CInvalidFeedback>
                  </CFormGroup>
                )}
              </CCol>
              {accMerchant?.toleranceAllowed && (
                <CCol md="6">
                  <div className="d-flex justify-content-between align-items-center mb-2">
                    <p className="mb-0">Add Tolerance</p>
                    <div className="d-flex align-items-center">
                      <CTooltip
                        placement="bottom-end"
                        content={tooltipInfoHelper["toleranceAllowed"]}
                      >
                        <CIcon
                          tabIndex="-1"
                          className="mx-3"
                          name="cil-info-circle"
                          height={14}
                        />
                      </CTooltip>
                      <CSwitch
                        disabled={!accMerchant?.toleranceAllowed}
                        className="ml-2"
                        color="secondary"
                        checked={values.AllowTolerance}
                        onChange={() => {
                          setFieldValue(
                            "AllowTolerance",
                            !values.AllowTolerance
                          );
                          if (!values.AllowTolerance) {
                            setFieldValue("Tolerance", "");
                          }
                        }}
                        shape="pill"
                      />
                    </div>
                  </div>
                  {values?.AllowTolerance && values?.AmountLimit && (
                    <CFormGroup>
                      <NumberFormat
                        name="Tolerance"
                        value={values.Tolerance}
                        onValueChange={(val) => {
                          setFieldValue("Tolerance", val.floatValue);
                        }}
                        prefix="$"
                        thousandSeparator
                        customInput={CInput}
                        placeholder="Enter Tolerance"
                        onBlur={handleBlur}
                        invalid={!!errors.Tolerance}
                      />
                      <CInvalidFeedback>{errors.Tolerance}</CInvalidFeedback>
                    </CFormGroup>
                  )}
                </CCol>
              )}
              <CCol md="6">
                <div className="d-flex align-items-center justify-content-between">
                  <p className="mb-0">Non-Domestic</p>
                  <div className="d-flex align-items-center">
                    <CTooltip
                      placement="bottom-end"
                      content={tooltipInfoHelper["nonDomesticSupplier"]}
                    >
                      <CIcon
                        tabIndex="-1"
                        className="mx-3"
                        name="cil-info-circle"
                        height={14}
                      />
                    </CTooltip>
                    <CSwitch
                      className="ml-2"
                      color="secondary"
                      checked={values.NonDomesticSupplier}
                      onChange={() =>
                        setFieldValue(
                          "NonDomesticSupplier",
                          !values.NonDomesticSupplier
                        )
                      }
                      shape="pill"
                    />
                  </div>
                </div>
              </CCol>
            </CRow>

            <div className="mt-2 d-md-flex justify-content-between">
              <div />
              {(purchaseCount > 1 ||
                itcData?.expectedOutcomingPayments > 1) && (
                  <div className="mt-3 mt-md-0 d-flex justify-content-between align-items-center">
                    <p className="mb-0 mx-2 text-secondary">
                      Issued:{" "}
                      {itcData?.expectedOutcomingPayments -
                        itcData?.openTransactions}
                    </p>
                    <p className="mb-0 mx-2 text-secondary">
                      Available to Issue: {itcData?.openTransactions}
                    </p>
                  </div>
                )}
              <CButton
                disabled={isSubmitting}
                color="primary"
                className="mt-3 mt-md-0 px-4 py-2 w-100 w-md-unset"
                onClick={handleSubmit}
                type="submit"
              >
                {!isSubmitting ? (
                  "Create Virtual Card"
                ) : (
                  <>
                    <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
                    Loading...
                  </>
                )}
              </CButton>
            </div>
            <div>
              {resAlert && (
                <CAlert color={resAlert?.color} className="mt-3">
                  {resAlert?.message}
                </CAlert>
              )}
            </div>
          </CForm>
        </CModalBody>
      </CModal>
      <CModal
        className="modal-sidebar"
        show={modalConfirmation}
        onClose={handleCancelConfirmation}
      >
        <CModalHeader closeButton>
          <CModalTitle>Purchase Confirmation</CModalTitle>
        </CModalHeader>
        <CModalBody>
          Are you sure you want to create this virtual card?
        </CModalBody>
        <CModalFooter>
          <CButton
            className="px-4"
            color="light"
            onClick={handleCancelConfirmation}
          >
            Cancel
          </CButton>
          <CButton
            disabled={isSubmitting}
            className="px-4"
            onClick={handleSubmitConfirmation}
            color="primary"
          >
            {!isSubmitting ? (
              "Submit"
            ) : (
              <>
                <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
                Loading...
              </>
            )}
          </CButton>
        </CModalFooter>
      </CModal>
    </>
  );
};

export default VirtualCardForm;
