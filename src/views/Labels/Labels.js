import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { isEmpty } from "lodash";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CSpinner,
  CListGroup,
  CListGroupItem,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { Prompt } from "src/components";
import { briGetUserById, briDeleteLabelById } from "src/services";
import AddLabelModal from "./AddLabelModal";
import { jwtDecodeBridge } from "src/services/serviceAuth";

const Labels = (props) => {
  const { history } = props;
  const [labelData, setLabelData] = useState(null);
  const [modalAddLabelShow, setModalAddLabelShow] = useState(false);
  const [modalAddLabelData, setModalAddLabelData] = useState(null);

  const [isLoadingGet, setIsLoadingGet] = useState(false);
  const [isLoadingDel, setIsLoadingDel] = useState(null);
  const [promptDelete, setPromptDelete] = useState(false);
  const [promptDeleteLabelId, setPromptDeleteLabelId] = useState(null);

  const dispatch = useDispatch();
  const userData = useSelector((state) => state.userData);
  const toaster = useSelector((state) => state.toaster);

  useEffect(() => {
    if (userData?.merchantSettings?.showLabels && userData?.canManageLabels) {
      setLabelData(userData?.labelHierarchy);
    } else {
      history.push("/");
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userData]);

  const _fetchUserData = async () => {
    try {
      setIsLoadingGet(true);
      const authBridge = jwtDecodeBridge();
      const res = await briGetUserById(authBridge?.extension_Guid);

      if (res) {
        dispatch({ type: "set", userData: res });
      }
    } catch (error) {
      console.error(error);
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body:
          error?.response?.data?.message || "The request cannot be completed.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
    } finally {
      setIsLoadingGet(false);
    }
  };

  const handleShowModalAddLabel = (item) => {
    setModalAddLabelShow(true);
    setModalAddLabelData(item);
  };

  const handleHideModalAddLabel = () => {
    setModalAddLabelShow(false);
    setModalAddLabelData(null);
  };

  const handlePromptDelete = (labelId) => {
    setPromptDelete(true);
    setPromptDeleteLabelId(labelId);
  };

  const handleDeleteLabel = async (labelId) => {
    try {
      setIsLoadingDel(labelId);
      await briDeleteLabelById(labelId);
      const toasterConfig = {
        show: true,
        type: "success",
        title: "Success",
        body: "Label successfully deleted.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      _fetchUserData();
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body:
          error?.response?.data?.message ||
          error?.response?.data ||
          "Unable to delete label.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      console.error(error);
    } finally {
      setIsLoadingDel(null);
    }
  };

  const renderList = (objLabel) => {
    return (
      <>
        <div>
          <CIcon
            onClick={() => handleShowModalAddLabel(objLabel)}
            className="text-secondary hover-pointer mr-2"
            name="cis-plus-circle"
          />
          {objLabel?.name} ({objLabel?.id})
        </div>
        {isLoadingDel === objLabel?.id ? (
          <CSpinner
            color="danger"
            component="span"
            size="sm"
            aria-hidden="true"
          />
        ) : (
          <CIcon
            onClick={() => handlePromptDelete(objLabel?.id)}
            className="text-danger hover-pointer"
            name="cis-x"
          />
        )}
      </>
    );
  };

  return (
    <>
      <CCard>
        <CCardHeader className="d-md-flex flex-md-row align-items-center">
          <h4 className="mr-2 mb-md-0">Labels</h4>
        </CCardHeader>
        <CCardBody>
          {isLoadingGet && (
            <div className="text-center my-2">
              <CSpinner color="primary" component="span" aria-hidden="true" />
            </div>
          )}
          {!isEmpty(labelData) &&
            labelData?.map((data0, iData0) => (
              <CListGroup className="mb-2" key={iData0}>
                <CListGroupItem className="mb-2 d-flex justify-content-between">
                  {renderList(data0)}
                </CListGroupItem>

                {data0?.children?.map((data1, iData1) => (
                  <CListGroup className="ml-4" key={iData1}>
                    <CListGroupItem className="mb-2 d-flex justify-content-between">
                      {renderList(data1)}
                    </CListGroupItem>

                    {data1?.children?.map((data2, iData2) => (
                      <CListGroup className="ml-4" key={iData2}>
                        <CListGroupItem className="mb-2 d-flex justify-content-between">
                          {renderList(data2)}
                        </CListGroupItem>

                        {data2?.children?.map((data3, iData3) => (
                          <CListGroup className="ml-4" key={iData3}>
                            <CListGroupItem className="mb-2 d-flex justify-content-between">
                              {renderList(data3)}
                            </CListGroupItem>

                            {data3?.children?.map((data4, iData4) => (
                              <CListGroup className="ml-4" key={iData4}>
                                <CListGroupItem className="mb-2 d-flex justify-content-between">
                                  {renderList(data4)}
                                </CListGroupItem>

                                {data4?.children?.map((data5, iData5) => (
                                  <CListGroup className="ml-4" key={iData5}>
                                    <CListGroupItem className="mb-2 d-flex justify-content-between">
                                      {renderList(data5)}
                                    </CListGroupItem>

                                    {/* {data5?.children?.map((data6, iData6) => ()} */}
                                  </CListGroup>
                                ))}
                              </CListGroup>
                            ))}
                          </CListGroup>
                        ))}
                      </CListGroup>
                    ))}
                  </CListGroup>
                ))}
              </CListGroup>
            ))}
        </CCardBody>
      </CCard>
      <AddLabelModal
        modalShow={modalAddLabelShow}
        modalData={modalAddLabelData}
        labelData={labelData}
        handleHideModal={handleHideModalAddLabel}
        handlePostSubmit={_fetchUserData}
      />
      <Prompt
        title="Delete Label"
        body="Are you sure want to delete this label?"
        modalShow={promptDelete}
        handleHideModal={() => setPromptDelete(false)}
        handleSubmit={() => handleDeleteLabel(promptDeleteLabelId)}
      />
    </>
  );
};

export default Labels;
