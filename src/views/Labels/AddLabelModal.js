import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CFormGroup,
  CLabel,
  CInput,
  CInvalidFeedback,
} from "@coreui/react";
import { useFormik } from "formik";
import { mapOptLabels } from "src/helper";
import { briUpdateLabels } from "src/services";
import * as Yup from "yup";
import Select from "react-select";

const initialValues = {
  name: "",
  parentLabelId: "",
};

const validationSchema = () => {
  return Yup.object().shape({
    name: Yup.string().required("Label Name is required"),
  });
};

const AddLabelModal = (props) => {
  const { modalShow, modalData, labelData, handleHideModal, handlePostSubmit } =
    props;

  const [optParentLabels, setOptParentLabels] = useState([]);
  const [isLoadingAdd, setIsLoadingAdd] = useState(false);

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);

  const handleSubmitForm = (values) => {
    handleSubmitAdd(values);
  };

  const handleSubmitAdd = async (values) => {
    try {
      setIsLoadingAdd(true);
      const payload = {
        ...values,
      };
      const res = await briUpdateLabels(payload);
      if (res) {
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "Label successfully added.",
        };
        dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
        handlePostSubmit();
        handleHideModal();
        resetForm();
      }
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body: error?.response?.data?.title || "Unable to add label.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      console.error(error);
    } finally {
      setIsLoadingAdd(false);
    }
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitForm,
  });
  const {
    values,
    touched,
    errors,
    handleChange,
    handleBlur,
    setFieldValue,
    handleSubmit,
    resetForm,
  } = formik;

  useEffect(() => {
    const arrOptLabels = mapOptLabels(labelData);
    setOptParentLabels(arrOptLabels);
    setFieldValue("parentLabelId", modalData?.id);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Add Label</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <div>
          <CFormGroup>
            <CLabel htmlFor="parentLabelId">Parent Label</CLabel>
            <Select
              classNamePrefix="default-select"
              name="city"
              value={
                optParentLabels.find(
                  (val) => val.value === values.parentLabelId
                ) || ""
              }
              options={optParentLabels}
              onChange={(selected) => {
                setFieldValue("parentLabelId", selected.value);
              }}
              placeholder="Select Parent Label"
              components={{
                IndicatorSeparator: () => null,
              }}
            />
            <CInvalidFeedback>{errors.parentLabelId}</CInvalidFeedback>
          </CFormGroup>
          <CFormGroup>
            <CLabel htmlFor="name">
              Label Name<span className="label-required">*</span>
            </CLabel>
            <CInput
              autoComplete="none"
              name="name"
              type="text"
              value={values.name}
              placeholder="Enter Label Name"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.name && !!errors.name}
            />
            <CInvalidFeedback>{errors.name}</CInvalidFeedback>
          </CFormGroup>
        </div>
      </CModalBody>
      <CModalFooter>
        <CButton
          disabled={isLoadingAdd}
          onClick={handleSubmit}
          color="primary"
          className="px-4 mx-1"
        >
          Submit
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default AddLabelModal;
