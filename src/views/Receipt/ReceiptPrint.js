import React from "react";

class ReceiptPrint extends React.PureComponent {
  render() {
    const { customerReceipt } = this.props;
    return (
      <div
        style={{
          whiteSpace: "pre-line",
          textAlign: "center",
          margin: "72px 0",
        }}
        dangerouslySetInnerHTML={{
          __html: customerReceipt?.replace(/\\n/g, "\n"),
        }}
      ></div>
    );
  }
}

export default ReceiptPrint;
