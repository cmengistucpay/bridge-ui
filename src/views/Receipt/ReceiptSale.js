import { useState, useEffect, useRef } from "react";
import { CModal, CModalBody, CButton } from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useReactToPrint } from "react-to-print";
import { convertCashSaleReceipt, convertCreditSaleReceipt } from "src/helper";

import ReceiptPrint from "src/views/Receipt/ReceiptPrint";
import ModalReceiptEmail from "src/views/Receipt/ModalReceiptEmail";

// const customerReceipt =
//   "Ultra Lux Jewerly\\n15 Old Country Road\\n\\nHempstead New York 11550\\n6/16/2021 12:20:49 PM\\n\\nCASH - SALE\\n\\nEntry Mode : MANUAL\\n\\nTRANSACTION ID : 63759428449439\\nInvoice number : Cash-63759428449439\\nSubtotal:                      $100.00\\n--------------------------------------\\nTotal:                         $100.00\\n--------------------------------------\\n\\n\\n\\nCUSTOMER ACKNOWLEDGES RECEIPT OF\\nGOODS AND/OR SERVICES IN THE AMOUNT\\nOF THE TOTAL SHOWN HEREON AND AGREES\\nTO PERFORM THE OBLIGATIONS SET FORTH\\nBY THE CUSTOMER`S AGREEMENT WITH THE\\nISSUER\\nAPPROVED\\n\\n\\n\\n\\nCustomer Copy\\n";

const IssuePurchaseDialog = (props) => {
  const componentRef = useRef();
  const { modalShow, modalData, handleHideModal, type } = props;

  const [isModalShow, setIsModalShow] = useState(true);
  const [receiptData, setReceiptData] = useState({});
  const [receiptType, setReceiptType] = useState("");
  const [modalEmail, setModalEmail] = useState(false);

  const handlePrint = useReactToPrint({
    content: () => componentRef.current,
  });

  useEffect(() => {
    let customerReceiptData = {};
    if (modalData?.customerReceipt?.includes("CASH - SALE")) {
      setReceiptType("cashSale");
      customerReceiptData = convertCashSaleReceipt(modalData?.customerReceipt);
    }
    if (modalData?.customerReceipt?.includes("CREDIT - SALE")) {
      setReceiptType("creditSale");
      customerReceiptData = convertCreditSaleReceipt(
        modalData?.customerReceipt,
        "SALE"
      );
    }
    if (modalData?.customerReceipt?.includes("CREDIT - VOID")) {
      setReceiptType("creditVoid");
      customerReceiptData = convertCreditSaleReceipt(
        modalData?.customerReceipt,
        "VOID"
      );
    }
    setReceiptData(customerReceiptData);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalData?.customerReceipt]);

  const handleShowModalEmail = () => {
    setIsModalShow(false);
    setModalEmail(true);
  };

  const handleHideModalEmail = () => {
    setIsModalShow(true);
    setModalEmail(false);
  };

  const renderReceiptSummaryCashSale = () => {
    return (
      <div className="my-2 text-left">
        <h4>Summary</h4>
        <div className="receipt-table">
          <div className="t-header">
            <h4>{type || "Cash - Sale"}</h4>
          </div>
          <div className="t-body">
            {/* <div className="d-flex justify-content-between">
              <p>Entry Mode:</p>
              <p>{receiptData?.receiptSummary?.entryMode}</p>
            </div> */}
            <div className="d-flex justify-content-between">
              <p>Incoming Transaction Code:</p>
              <p>{modalData?.incomingTransactionCode}</p>
            </div>
            <div className="d-flex justify-content-between">
              <p>Transaction ID:</p>
              <p>{receiptData?.receiptSummary?.transactionId}</p>
            </div>
            <div className="d-flex justify-content-between">
              <p>Invoice Number:</p>
              <p className="invoice">{receiptData?.receiptSummary?.invoiceNumber}</p>
            </div>
            {/* <div className="d-flex justify-content-between">
              <p>Subtotal:</p>
              <p>{receiptData?.receiptSummary?.subTotal}</p>
            </div> */}
            <div className="divider" />
            <div className="d-flex justify-content-between">
              <p className="font-weight-bold mb-0">Total:</p>
              <p className="font-weight-bold mb-0">
                {receiptData?.receiptSummary?.total}
              </p>
            </div>
          </div>
        </div>
      </div>
    );
  };

  const renderReceiptSummaryCreditSale = (creditType) => {
    return (
      <div className="my-2 text-left">
        <h4>Summary</h4>
        <div className="receipt-table">
          <div className="t-header">
            {/* {type === "creditSale" && <h4>Credit - Sale</h4>} */}
            {/* {type === "creditVoid" && <h4>Credit - Void</h4>} */}
            <h4>{type || "Credit - Sale"}</h4>
          </div>
          <div className="t-body">
          {(type === "creditSale" || type === "Credit") && (
          <div className="d-flex justify-content-between">
              <p>Incoming Transaction Code:</p>
              <p>{modalData?.incomingTransactionCode}</p>
            </div>
            )}
            <div className="d-flex justify-content-between">
              <p>Card #:</p>
              <p>{receiptData?.receiptSummary?.cardNumber}</p>
            </div>
            <div className="d-flex justify-content-between">
              <p>Card Type:</p>
              <p>{receiptData?.receiptSummary?.cardType}</p>
            </div>
            {/* <div className="d-flex justify-content-between">
              <p>Entry Mode:</p>
              <p>{receiptData?.receiptSummary?.entryMode}</p>
            </div> */}
            <div className="d-flex justify-content-between">
              <p>Transaction ID:</p>
              <p>{receiptData?.receiptSummary?.transactionId}</p>
            </div>
            <div className="d-flex justify-content-between">
              <p>Invoice Number:</p>
              <p className="invoice">{receiptData?.receiptSummary?.invoiceNumber}</p>
            </div>
            {/* {type === "creditSale" && (
              <div className="d-flex justify-content-between">
                <p>Subtotal:</p>
                <p>{receiptData?.receiptSummary?.subTotal}</p>
              </div>
            )} */}
            <div className="divider" />
            <div className="d-flex justify-content-between">
              {(type === "creditSale" || type === "Credit") && (
                <>
                  <p className="font-weight-bold mb-0">Total:</p>
                  <p className="font-weight-bold mb-0">
                    {receiptData?.receiptSummary?.total}
                  </p>
                </>
              )}
              {(type === "creditVoid" || type === "Void") && (
                <>
                  <p className="font-weight-bold mb-0">Void Amount:</p>
                  <p className="font-weight-bold mb-0">
                    {receiptData?.receiptSummary?.subTotal?.includes("$")
                      ? receiptData?.receiptSummary?.subTotal
                      : receiptData?.receiptSummary?.total}
                  </p>
                </>
              )}
            </div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <>
      <CModal
        className="modal-receipt"
        show={modalShow && isModalShow}
        onClose={handleHideModal}
      >
        <CModalBody>
          <div className="text-center">
            <CIcon name="logo-secondary" height="35" alt="Logo" />
            <h4 className="my-3">Receipt From ConnexPay</h4>
            <div className="my-2">
              {receiptData?.receiptHeader?.map((val, i) => (
                <p className="mb-0" key={i}>
                  {val}
                </p>
              ))}
            </div>
            {receiptType === "cashSale" && renderReceiptSummaryCashSale()}
            {receiptType === "creditSale" &&
              renderReceiptSummaryCreditSale("creditSale")}
            {receiptType === "creditVoid" &&
              renderReceiptSummaryCreditSale("creditVoid")}
            <div className="my-2 text-left">
              <p className="text-ack">{receiptData?.receiptAck}</p>
            </div>
          </div>
          <div className="d-flex justify-content-between mt-4">
            <div>
              <CButton
                onClick={handleShowModalEmail}
                color="primary"
                className="px-4 py-2 mx-1"
              >
                Email
              </CButton>
              <CButton
                color="primary"
                className="px-4 py-2 mx-1"
                onClick={handlePrint}
              >
                Print
              </CButton>
            </div>
            <CButton
              color="danger"
              className="px-4 py-2"
              onClick={handleHideModal}
            >
              Close
            </CButton>
          </div>
        </CModalBody>
      </CModal>
      <ModalReceiptEmail
        modalShow={modalEmail}
        modalData={modalData}
        handleHideModal={handleHideModalEmail}
      />
      <div className="d-none">
        <ReceiptPrint
          ref={componentRef}
          customerReceipt={modalData?.customerReceipt}
        />
      </div>
    </>
  );
};

export default IssuePurchaseDialog;
