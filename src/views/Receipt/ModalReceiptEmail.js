import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CAlert,
  CSpinner,
  CFormGroup,
  CLabel,
  CInput,
  CInvalidFeedback,
} from "@coreui/react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { salResendTransactionEmail } from "src/services";
import { isEmail } from "src/helper";

const initialValues = {
  Email: "",
};

const mailTypeHelper = {
  Sale: 10,
  Void: 12,
  Return: 14,
};

const validationSchema = () => {
  return Yup.object().shape({
    Email: Yup.string().email("Email format is invalid"),
  });
};

const ModalActivationSettings = (props) => {
  const { modalShow, modalData, handleHideModal } = props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  useEffect(() => {
    setResAlert(null);
  }, [modalShow]);

  const handleSubmitEmail = async (values) => {
    try {
      setIsSubmitting(true);
      setResAlert(null);
      const payload = {
        mailing: values?.Email,
        Guid: modalData?.guid,
        MailType: mailTypeHelper[modalData?.tenderType] || 10,
        OverrideTo: true,
      };
      const res = await salResendTransactionEmail(payload);
      if (res) {
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "Request successful",
        };
        dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
        handleHideModal();
        resetForm();
      }
    } catch (error) {
      setResAlert({
        color: "danger",
        message:
          error?.response?.data?.message || "Unable to send transaction email.",
      });
      console.error(error);
    } finally {
      setIsSubmitting(false);
    }
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitEmail,
  });
  const {
    values,
    handleChange,
    handleBlur,
    touched,
    errors,
    handleSubmit,
    resetForm,
  } = formik;

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Send Email</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CFormGroup>
          <CLabel htmlFor="Email">Email</CLabel>
          <CInput
            autoComplete="none"
            name="Email"
            type="text"
            value={values.Email}
            placeholder="Enter Email Address"
            onChange={handleChange}
            onBlur={handleBlur}
            valid={touched.Email && isEmail(values.Email)}
            invalid={touched.Email && !!errors.Email}
          />
          <CInvalidFeedback>{errors.Email}</CInvalidFeedback>
        </CFormGroup>
        <div>
          {resAlert && (
            <CAlert color={resAlert?.color} className="mt-2 mb-0">
              {resAlert?.message}
            </CAlert>
          )}
        </div>
      </CModalBody>
      <CModalFooter>
        <CButton
          onClick={handleHideModal}
          color="light"
          className="mx-1 w-50 w-md-unset"
        >
          Cancel
        </CButton>
        <CButton
          disabled={isSubmitting}
          onClick={handleSubmit}
          color="primary"
          className="mx-1 w-50 w-md-unset"
        >
          {!isSubmitting ? (
            "Submit"
          ) : (
            <>
              <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
              Loading...
            </>
          )}
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default ModalActivationSettings;
