import React, { useState, useEffect } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInvalidFeedback,
  CRow,
  CAlert,
  CSpinner,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useFormik } from "formik";
import * as Yup from "yup";
// import { salUserLogin, purUserLogin } from "../../services";
import { briUserForgotUsername } from "src/services";
import { getAuthToken } from "../../services/serviceAuth";
import { isEmail } from "src/helper";

const validationSchema = () => {
  return Yup.object().shape({
    email: Yup.string()
      .email("Email format is invalid")
      .required("Email is required"),
  });
};

const initialValues = {
  email: "",
};

const ForgotUsername = (props) => {
  const { history } = props;

  const [activeBody, setActiveBody] = useState("form");
  const [resAlert, setResAlert] = useState(null);
  const [isSubmitting, setIsSubmitting] = useState(false);

  useEffect(() => {
    const authToken = getAuthToken();
    if (authToken) {
      history.push("/");
    }
  }, [history]);

  const handleSubmitLogin = async (values) => {
    try {
      setIsSubmitting(true);
      setResAlert(null);

      await briUserForgotUsername(values?.email);
      setActiveBody("success");
    } catch (error) {
      setResAlert({
        color: "danger",
        message: error?.response?.data?.error_description || "Network error.",
      });
      console.error(error);
    } finally {
      setIsSubmitting(false);
    }
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitLogin,
  });
  const { values, touched, errors, handleChange, handleBlur, handleSubmit } =
    formik;

  const renderForm = () => {
    return (
      <>
        <CForm onSubmit={handleSubmit}>
          <h2 className="mb-3">Forgot Username</h2>
          <p>
            Enter the email address associated with your account and we’ll sand
            a reminder
          </p>
          <CInputGroup className="mb-3">
            <CInput
              autoComplete="none"
              name="email"
              type="text"
              value={values.email}
              placeholder="Enter Email Address"
              onChange={handleChange}
              onBlur={handleBlur}
              valid={touched.email && isEmail(values.email)}
              invalid={touched.email && !!errors.email}
              required
            />
            <CInvalidFeedback>{errors.email}</CInvalidFeedback>
          </CInputGroup>
          <CButton
            disabled={isSubmitting}
            color="primary"
            className="px-4"
            onClick={handleSubmit}
            type="submit"
          >
            {!isSubmitting ? (
              "Send Reminder"
            ) : (
              <>
                <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
                Loading...
              </>
            )}
          </CButton>
          {resAlert && (
            <CAlert color={resAlert?.color} className="mt-3">
              {resAlert?.message}
            </CAlert>
          )}
        </CForm>
      </>
    );
  };

  const renderSuccess = () => {
    return (
      <div className="text-center">
        <CIcon
          name="cil-check-circle"
          size="3xl"
          className="text-success mb-3"
        />
        <h2 className="text-success">Email Sent</h2>
        <p>
          Congratulations!! We sent reminder to your email address successfully!
        </p>
        <CButton
          color="primary"
          className="mt-3 px-4"
          onClick={() => history.push("/login")}
        >
          Back to Login
        </CButton>
      </div>
    );
  };

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="4">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  {activeBody === "form" && renderForm()}
                  {activeBody === "success" && renderSuccess()}
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default ForgotUsername;
