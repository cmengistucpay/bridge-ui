import React, { useState, useEffect } from "react";
import {
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CRow,
  CSpinner,
  CAlert,
  // CButton,
  // CForm,
  // CInput,
  // CInputGroup,
  // CInputGroupPrepend,
  // CInputGroupText,
  // CInvalidFeedback,
} from "@coreui/react";
// import CIcon from "@coreui/icons-react";
// import { Formik } from "formik";
// import * as Yup from "yup";
// import { salUserLogin, purUserLogin } from "../../services";
import {
  // getAuthToken,
  // setAuthToken,
  setAuthTokenBridge,
  getAuthTokenBridge,
} from "../../services/serviceAuth";
import { loginAzure, getUserAzure } from "src/services/serviceAzure";

// const validationSchema = (values) => {
//   return Yup.object().shape({
//     username: Yup.string().required("Username is required"),
//     password: Yup.string().required("Password is required"),
//   });
// };

// const initialValues = {
//   username: "",
//   password: "",
// };

const Login = (props) => {
  const { history } = props;
  const [resAlert, setResAlert] = useState(null);
  // const [isSubmitting, setIsSubmitting] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [isRedirecting, setIsRedirecting] = useState(false);

  useEffect(() => {
    const authToken = getAuthTokenBridge();
    if (authToken) {
      history.push("/");
    } else {
      _fetchUserAzure();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const _fetchUserAzure = async () => {
    try {
      const res = await getUserAzure();
      if (res) {
        setIsRedirecting(true);
        setAuthTokenBridge(res);
        window.location.href = "/";
      } else {
        loginAzure();
      }
    } catch (error) {
      console.error(error);
      setIsLoading(false);
      setResAlert({
        color: "danger",
        message: error?.response?.data?.error_description || "Network error.",
      });
    } finally {
      setIsRedirecting(false);
    }
  };

  // useEffect(() => {
  //   const authToken = getAuthToken();
  //   if (authToken) {
  //     history.push("/");
  //   }
  // }, [history]);

  // const handleSubmitLogin = async (values) => {
  //   try {
  //     setIsSubmitting(true);
  //     setResAlert(null);

  //     const params = new URLSearchParams();
  //     params.append("username", values.username);
  //     params.append("password", values.password);
  //     params.append("grant_type", "password");

  //     const salRes = await salUserLogin(params);
  //     const pulRes = await purUserLogin(params);

  //     if (salRes && pulRes) {
  //       const objAuthToken = {
  //         sales: salRes,
  //         purchase: pulRes,
  //       };
  //       await setAuthToken(objAuthToken);
  //       window.location.href = "/";
  //     }
  //   } catch (error) {
  //     setResAlert({
  //       color: "danger",
  //       message: error?.response?.data?.error_description || "Network error.",
  //     });
  //     console.error(error);
  //   } finally {
  //     setIsSubmitting(false);
  //   }
  // };

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      {false && (
        <CContainer>
          <CRow className="justify-content-center">
            <CCol md="4">
              <CCardGroup>
                <CCard className="p-4">
                  <CCardBody>
                    {!isRedirecting && (
                      <>
                        <h2>Sign In</h2>
                        <p className="mb-4">Sign In to your account</p>
                      </>
                    )}
                    <div className="text-center mt-3">
                      {isLoading && (
                        <CSpinner
                          color="primary"
                          component="span"
                          size="lg"
                          aria-hidden="true"
                        />
                      )}
                      {isRedirecting && (
                        <p className="mt-3">
                          Authentication Success. Redirecting...
                        </p>
                      )}
                    </div>
                    {/* <Formik
                    initialValues={initialValues}
                    onSubmit={handleSubmitLogin}
                    validationSchema={validationSchema}
                  >
                    {({
                      values,
                      errors,
                      touched,
                      handleChange,
                      handleSubmit,
                    }) => (
                      <CForm onSubmit={handleSubmit}>
                        <CInputGroup className="mb-3">
                          <CInputGroupPrepend>
                            <CInputGroupText>
                              <CIcon name="cil-user" />
                            </CInputGroupText>
                          </CInputGroupPrepend>
                          <CInput
                            autoComplete="none"
                            name="username"
                            type="text"
                            value={values.username}
                            placeholder="Username"
                            onChange={handleChange}
                            invalid={touched.username && !!errors.username}
                            required
                          />
                          <CInvalidFeedback>{errors.username}</CInvalidFeedback>
                        </CInputGroup>
                        <CInputGroup className="mb-4">
                          <CInputGroupPrepend>
                            <CInputGroupText>
                              <CIcon name="cil-lock-locked" />
                            </CInputGroupText>
                          </CInputGroupPrepend>
                          <CInput
                            name="password"
                            type="password"
                            value={values.password}
                            placeholder="Password"
                            autoComplete="none"
                            onChange={handleChange}
                            invalid={touched.password && !!errors.password}
                            required
                          />
                          <CInvalidFeedback>{errors.password}</CInvalidFeedback>
                        </CInputGroup>
                        <CRow>
                          <CCol xs="6">
                            <CButton
                              disabled={isSubmitting}
                              color="primary"
                              className="px-4"
                              onClick={handleSubmit}
                              type="submit"
                            >
                              {!isSubmitting ? (
                                "Sign In"
                              ) : (
                                <>
                                  <CSpinner
                                    component="span"
                                    size="sm"
                                    aria-hidden="true"
                                  />{" "}
                                  Loading...
                                </>
                              )}
                            </CButton>
                          </CCol>
                          <CCol xs="6" className="text-right">
                            <CButton
                              color="link"
                              className="px-0"
                              onClick={() => history.push("/forgot-username")}
                            >
                              Forgot Username?
                            </CButton>
                          </CCol>
                        </CRow>
                        
                      </CForm>
                    )}
                  </Formik> */}
                    {resAlert && (
                      <CAlert color={resAlert?.color} className="mt-3">
                        {resAlert?.message}
                      </CAlert>
                    )}
                  </CCardBody>
                </CCard>
              </CCardGroup>
            </CCol>
          </CRow>
        </CContainer>
      )}
    </div>
  );
};

export default Login;
