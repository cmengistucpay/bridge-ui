import React, { useState, useEffect } from "react";
import {
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CRow,
  CSpinner,
  CAlert,
} from "@coreui/react";
import {
  setAuthTokenBridge,
  getAuthTokenBridge,
} from "src/services/serviceAuth";
import { loginAzure, getUserAzure } from "src/services/serviceAzure";

const Login = (props) => {
  const { history } = props;
  const [resAlert, setResAlert] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    const authToken = getAuthTokenBridge();
    if (authToken) {
      history.push("/");
    } else {
      _fetchUserAzure();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const _fetchUserAzure = async () => {
    try {
      const res = await getUserAzure();
      if (res) {
        setAuthTokenBridge(res);
        window.location.href = "/";
      } else {
        loginAzure();
      }
    } catch (error) {
      console.error(error);
      setIsLoading(false);
      setResAlert({
        color: "danger",
        message: error?.response?.data?.error_description || "Network error.",
      });
    }
  };

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md="4">
            <CCardGroup>
              <CCard className="p-4">
                <CCardBody>
                  <div className="text-center mt-3">
                    {isLoading && (
                      <CSpinner
                        color="primary"
                        component="span"
                        size="lg"
                        aria-hidden="true"
                      />
                    )}
                    <p className="mt-3">
                      Authentication Success. Redirecting...
                    </p>
                  </div>
                  {resAlert && (
                    <CAlert color={resAlert?.color} className="mt-3">
                      {resAlert?.message}
                    </CAlert>
                  )}
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Login;
