import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CButton,
  CCard,
  CCardBody,
  CCardGroup,
  CCol,
  CContainer,
  CForm,
  CInput,
  CFormGroup,
  CLabel,
  CInvalidFeedback,
  CRow,
  CAlert,
  CSpinner,
} from "@coreui/react";
import { useFormik } from "formik";
import * as Yup from "yup";

import { briRegisterUser } from "src/services";

const validationSchema = () => {
  return Yup.object().shape({
    email: Yup.string(),
    userName: Yup.string()
      .required("Username is required")
      .matches(/^[A-Za-z0-9]*$/, "Only letters & numbers are allowed"),
    firstName: Yup.string()
      .required("First Name is required")
      .matches(/^([^0-9]*)$/, "Numbers are not allowed"),
    lastName: Yup.string()
      .required("Last Name is required")
      .matches(/^([^0-9]*)$/, "Numbers are not allowed"),
    phone: Yup.string()
      .max(15, "Max 15 digits")
      .matches(/^\+?[0-9]*$/, "Number format is invalid"),
    password: Yup.string()
      .min(8, "Password must be at least 8 characters")
      .required("Password is required"),
    passwordRepeat: Yup.string()
      .oneOf([Yup.ref("password"), null], "Passwords must match")
      .required("Password Confirmation is required"),
  });
};

const initialValues = {
  guid: "",
  email: "",
  userName: "",
  firstName: "",
  lastName: "",
  phone: "",
  password: "",
  passwordRepeat: "",
};

const Register = (props) => {
  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);

  const { history, location } = props;
  const [resAlert, setResAlert] = useState(null);
  const [isSubmitting, setIsSubmitting] = useState(false);

  useEffect(() => {
    const { search } = location;
    const params = new URLSearchParams(search);
    const guid = params.get("guid") || "";
    const email = params.get("email") || "";
    setFieldValue("guid", guid);
    setFieldValue("email", email);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleSubmitRegister = async (values) => {
    try {
      setIsSubmitting(true);
      setResAlert(null);
      const payload = {
        ...values,
      };

      const res = await briRegisterUser(payload);
      if (res) {
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "User successfully registered.",
        };
        dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
        history.push("/login");
      }
    } catch (error) {
      if (error.response) {
        if (error?.response?.data && typeof error.response.data === "string") {
          setResAlert({
            color: "danger",
            message: error?.response?.data,
          });
        } else if (error?.response?.data?.errors?.Password) {
          setResAlert({
            color: "danger",
            message: error?.response?.data?.errors?.Password[0],
          });
        } else if (error?.response?.data?.errors?.UserName) {
          setResAlert({
            color: "danger",
            message: error?.response?.data?.errors?.UserName[0],
          });
        } else {
          setResAlert({
            color: "danger",
            message: "Network Error",
          });
        }
      } else {
        setResAlert({
          color: "danger",
          message: "Network Error",
        });
      }
    } finally {
      setIsSubmitting(false);
    }
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitRegister,
  });
  const {
    values,
    touched,
    errors,
    setFieldValue,
    handleChange,
    handleBlur,
    handleSubmit,
  } = formik;

  return (
    <div className="c-app c-default-layout flex-row align-items-center">
      <CContainer>
        <CRow className="justify-content-center">
          <CCol md={4}>
            <CCardGroup>
              <CCard className="p-1 p-md-2">
                <CCardBody>
                  <CForm onSubmit={handleSubmit}>
                    <h5 className="mb-3">Enter Details</h5>
                    {values?.email && (
                      <CFormGroup>
                        <CLabel htmlFor="email">
                          Email Address<span className="label-required">*</span>
                        </CLabel>
                        <CInput
                          autoComplete="none"
                          disabled
                          name="email"
                          type="text"
                          value={values.email}
                          placeholder="Enter Email"
                          onChange={handleChange}
                          onBlur={handleBlur}
                          invalid={touched.email && !!errors.email}
                        />
                        <CInvalidFeedback>{errors.email}</CInvalidFeedback>
                      </CFormGroup>
                    )}
                    <CFormGroup>
                      <CLabel htmlFor="userName">
                        Username<span className="label-required">*</span>
                      </CLabel>
                      <CInput
                        autoComplete="none"
                        name="userName"
                        type="text"
                        value={values.userName}
                        placeholder="Enter Username"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        invalid={touched.userName && !!errors.userName}
                      />
                      <CInvalidFeedback>{errors.userName}</CInvalidFeedback>
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel htmlFor="firstName">
                        First Name<span className="label-required">*</span>
                      </CLabel>
                      <CInput
                        autoComplete="none"
                        name="firstName"
                        type="text"
                        value={values.firstName}
                        placeholder="Enter First Name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        invalid={touched.firstName && !!errors.firstName}
                      />
                      <CInvalidFeedback>{errors.firstName}</CInvalidFeedback>
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel htmlFor="lastName">
                        Last Name<span className="label-required">*</span>
                      </CLabel>
                      <CInput
                        autoComplete="none"
                        name="lastName"
                        type="text"
                        value={values.lastName}
                        placeholder="Enter Last Name"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        invalid={touched.lastName && !!errors.lastName}
                      />
                      <CInvalidFeedback>{errors.lastName}</CInvalidFeedback>
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel htmlFor="phone">Phone Number</CLabel>
                      <CInput
                        id="phone"
                        name="phone"
                        type="text"
                        value={values.phone}
                        placeholder="Enter Phone Number"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        invalid={touched.phone && !!errors.phone}
                      />
                      <CInvalidFeedback>{errors.phone}</CInvalidFeedback>
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel htmlFor="password">
                        Password<span className="label-required">*</span>
                      </CLabel>
                      <CInput
                        autoComplete="none"
                        name="password"
                        type="password"
                        value={values.password}
                        placeholder="Enter Password"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        invalid={touched.password && !!errors.password}
                      />
                      <CInvalidFeedback>{errors.password}</CInvalidFeedback>
                    </CFormGroup>
                    <CFormGroup>
                      <CLabel htmlFor="passwordRepeat">
                        Re-enter Password
                        <span className="label-required">*</span>
                      </CLabel>
                      <CInput
                        autoComplete="none"
                        name="passwordRepeat"
                        type="password"
                        value={values.passwordRepeat}
                        placeholder="Re-enter Password"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        invalid={
                          touched.passwordRepeat && !!errors.passwordRepeat
                        }
                      />
                      <CInvalidFeedback>
                        {errors.passwordRepeat}
                      </CInvalidFeedback>
                    </CFormGroup>
                    <div className="d-flex mt-2 mt-md-0">
                      <CButton
                        color="light"
                        className="mx-1 w-48"
                        onClick={() => history.push("/login")}
                      >
                        Login
                      </CButton>
                      <CButton
                        disabled={isSubmitting}
                        color="primary"
                        className="mx-1 w-48"
                        onClick={handleSubmit}
                        type="submit"
                      >
                        {!isSubmitting ? (
                          "Signup"
                        ) : (
                          <>
                            <CSpinner
                              component="span"
                              size="sm"
                              aria-hidden="true"
                            />{" "}
                            Loading...
                          </>
                        )}
                      </CButton>
                    </div>
                    {resAlert && (
                      <CAlert color={resAlert?.color} className="mt-3">
                        {resAlert?.message}
                      </CAlert>
                    )}
                  </CForm>
                </CCardBody>
              </CCard>
            </CCardGroup>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Register;
