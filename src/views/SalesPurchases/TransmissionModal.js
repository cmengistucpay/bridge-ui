import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CForm,
  CFormGroup,
  CLabel,
  CInput,
  CTextarea,
  CInvalidFeedback,
  CButton,
  CAlert,
  CSpinner,
} from "@coreui/react";
import { useFormik } from "formik";
import * as Yup from "yup";
import copy from "copy-to-clipboard";
import NumberFormat from "react-number-format";
import { purPostTransmission } from "src/services";

const initialValues = {
  EmailRecipient: "",
  MerchantPhoneNumber: "",
  EmailFrom: "",
  RecipientName: "",
  Message: "",
  Subject: "",
  //   EmailRecipient: "emailTo@gmail.com",
  //   MerchantPhoneNumber: "7411237489",
  //   EmailFrom: "emailFrom@gmail.com",
  //   RecipientName: "Smith",
  //   Message: "here is your card information...",
};

const validationSchema = (values) => {
  return Yup.object().shape({
    EmailRecipient: Yup.string().required("Email Recipient is required"),
    RecipientName: Yup.string().required("Name on Card is required"),
    Message: Yup.string().required("Message is required"),
  });
};

const TransmissionModal = (props) => {
  const { modalShow, handleHideModal, activeCardGuid } = props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);

  const [isSubmitting, setIsSubmitting] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  useEffect(() => {
    setResAlert(null);
  }, []);

  const handleSubmitForm = async (values) => {
    try {
      setIsSubmitting(true);
      setResAlert(null);
      const payload = {
        ...values,
        TransmissionMethods: ["Email"],
        DaysToExpire: 3,
      };
      console.log({ payload });
      const res = await purPostTransmission(activeCardGuid, payload);
      if (res) {
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "Payment Information Sent",
        };
        copy(res, {
          format: "text/plain",
          onCopy: () =>
            dispatch({ type: "set", toaster: [...toaster, toasterConfig] }),
        });
        resetForm();
        handleHideModal();
      }
    } catch (error) {
      setResAlert({
        color: "danger",
        message: error?.response?.data?.message || "Network error.",
      });
      console.error(error);
    } finally {
      setIsSubmitting(false);
    }
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitForm,
  });
  const {
    values,
    touched,
    errors,
    setFieldValue,
    handleChange,
    handleBlur,
    handleSubmit,
    resetForm,
  } = formik;

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Transmission Details</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CForm>
          <CFormGroup>
            <CLabel htmlFor="EmailRecipient">
              Recipient’s Email Address<span className="label-required">*</span>
            </CLabel>
            <CInput
              autoComplete="none"
              name="EmailRecipient"
              type="text"
              value={values.EmailRecipient}
              placeholder="Enter Recipient’s Email Address"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.EmailRecipient && !!errors.EmailRecipient}
            />
            <CInvalidFeedback>{errors.EmailRecipient}</CInvalidFeedback>
          </CFormGroup>

          <CFormGroup>
            <CLabel htmlFor="MerchantPhoneNumber">Merchant Phone Number</CLabel>
            <NumberFormat
              name="MerchantPhoneNumber"
              customInput={CInput}
              value={values.MerchantPhoneNumber}
              format="(###) ###-####"
              placeholder="Enter Merchant Phone Number"
              onValueChange={(val) =>
                setFieldValue("MerchantPhoneNumber", val.value)
              }
              mask={"_"}
              onBlur={handleBlur}
              invalid={
                touched.MerchantPhoneNumber && !!errors.MerchantPhoneNumber
              }
            />
            <CInvalidFeedback>{errors.MerchantPhoneNumber}</CInvalidFeedback>
          </CFormGroup>

          <CFormGroup>
            <CLabel htmlFor="EmailFrom">Sender’s Email Address</CLabel>
            <CInput
              autoComplete="none"
              name="EmailFrom"
              type="text"
              value={values.EmailFrom}
              placeholder="Enter Sender’s Email Address"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.EmailFrom && !!errors.EmailFrom}
            />
            <CInvalidFeedback>{errors.EmailFrom}</CInvalidFeedback>
          </CFormGroup>

          <CFormGroup>
            <CLabel htmlFor="RecipientName">
              Name on Card<span className="label-required">*</span>
            </CLabel>
            <CInput
              autoComplete="none"
              name="RecipientName"
              type="text"
              value={values.RecipientName}
              placeholder="Enter Name on Card"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.RecipientName && !!errors.RecipientName}
            />
            <CInvalidFeedback>{errors.RecipientName}</CInvalidFeedback>
          </CFormGroup>

          <CFormGroup>
            <CLabel htmlFor="Subject">
              Subject<span className="label-required">*</span>
            </CLabel>
            <CInput
              autoComplete="none"
              name="Subject"
              type="text"
              value={values.Subject}
              placeholder="Enter Subject"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.Subject && !!errors.Subject}
            />
            <CInvalidFeedback>{errors.Subject}</CInvalidFeedback>
          </CFormGroup>

          <CFormGroup>
            <CLabel htmlFor="Message">
              Email Message<span className="label-required">*</span>
            </CLabel>
            <CTextarea
              name="Message"
              rows="4"
              value={values.Message}
              placeholder="Enter Email Message"
              onChange={handleChange}
              onBlur={handleBlur}
              invalid={touched.Message && !!errors.Message}
            />
            <CInvalidFeedback>{errors.Message}</CInvalidFeedback>
          </CFormGroup>

          <div>
            {resAlert && (
              <CAlert color={resAlert?.color} className="mt-2 mb-0">
                {resAlert?.message}
              </CAlert>
            )}
          </div>
        </CForm>
      </CModalBody>
      <CModalFooter>
        <CButton
          disabled={isSubmitting}
          color="primary"
          className="px-4"
          onClick={handleSubmit}
          type="submit"
        >
          {!isSubmitting ? (
            "Submit"
          ) : (
            <>
              <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
              Loading...
            </>
          )}
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default TransmissionModal;
