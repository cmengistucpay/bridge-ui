import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CAlert,
  CSpinner,
} from "@coreui/react";
import { purTerminateCard } from "src/services";

const ModalTerminate = (props) => {
  const { modalShow, activeCardGuid, handleHideModal, handlePostSubmit } =
    props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);

  const [isLoading, setIsLoading] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  const handleSubmit = async () => {
    try {
      setIsLoading(true);
      setResAlert(null);
      await purTerminateCard(activeCardGuid);
      const toasterConfig = {
        show: true,
        type: "success",
        title: "Success",
        body: "Request successful",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      handleHideModal();
      handlePostSubmit();
    } catch (error) {
      setResAlert({
        color: "danger",
        message: error?.response?.data?.message || "Unable to cancel sale.",
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Terminate VCC</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <div className="d-flex justify-content-between align-items-center">
          <p className="mb-0">Are you sure you want to terminate this VCC?</p>
        </div>
        <div>
          {resAlert && (
            <CAlert color={resAlert?.color} className="mt-2 mb-0">
              {resAlert?.message}
            </CAlert>
          )}
        </div>
      </CModalBody>
      <CModalFooter>
        <CButton
          onClick={handleHideModal}
          color="light"
          className="mx-1 w-50 w-md-unset"
        >
          Cancel
        </CButton>
        <CButton
          disabled={isLoading}
          onClick={handleSubmit}
          color="primary"
          className="mx-1 w-50 w-md-unset"
        >
          {!isLoading ? (
            "Submit"
          ) : (
            <>
              <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
              Loading...
            </>
          )}
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default ModalTerminate;
