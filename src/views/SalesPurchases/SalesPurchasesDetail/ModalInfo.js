import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CAlert,
  CSpinner,
} from "@coreui/react";

const ModalInfo = (props) => {
  const { modalShow, data, handleHideModal } = props;
  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>{data?.title ? data?.title: 'Info'}</CModalTitle>
      </CModalHeader>
      <CModalBody>
        {data?.body}
      </CModalBody>
      <CModalFooter>
        <CButton
          onClick={handleHideModal}
          color="light"
          className="mx-1 w-50 w-md-unset">
          Ok
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default ModalInfo;
