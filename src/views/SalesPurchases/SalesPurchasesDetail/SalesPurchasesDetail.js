import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { isEmpty, sumBy } from "lodash";
import {
  CCard,
  CCardBody,
  CCardHeader,
  CButton,
  CDropdown,
  CDropdownToggle,
  CDropdownMenu,
  CDropdownItem,
  CSpinner,
  CTooltip,
} from "@coreui/react";

import {
  salGetSalesDetail,
  purGetPurchases,
  purGetIncomingTransactionCodeDetail,
  purGetIssuedCards,
  purGetCardDetail,
  getCardDetails
} from "src/services";
import DetailSales from "./DetailSales/DetailSales";
import PurchaseDetail from "./PurchaseDetail";
import Receipt from "src/views/Receipt/ReceiptSale";
import VirtualCardPurchase from "src/views/IssuePurchase/VirtualCardPurchase/VirtualCardPurchase";
import ACHPurchase from "src/views/IssuePurchase/ACHPurchase/ACHPurchase";

const SalesPurchasesDetail = (props) => {
  const { match } = props;
  const { itcId } = match.params;
  const labelMerchantData = JSON.parse(localStorage.getItem("labelMarchantData"));

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);
  const userData = useSelector((state) => state.userData);
  const accDevice = useSelector((state) => state.accDevices?.[0]);

  const [tableData, setTableData] = useState([]);
  const [purchaseDetails, setPurchaseDetails] = useState([]);
  const [purchaseCardDetails, setPurchaseCardDetails] = useState([]);
  const [itcData, setItcData] = useState({});

  const [modalReceipt, setModalReceipt] = useState(false);
  const [receiptData, setReceiptData] = useState("");
  const [receiptType, setReceiptType] = useState("");
  const [modalPurchase, setModalPurchase] = useState("");
  const [isLoadingSales, setIsLoadingSales] = useState(false);
  const [isLoadingPurchase, setIsLoadingPurchase] = useState(false);

  const fetchSalesDetails = async () => {
    try {
      setIsLoadingSales(true);
      let res = await salGetSalesDetail(itcId);

      if (res) {
        res = {
          ...res,
          relatedVoids: res?.relatedVoids?.map((val) => ({
            ...val,
            tenderType: "Void",
            orderNumber: res?.orderNumber,
          })),
          relatedReturns: res?.relatedReturns?.map((val) => ({
            ...val,
            tenderType: "Return",
            orderNumber: res?.orderNumber,
          })),
        };
        setTableData([
          ...[res],
          ...(res?.relatedVoids || []),
          ...(res?.relatedReturns || []),
          ...(res?.effectiveAmount !== res?.amount &&
            !isEmpty([
              ...(res?.relatedVoids || []),
              ...(res?.relatedReturns || []),
            ])
            ? [{ isNetSale: true }]
            : []),
        ]);
      }
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body:
          error?.response?.data?.message || "Unable to complete the request.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
    } finally {
      setIsLoadingSales(false);
    }
  };

  const fetchPurchaseDetails = async (merchantGuid) => {
    try {
      setIsLoadingPurchase(true);
      const payload = {
        MerchantGuid: merchantGuid || labelMerchantData?.merchantGuid,
        incomingTransactionCode: itcId,
        DeviceGuid: labelMerchantData?.deviceGuid,
      };
      const res = await purGetPurchases(1, 100, payload);
      if (res) {
        setPurchaseDetails(res?.searchResultDTO);
      }
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body:
          error?.response?.data?.message || "Unable to complete the request.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
    } finally {
      setIsLoadingPurchase(false);
    }
  };

  const fetchPurchaseCardDetails = async (merchantGuid) => {
    try {
      setIsLoadingPurchase(true);
      const payload = {
        MerchantGuid: merchantGuid || labelMerchantData?.merchantGuid,
        incomingTransactionCode: itcId,
        DeviceGuid: labelMerchantData?.deviceGuid,
      };

      const resDetail = await purGetIssuedCards(1, 100, payload);
      if (resDetail) {
        const resCardVCC = purchaseDetails?.filter(
          (val) => val.cardType !== "ACH"
        );
        const resCardUrls = resCardVCC?.map((val) =>
          purGetCardDetail(val?.guid)
        );
        const resCard = await Promise.all(resCardUrls);
        const resCardDetailsUrls = resCardVCC?.map((val) =>
        getCardDetails(val?.guid)
        );
        const resCardDetails = await Promise.all(resCardDetailsUrls);
        const arrDetail = purchaseDetails?.map((valDetail) => ({
          ...valDetail,
          card: resCard?.find(
            (valCard) => valCard?.cardGuid === valDetail?.guid
          ),
          cardDetails: resCardDetails?.find(
            (valCard) => valCard?.vccGuid === valDetail?.guid
          )
        }));
        setPurchaseCardDetails(arrDetail);
      }
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body:
          error?.response?.data?.message || "Unable to complete the request.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
    } finally {
      setIsLoadingPurchase(false);
    }
  };

  const fetchDataItcDetails = async () => {
    try {
      setIsLoadingSales(true);
      const res = await purGetIncomingTransactionCodeDetail(itcId);
      setItcData(res);
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body:
          error?.response?.data?.message || "Unable to complete the request.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
    } finally {
      setIsLoadingSales(false);
    }
  };

  useEffect(() => {
    fetchPurchaseDetails(labelMerchantData?.merchantGuid);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (purchaseDetails?.length) {
      fetchPurchaseCardDetails(labelMerchantData?.merchantGuid);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [purchaseDetails]);

  useEffect(() => {
    if (itcId) {
      fetchDataItcDetails();
      fetchSalesDetails();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [itcId]);

  const issuePurchaseDropdown = [
    {
      label: "Virtual Card Purchase",
      action: () => setModalPurchase("virtualCardPurchase"),
    },
    {
      label: "ACH Purchase",
      action: () => setModalPurchase("achPurchase"),
      className:
        userData?.merchantSettings?.showACHPurchases && userData?.canIssueACH
          ? ""
          : "d-none",
    },
  ];

  const isIssuePurchaseActive =
    itcData?.amount !== sumBy(purchaseCardDetails, "issuedAmount") &&
    itcData?.openTransactions !== 0 &&
    userData?.canCreateVCC &&
    userData?.merchantSettings?.showIssuePurchase &&
    !itcData?.status?.includes('Closed');

  const isIssuePurchaseActiveACH =
    itcData?.amount !== sumBy(purchaseCardDetails, "issuedAmount") &&
    itcData?.openTransactions !== 0 &&
    !userData?.canCreateVCC &&
    userData?.canIssueACH &&
    userData?.merchantSettings?.showIssuePurchase &&
    !itcData?.status?.includes('Closed');

  const isIssuePurchaseDisabled =
    itcData?.openTransactions === 0 ||
    itcData?.amount === sumBy(purchaseCardDetails, "issuedAmount") || 
    itcData?.status?.includes('Closed');

  return (
    <>
      <DetailSales
        isLoading={isLoadingSales}
        tableData={tableData}
        handleShowReceipt={(customerReceipt, receiptType) => {
          setModalReceipt(true);
          setReceiptData(customerReceipt);
          setReceiptType(receiptType);
        }}
        loadDetail={() => {
          fetchSalesDetails();
        }}
        userData={userData}
        itcData={itcData}
        loadItcData={() => {
          fetchDataItcDetails();
        }}
      />

      {userData?.merchantSettings?.showIssuePurchase && (
        <CCard>
          <CCardHeader className="d-md-flex flex-md-row justify-content-between align-items-center">
            <h4 className="mb-md-0">Purchase Details</h4>
            {isIssuePurchaseActive && (
              <CDropdown>
                <CDropdownToggle color="primary" className="no-arrow-down">
                  Issue Purchase
                </CDropdownToggle>
                <CDropdownMenu>
                  {issuePurchaseDropdown.map((action, i) => (
                    <CDropdownItem
                      key={i}
                      onClick={action.action}
                      className={`${action.className ? action.className : ""}`}
                    >
                      {action.label}
                    </CDropdownItem>
                  ))}
                </CDropdownMenu>
              </CDropdown>
            )}
            {isIssuePurchaseActiveACH && (
              <CButton
                onClick={() => setModalPurchase("achPurchase")}
                color="primary"
                size="md"
              >
                Issue Purchase
              </CButton>
            )}
            {isIssuePurchaseDisabled && (
              <CTooltip
                placement="bottom-end"
                content={
                  "No more purchases can be issued of this sale. Create a new sale to issue a purchase."
                }
              >
                <div className="d-inline-block">
                  <CButton className="btn-disabled-grey" disabled color="light">
                    Issue Purchase
                  </CButton>
                </div>
              </CTooltip>
            )}
          </CCardHeader>
          <CCardBody>
            {!isLoadingPurchase && (
              <PurchaseDetail
                purchaseDetails={purchaseCardDetails}
                loadDetail={() => {
                  fetchPurchaseDetails();
                  fetchPurchaseCardDetails();
                }}
              />
            )}
            {isLoadingPurchase && (
              <div className="text-center">
                <CSpinner grow component="span" aria-hidden="true" />
              </div>
            )}
          </CCardBody>
        </CCard>
      )}
      <Receipt
        modalShow={modalReceipt}
        modalData={receiptData}
        handleHideModal={() => setModalReceipt(false)}
        type={receiptType}
      />
      <VirtualCardPurchase
        connexPayTransaction={{
          incomingTransCode: itcData?.incomingTransactionCode,
        }}
        customerData={itcData?.customer}
        purchaseCount={itcData?.openTransactions}
        modalShow={modalPurchase === "virtualCardPurchase"}
        handleHideModal={() => setModalPurchase("")}
        accDevice={accDevice}
        handlePostSubmit={() => {
          fetchPurchaseDetails();
          fetchDataItcDetails();
        }}
      />
      <ACHPurchase
        connexPayTransaction={{
          incomingTransCode: itcData?.incomingTransactionCode,
        }}
        customerData={itcData?.customer}
        purchaseCount={itcData?.openTransactions}
        modalShow={modalPurchase === "achPurchase"}
        handleHideModal={() => setModalPurchase("")}
        accDevice={accDevice}
        handlePostSubmit={() => {
          fetchPurchaseDetails();
          fetchDataItcDetails();
        }}
      />
    </>
  );
};

export default SalesPurchasesDetail;
