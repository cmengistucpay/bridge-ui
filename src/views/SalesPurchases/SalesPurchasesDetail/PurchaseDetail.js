import React, { useState } from "react";
import {
  CBadge,
  CRow,
  CCol,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CDataTable,
  CTooltip,
  CSpinner,
} from "@coreui/react";
import { useSelector, useDispatch } from "react-redux";
import { format, parseISO, } from "date-fns";
import copy from "copy-to-clipboard";
import CIcon from "@coreui/icons-react";

import { CardVCC, CardACH } from "src/components";
import { optPurchaseType, formatUSD } from "src/helper";
import {
  purPostTransmission,
  purGetCardACHDetail,
  purGetCardTransaction,
} from "src/services";
import VccModal from "./VCCModal";
import ReloadAmountModal from "./ReloadAmountModal";
import TransmissionModal from "src/views/SalesPurchases/TransmissionModal";
import ModalTerminate from "./ModalTerminate";
import ModalInfo from "./ModalInfo";
import UpdateLodgedCard from "src/views/IssuePurchase/LodgedCard/UpdateLodgedCard"

const getBadge = (status) => {
  switch (true) {
    case status?.includes("Active"):
      return "success";
    case status?.includes("Deactivated"):
    case status?.includes("PENDING"):
      return "warning";
    case status?.includes("DECLINED"):
      return "danger";
    default:
      return "primary";
  }
};

const PurchaseDetail = (props) => {
  const { purchaseDetails, loadDetail } = props;

  const [modalCardShow, setModalCardShow] = useState(false);
  const [modalCardData, setModalCardData] = useState({});
  const [modalCardFields, setModalCardFields] = useState([]);
  const [modalVCC, setModalVCC] = useState(false);
  const [modalReloadAmount, setModalReloadAmount] = useState(false);
  const [modalTerminate, setModalTerminate] = useState(false);
  const [modalInfo, setModalInfo] = useState(false);
  const [modalInfoData, setModalInfoData] = useState(false);
  const [activeCardGuid, setActiveCardGuid] = useState("");
  const [activeCardData, setActiveCardData] = useState({});
  const [modalTransmission, setModalTransmission] = useState(false);
  const [isModalCardLoading, setIsModalCardLoading] = useState(false);
  const [modalUpdateLodgedCard, setModalUpdateLodgedCard] = useState(false);
  const [modalMoreShow, setModalMoreShow] = useState(false);
  const [modalMoreData, setModalMoreData] = useState([]);

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);
  const userData = useSelector((state) => state.userData);

  const handleClickEmail = (cardGuid) => {
    setModalTransmission(true);
    setActiveCardGuid(cardGuid);
  };

  const handleClickShare = async (cardGuid) => {
    try {
      const payload = {
        TransmissionMethods: ["Link"],
      };
      const res = await purPostTransmission(cardGuid, payload);
      if (res) {
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Copied",
          body: "Link to card was copied to your clipboard.",
        };
        copy(res?.linkToCard, {
          format: "text/plain",
          onCopy: () =>
            dispatch({ type: "set", toaster: [...toaster, toasterConfig] }),
        });
      }
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body: "Request cannot be completed.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
    }
  };

  const handleClickCopy = (accountNumber) => {
    const toasterConfig = {
      show: true,
      type: "success",
      title: "Copied",
      body: " Card number was copied to your clipboard.",
    };
    copy(accountNumber, {
      format: "text/plain",
      onCopy: () =>
        dispatch({ type: "set", toaster: [...toaster, toasterConfig] }),
    });
  };

  const handleShowModalCard = async (cardGuid, cardType) => {
    try {
      setModalCardShow(true);
      setIsModalCardLoading(true);
      if (cardType !== "ACH") {
        const res = await purGetCardTransaction(cardGuid);
        const transactions = res?.transactions.length && res?.transactions.map((transaction) => {
          return {
            ...transaction,
            formattedAmount: `$${transaction.amount} ${transaction.currencyCode}`,
          }
        });

        setModalCardData(transactions || []);
        setModalCardFields([
          "timestamp",
          "type",
          { key: "formattedAmount", label: "Amount" },
          "status",
          "description",
          "merchantName",
          { key: "transactionGuid", label: "Transaction ID" },
        ]);
      } else {
        const res = await purGetCardACHDetail(cardGuid);
        setModalCardData(res?.result?.transactions || []);
        setModalCardFields(["timestamp", "amount", "status", "merchant",]);
      }
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body: "Request cannot be completed.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
    } finally {
      setIsModalCardLoading(false);
    }
  };

  const calculateAndShowTotal = (type, transactions) => {

    const currencyList = [...new Set(transactions.map(transaction => transaction.currencyCode))];
    const currencyListLength = currencyList.length;

    if (currencyListLength) {
      if (currencyListLength === 1) {
        const currencyCode = currencyList[0];
        const total = transactions.filter(transaction => transaction.type === type).reduce((a, b) => a + b.amount, 0);

        let label = "";
        switch (type) {
          case "authorization":
            label = "Total Authorized:";
            break;
          case "authorization.clearing":
            label = "Total Settled:";
            break;
          case "refund":
            label = "Total Refunded:";
            break;
          default:
            label = "";
        }
        return (
          <span>
            {label} <span className="font-weight-bold">{`$${total.toFixed(2)} ${currencyCode}`}</span>
          </span>
        );
      } else {
        let amounts = [];
        // eslint-disable-next-line array-callback-return
        currencyList.map((currencyCode) => {
          const total = transactions
            .filter(transaction => transaction.type === type && transaction.currencyCode === currencyCode)
            .reduce((a, b) => a + b.amount, 0);
          if (total > 0) {
            const currencyTotal = `$${total.toFixed(2)} ${currencyCode}`;
            amounts.push(currencyTotal);
          }
        });

        const totalAmount = amounts.join("|");

        let label = "";
        switch (type) {
          case "authorization":
            label = "Total Authorized:";
            break;
          case "authorization.clearing":
            label = "Total Settled:";
            break;
          case "refund":
            label = "Total Refunded:";
            break;
          default:
            label = "";
        }

        return (
          <span>
            {label} <span className="font-weight-bold">{`${totalAmount}`}</span>
          </span>
        );
      }
    }
  };

  const handleHideModalCard = () => {
    setModalCardShow(false);
    setModalCardData({});
  };

  const handleShowModalVCC = (cardGuid, cardData) => {
    setModalVCC(true);
    setActiveCardGuid(cardGuid);
    setActiveCardData(cardData);
  };
  const handleShowModalInfo = (title, body) => {
    setModalInfo(true);
    setModalInfoData({ title, body });
  };

  const handleShowModalReloadAmount = (cardGuid, cardData) => {
    setModalReloadAmount(true);
    setActiveCardGuid(cardGuid);
    setActiveCardData(cardData);
  };

  const handleShowModalTerminate = (cardGuid) => {
    setModalTerminate(true);
    setActiveCardGuid(cardGuid);
  };

  const handleShowUpdateLodgedCard = (cardGuid) => {
    setModalUpdateLodgedCard(true);
    setActiveCardGuid(cardGuid);
  };

  const handleShowModalMore = (item) => {
    setModalMoreShow(true);

    let arrItems = [];
    arrItems = [
      {
        label: "Virtual Card GUID",
        value: item?.vccGuid,
      },
      { label: "Cardholder Name", value: item?.cardHolderName },
      { label: "Supplier ID", value: item?.supplierId },
      { label: "Association ID", value: item?.associationId },
      { label: "Tolerance Amount", value: item?.toleranceAmount },
      // { label: "Tolerance Percent", value: item?.tolerancePercent },
      {
        label: "Incoming Transaction Code",
        value: item?.incomingTransactionCode,
      },
      { label: "Outgoing Transaction Code", value: item?.outgoingTransactionCode },
      { label: "Global Card", value: item?.globalCard? 'Yes' : 'No' },
      { label: "Created By", value: item?.createdBy },
      { label: "Issuer ID", value: item?.issuerId },
      { label: "Bank ID", value: item?.bankId }
    ];
    setModalMoreData(arrItems);
  };
  const handleHideModalMore = () => {
    setModalMoreShow(false);
    setModalMoreData([]);
  };
  const renderModalMore = () => {
    return (
      <CModal
        show={modalMoreShow}
        onClose={handleHideModalMore}
        color="secondary"
        size="lg"
      >
        <CModalHeader closeButton>
          <CModalTitle>Additional Data</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <div>
            {modalMoreData?.map((val, i) => (
              <div
                key={i}
                className="d-flex flex-row justify-content-between py-2 border-bottom"
              >
                <p className="mb-0 w-25">{val?.label}</p>
                <p className="mb-0 w-75 text-right">{val?.value}</p>
              </div>
            ))}
          </div>
        </CModalBody>
      </CModal>
    );
  };

  const renderModalCard = () => {
    return (
      <CModal
        show={modalCardShow}
        onClose={handleHideModalCard}
        color="secondary"
        size="xl"
      >
        <CModalHeader closeButton>
          <CModalTitle>Transaction Data</CModalTitle>
        </CModalHeader>
        <CModalBody>
          {isModalCardLoading && (
            <div className="text-center my-2">
              <CSpinner
                color="primary"
                component="span"
                size="sm"
                aria-hidden="true"
              />
            </div>
          )}
          {modalCardData?.length ? (
            <>
              <p className="text-right mb-1">
                {calculateAndShowTotal("authorization", modalCardData)}
              </p>

              <p className="text-right mb-1">
                {calculateAndShowTotal("authorization.clearing", modalCardData)}
              </p>

              <p className="text-right mb-3">
                {calculateAndShowTotal("refund", modalCardData)}
              </p>

              <CDataTable
                loading={isModalCardLoading}
                items={modalCardData}
                fields={modalCardFields}
                scopedSlots={{
                  timestamp: (item) => (
                    <td>
                      {format(parseISO(`${item.timestamp}Z`), "Pp")} <br/>
                    </td>
                  ),
                  status: (item) => (
                    <td>
                      <CBadge color={getBadge(item?.status)}>
                        {item?.status}
                      </CBadge>
                    </td>
                  ),
                }}
                itemsPerPage={10}
                noItemsViewSlot={<div className="my-5" />}
              />
            </>
          ) : null}
          {!isModalCardLoading && !modalCardData?.length && (
            <>
              <p className="text-center">No Transaction Activity To Date</p>
            </>
          )}
        </CModalBody>
      </CModal>
    );
  };

  return (
    <>
      <CRow>
        {purchaseDetails?.map((val, i) => {
          const cardType = optPurchaseType.find(
            (type) => val?.card?.purchaseType === type.value
          );
          const isDeactive = val?.card?.status?.includes("Deactivated");
          return (
            <CCol key={i} xxl="6" className="px-md-2 px-md-0">
              <div className="card-shadow align-items-start d-md-flex px-2 px-md-3 py-2 mb-4 mx-md-2">
                <div>
                  <CTooltip
                    placement="bottom-end"
                    content={"See Transaction History"}
                  >
                    <div
                      className="hover-pointer"
                      onClick={() =>
                        handleShowModalCard(val.guid, val.cardType)
                      }
                    >
                      {val?.cardType !== "ACH" ? (
                        <CardVCC data={val} />
                      ) : (
                        <CardACH data={val} />
                      )}
                    </div>
                  </CTooltip>
                  <div>
                    <div className={`card-button-list mb-2 mb-md-0 mt-2`}>
                      {val?.cardType !== "ACH" && !isDeactive && (
                        <div
                          className="_item"
                          onClick={() => handleClickEmail(val.guid)}
                        >
                          <CTooltip
                            placement="bottom-end"
                            content={"Click to send card via email"}
                          >
                            <CIcon size="sm" name="cil-mail" />
                          </CTooltip>
                        </div>
                      )}
                      {val?.cardType !== "ACH" && !isDeactive && (
                        <div
                          className="_item"
                          onClick={() => handleClickShare(val.guid)}
                        >
                          <CTooltip
                            placement="bottom-end"
                            content={"Click to copy shareable URL"}
                          >
                            <CIcon size="sm" name="cil-link" />
                          </CTooltip>
                        </div>
                      )}
                      {val?.cardType !== "ACH" && !isDeactive && (
                        <div
                          className="_item"
                          onClick={() =>
                            handleClickCopy(val?.card?.accountNumber)
                          }
                        >
                          <CTooltip
                            placement="bottom-end"
                            content={"Click to copy card number"}
                          >
                            <CIcon size="sm" name="cil-copy" />
                          </CTooltip>
                        </div>
                      )}
                      {val?.cardType !== "ACH" && !isDeactive && (
                        <div
                          className="_item"
                          onClick={() =>
                            handleShowModalMore(val?.cardDetails)
                          }
                        >
                          <CTooltip
                            placement="bottom-end"
                            content={"Additional Virtual Card Details"}
                          >
                            <CIcon size="sm" name="cis-options-alt" />
                          </CTooltip>
                        </div>
                      )}
                    </div>
                  </div>
                </div>
                <div className="pl-md-2 ml-md-2 mt-2 mt-md-1 w-100">
                  <div>
                    <div className="d-flex flex-row justify-content-between">
                      <p>Status</p>
                      <div>
                        <CBadge color={getBadge(val?.cardStatus)}>
                          {val?.cardStatus}
                        </CBadge>
                      </div>
                    </div>
                    {/* {val?.cardType !== "ACH" ? (
                      val?.card?.leftoverAuthorizations ? (
                        <div className="d-flex flex-row justify-content-between">
                          <p>Authorizations Remaining</p>
                          <div>
                            <p className="text-right">
                              {val?.card?.leftoverAuthorizations || "0"}
                            </p>
                          </div>
                        </div>
                      ) : (
                        <div className="d-flex flex-row justify-content-between">
                          <p>Max Authorizations</p>
                          <div>
                            <p className="text-right">Unlimited</p>
                          </div>
                        </div>
                      )
                    ) : null} */}
                    <div className="d-flex flex-row justify-content-between">
                      <p>Issued Date</p>
                      <p className="text-right">
                        {val?.timeStamp &&
                          format(parseISO(val?.timeStamp), "Pp")}
                      </p>
                    </div>
                    <div className="d-flex flex-row justify-content-between mb-2">
                      <p className="mb-0">Issued Amount</p>
                      <div className="d-flex">
                        <p className="text-right mb-0">
                          {val?.issuedAmount
                            ? formatUSD(val?.issuedAmount)
                            : "-"}
                        </p>
                        {val?.cardType !== "ACH" &&
                          !isDeactive &&
                          userData?.canReloadVcc && (
                            <div
                              className="icon-button-primary hover-pointer ml-1"
                              onClick={() =>
                                handleShowModalReloadAmount(val.guid, val)
                              }
                            >
                              <CTooltip placement="top-end" content={"Reload"}>
                                <CIcon size="sm" name="cil-reload" />
                              </CTooltip>
                            </div>
                          )}
                      </div>
                    </div>
                    {val?.cardStatus.includes('Lodged Card') && (
                      <div className="d-flex flex-row justify-content-between mb-2">
                        <p className="mb-0">Limit Window</p>
                        <div className="d-flex">
                          <p className="text-right mb-0">
                            {val?.card?.limitWindow
                              ? val?.card?.limitWindow
                              : ""}
                          </p>
                        </div>
                      </div>
                    )}
                    {val?.cardType !== "ACH" && (
                      <div className="d-flex flex-row justify-content-between mb-2">
                        <p className="mb-0">MCC Group</p>
                        <div className="d-flex">
                          <p className="text-right mb-0">
                            {cardType?.value
                              ? `${cardType.value} - ${cardType.label}`
                              : ""}
                          </p>
                          {cardType?.value !== "98" &&
                            !isDeactive &&
                            userData?.canUnlockVcc && (
                              <div
                                className="icon-button-primary hover-pointer ml-1"
                                onClick={() => handleShowModalVCC(val.guid)}
                              >
                                <CTooltip
                                  placement="bottom-end"
                                  content={"Unlock"}
                                >
                                  <CIcon size="sm" name="cil-lock-locked" />
                                </CTooltip>
                              </div>
                            )}
                        </div>
                      </div>
                    )}
                    {val?.cardStatus.includes('Lodged Card') && (
                      <div className="d-flex flex-row justify-content-between mb-2">
                        <p className="mb-0">MID Whitelist</p>
                        <div className="d-flex">
                          <div className="text-right mb-0">
                            <div
                              className="icon-button-primary bg-success text-white hover-pointer ml-1"
                              onClick={() => handleShowModalInfo('MID Whitelist', (val?.card?.midWhitelist?.length > 0
                                ? val?.card?.midWhitelist.join()
                                : ""))}>
                              Show
                            </div>

                          </div>
                        </div>
                      </div>
                    )}
                    {val?.cardStatus.includes('Lodged Card') && (
                      <div className="d-flex flex-row justify-content-between mb-2">
                        <p className="mb-0">MID Blacklist</p>
                        <div className="d-flex">
                          <div className="text-right mb-0">
                            <div
                              className="icon-button-primary bg-danger text-white hover-pointer ml-1"
                              onClick={() => handleShowModalInfo('MID Blacklist', (val?.card?.midBlacklist?.length > 0
                                ? val?.card?.midBlacklist.join()
                                : ""))}>
                              Show
                            </div>
                          </div>
                        </div>
                      </div>
                    )}
                    {val?.cardType !== "ACH" && (
                      <div className="d-flex flex-row justify-content-between">
                        <p className="mb-0">Termination Date</p>
                        <div className="d-flex">
                          <p className="text-right mb-0">
                            {val.terminateDate
                              ? format(parseISO(val.terminateDate), "P")
                              : "None"}
                          </p>
                          {!isDeactive && (
                            <div
                              className="icon-button-primary bg-danger text-white hover-pointer ml-1"
                              onClick={() => handleShowModalTerminate(val.guid)}
                            >
                              <CTooltip
                                placement="bottom-end"
                                content={"Terminate immediately"}
                              >
                                <CIcon size="sm" name="cis-x" />
                              </CTooltip>
                            </div>
                          )}
                        </div>
                      </div>
                    )}
                    {val?.cardStatus.includes('Lodged Card') && (
                      <div className="d-flex flex-row justify-content-between mb-2">
                        <div
                          className=" mb-0 icon-button-primary bg-primary text-white hover-pointer ml-1"
                          onClick={() => handleShowUpdateLodgedCard(val.guid)}>
                          Edit Spend Settings
                        </div>
                      </div>
                    )}
                  </div>
                </div>
              </div>
            </CCol>
          );
        })}
      </CRow>
      {renderModalMore()}
      <TransmissionModal
        modalShow={modalTransmission}
        activeCardGuid={activeCardGuid}
        handleHideModal={() => setModalTransmission(false)}
      />
      <VccModal
        modalShow={modalVCC}
        activeCardGuid={activeCardGuid}
        handleHideModal={() => setModalVCC(false)}
        handlePostSubmit={loadDetail}
      />
      <ReloadAmountModal
        modalShow={modalReloadAmount}
        activeCardGuid={activeCardGuid}
        activeCardData={activeCardData}
        handleHideModal={() => setModalReloadAmount(false)}
        handlePostSubmit={loadDetail}
      />
      <ModalTerminate
        modalShow={modalTerminate}
        activeCardGuid={activeCardGuid}
        handleHideModal={() => setModalTerminate(false)}
        handlePostSubmit={() => {
          loadDetail();
        }}
      />
      <ModalInfo
        modalShow={modalInfo}
        data={modalInfoData}
        handleHideModal={() => setModalInfo(false)}
      />
      <UpdateLodgedCard
        modalShow={modalUpdateLodgedCard}
        activeCardGuid={activeCardGuid}
        handleHideModal={() => setModalUpdateLodgedCard(false)}
        handlePostSubmit={() => {
          loadDetail();
        }}
      />
      {renderModalCard()}
    </>
  );
};

export default PurchaseDetail;
