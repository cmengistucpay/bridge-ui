import { useState, useEffect } from "react";
// import { useSelector, useDispatch } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CAlert,
  CSpinner,
  CFormGroup,
  CLabel,
  CInput,
} from "@coreui/react";
import { useFormik } from "formik";
import * as Yup from "yup";
import NumberFormat from "react-number-format";

const initialValues = {
  Amount: "",
  Date: "",
};

const validationSchema = () => {
  return Yup.object().shape({});
};

const ModalActivationSettings = (props) => {
  const { modalShow, handleHideModal } = props;

  //   const dispatch = useDispatch();
  //   const toaster = useSelector((state) => state.toaster);

  const [isLoading, setIsLoading] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  useEffect(() => {
    setResAlert(null);
  }, [modalShow]);

  const handleSubmitActivate = async (values) => {
    try {
      setIsLoading(true);
      setResAlert(null);
      const payload = {
        ...values,
      };
      console.log({ payload });
      //   const res = await purUpdateIssuedCard(activeCardGuid, payload);

      //   const toasterConfig = {
      //     show: true,
      //     type: "success",
      //     title: "Success",
      //     body: "The VCC was successfully reloaded.",
      //   };
      //   dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      handleHideModal();
      // handlePostSubmit(activeCardGuid);
    } catch (error) {
      setResAlert({
        color: "danger",
        message: error?.response?.data?.message || "Network error.",
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  const formik = useFormik({
    initialValues,
    validationSchema,
    onSubmit: handleSubmitActivate,
  });
  const { values, setFieldValue, handleChange, handleBlur, handleSubmit } =
    formik;

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Activation Settings</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CFormGroup>
          <CLabel htmlFor="Amount">Amount</CLabel>
          <NumberFormat
            name="Amount"
            value={values.Amount}
            onValueChange={(val) => setFieldValue("Amount", val.floatValue)}
            customInput={CInput}
            prefix="$"
            decimalScale={2}
            fixedDecimalScale
            thousandSeparator
            placeholder="Enter Amount"
          />
        </CFormGroup>
        <CFormGroup>
          <CLabel htmlFor="Date">Date</CLabel>
          <CInput
            autoComplete="none"
            name="Date"
            type="date"
            value={values.Date}
            placeholder="Enter Date"
            onChange={handleChange}
            onBlur={handleBlur}
          />
        </CFormGroup>
        <div>
          {resAlert && (
            <CAlert color={resAlert?.color} className="mt-2 mb-0">
              {resAlert?.message}
            </CAlert>
          )}
        </div>
      </CModalBody>
      <CModalFooter>
        <CButton
          disabled={isLoading}
          onClick={handleSubmit}
          color="primary"
          className="mx-1 w-50 w-md-unset"
        >
          {!isLoading ? (
            "Activate"
          ) : (
            <>
              <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
              Loading...
            </>
          )}
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default ModalActivationSettings;
