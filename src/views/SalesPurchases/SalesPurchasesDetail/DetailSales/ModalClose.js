import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CAlert,
  CSpinner,
} from "@coreui/react";
import { purUpdateIncomingTransactionCodeDetail } from "src/services";

const ModalClose = (props) => {
  const { modalShow, itcData, handleHideModal, handlePostSubmit} = props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);

  const [isLoading, setIsLoading] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  const handleSubmit = async () => {
    try {
      setIsLoading(true);
      setResAlert(null);
      const payload = {
       ...itcData,
        status: 'IncomingTransactionCode - Closed',
      };
      const res = await purUpdateIncomingTransactionCodeDetail(itcData?.incomingTransactionCode,payload);
      if (res) {
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "The sale has been closed successfully",
        };
        dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
        handleHideModal();
        handlePostSubmit(res, "Cancel");
      }
    } catch (error) {
      const toasterConfig = {
        show: true,
        type: "danger",
        title: "Failed",
        body: error?.response?.data?.message || "We are unable to close the sale at this time. Please contact your relationship manager for assistance on closing the sale.",
      };
      dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Close Sale</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <div className="d-flex justify-content-between align-items-center">
          <p className="mb-0">Are you sure you want to close this sale?</p>
        </div>
        <div>
          {resAlert && (
            <CAlert color={resAlert?.color} className="mt-2 mb-0">
              {resAlert?.message}
            </CAlert>
          )}
        </div>
      </CModalBody>
      <CModalFooter>
        <CButton
          onClick={handleHideModal}
          color="light"
          className="mx-1 w-50 w-md-unset"
        >
          Cancel
        </CButton>
        <CButton
          disabled={isLoading}
          onClick={handleSubmit}
          color="primary"
          className="mx-1 w-50 w-md-unset"
        >
          {!isLoading ? (
            "Submit"
          ) : (
            <>
              <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
              Loading...
            </>
          )}
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default ModalClose;
