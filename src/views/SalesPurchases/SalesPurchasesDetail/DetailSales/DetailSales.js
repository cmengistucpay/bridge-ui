import React, { useState } from "react";
import {
  CCard,
  CCardHeader,
  CCardBody,
  CBadge,
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CDataTable,
  CDropdown,
  CDropdownToggle,
  CDropdownMenu,
  CDropdownItem,
  CButton,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { format, parseISO } from "date-fns";
import { formatUSD } from "src/helper";

import ModalCancel from "./ModalCancel";
import ModalVoid from "./ModalVoid";
import ModalReturn from "./ModalReturn";
import ModalActivationSettings from "./ModalActivationSettings";
import ModalClose from "./ModalClose";

const mainFields = [
  "timeStamp",
  "amount",
  "type",
  "status",
  "orderNumber",
  "customerID",
  "lastFour",
  "customerName",
  "createdBy",
  "additionalData",
  "receipt",
];

const getBadge = (status) => {
  switch (true) {
    case status?.includes("Active"):
    case status?.includes("Approved"):
      return "success";
    case status?.includes("Deactivated"):
      return "warning";
    case status?.includes("DECLINED"):
      return "danger";
    default:
      return "primary";
  }
};

const DetailSales = (props) => {
  const { isLoading, tableData, handleShowReceipt, loadDetail, userData, itcData, loadItcData} =
    props;

  const [modalMoreShow, setModalMoreShow] = useState(false);
  const [modalMoreData, setModalMoreData] = useState([]);

  const [modalAction, setModalAction] = useState("");

  const actionsDropdown = [
    ...(userData?.canVoid
      ? [
          {
            label: "Void",
            action: () => setModalAction("void"),
            disabled: !tableData?.[0]?.isVoidAllowed,
          },
        ]
      : []),
    ...(userData?.canReturn
      ? [{ label: "Return", action: () => setModalAction("return") }]
      : []),
    { label: "Cancel", action: () => setModalAction("cancel") },
    { label: "Close", action: () => setModalAction("close"), disabled: itcData?.status?.includes('Closed') },
    ...(userData?.merchantSettings?.allowDelayedActivation
      ? [{ label: "Activate", action: () => setModalAction("activate") }]
      : []),
    ...(userData?.merchantSettings?.allowDelayedActivation
      ? [
          {
            label: "Change Activation Settings",
            action: () => setModalAction("activationSettings"),
          },
        ]
      : []),
  ];

  const renderDropdownAction = () => {
    return (
      <CDropdown>
        <CDropdownToggle color="primary" className="no-arrow-down px-3">
          Actions
        </CDropdownToggle>
        <CDropdownMenu>
          {actionsDropdown.map((action, i) => (
            <CDropdownItem
              key={i}
              onClick={action.action}
              disabled={action?.disabled}
            >
              {action.label}
            </CDropdownItem>
          ))}
        </CDropdownMenu>
      </CDropdown>
    );
  };

  const handleShowModalMore = (item) => {
    setModalMoreShow(true);

    let arrItems = [];
    arrItems = [
      {
        label: "Incoming Transaction Code",
        value: item?.incomingTransactionCode,
      },
      { label: "Batch Status", value: item?.batchStatus },
      { label: "Device GUID", value: item?.deviceGuid },
      { label: "Activated", value: item?.activated ? "true" : "" },
      { label: "Batch GUID", value: item?.batchGuid },
      { label: "Processor Status Code", value: item?.processorStatusCode },
      {
        label: "Processor Reponse Message",
        value: item?.processorResponseMessage,
      },
      { label: "Reference Number", value: item?.refNumber },
      {
        label: "Address Verification Result",
        value: item?.addressVerificationResult,
      },
      { label: "CVV Verification Result", value: item?.cvvVerificationResult },
      { label: "CVV Response Code", value: item?.cavvResponseCode },
      {
        label: "Auth Code",
        value: item?.authCode,
      },
      { label: "GUID", value: item?.guid },
    ];
    setModalMoreData(arrItems);
  };

  const handleHideModalMore = () => {
    setModalMoreShow(false);
    setModalMoreData([]);
  };

  const renderModalMore = () => {
    return (
      <CModal
        show={modalMoreShow}
        onClose={handleHideModalMore}
        color="secondary"
        size="lg"
      >
        <CModalHeader closeButton>
          <CModalTitle>Additional Data</CModalTitle>
        </CModalHeader>
        <CModalBody>
          <div>
            {modalMoreData?.map((val, i) => (
              <div
                key={i}
                className="d-flex flex-row justify-content-between py-2 border-bottom"
              >
                <p className="mb-0 w-25">{val?.label}</p>
                <p className="mb-0 w-75 text-right">{val?.value}</p>
              </div>
            ))}
          </div>
        </CModalBody>
      </CModal>
    );
  };

  return (
    <>
      <CCard>
        <CCardHeader className="d-md-flex flex-md-row justify-content-between align-items-center">
          <h4 className="mb-md-0">Sales Details</h4>
          {renderDropdownAction()}
        </CCardHeader>
        <CCardBody>
          <div className="table-not-relative">
            <CDataTable
              loading={isLoading}
              className="mb-0"
              items={[...tableData]}
              fields={mainFields}
              itemsPerPage={10}
              noItemsViewSlot={<div className="my-5" />}
              columnHeaderSlot={{
                lastFour: "Last 4",
                timeStamp: "Timestamp",
                additionalData: (
                  <div className="text-center">Additional Data</div>
                ),
                action: <div className="text-center">Action</div>,
              }}
              scopedSlots={{
                timeStamp: (item) => {
                  if (!item.isNetSale) {
                    return (
                      <td>
                        {item.timeStamp &&
                          format(parseISO(item.timeStamp), "Pp")}
                      </td>
                    );
                  } else {
                    return <td className="font-weight-bold">Net Sale</td>;
                  }
                },
                amount: (item) => {
                  if (!item.isNetSale) {
                    return <td>{`$ ${item?.amount?.toFixed(2) ?? "-"}`}</td>;
                  } else {
                    return (
                      <td className="font-weight-bold">
                        {formatUSD(tableData?.[0]?.effectiveAmount)}
                      </td>
                    );
                  }
                },
                customerName: (item) => {
                  const firstName = item?.customer?.firstName || "";
                  const lastName = item?.customer?.lastName || "";
                  const fullName = `${firstName} ${lastName}`;
                  if (!item.isNetSale) {
                    return (
                      <td>
                        {item?.customerName ||
                          item?.card?.cardHolderName ||
                          fullName ||
                          ""}
                      </td>
                    );
                  } else {
                    return <td />;
                  }
                },
                orderNumber: (item) => <td>{item?.orderNumber ?? ""}</td>,
                customerID: (item) => <td>{item?.customerID ?? ""}</td>,
                type: (item) => <td>{item?.tenderType || ""}</td>,
                lastFour: (item) => <td>{item?.card?.last4 ?? ""}</td>,
                status: (item) => (
                  <td>
                    {item?.cardStatus && (
                      <CBadge color={getBadge(item?.cardStatus)}>
                        {item?.cardStatus}
                      </CBadge>
                    )}
                    {item?.status && (
                      <CBadge color={getBadge(item?.status)}>
                        {item?.status}
                      </CBadge>
                    )}
                  </td>
                ),
                createdBy: (item) => <td>{item.generatedBy || ""}</td>,
                additionalData: (item) => {
                  if (!item.isNetSale) {
                    return (
                      <td className="text-center">
                        <CButton
                          onClick={() => handleShowModalMore(item)}
                          color="primary"
                        >
                          <CIcon name="cis-options-alt" />
                        </CButton>
                      </td>
                    );
                  } else {
                    return <td />;
                  }
                },
                receipt: (item) => {
                  if (!item.isNetSale) {
                    return (
                      <td className="text-center">
                        <CButton
                          color="primary"
                          onClick={() =>
                            handleShowReceipt(item, item?.tenderType)
                          }
                        >
                          <CIcon name="cil-script" />
                        </CButton>
                      </td>
                    );
                  } else {
                    return <td />;
                  }
                },
              }}
            />
          </div>
        </CCardBody>
      </CCard>

      {renderModalMore()}
      <ModalCancel
        modalShow={modalAction === "cancel"}
        saleGuid={tableData?.[0]?.guid}
        handleHideModal={() => setModalAction("")}
        handlePostSubmit={(item, receiptType) => {
          loadDetail();
          handleShowReceipt(item, receiptType);
        }}
      />
      <ModalVoid
        modalShow={modalAction === "void"}
        saleGuid={tableData?.[0]?.guid}
        saleAmount={tableData?.[0]?.effectiveAmount}
        handleHideModal={() => setModalAction("")}
        handlePostSubmit={(item, receiptType) => {
          loadDetail();
          handleShowReceipt(item, receiptType);
        }}
      />
      <ModalReturn
        modalShow={modalAction === "return"}
        saleGuid={tableData?.[0]?.guid}
        saleAmount={tableData?.[0]?.effectiveAmount}
        handleHideModal={() => setModalAction("")}
        handlePostSubmit={(item, receiptType) => {
          loadDetail();
          handleShowReceipt(item, receiptType);
        }}
      />
      <ModalActivationSettings
        modalShow={modalAction === "activationSettings"}
        handleHideModal={() => setModalAction("")}
      />
       <ModalClose
        modalShow={modalAction === "close"}
        itcData={itcData}
        handleHideModal={() => setModalAction("")}
        handlePostSubmit={() => {
          loadDetail();
          loadItcData();
        }}
      />
    </>
  );
};

export default DetailSales;
