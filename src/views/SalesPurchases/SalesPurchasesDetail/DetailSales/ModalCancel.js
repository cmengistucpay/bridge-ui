import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CAlert,
  CSpinner,
} from "@coreui/react";
import { salPostSalesCancel } from "src/services";

const ModalCancel = (props) => {
  const { modalShow, saleGuid, handleHideModal, handlePostSubmit } = props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);
  const accDevice = useSelector((state) => state.accDevices?.[0]);

  const [isLoading, setIsLoading] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  const handleSubmit = async () => {
    try {
      setIsLoading(true);
      setResAlert(null);
      const payload = {
        DeviceGuid: accDevice?.guid,
        SaleGuid: saleGuid,
      };
      const res = await salPostSalesCancel(payload);
      if (res) {
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "Request successful",
        };
        dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
        handleHideModal();
        handlePostSubmit(res, "Cancel");
      }
    } catch (error) {
      setResAlert({
        color: "danger",
        message: error?.response?.data?.message || "Unable to cancel sale.",
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Cancel Sale</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <div className="d-flex justify-content-between align-items-center">
          <p className="mb-0">Are you sure you want to cancel this sale?</p>
        </div>
        <div>
          {resAlert && (
            <CAlert color={resAlert?.color} className="mt-2 mb-0">
              {resAlert?.message}
            </CAlert>
          )}
        </div>
      </CModalBody>
      <CModalFooter>
        <CButton
          onClick={handleHideModal}
          // onClick={() =>
          //   handlePostSubmit(
          //     "Auto Merchant GNQOGR\\n35 Edinburgh St\\n\\nVancouver British Columbia  V5K 1B7\\n8/26/2021 3:50:58 PM\\n\\nCASH - SALE\\n\\nEntry Mode : MANUAL\\n\\nTRANSACTION ID : 63765575458646\\nInvoice number : Cash-63765575458646\\nSubtotal:                      $100.00\\n--------------------------------------\\nTotal:                         $100.00\\n--------------------------------------\\n\\n\\n\\nCUSTOMER ACKNOWLEDGES RECEIPT OF\\nGOODS AND/OR SERVICES IN THE AMOUNT\\nOF THE TOTAL SHOWN HEREON AND AGREES\\nTO PERFORM THE OBLIGATIONS SET FORTH\\nBY THE CUSTOMER`S AGREEMENT WITH THE\\nISSUER\\nAPPROVED\\n\\n\\n\\n\\nCustomer Copy\\n"
          //   )
          // }
          color="light"
          className="mx-1 w-50 w-md-unset"
        >
          Cancel
        </CButton>
        <CButton
          disabled={isLoading}
          onClick={handleSubmit}
          color="primary"
          className="mx-1 w-50 w-md-unset"
        >
          {!isLoading ? (
            "Submit"
          ) : (
            <>
              <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
              Loading...
            </>
          )}
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default ModalCancel;
