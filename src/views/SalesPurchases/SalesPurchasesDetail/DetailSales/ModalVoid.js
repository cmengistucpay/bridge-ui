import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CAlert,
  CSpinner,
  CFormGroup,
  CLabel,
  CInput,
} from "@coreui/react";
import NumberFormat from "react-number-format";
import { salPostSalesVoid } from "src/services";

const ModalVoid = (props) => {
  const { modalShow, saleGuid, saleAmount, handleHideModal, handlePostSubmit } =
    props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);
  const accDevice = useSelector((state) => state.accDevices?.[0]);

  const [amount, setAmount] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  useEffect(() => {
    setAmount(saleAmount);
    setResAlert(null);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  const handleSubmit = async () => {
    try {
      setIsLoading(true);
      const payload = {
        DeviceGuid: accDevice?.guid,
        SaleGuid: saleGuid,
        Amount: amount,
      };
      const res = await salPostSalesVoid(payload);
      if (res) {
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "Request successful",
        };
        dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
        handleHideModal();
        handlePostSubmit(res, "Void");
      }
    } catch (error) {
      setResAlert({
        color: "danger",
        message: error?.response?.data?.message || "Unable to void sale.",
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Void Sale</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CFormGroup>
          <CLabel htmlFor="Amount">Amount</CLabel>
          <NumberFormat
            name="Amount"
            value={amount}
            onValueChange={(val) => setAmount(val.floatValue)}
            customInput={CInput}
            prefix="$"
            decimalScale={2}
            fixedDecimalScale
            thousandSeparator
            placeholder="Enter Amount"
          />
        </CFormGroup>
        <div>
          {resAlert && (
            <CAlert color={resAlert?.color} className="mt-2 mb-0">
              {resAlert?.message}
            </CAlert>
          )}
        </div>
      </CModalBody>
      <CModalFooter>
        <CButton
          onClick={handleHideModal}
          // onClick={() =>
          //   handlePostSubmit(
          //     "CONNEXPAY\\n8320 S HARDY DRIVE\\nTEMPE AZ 85284\\n08/26/2021 10:57:23\\n\\nCREDIT - VOID\\n\\nCARD # : **** **** **** 0000\\nCARD TYPE :DEBIT\\nEntry Mode : MANUAL\\n\\nTRANSACTION ID : 11649693\\nInvoice number : 11600045\\n\\nVoid Amount:                    $10.00\\n--------------------------------------\\n\\n\\n\\n\\n\\nCUSTOMER ACKNOWLEDGES RECEIPT OF\\nGOODS AND/OR SERVICES IN THE AMOUNT\\nOF THE TOTAL SHOWN HEREON AND AGREES\\nTO PERFORM THE OBLIGATIONS SET FORTH\\nBY THE CUSTOMER`S AGREEMENT WITH THE\\nISSUER\\nAPPROVED\\n\\n\\n\\n\\nCustomer Copy\\n"
          //   )
          // }
          color="light"
          className="mx-1 w-50 w-md-unset"
        >
          Cancel
        </CButton>
        <CButton
          disabled={!amount || isLoading}
          onClick={handleSubmit}
          color="primary"
          className="mx-1 w-50 w-md-unset"
        >
          {!isLoading ? (
            "Submit"
          ) : (
            <>
              <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
              Loading...
            </>
          )}
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default ModalVoid;
