import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CAlert,
  CSpinner,
  CFormGroup,
  CLabel,
  CInput,
} from "@coreui/react";
import NumberFormat from "react-number-format";
import { salPostSalesReturn } from "src/services";

const ModalReturn = (props) => {
  const { modalShow, saleGuid, saleAmount, handleHideModal, handlePostSubmit } =
    props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);
  const accDevice = useSelector((state) => state.accDevices?.[0]);

  const [amount, setAmount] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  useEffect(() => {
    setAmount(saleAmount);
    setResAlert(null);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  const handleSubmit = async () => {
    try {
      setIsLoading(true);
      const payload = {
        DeviceGuid: accDevice?.guid,
        SaleGuid: saleGuid,
        ReturnRetryCard: null,
        Amount: amount,
      };
      const res = await salPostSalesReturn(payload);
      if (res) {
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "Request successful",
        };
        dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
        handleHideModal();
        handlePostSubmit(res, "Return");
      }
    } catch (error) {
      setResAlert({
        color: "danger",
        message: error?.response?.data?.message || "Unable to return sale.",
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Return</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CFormGroup>
          <CLabel htmlFor="Amount">Amount</CLabel>
          <NumberFormat
            name="Amount"
            value={amount}
            onValueChange={(val) => setAmount(val.floatValue)}
            customInput={CInput}
            prefix="$"
            decimalScale={2}
            fixedDecimalScale
            thousandSeparator
            placeholder="Enter Amount"
          />
        </CFormGroup>
        <div>
          {resAlert && (
            <CAlert color={resAlert?.color} className="mt-2 mb-0">
              {resAlert?.message}
            </CAlert>
          )}
        </div>
      </CModalBody>
      <CModalFooter>
        <CButton
          onClick={handleHideModal}
          // onClick={() =>
          //   handlePostSubmit(
          //     "Auto Merchant GNQOGR\\n35 Edinburgh St\\n\\nVancouver British Columbia  V5K 1B7\\n8/26/2021 5:45:56 PM\\n\\nCASH - SALE\\n\\nEntry Mode : MANUAL\\n\\nTRANSACTION ID : 63765582356641\\nInvoice number : Cash-63765582356641\\nSubtotal:                      $2.00\\n--------------------------------------\\nTotal:                         $2.00\\n--------------------------------------\\n\\n\\n\\nCUSTOMER ACKNOWLEDGES RECEIPT OF\\nGOODS AND/OR SERVICES IN THE AMOUNT\\nOF THE TOTAL SHOWN HEREON AND AGREES\\nTO PERFORM THE OBLIGATIONS SET FORTH\\nBY THE CUSTOMER`S AGREEMENT WITH THE\\nISSUER\\nAPPROVED\\n\\n\\n\\n\\nCustomer Copy\\n"
          //   )
          // }
          color="light"
          className="mx-1 w-50 w-md-unset"
        >
          Cancel
        </CButton>
        <CButton
          disabled={!amount || isLoading}
          onClick={handleSubmit}
          color="primary"
          className="mx-1 w-50 w-md-unset"
        >
          {!isLoading ? (
            "Submit"
          ) : (
            <>
              <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
              Loading...
            </>
          )}
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default ModalReturn;
