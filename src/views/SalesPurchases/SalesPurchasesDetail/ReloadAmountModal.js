import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CAlert,
  CSpinner,
  CFormGroup,
  CLabel,
  CInput,
} from "@coreui/react";
import NumberFormat from "react-number-format";
import { purUpdateIssuedCard } from "src/services";

const ReloadAmount = (props) => {
  const { modalShow, activeCardGuid, handleHideModal, handlePostSubmit } =
    props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);

  const [amount, setAmount] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  useEffect(() => {
    setAmount("");
    setResAlert(null);
  }, [modalShow]);

  const handleSubmit = async () => {
    try {
      setIsLoading(true);
      setResAlert(null);
      const payload = {
        reloadAmount: amount,
      };
      const res = await purUpdateIssuedCard(activeCardGuid, payload);
      if (res) {
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "The VCC was successfully reloaded.",
        };
        dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
        handleHideModal();
        handlePostSubmit(activeCardGuid);
      }
    } catch (error) {
      setResAlert({
        color: "danger",
        message: error?.response?.data?.message || "Network error.",
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Reload Amount</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CFormGroup>
          <CLabel htmlFor="Amount">Amount</CLabel>
          <NumberFormat
            name="Amount"
            value={amount}
            onValueChange={(val) => setAmount(val.floatValue)}
            customInput={CInput}
            prefix="$"
            decimalScale={2}
            fixedDecimalScale
            thousandSeparator
            placeholder="Enter Amount"
          />
        </CFormGroup>
        <div>
          {resAlert && (
            <CAlert color={resAlert?.color} className="mt-2 mb-0">
              {resAlert?.message}
            </CAlert>
          )}
        </div>
      </CModalBody>
      <CModalFooter>
        <CButton
          onClick={handleHideModal}
          color="light"
          className="mx-1 w-50 w-md-unset"
        >
          Cancel
        </CButton>
        <CButton
          disabled={!amount || isLoading}
          onClick={handleSubmit}
          color="primary"
          className="mx-1 w-50 w-md-unset"
        >
          {!isLoading ? (
            "Submit"
          ) : (
            <>
              <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
              Loading...
            </>
          )}
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default ReloadAmount;
