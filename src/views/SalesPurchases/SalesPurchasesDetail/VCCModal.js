import { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
  CTooltip,
  CAlert,
  CSpinner,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { purUpdateIssuedCard } from "src/services";

const tooltipInfoHelper = {
  bulkUpload:
    "This will remove restrictions on the VCC and allow it to be used at any supplier.",
};

const VccModal = (props) => {
  const { modalShow, activeCardGuid, handleHideModal, handlePostSubmit } =
    props;

  const dispatch = useDispatch();
  const toaster = useSelector((state) => state.toaster);

  const [isLoading, setIsLoading] = useState(false);
  const [resAlert, setResAlert] = useState(null);

  const handleSubmit = async () => {
    try {
      setIsLoading(true);
      const payload = {
        purchaseType: "98",
      };
      const res = await purUpdateIssuedCard(activeCardGuid, payload);
      if (res) {
        const toasterConfig = {
          show: true,
          type: "success",
          title: "Success",
          body: "VCC Unlocked",
        };
        dispatch({ type: "set", toaster: [...toaster, toasterConfig] });
        handleHideModal();
        handlePostSubmit();
      }
    } catch (error) {
      setResAlert({
        color: "danger",
        message: error?.response?.data?.message || "Network error.",
      });
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>Unlock VCC</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <div className="d-flex justify-content-between align-items-center">
          <p className="mb-0">Are you sure you want to unlock the VCC?</p>
          <CTooltip
            placement="bottom-end"
            content={tooltipInfoHelper["bulkUpload"]}
          >
            <CIcon className="ml-3" name="cil-info-circle" height={18} />
          </CTooltip>
        </div>
        <div>
          {resAlert && (
            <CAlert color={resAlert?.color} className="mt-2 mb-0">
              {resAlert?.message}
            </CAlert>
          )}
        </div>
      </CModalBody>
      <CModalFooter>
        <CButton
          onClick={handleHideModal}
          color="light"
          className="mx-1 w-50 w-md-unset"
        >
          Cancel
        </CButton>
        <CButton
          disabled={isLoading}
          onClick={handleSubmit}
          color="primary"
          className="mx-1 w-50 w-md-unset"
        >
          {!isLoading ? (
            "Submit"
          ) : (
            <>
              <CSpinner component="span" size="sm" aria-hidden="true" />{" "}
              Loading...
            </>
          )}
        </CButton>
      </CModalFooter>
    </CModal>
  );
};

export default VccModal;
