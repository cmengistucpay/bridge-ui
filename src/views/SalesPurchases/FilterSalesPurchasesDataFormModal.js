import { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CRow,
  CCol,
  CForm,
  CFormGroup,
  CLabel,
  CInput,
  CInvalidFeedback,
  CButton,
  CAlert,
} from "@coreui/react";
import { useFormik } from "formik";
import Select from "react-select";
// import * as Yup from "yup";

const types = ["Virtual Card Purchase", "ACH Purchase", "Credit Sale", "ACH Sale", "Cash Sale"];
const statuses = ["Approved - Open", "Approved - Closed", "Approved - Voided", "Declined", "Approved - Returned", "Approved - Partial Return",];

const optType = types.map((val) => ({
  value: val,
  label: val,
}));

const optStatus = statuses.map((val) => ({
  value: val,
  label: val,
}));


// const validationSchema = (values) => {
//   return Yup.object().shape({
//     startDate: Yup.date(),
//     // .min(new Date(), "Expiration Date must be greater than today"),
//     endDate: Yup.date(),
//     customerName: Yup.string()
//       .max(50, "Maximum 50 characters")
//       .matches(/^[ A-Za-z]*$/, "Only letters & whitespace are allowed"),
//     OrderNumber: Yup.string()
//       .max(50, "Maximum 50 characters")
//       .matches(
//         /^[ A-Za-z0-9'|.-]*$/,
//         "Only letters & numbers, -, ., | and ' are allowed"
//       ),
//     amountFrom: Yup.number()
//       .min(0.01, "Must be more than $ 0.01")
//       .max(999999.99, "Must be less than $ 999,999.99"),
//     amountTo: Yup.number()
//       .min(0.01, "Must be more than $ 0.01")
//       .max(999999.99, "Must be less than $ 999,999.99"),
//   });
// };

const FilterSalesPurchasesDataFormModal = (props) => {
  const { modalShow,
    handleHideModal,
    handleSubmitFilter,
  } = props;
  const dispatch = useDispatch();
  const initialValues = useSelector((state) => state.initialFilterSalesPurchase) ?? {
    startDate: "",
    endDate: "",
    customerName: "",
    type: "",
    orderNumber: "",
    amountFrom: "",
    amountTo: "",
    lastFour: "",
    status: "",
    searchTerm: "",
    associationId: ''
  };
  const [resAlert, setResAlert] = useState(null);

  const handleSubmitForm = (values) => {
    dispatch({
      type: "set",
      initialFilterSalesPurchase:values
    });
    handleSubmitFilter(values);
    handleHideModal();
  };

  const formik = useFormik({
    initialValues,
    // validationSchema,
    onSubmit: handleSubmitForm,
  });

  const resetSearchValue = () => {
    formik.setFieldValue("startDate", "");
    formik.setFieldValue("endDate", "");
    formik.setFieldValue("customerName", "");
    formik.setFieldValue("type", "");
    formik.setFieldValue("orderNumber", "");
    formik.setFieldValue("amountFrom", "");
    formik.setFieldValue("amountTo", "");
    formik.setFieldValue("lastFour", "");
    formik.setFieldValue("status", "");
    formik.setFieldValue("searchTerm", "");
    formik.setFieldValue("associationId", "");
    dispatch({
      type: "set",
      initialFilterSalesPurchase:{}
    });
  };

  const {
    values,
    touched,
    errors,
    setFieldValue,
    handleChange,
    handleBlur,
    handleSubmit
  } = formik;

  useEffect(() => {
    setResAlert(null);
    // resetForm();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [modalShow]);

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
      size="md"
    >
      <CModalHeader closeButton>
        <CModalTitle>Filter Table</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <CForm>
          <CRow>
            <CCol md="6">
              <CFormGroup>
                <CLabel htmlFor="startDate">
                  Start Date
                </CLabel>
                <CInput
                  autoComplete="none"
                  name="startDate"
                  type="date"
                  value={values.startDate}
                  placeholder="Enter Start Date"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={touched.startDate && !!errors.startDate}
                />
                <CInvalidFeedback>{errors.startDate}</CInvalidFeedback>
              </CFormGroup>
            </CCol>

            <CCol md="6">
              <CFormGroup>
                <CLabel htmlFor="endDate">
                  End Date
                </CLabel>
                <CInput
                  autoComplete="none"
                  name="endDate"
                  type="date"
                  value={values.endDate}
                  placeholder="Enter End Date"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={touched.endDate && !!errors.endDate}
                />
                <CInvalidFeedback>{errors.endDate}</CInvalidFeedback>
              </CFormGroup>
            </CCol>

            <CCol md="12">
              <CFormGroup>
                <CLabel htmlFor="customerName">Customer Name</CLabel>
                <CInput
                  autoComplete="none"
                  name="customerName"
                  type="text"
                  value={values.customerName}
                  placeholder="Enter Customer Name"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={touched.customerName && !!errors.customerName}
                />
                <CInvalidFeedback>{errors.customerName}</CInvalidFeedback>
              </CFormGroup>
            </CCol>

            <CCol md="12">
              <CFormGroup>
                <CLabel htmlFor="type">
                  Type
                </CLabel>
                <div
                  className={
                    touched.type && !!errors.type
                      ? "is-invalid"
                      : ""
                  }
                >
                  <Select
                    classNamePrefix="default-select"
                    name="type"
                    value={
                      optType.find(
                        (val) => val.value === values.type
                      ) || ""
                    }
                    options={optType}
                    onChange={(selected) =>
                      setFieldValue("type", selected.value)
                    }
                    placeholder="Select Type"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    invalid={touched.type && !!errors.type}
                    isSearchable={false}
                  />
                </div>
                <CInvalidFeedback>{errors.type}</CInvalidFeedback>
              </CFormGroup>
            </CCol>

            <CCol md="12">
              <CFormGroup>
                <CLabel htmlFor="orderNumber">Order Number</CLabel>
                <CInput
                  autoComplete="none"
                  name="orderNumber"
                  type="text"
                  value={values.orderNumber}
                  placeholder="Enter Order Number"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={touched.orderNumber && !!errors.orderNumber}
                />
                <CInvalidFeedback>{errors.orderNumber}</CInvalidFeedback>
              </CFormGroup>
            </CCol>

            <CCol md="12">
              <CFormGroup>
                <CLabel htmlFor="associationId">Association ID</CLabel>
                <CInput
                  autoComplete="none"
                  name="associationId"
                  type="text"
                  value={values.associationId}
                  placeholder="Enter Association ID"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={touched.associationId && !!errors.associationId}
                />
                <CInvalidFeedback>{errors.associationId}</CInvalidFeedback>
              </CFormGroup>
            </CCol>

            <CCol md="12">
              <CFormGroup>
                <CLabel>Amount</CLabel>
                <CRow>
                  <CCol md="6">
                    <CInput
                      autoComplete="none"
                      name="amountFrom"
                      type="text"
                      value={values.amountFrom}
                      placeholder="Amount From"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      invalid={touched.amountFrom && !!errors.amountFrom}
                    />
                    <CInvalidFeedback>{errors.amountFrom}</CInvalidFeedback>
                  </CCol>
                  <CCol md="6">
                    <CInput
                      autoComplete="none"
                      name="amountTo"
                      type="text"
                      value={values.amountTo}
                      placeholder="Amount To"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      invalid={touched.amountTo && !!errors.amountTo}
                    />
                    <CInvalidFeedback>{errors.amountTo}</CInvalidFeedback>
                  </CCol>
                </CRow>
              </CFormGroup>
            </CCol>

            <CCol md="12">
              <CFormGroup>
                <CLabel htmlFor="lastFour">Last Four</CLabel>
                <CInput
                  autoComplete="none"
                  name="lastFour"
                  type="text"
                  value={values.lastFour}
                  placeholder="Last Four"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={touched.lastFour && !!errors.lastFour}
                />
                <CInvalidFeedback>{errors.lastFour}</CInvalidFeedback>
              </CFormGroup>
            </CCol>

            <CCol md="12">
              <CFormGroup>
                <CLabel htmlFor="status">
                  Status
                </CLabel>
                <div
                  className={
                    touched.status && !!errors.status
                      ? "is-invalid"
                      : ""
                  }
                >
                  <Select
                    classNamePrefix="default-select"
                    name="status"
                    value={
                      optStatus.find(
                        (val) => val.value === values.status
                      ) || ""
                    }
                    options={optStatus}
                    onChange={(selected) =>
                      setFieldValue("status", selected.value)
                    }
                    placeholder="Select Status"
                    components={{
                      IndicatorSeparator: () => null,
                    }}
                    invalid={touched.status && !!errors.type}
                    isSearchable={false}
                  />
                </div>
                <CInvalidFeedback>{errors.type}</CInvalidFeedback>
              </CFormGroup>
            </CCol>

            <CCol md="12">
              <CFormGroup>
                <CLabel htmlFor="searchTerm">Card GUID</CLabel>
                <CInput
                  id="searchTerm"
                  autoComplete="none"
                  name="searchTerm"
                  type="text"
                  value={values.searchTerm}
                  placeholder="Enter Card GUID"
                  onChange={handleChange}
                  onBlur={handleBlur}
                  invalid={touched.searchTerm && !!errors.searchTerm}
                />
                <CInvalidFeedback>{errors.searchTerm}</CInvalidFeedback>
              </CFormGroup>
            </CCol>
          </CRow>

          <div className="mt-2 d-flex justify-content-end">
            <CButton
              onClick={resetSearchValue}
              color="light"
              className="px-4 mx-1 w-50 w-md-unset"
            >
              Clear
            </CButton>
            <CButton
              color="primary"
              className="px-4 py-2"
              onClick={handleSubmit}
              type="submit"
            >
              Submit
            </CButton>
          </div>
          <div>
            {resAlert && (
              <CAlert color={resAlert?.color} className="mt-3">
                {resAlert?.message}
              </CAlert>
            )}
          </div>
        </CForm>
      </CModalBody>
    </CModal>
  );
};

export default FilterSalesPurchasesDataFormModal;
