import React, {
  useState,
  useEffect,
} from "react";
import { useSelector, useDispatch } from "react-redux";
import { isEmpty, isEqual } from "lodash";
import { format, parseISO, subDays, startOfDay, endOfDay } from "date-fns";
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CDataTable,
  CPagination,
  CButton,
  CTooltip,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { CSVLink } from "react-csv";

import { briGetSales } from "./../../services";
import FilterSalesPurchasesDataFormModal from "./FilterSalesPurchasesDataFormModal";
import { useLocation } from 'react-router-dom';

const getBadge = (status) => {
  switch (true) {
    case status?.includes("Active"):
    case status?.includes("Approved"):
      return "success";
    case status?.includes("Declined"):
    case status?.includes("Deactivated"):
      return "danger";
    default:
      return "primary";
  }
};

const dateToday = new Date();
const startDate = subDays(dateToday, 7);
const endDate = dateToday;

const SalesPurchases = (props) => {
  const { history } = props;
  const dispatch = useDispatch();
  const globalLabel = useSelector((state) => state.globalLabel);
  const shouldReloadSalesAndPurchasesTable = useSelector((state) => state.shouldReloadSalesAndPurchasesTable);
  const userData = useSelector((state) => state.userData);
  const merchantSettings = userData?.merchantSettings;

  const [tableData, setTableData] = useState([]);
  const [exportData, setExportData] = useState("");
  const [tableParams, setTableParams] = useState(null);
  const [tableFilter, setTableFilter] = useState(useSelector((state) => state.initialFilterSalesPurchase) ?? {});
  // const [startDate, setStartDate] = useState(subDays(dateToday, 7));
  // const [endDate, setEndDate] = useState(dateToday);
  const [isLoading, setIsLoading] = useState(false);
  const [isCSVDataLoading, setIsCSVDataLoading] = useState(false);
  const [isFilteringOn, setIsFilteringOn] = useState(useSelector((state) => state.initialFilterOn) ?? false);
  const [hasFilterDataChanged, setHasFilterDataChanged] = useState(false);
  const [showFilterFormModal, setShowFilterFormModal] = useState(false);
  const location = useLocation();

  const fetchSales = async (exportCSV) => {
    try {

      if (exportCSV) {
        setIsCSVDataLoading(true)
      } else {
        setIsLoading(true);
      }

      const payload = {
        labelIds: globalLabel?.map((val) => val),
        pageNumber: exportCSV ? 1 : (tableParams?.pageCurrent > 1 && hasFilterDataChanged ? 1 : tableParams?.pageCurrent),
        startDate: tableFilter.startDate ? new Date(tableFilter.startDate) : startOfDay(startDate),
        endDate: tableFilter.endDate ? new Date(tableFilter.endDate) : endOfDay(endDate),
        customerName: tableFilter.customerName ? tableFilter.customerName : null,
        type: tableFilter.type ? tableFilter.type : null,
        orderNumber: tableFilter.orderNumber ? tableFilter.orderNumber : null,
        associationId: tableFilter.associationId ? tableFilter.associationId : null,
        amountFrom: tableFilter.amountFrom ? tableFilter.amountFrom : null,
        amountTo: tableFilter.amountTo ? tableFilter.amountTo : null,
        lastFour: tableFilter.lastFour ? tableFilter.lastFour : null,
        status: tableFilter.status ? tableFilter.status : null,
        searchTerm: tableFilter.searchTerm ? tableFilter.searchTerm : null,
        exportCSV,
      };

      if (isFilteringOn) {
        if (!tableFilter.startDate) {
          delete payload.startDate;
        }
        if (!tableFilter.endDate) {
          delete payload.endDate;
        }
      }

      const res = await briGetSales(payload);

      if (exportCSV) {
        setExportData(res);
      } else {
        setTableData(res?.searchResults);
        setHasFilterDataChanged(false);
        setExportData([]);

        if (res?.totalCount) {
          setTableParams({
            ...tableParams,
            pageCurrent: res?.currentPage,
            pageTotal: res?.totalPageCount,
          });
        } else {
          setTableParams({
            ...tableParams,
            pageTotal: 1,
          });
        }
      }

    } catch (error) {
      // console.error(error);
    } finally {
      setIsLoading(false);
      setIsCSVDataLoading(false);
      dispatch({ type: "set", shouldReloadSalesAndPurchasesTable: false });
    }
  };

  useEffect(() => {
    const params = new URLSearchParams(location.search);
    if(params){
      let paramObj = {};
      for(var value of params.keys()) {
          paramObj[value] = params.get(value);
      }
      if(!(JSON.stringify(paramObj) === JSON.stringify({})))
      {
        setTableFilter(paramObj);
      } else {
        setTableFilter({});
      }
    }
  }, [
    location
  ]);

  useEffect(() => {
    if (!isEmpty(userData) && !isEmpty(tableParams)) {
      fetchSales();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    userData,
    globalLabel,
    tableParams?.pageCurrent,
    // tableParams?.endDate,
    tableFilter
  ]);

  useEffect(() => {
    // _getParams();
    setTableParams({
      pageCurrent: 1,
      pageTotal: 10,
    });
  }, []);

  // const _getParams = () => {
  //   const getParamsPersist = localStorage.getItem("cxp-data-persist");
  //   const objParams = getParamsPersist ? JSON.parse(getParamsPersist) : null;
  //   const salesPurchasesParams = objParams?.salesPurchases;

  //   if (!isEmpty(salesPurchasesParams)) {
  //     setTableParams(salesPurchasesParams);
  //     setStartDate(new Date(salesPurchasesParams?.startDate) || null);
  //     setEndDate(new Date(salesPurchasesParams?.endDate) || null);
  //   } else {
  //     setTableParams({
  //       pageCurrent: 1,
  //       pageTotal: 10,
  //       startDate: subDays(dateToday, 7),
  //       endDate: dateToday,
  //     });
  //   }
  // };

  // const _setParams = (newTableParams) => {
  //   const objParams = {
  //     salesPurchases: newTableParams,
  //   };
  //   localStorage.setItem("cxp-data-persist", JSON.stringify(objParams));
  // };

  const handleChangePage = (page) => {
    const newTableParams = {
      ...tableParams,
      pageCurrent: page,
    };
    setTableParams(newTableParams);
    // _setParams(newTableParams);
  };

  const handleShowMFilterFormModal = () => {
    setShowFilterFormModal(true);
  };

  const handleHideModelUserForm = () => {
    setShowFilterFormModal(false);
  };

  const handleSubmitFilter = (values) => {
    // setTableParams({...tableParams, pageCurrent: 1});

    if (!isEmpty(tableFilter) && !isEqual(values, tableFilter)) {
      setHasFilterDataChanged(true);
    }
    setIsFilteringOn(true);
    setTableFilter(values);
    handleClick(values);
    dispatch({
      type: "set",
      initialFilterOn: true
    });
  };

  const handleClick = (values) =>{
    history.push({
      pathname: '/sales-purchases',
      search: "?" + new URLSearchParams(values).toString()
  });
  }

  const reloadTableData = () => {
    setTableFilter({});
    setIsFilteringOn(false);
    setTableParams({ ...tableParams, pageCurrent: 1 });
    dispatch({
      type: "set",
      initialFilterSalesPurchase: {}
    });
    dispatch({
      type: "set",
      initialFilterOn: false
    });
    // setHasFilterDataChanged(false);
    // _setParams({...tableParams, pageCurrent: 1});
  };

  useEffect(() => {
    if (shouldReloadSalesAndPurchasesTable) {
      reloadTableData();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [shouldReloadSalesAndPurchasesTable]);

  const renderBadge = (item) => {
    const status = item?.status ? item?.status : "";
    const processorMessage = item?.processorMessage ? item?.processorMessage : "";

    switch (true) {
      case status.includes("Declined"):

        return (
          <CTooltip
            placement="bottom-end"
            content={<span>{processorMessage}</span>}
          >
            <CBadge color={getBadge(status)}>
              {status}
            </CBadge>
          </CTooltip>
        );

      case status.includes("Partial Return"):

        let remainingAmount, content;
        if (!isNaN(item?.amountReturned) && !isNaN(item?.amount)) {
          remainingAmount = item?.amount - item?.amountReturned;
          content = (
            <>
              <p className="mb-0">{`Return Amount: $${item?.amountReturned.toFixed(2)}`}</p>
              <p className="mb-0">{`Remaining Amount: $${remainingAmount.toFixed(2)}`}</p>
            </>
          );
        }

        return (
          <CTooltip
            placement="bottom-end"
            content={content}
          >
            <CBadge color={getBadge(status)}>
              {status}
            </CBadge>
          </CTooltip>
        );
      default:
        return <CBadge color={getBadge(status)}>{status}</CBadge>;
    }
  };

  return (
    <>
      <CCard>
        <CCardHeader className="d-md-flex flex-md-row justify-content-between align-items-center">
          <h4 className="mr-2 mb-md-0">Sales And Purchases</h4>
          <div className="d-flex">
            <CButton
              color="light"
              className="px-3 py-1 mr-2"
              onClick={reloadTableData}
            >
              <CIcon name="cil-reload" height={12} />
            </CButton>
            <CButton
              color="primary"
              className="px-3 py-1 mr-2"
              onClick={() => handleShowMFilterFormModal()}
            >
              <CIcon name="cil-filter" size="sm" className="mr-2" />
              Filter
            </CButton>

            {
              !!tableData.length && !exportData.length &&
              <CButton
                color="primary"
                className="px-3 py-1 mr-2"
                onClick={() => fetchSales(true)}
                disabled={isCSVDataLoading}
              >
                {isCSVDataLoading ? "Loading..." : "Export"}
              </CButton>
            }

            {
              !!exportData.length &&
              <CSVLink
                data={exportData}
                filename={`sales-purchase-data${Date.now()}.csv`}
                className="btn btn-primary"
                target="_blank"
              >
                Download
              </CSVLink>
            }
          </div>
        </CCardHeader>
        <CCardBody>
          <CDataTable
            onRowClick={(item) =>
              item?.status?.includes("Declined")
                ? null
                : history.push(
                  `/sales-purchases/${item.incomingTransactionCode}`
                )
            }
            clickableRows
            hover
            loading={isLoading}
            items={tableData}
            fields={[
              "customerName",
              "type",
              "orderNumber",
              "amount",
              "lastFour",
              "timestamp",
              "status",
              ...(merchantSettings?.showLabels ? ["label"] : []),
            ]}
            itemsPerPage={10}
            noItemsViewSlot={<div className="my-5" />}
            columnHeaderSlot={{
              timestamp: <div>Timestamp (Local Time)</div>,
            }}
            scopedSlots={{
              customerName: (item) => <td>{item?.customerName ?? "-"}</td>,
              orderNumber: (item) => <td>{item?.orderNumber ?? ""}</td>,
              amount: (item) => (
                <td>{`$ ${item?.amount?.toFixed(2) ?? "-"}`}</td>
              ),
              timestamp: (item) => (
                <td>
                  {item.timestamp && format(parseISO(item.timestamp), "Pp")}
                </td>
              ),
              status: (item) => { return <td>{renderBadge(item)}</td> },
              label: (item) => {
                const objLabel = userData?.labelHierarchy?.find(
                  (val) => val.id === item?.idLabel
                );
                return <td>{objLabel?.name ?? "-"}</td>;
              },
            }}
          />
          <CPagination
            pages={tableParams?.pageTotal}
            activePage={tableParams?.pageCurrent}
            onActivePageChange={(page) =>
              !isLoading ? handleChangePage(page) : null
            }
            className={tableParams?.pageTotal < 2 ? "d-none" : ""}
          />
        </CCardBody>
      </CCard>
      <FilterSalesPurchasesDataFormModal
        modalShow={showFilterFormModal}
        handleHideModal={handleHideModelUserForm}
        handleSubmitFilter={handleSubmitFilter}
      />
    </>
  );
};

export default SalesPurchases;
