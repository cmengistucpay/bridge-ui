import { CSwitch, CTooltip } from "@coreui/react";
import CIcon from "@coreui/icons-react";

export const Switch = (props) => {
  const { checked, onChange, label, tooltipInfo } = props;
  return (
    <div className="d-flex justify-content-between mb-2">
      <p className="mb-0">{label}</p>
      <div className="d-flex align-items-center">
        <CSwitch
          className="ml-2"
          color="secondary"
          checked={checked}
          onChange={onChange}
          shape="pill"
        />
        {tooltipInfo && (
          <CTooltip placement="bottom-end" content={tooltipInfo}>
            <CIcon
              className="mx-2"
              name="cil-info-circle"
              height={14}
              tabIndex="-1"
            />
          </CTooltip>
        )}
      </div>
    </div>
  );
};
