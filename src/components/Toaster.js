import { CToaster, CToast, CToastHeader } from "@coreui/react";
import { useSelector } from "react-redux";
import CIcon from "@coreui/icons-react";

export const Toaster = () => {
  const toaster = useSelector((state) => state.toaster);

  return (
    <CToaster position={"top-right"} className="toaster-wrapper">
      {toaster.map((val, i) => (
        <CToast
          key={i}
          show={val.show}
          autohide={2000}
          className={`_content ${val.type || ""}`}
        >
          <CToastHeader closeButton={true}>
            <div className="d-flex justify-center items-center">
              <div className="mr-2">
                {val.type === "success" && (
                  <CIcon size="md" name="cil-check-circle" />
                )}
                {val.type === "danger" && (
                  <CIcon size="md" name="cil-x-circle" />
                )}
              </div>
              <p className="_title">{val.title}</p>
            </div>
          </CToastHeader>
          <div className="_body">
            <div className="ml-3">
              <p>{val.body}</p>
            </div>
          </div>
        </CToast>
      ))}
    </CToaster>
  );
};
