import {
  CModal,
  CModalHeader,
  CModalTitle,
  CModalBody,
  CModalFooter,
  CButton,
} from "@coreui/react";

export const Prompt = (props) => {
  const { title, body, modalShow, handleHideModal, handleSubmit } = props;

  const handleSubmitPrompt = () => {
    handleSubmit();
    handleHideModal();
  };

  return (
    <CModal
      className="modal-sidebar"
      show={modalShow}
      onClose={handleHideModal}
    >
      <CModalHeader closeButton>
        <CModalTitle>{title}</CModalTitle>
      </CModalHeader>
      <CModalBody>
        <div className="d-flex justify-content-between align-items-center">
          <p className="mb-0">{body}</p>
        </div>
      </CModalBody>
      <CModalFooter>
        <CButton
          onClick={handleHideModal}
          color="light"
          className="mx-1 w-50 w-md-unset"
        >
          Cancel
        </CButton>
        <CButton
          onClick={handleSubmitPrompt}
          color="primary"
          className="mx-1 w-50 w-md-unset"
        >
          Submit
        </CButton>
      </CModalFooter>
    </CModal>
  );
};
