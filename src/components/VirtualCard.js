import CIcon from "@coreui/icons-react";
import { useWindowSize, insertString } from "./../helper";

// const achCardHelper = {
//   mediapay: {
//     logo: "/images/logo-mediapay.png",
//   },
// };

const bankHelper = {
  1: "Sutton",
  2: "Meta",
  3: "Peoples",
  4: "Central",
};

export const CardVCC = (props) => {
  const { data } = props;
  const screenSize = useWindowSize();

  const vccCardHelper = {
    4: {
      logo: "/images/logo-visa-white.png",
      height: !screenSize.screenMobile ? 24 : 18,
      bank: "MetaBank®, N.A., Member FDIC",
      licensed: "Pursuant To A License From Visa U.S.A. Inc.",
    },
    5: {
      logo: "/images/logo-mastercard.png",
      height: !screenSize.screenMobile ? 32 : 24,
      bank: "Sutton Bank, Member FDIC",
      licensed: "Pursuant To License By Mastercard International.",
    },
    Visa: {
      logo: "/images/logo-visa-white.png",
      height: !screenSize.screenMobile ? 24 : 18,
      bank: "MetaBank®, N.A., Member FDIC",
      licensed: "Pursuant To A License From Visa U.S.A. Inc.",
    },
    Mastercard: {
      logo: "/images/logo-mastercard.png",
      height: !screenSize.screenMobile ? 32 : 24,
      bank: "Sutton Bank, Member FDIC",
      licensed: "Pursuant To License By Mastercard International.",
    },
  };

  return (
    <>
      <div className="card-vcc-wrapper">
        <div className="card-details">
          <div className="card-info-header">
            <div>
              <CIcon name="logo-negative" height="20" alt="Logo" />
              <p className="cs-label">
                Customer Service:{" "}
                {data?.card?.customerServicePhoneNumber || "1-612-601-0914"}
              </p>
            </div>
            {data?.card?.isVirtualCard && (
              <div>
                <p className="vc-label">Virtual Card</p>
              </div>
            )}
          </div>
          <div className="card-holder-name">
            <p className="name-value">{data?.card?.nameLine1 + " " + data?.card?.nameLine2}</p>
          </div>
          <div className="account-number">
            <p className="label">ACCOUNT NUMBER</p>
            <p className="value">{data?.card?.accountNumber}</p>
          </div>

          <div className="sub-info">
            <div className="sec-code">
              <p className="label">
                SECURITY <br />
                CODE
              </p>
              <p className="value">{data?.card?.securityCode}</p>
            </div>
            <div className="exp-date">
              <p className="label">
                VALID <br />
                THRU
              </p>
              <p className="value">
                {data?.card?.expiration &&
                  insertString(data?.card?.expiration, 2, "/")}
              </p>
            </div>
          </div>

          <div className="card-info-footer">
            <div className="info-text">
              <p>
                <span>LIMITED USE</span> <br />
                This Card Is Issued By {bankHelper[data?.card?.bank] || ""}{" "}
                Bank, Member FDIC
                <br />
                {
                  vccCardHelper[
                    data?.card?.cardType !== "Other" ? data?.card?.cardType : 5
                  ]?.licensed
                }
              </p>
            </div>
            <img
              src={
                vccCardHelper[
                  data?.card?.cardType !== "Other" ? data?.card?.cardType : 5
                ]?.logo
              }
              height={
                vccCardHelper[
                  data?.card?.cardType !== "Other" ? data?.card?.cardType : 5
                ]?.height
              }
              alt="Virtual Card"
            />
          </div>
        </div>
      </div>
    </>
  );
};

export const CardACH = (props) => {
  const { data } = props;
  return (
    <>
      <div className="card-ach-wrapper">
        <div className="card-details">
          <div className="card-info-header">
            <div>
              <CIcon name="logo" height="20" alt="Logo" />
              {/* {data?.cardBank !== "connexpay" ? (
                <img
                  src={achCardHelper[data?.cardBank]?.logo}
                  height={20}
                  alt="Card ACH"
                />
              ) : (
                <CIcon name="logo" height="20" alt="Logo" />
              )} */}
            </div>
            <div>
              <img src={"/images/logo-ach.png"} height={30} alt="Card ACH" />
            </div>
          </div>

          <div className="account-number">
            <p className="label">PAYEE</p>
            <p className="value">{data?.customerName}</p>
          </div>

          <div className="sub-info">
            {/* <div className="sec-code">
              <p className="label">ROUTING</p>
              <p className="value">{data?.routing || "XXXXX2021"}</p>
            </div> */}
            <div className="exp-date">
              <p className="label">ACCOUNT</p>
              <p className="value">
                {data?.lastFour ? `XXX${data?.lastFour}` : "-"}
              </p>
            </div>
          </div>

          <img
            className="overlay-bank"
            src={"/images/overlay-bank-icon.png"}
            height={120}
            alt="Card ACH"
          />
        </div>
      </div>
    </>
  );
};
