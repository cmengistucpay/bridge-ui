import React, { Component } from "react";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import { getAuthTokenBridge } from "./services/serviceAuth";

import "./scss/style.scss";
import "react-datepicker/dist/react-datepicker.css";

const loading = (
  <div className="pt-3 text-center">
    <div className="sk-spinner sk-spinner-pulse"></div>
  </div>
);

// Containers
const Login = React.lazy(() => import("./views/Login/Login"));
const LoginCallback = React.lazy(() => import("./views/Login/LoginCallback"));
const ForgotUsername = React.lazy(() => import("./views/Login/ForgotUsername"));
const Register = React.lazy(() => import("./views/Login/Register"));
const TheLayout = React.lazy(() => import("./containers/TheLayout"));

class App extends Component {
  render() {
    const authToken = getAuthTokenBridge();
    return (
      <BrowserRouter>
        <React.Suspense fallback={loading}>
          <Switch>
            <Route
              exact
              path="/login"
              name="Login Page"
              render={(props) => <Login {...props} />}
            />
            <Route
              exact
              path="/login-callback"
              name="Login Page"
              render={(props) => <LoginCallback {...props} />}
            />
            <Route
              exact
              path="/forgot-username"
              name="Forgot Username Page"
              render={(props) => <ForgotUsername {...props} />}
            />
            <Route
              exact
              path="/register"
              name="Login Details Page"
              render={(props) => <Register {...props} />}
            />
            <Route
              path="/"
              name="Home"
              render={(props) =>
                authToken ? <TheLayout {...props} /> : <Redirect to="/login" />
              }
            />
          </Switch>
        </React.Suspense>
      </BrowserRouter>
    );
  }
}

export default App;
