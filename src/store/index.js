import { createStore } from "redux";

const initialState = {
  sidebarShow: "responsive",
  asideShow: false,
  darkMode: false,
  userData: {},
  toaster: [],
  globalLabel: [],
  accDevices: [],
  accMerchants: [],
  saleDescriptions: [],
  supplierIdList: [],
  userExpiration: {},
  shouldReloadSalesAndPurchasesTable: false,
};

const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case "set":
      return { ...state, ...rest };
    default:
      return state;
  }
};

const store = createStore(changeState);
export default store;

// userData: {
//     userId: 33,
//     guid: "f0f452e8-c1c3-4331-a39b-66f5208f2118",
//     userName: "pdonahue_cxpay",
//     firstName: "Patricia",
//     lastName: "Donahue",
//     phone: "2395551111",
//     email: "pdonahue@upwork.com",
//     canProcessPaymentInstructionFile: false,
//     canProcessSale: true,
//     canVoid: false,
//     canReturn: false,
//     canCreateVCC: false,
//     canIssueACH: false,
//     loadAmountLimitPerVCC: 20.0,
//     maxACHTransactionAmount: 110.0,
//     loadAmountLimitPerDay: 70.0,
//     maxDailyACHAmount: 0.0,
//     canCreateAPIKey: false,
//     canAccessCMS: false,
//     canAccessCashBalance: false,
//     canManageCustomFormPermissions: false,
//     canManageUsers: false,
//     canAccessAnalytics: false,
//     canAccessStatements: false,
//     canCreatePaymentLink: false,
//     canManageLabels: false,
//     status: 100,
//     labelHierarchy: [
//       {
//         id: 135,
//         name: "Auto Merchant GNQOGR",
//         isInUse: false,
//         level: 0,
//         children: [
//           {
//             id: 571,
//             name: "USA",
//             isInUse: false,
//             level: 1,
//             children: [
//               {
//                 id: 572,
//                 name: "FL",
//                 isInUse: false,
//                 level: 2,
//                 children: [
//                   {
//                     id: 573,
//                     name: "FMB",
//                     isInUse: false,
//                     level: 3,
//                     children: [],
//                   },
//                 ],
//               },
//             ],
//           },
//         ],
//       },
//       {
//         id: 299,
//         name: "Judson Central Eight",
//         isInUse: false,
//         level: 0,
//         children: [
//           {
//             id: 574,
//             name: "USA",
//             isInUse: false,
//             level: 1,
//             children: [
//               {
//                 id: 580,
//                 name: "MN",
//                 isInUse: false,
//                 level: 2,
//                 children: [],
//               },
//             ],
//           },
//           {
//             id: 575,
//             name: "CAN",
//             isInUse: false,
//             level: 1,
//             children: [
//               {
//                 id: 581,
//                 name: "ALBERTA",
//                 isInUse: false,
//                 level: 2,
//                 children: [],
//               },
//             ],
//           },
//           {
//             id: 576,
//             name: "INDIA",
//             isInUse: false,
//             level: 1,
//             children: [
//               {
//                 id: 582,
//                 name: "BANGALORE",
//                 isInUse: false,
//                 level: 2,
//                 children: [],
//               },
//             ],
//           },
//         ],
//       },
//       {
//         id: 417,
//         name: "Pacific LLC",
//         isInUse: false,
//         level: 0,
//         children: [],
//       },
//       {
//         id: 431,
//         name: "Peoples Seven",
//         isInUse: false,
//         level: 0,
//         children: [],
//       },
//       {
//         id: 508,
//         name: "Test New Prospect",
//         isInUse: false,
//         level: 0,
//         children: [],
//       },
//     ],
//     merchantSettings: {
//       clientLogoUrl: "http://www.facebook.com",
//       customerServicePhoneNbr: "2392229999",
//       showFiles: true,
//       showPaymentLinks: true,
//       showFlightFormElements: false,
//       showACHSales: true,
//       showACHPurchases: false,
//       showLabels: true,
//     },
//   },
