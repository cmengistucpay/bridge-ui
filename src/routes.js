import React from "react";

const Dashboard = React.lazy(() => import("./views/Dashboard/Dashboard"));
const SalesPurchases = React.lazy(() =>
  import("./views/SalesPurchases/SalesPurchases")
);
const SalesPurchasesDetail = React.lazy(() =>
  import("./views/SalesPurchases/SalesPurchasesDetail/SalesPurchasesDetail")
);
const Files = React.lazy(() => import("./views/Files/Files"));
const Admin = React.lazy(() => import("./views/Admin/Admin"));
const PaymentLink = React.lazy(() =>
  import("./views/PaymentLink/PaymentLink/PaymentLink")
);
const ReportsCashBalance = React.lazy(() =>
  import("./views/Reports/CashBalance/CashBalance")
);
const ReportAvailableToIssue = React.lazy(() =>
  import("./views/Reports/AvailableToIssue")
);
const ReportsStatements = React.lazy(() =>
  import("./views/Reports/Statements")
);
const Users = React.lazy(() => import("./views/Users/Users"));
const Labels = React.lazy(() => import("./views/Labels/Labels"));
const AccountSettings = React.lazy(() =>
  import("./views/Users/AccountSettings/AccountSettings")
);

// https://github.com/ReactTraining/react-router/tree/master/packages/react-router-config
const routes = [
  { path: "/", exact: true, name: "Home" },
  { path: "/dashboard", name: "Dashboard", component: Dashboard },
  {
    path: "/sales-purchases",
    name: "Sales & Purchases",
    component: SalesPurchases,
    exact: true,
  },
  {
    path: "/sales-purchases/:itcId",
    name: "Sales & Purchase Details",
    component: SalesPurchasesDetail,
  },
  { path: "/admin", name: "Admin", component: Admin },
  { path: "/files", name: "Files", component: Files },
  { path: "/payment-link", name: "Payment Link", component: PaymentLink },
  {
    path: "/reports",
    name: "Reports",
    component: ReportsCashBalance,
    exact: true,
  },
  {
    path: "/reports/cash-balance",
    name: "Cash Balance",
    component: ReportsCashBalance,
  },
  {
    path: "/reports/available-to-issue",
    name: "Available to Issue",
    component: ReportAvailableToIssue,
  },
  {
    path: "/reports/statements",
    name: "Statements",
    component: ReportsStatements,
  },
  { path: "/users", name: "Users", component: Users, exact: true },
  { path: "/labels", name: "Labels", component: Labels },
  {
    path: "/account-settings",
    name: "Account Settings",
    component: AccountSettings,
  },
];

export default routes;
